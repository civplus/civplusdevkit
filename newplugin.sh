#!/usr/bin/env bash

set -e

if [ $# -eq 0 ]; then
    echo "$0 [PLUGIN_PATH]"
    exit 1
fi

TEMPLATE_NAME_CAMEL="TemplatePlugin"
TEMPLATE_NAME_LOWER="templateplugin"
TEMPLATE_PATH="Plugins/$TEMPLATE_NAME_CAMEL"

PLUGIN_PATH=$1
PLUGIN_NAME_CAMEL=$(basename "$PLUGIN_PATH")
PLUGIN_NAME_LOWER=$(echo "$PLUGIN_NAME_CAMEL" | tr '[:upper:]' '[:lower:]')

echo "Main Class: plus.civ.$PLUGIN_NAME_LOWER.$PLUGIN_NAME_CAMEL"

if [ -e "$PLUGIN_PATH" ]; then
	echo "Plugin already exists"
	exit 1
fi

# move the plugin files to the right places
cp -r "$TEMPLATE_PATH" "$PLUGIN_PATH"
mv "$PLUGIN_PATH/src/main/kotlin/plus/civ/$TEMPLATE_NAME_LOWER" "$PLUGIN_PATH/src/main/kotlin/plus/civ/$PLUGIN_NAME_LOWER"
mv "$PLUGIN_PATH/src/main/kotlin/plus/civ/$PLUGIN_NAME_LOWER/$TEMPLATE_NAME_CAMEL.kt" "$PLUGIN_PATH/src/main/kotlin/plus/civ/$PLUGIN_NAME_LOWER/$PLUGIN_NAME_CAMEL.kt"

sed "s/$TEMPLATE_NAME_CAMEL/$PLUGIN_NAME_CAMEL/; s/$TEMPLATE_NAME_LOWER/$PLUGIN_NAME_LOWER/" "$PLUGIN_PATH/src/main/kotlin/plus/civ/$PLUGIN_NAME_LOWER/$PLUGIN_NAME_CAMEL.kt" > "$PLUGIN_PATH/src/main/kotlin/plus/civ/$PLUGIN_NAME_LOWER/$PLUGIN_NAME_CAMEL-out.kt"
sed "s/$TEMPLATE_NAME_CAMEL/$PLUGIN_NAME_CAMEL/; s/$TEMPLATE_NAME_LOWER/$PLUGIN_NAME_LOWER/" "$PLUGIN_PATH/BUILD" > "$PLUGIN_PATH/BUILD-out"
mv -f "$PLUGIN_PATH/src/main/kotlin/plus/civ/$PLUGIN_NAME_LOWER/$PLUGIN_NAME_CAMEL-out.kt" "$PLUGIN_PATH/src/main/kotlin/plus/civ/$PLUGIN_NAME_LOWER/$PLUGIN_NAME_CAMEL.kt"
mv -f "$PLUGIN_PATH/BUILD-out" "$PLUGIN_PATH/BUILD"
