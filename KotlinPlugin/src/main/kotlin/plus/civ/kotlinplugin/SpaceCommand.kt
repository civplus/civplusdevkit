package plus.civ.kotlinplugin

import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabExecutor

open class SpaceCommand(
	vararg subCommands: CommandInfo,
	/**
	 * If no arguments are passed to the space command to specify which executor in subCommands to run, this command is ran with no arguments.
	 *
	 * If this isn't specified, it defaults to the help command.
	 */
	baseCommand: TabExecutor? = null) : TabExecutor {
	data class CommandInfo(
		val name: String,
		val description: String? = null,
		val aliases: List<String> = listOf(),
		val permission: String? = null,
		val permissionMessage: String = "${ChatColor.RED}I'm sorry, but you do not have permission to perform this command." +
				" Please contact the server administrators if you believe this is in error.",
		val usage: String? = null,
		val executor: TabExecutor,
	)

	private val helpCommand = CommandInfo(
		name = "help",
		description = "Get help about a subcommand",
		aliases = listOf("?", "hlep"),
		executor = HelpCommand(subCommands.toSet().plus(
			CommandInfo(
				name = "help",
				description = "Get help about a subcommand",
				aliases = listOf("?", "hlep"),
				executor = object : TabExecutor {
					override fun onTabComplete(
						p0: CommandSender?, p1: Command?, p2: String?, p3: Array<out String>? ): MutableList<String> {
						error("Not implemented")
					}

					override fun onCommand(
						p0: CommandSender?, p1: Command?, p2: String?, p3: Array<out String>? ): Boolean {
						error("Not implemented")
					}
				}
			)
		)))

	val subCommands: Set<CommandInfo> = subCommands.toSet().plus(this.helpCommand)

	val baseCommand = baseCommand ?: helpCommand.executor

	override fun onCommand(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): Boolean {
		if (args.isEmpty()) {
			return baseCommand.onCommand(sender, command, commandName, args)
		} else {
			val subCommandName = args[0]
			val subCommandInfo =
				subCommands.firstOrNull { subCommandInfo ->
					subCommandInfo.name == subCommandName || subCommandInfo.aliases.contains(subCommandName)
				} ?: return false

			if (subCommandInfo.permission != null) {
				if (!sender.hasPermission(subCommandInfo.permission)) {
					sender.sendMessage(subCommandInfo.permissionMessage)
					return true
				}
			}
			val result = subCommandInfo.executor.onCommand(sender, command, "$commandName $subCommandName", args.slice(1..args.lastIndex).toTypedArray())
			if (!result) {
				sender.sendMessage(subCommandInfo.usage ?: "/$commandName help $subCommandName")
			}
			return true
		}
	}

	override fun onTabComplete(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): MutableList<String> {
		if (args.isEmpty() || args.size == 1) {
			val prefix = args.getOrNull(0) ?: ""
			return subCommands.asSequence()
				.map { it.aliases.plus(it.name) }
				.flatten()
				.filter { it.startsWith(prefix) }
				.sorted()
				.toMutableList()
		} else {
			val subCommandName = args[0]
			val subCommand =
				subCommands.firstOrNull { subCommandInfo ->
					(subCommandInfo.name == subCommandName || subCommandInfo.aliases.contains(subCommandName))
							&& (subCommandInfo.permission == null || sender.hasPermission(subCommandInfo.permission))
				} ?: return mutableListOf()

			return subCommand.executor.onTabComplete(sender,  command, "$commandName $subCommandName", args.slice(1..args.lastIndex).toTypedArray())
		}
	}

	internal class HelpCommand(val subCommands: Set<CommandInfo>): TabExecutor {
		override fun onCommand(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): Boolean {
			val baseCommandName = if (commandName.contains(' ')) commandName.subSequence(0, commandName.lastIndexOf(' ')) else commandName
			val topics = subCommands.filter { it.permission == null || sender.hasPermission(it.permission) }

			if (args.isEmpty()) {
				if (topics.isEmpty()) {
					sender.sendMessage("${ChatColor.RED}No topics available for /$commandName")
					return true
				}

				val header = StringBuilder()
				header.append(ChatColor.YELLOW)
				header.append("--------- ")
				header.append(ChatColor.WHITE)
				header.append("Help: /")
				header.append(baseCommandName)
				header.append(" ")
				header.append(ChatColor.YELLOW)

				for (i in header.length..54) {
					header.append("-")
				}

				sender.sendMessage(header.toString())

				for (topic in topics) {
					sender.sendMessage("${ChatColor.GOLD}/$baseCommandName ${topic.name}:${ChatColor.RESET} ${topic.description}")
				}

				return true
			} else if (args.size == 1) {
				val topicName = args[0]
				val topic = topics.firstOrNull { (it.permission == null || sender.hasPermission(it.permission)) && (it.name == topicName || topicName in it.aliases) }
				if (topic == null) {
					sender.sendMessage("${ChatColor.RED}No help for /$baseCommandName $topicName")
					return true
				}

				val header = StringBuilder()
				header.append(ChatColor.YELLOW)
				header.append("--------- ")
				header.append(ChatColor.WHITE)
				header.append("Help: ")
				header.append("/$baseCommandName $topicName")
				header.append(" ")
				header.append(ChatColor.YELLOW)

				for (i in header.length..54) {
					header.append("-")
				}

				sender.sendMessage(header.toString())

				if (topic.description != null) {
					sender.sendMessage("${ChatColor.GOLD}Description:${ChatColor.RESET} ${topic.description}")
				}
				val usage = topic.usage ?: if (topic.name == "help") "/$baseCommandName help [topic]" else "/$baseCommandName ${topic.name}"
				sender.sendMessage("${ChatColor.GOLD}Usage:${ChatColor.RESET} $usage")
				if (topic.aliases.isNotEmpty()) {
					sender.sendMessage("${ChatColor.GOLD}Aliases:${ChatColor.RESET} ${topic.aliases.joinToString()}")
				}
				return true
			} else {
				return false
			}
		}

		override fun onTabComplete(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): MutableList<String> {
			if (args.isEmpty() || args.size == 1) {
				val prefix = args.getOrNull(0) ?: ""
				return subCommands.map { it.name }.filter { it.startsWith(prefix) }.sorted().toMutableList()
			}
			return mutableListOf()
		}
	}
}
