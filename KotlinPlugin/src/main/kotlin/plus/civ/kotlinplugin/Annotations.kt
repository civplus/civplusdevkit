package plus.civ.kotlinplugin

enum class PermissionDefault(val yamlName: Any) {
	TRUE(true),
	FALSE(false),
	OP("op"),
	NOT_OP("not op"),
}

enum class APIVersion(val yamlName: String) {
	V1_13("1.13"),
	V1_14("1.14"),
	V1_15("1.15"),
	V1_16("1.16"),
	V1_17("1.17"),
	V1_18("1.18"),
}

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class CommandInfo(
	val name: String,
	val description: String = "",
	vararg val aliases: String = [],
	val pluginPermission: PluginPermission = PluginPermission(""),
	val permission: String = "",
	val permissionMessage: String = "",
	val usage: String = "",
)

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class PluginInfo(
	val name: String,
	val description: String = "",
	val author: String = "Civ+",
	val authors: Array<String> = [],
	val depends: Array<String> = [],
	val softDepends: Array<String> = [],
	val permissions: Array<PluginPermission> = [],
	val apiVersion: APIVersion = APIVersion.V1_15,
	val commandsPackage: String = "commands",
	val listenersPackage: String = "listeners",
	val loadEarly: Boolean = false,
)

annotation class PluginPermission(
	val name: String,
	val description: String = "",
	val default: PermissionDefault = PermissionDefault.OP,
	val children: Array<PluginPermission> = []
)
