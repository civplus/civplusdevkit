@file:Suppress("UnstableApiUsage")

package plus.civ.kotlinplugin

import com.google.common.collect.ImmutableSet
import com.google.common.reflect.ClassPath
import org.bukkit.command.CommandExecutor
import org.bukkit.event.Listener
import org.bukkit.plugin.java.JavaPlugin
import java.util.logging.Level
import kotlin.reflect.full.findAnnotation

abstract class KotlinPlugin: JavaPlugin() {
	override fun onEnable() {
		val pluginInfo: PluginInfo = try {
			this::class.findAnnotation()!!
		} catch (e: NullPointerException) {
			this.logger.log(Level.SEVERE, "A KotlinCommand must have a PluginInfo annotation! Plugin ${this::class.simpleName} does not.")
			return
		}
		loadAllCommands(pluginInfo.commandsPackage)
		loadAllListeners(pluginInfo.listenersPackage)
		loadSelfAsCommandOrListener()
	}

	private fun loadAllCommands(commandsPackage: String) {
		this.logger.log(Level.INFO, "Automatically loading commands...")
		val samplersPath = ClassPath.from(this.classLoader)
		val currPackage = this.javaClass.`package`.name
		val potentialCommandClassesMaybe: ImmutableSet<ClassPath.ClassInfo>
		try {
			potentialCommandClassesMaybe = samplersPath.getTopLevelClasses("$currPackage.$commandsPackage")
		} catch (e: Exception) {
			this.logger.log(Level.WARNING, "No commands package found. Skipping automated command registration.")
			return
		}
		val potentialCommandClasses: ImmutableSet<ClassPath.ClassInfo> = potentialCommandClassesMaybe
		for (clsInfo in potentialCommandClasses) {
			val clazz = clsInfo.load()
			val commandInfo = clazz?.kotlin?.findAnnotation<CommandInfo>()
			if (commandInfo != null) {
				logger.log(Level.INFO, "Found annotated command ${clazz.typeName}.")
				if (!CommandExecutor::class.java.isAssignableFrom(clazz)) {
					logger.log(Level.WARNING, "Command ${commandInfo.name} has a command annotation but doesn't extend CommandExecutor. Skipping it.")
					continue
				}
				val command = (clazz.kotlin.objectInstance ?: clazz.getDeclaredConstructor().newInstance()) as CommandExecutor
				val bukkitCommand = this.getCommand(commandInfo.name)
				if (bukkitCommand == null) {
					this.logger.log(
						Level.SEVERE,
						"Failed to load command ${commandInfo.name}. This may mean that the command is not properly entered in the plugin.yml."
					)
					continue
				}
				bukkitCommand.executor = command
			}
		}
	}

	private fun loadAllListeners(listenersPackage: String) {
		this.logger.log(Level.INFO, "Automatically loading listeners...")
		val samplersPath = ClassPath.from(this.classLoader)
		val currPackage = this.javaClass.`package`.name
		val potentialListenerClassesMaybe: ImmutableSet<ClassPath.ClassInfo>
		try {
			potentialListenerClassesMaybe = samplersPath.getTopLevelClasses("$currPackage.$listenersPackage")
		} catch (e: Exception) {
			this.logger.log(Level.WARNING, "No listeners package found. Skipping automated listener registration.")
			return
		}
		val potentialListenerClasses: ImmutableSet<ClassPath.ClassInfo> = potentialListenerClassesMaybe
		for (clsInfo in potentialListenerClasses) {
			val clazz = clsInfo.load()
			if (clazz != null && Listener::class.java.isAssignableFrom(clazz)) {
				logger.log(Level.INFO, "Found listener " + clazz.typeName)
				val listener = (clazz.kotlin.objectInstance ?: clazz.getDeclaredConstructor().newInstance()) as Listener
				this.server.pluginManager.registerEvents(listener, this)
			}
		}
	}

	private fun loadSelfAsCommandOrListener() {
		if (this is Listener) {
			this.logger.log(Level.INFO, "Automatically registering self as listener.")
			this.server.pluginManager.registerEvents(this, this)
		}

		val commandInfo = this::class.findAnnotation<CommandInfo>()
		if (commandInfo != null) {
			this.logger.log(Level.INFO, "Automatically loading self as command ${commandInfo.name}.")

			val bukkitCommand = this.getCommand(commandInfo.name)
			if (bukkitCommand == null) {
				this.logger.log(
						Level.SEVERE,
						"Failed to load command ${commandInfo.name}. This may mean that the command is not properly entered in the plugin.yml."
				)
			} else {
				bukkitCommand.executor = this
			}
		}
	}
}
