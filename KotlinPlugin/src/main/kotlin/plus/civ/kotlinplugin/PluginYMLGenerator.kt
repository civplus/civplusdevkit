package plus.civ.kotlinplugin

import org.bukkit.configuration.file.YamlConfiguration
import java.io.File
import java.io.FileNotFoundException
import java.net.MalformedURLException
import java.net.URL
import java.net.URLClassLoader
import java.util.jar.JarFile
import java.util.logging.Level
import kotlin.reflect.KAnnotatedElement
import kotlin.reflect.full.findAnnotation
import kotlin.system.exitProcess

fun main(args: Array<String>) {
	val pluginJarPath = args[0]
	val qualifiedPluginMainClassName = args[1]
	val pluginYMLOutputPath = args[2]
	val deps = args.clone().drop(3).toMutableList()
	deps.add(0, pluginJarPath)
	val jarFiles = deps.map {
		try {
			File(it)
		} catch(e: FileNotFoundException) {
			println("File $it was not found.")
			exitProcess(1)
		}
	}
	val urls = jarFiles.map{ URL("jar:${it.toURI().toURL()}!/") }

	val pluginClassLoader = try {
		URLClassLoader(urls.toTypedArray())
	} catch(e: MalformedURLException) {
		println("A bad filepath was supplied.")
		exitProcess(2)
	} catch(e: Exception) {
		e.printStackTrace()
		exitProcess(3)
	}
	val pluginMainClass = try {
		Class.forName(qualifiedPluginMainClassName, false, pluginClassLoader).kotlin
	} catch(e: ClassNotFoundException) {
		println("The plugin doesn't have a class called $qualifiedPluginMainClassName (which was passed as the main class name).")
		exitProcess(4)
	}

	val pluginInfo = pluginMainClass.findAnnotation<PluginInfo>()
	if (pluginInfo == null) {
		println("The plugin main class must have the PluginInfo annotation.")
		exitProcess(5)
	}

	val commandsPackagePath: String = qualifiedPluginMainClassName.substring(0, qualifiedPluginMainClassName.lastIndexOf('.')) + ".${pluginInfo.commandsPackage}"
	val commands = JarFile(jarFiles[0]).entries().asSequence()
		.filter{ it.name.contains(".class") } // only class files
		.map{ it.name.replace("/", ".").replace(".class", "") } // convert to qualified name
		.filter{ it.startsWith(commandsPackagePath) } // throw out classes outside the commands package
		.map{ pluginClassLoader.loadClass(it) } // load class instances
		.map { it.getAnnotation(CommandInfo::class.java) } // get the CommandInfo annotation
		.filterNotNull() // throw out any classes where there was no annotation
		.toList() // finally, collect into a list

	val pluginData = PluginData(pluginInfo = pluginInfo, mainClassPath = qualifiedPluginMainClassName, commands = commands)
	writeToYMLFile(pluginYMLOutputPath, pluginData)
}

private data class PluginData(val pluginInfo: PluginInfo, val mainClassPath: String, val commands: List<CommandInfo>)

private fun writeToYMLFile(path: String, pluginData: PluginData) {
	val yml = YamlConfiguration()
	// change separator character so we can use dots in permission names
	yml.options().pathSeparator('&')

	yml.set("name", pluginData.pluginInfo.name)
	yml.set("version", "1.0.0")
	yml.set("api-version", pluginData.pluginInfo.apiVersion.yamlName)
	yml.set("main", pluginData.mainClassPath)
	if (pluginData.pluginInfo.description.isNotBlank()) {
		yml.set("description", pluginData.pluginInfo.description)
	}
	if (pluginData.pluginInfo.author.isNotBlank()) {
		yml.set("author", pluginData.pluginInfo.author)
	}
	if (pluginData.pluginInfo.authors.isNotEmpty()) {
		yml.set("authors", pluginData.pluginInfo.authors)
	}
	if (pluginData.pluginInfo.depends.isNotEmpty()) {
		yml.set("depend", pluginData.pluginInfo.depends)
	}
	if (pluginData.pluginInfo.softDepends.isNotEmpty()) {
		yml.set("softdepend", pluginData.pluginInfo.softDepends)
	}
	if (pluginData.pluginInfo.loadEarly) {
		yml.set("load", "STARTUP")
	}

	for (command in pluginData.commands) {
		yml.set("commands&${command.name}", command.toYamlSection())
		if (command.pluginPermission.name.isNotBlank()) {
			yml.set("permissions&${command.pluginPermission.name}", command.pluginPermission.toYamlSection())
		}
	}
	for (permission in pluginData.pluginInfo.permissions) {
		yml.set("permissions&${permission.name}", permission.toYamlSection())
	}

	yml.save(path)
}

private fun CommandInfo.toYamlSection(): YamlConfiguration {
	val conf = YamlConfiguration()
	if (description.isNotBlank()) {
		conf.set("description", description)
	}
	if (aliases.isNotEmpty()) {
		conf.set("aliases", aliases)
	}
	if (permission.isNotBlank()) {
		conf.set("permission", permission)
	} else if (pluginPermission.name.isNotBlank()) {
		conf.set("permission", pluginPermission.name)
	}
	if (permissionMessage.isNotBlank()) {
		conf.set("permissionMessage", permissionMessage)
	}
	if (usage.isNotBlank()) {
		conf.set("usage", usage)
	}
	return conf
}

private fun PluginPermission.toYamlSection(): YamlConfiguration {
	val conf = YamlConfiguration()
	// change separator character so we can use dots in permission names
	conf.options().pathSeparator('&')

	if (description.isNotBlank()) {
		conf.set("description", description)
	}
	conf.set("default", default.yamlName)
	if (children.isNotEmpty()) {
		val childrenConf = YamlConfiguration()
		childrenConf.options().pathSeparator('&')
		for (child in children) {
			childrenConf.set(child.name, child.toYamlSection())
		}
		conf.set("children", childrenConf)
	}
	return conf
}

private val logFunction: (Level, String) -> Unit = {level: Level, string: String ->
	println("${level.localizedName}: $string")
}
