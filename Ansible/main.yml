---
- hosts: all
  become: yes

  handlers:
    - name: restart mongodb
      service:
        name: mongod
        state: restarted
        enabled: yes

    - name: restart mariadb
      service:
        name: mariadb
        state: restarted
        enabled: yes

    - name: restart mcmain
      service:
        name: mcmain
        state: restarted
        enabled: yes

  tasks:
    - name: Update installed packages
      apt:
        upgrade: safe
        autoclean: true

    - name: Import the public key used by mongodb
      apt_key:
        url: "https://www.mongodb.org/static/pgp/server-4.4.asc"
        state: present

    - name: Add MongoDB repository
      apt_repository:
        repo: 'deb http://repo.mongodb.org/apt/debian buster/mongodb-org/4.4 main'
        state: present

    - name: Add adoptopenjdk gpg key
      apt_key:
        url: "https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public"
        state: present

    - name: Add adoptopenjdk apt repo (required for java 8 on debian buster)
      apt_repository:
        repo: "deb https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/ buster main"
        state: present

    - name: Ensure MongoDB is installed
      apt:
        name: mongodb-org
        state: latest
        update_cache: yes
      notify: restart mongodb

    - name: Ensure mariadb is installed
      apt:
        name:
        - mariadb-server
        - mariadb-client
        - python3-mysqldb
        state: latest
        update_cache: yes
      notify: restart mariadb

    - name: Ensure java 8 is installed
      apt:
        name: adoptopenjdk-8-hotspot
        state: latest

    - name: Ensure random dependencies are installed
      apt:
        name:
          - unzip
          - screen
        state: latest

    - name: Ensure mysql user exists
      mysql_user:
        name: minecraft_user
        password: "*83BBAEFE2966AA3A5A6B1802DA29B43366DF3CEA" # tyGHbn6
        encrypted: true
        priv: "*.*:ALL PRIVILEGES"

    - name: Create mysql databases for plugins
      mysql_db:
        state: present
        encoding: latin1
        name:
          - citadel
          - civchat2
          - leer
          - namelayer
          - wordbank

    - name: Ensure minecraft_user user exists
      user:
        name: minecraft_user
        create_home: true
        home: /home/mc
        password: tyGHbn6
        shell: /usr/bin/bash

    - name: Ensure ~/CivPlus simulated git repo exists for build artifacts
      file:
        path: /home/mc/CivPlus
        state: directory
        owner: minecraft_user
        group: minecraft_user
        mode: "u=rwx,g=rwx,o=rx"

    - name: Copy only .tar plugin blobs over
      become_user: minecraft_user
      synchronize:
        src: ../bazel-bin
        dest: /home/mc/CivPlus
        owner: false
        copy_links: true
        recursive: true
        delete: true
        rsync_opts:
          - --prune-empty-dirs
          - --include=*/
          - --include=*.tar
          - --exclude=*
      notify:
        - restart mcmain

    - name: Download paper
      get_url:
        url: "https://api.papermc.io/v2/projects/paper/versions/1.8.8/builds/445/downloads/paper-1.8.8-445.jar"
        dest: /home/mc/paper18.jar
        owner: minecraft_user
        mode: "u=rx,g=rx,o=rx"
      notify:
        - restart mcmain

    - name: Copy over the scripts and configs in templates/ # todo: this always reports changed for some reason
      become_user: minecraft_user
      synchronize:
        src: templates/
        dest: /home/mc
        owner: false
        copy_links: true
        recursive: true
        times: false
        checksum: true
      notify:
        - restart mcmain

    - name: Ensure main server service exists
      copy:
        src: mcmain.service
        dest: /etc/systemd/system
        owner: root
        group: root
      notify: restart mcmain
