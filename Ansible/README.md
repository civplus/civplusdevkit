# Civ+ Development Setup in a Box

Using Ansible and Vagrant, a Civ+ server can be setup in a matter of minutes.

## Dependencies

To run Civ+ in a local virtual machine, you will need to install [Vagrant](https://wiki.archlinux.org/title/Vagrant), [VirtualBox](https://wiki.archlinux.org/title/VirtualBox), and [Ansible](https://wiki.archlinux.org/title/Ansible) (both ansible and ansible-core are required)

## Deploying the VM

To compile everything and prepare the code to be deployed to the VM, run `bazel build ...`

To spin up a new virtual machine, or start it up after rebooting/shutdown, run `vagrant up`

To put newly compiled code onto the VM, run `vagrant provision`. 
You do not have to run this after `vagrant up` unless you've changed something since running the command.

To shut down the VM, run `vagrant halt`

To delete the VM,  minecraft server, and all associated state, run `vagrant destroy`

To ssh into the VM and access the minecraft console, you can follow the following steps:

```shell
$ vagrant ssh
$ sudo su minecraft_user
$ screen -r mc
```
