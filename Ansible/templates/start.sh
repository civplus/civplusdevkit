#!/usr/bin/env bash

set -e

SCREEN_NAME="mc"
PACKAGE_NAME="Plugins"

# Calculate how much ram to use
TOTAL_RAM=$(free -b | grep Mem | awk '{print $2}')
CALCULATED_RAM=$((TOTAL_RAM * 60 / 100)) # currently 60% of available system memory
EIGHT_GB_IN_BYTES=$((8 * 1024 * 1024 * 1024))

# set ram allocation to the lesser of 8G or the calculated ram value
if [ "$CALCULATED_RAM" -lt "$EIGHT_GB_IN_BYTES" ]; then
    RAM_ALLOCATION=$(($CALCULATED_RAM / 1024 / 1024))M
else
    RAM_ALLOCATION=8G
fi

if [ ! -e world ]; then
	unzip "Mc_Construct.zip"
fi

mkdir -p plugins
rm -f plugins/*.jar
tar -xf "/home/mc/CivPlus/bazel-bin/$PACKAGE_NAME/Plugins.tar" --strip-components=2 -C plugins
screen -mDS $SCREEN_NAME java -Xms$RAM_ALLOCATION -Xmx$RAM_ALLOCATION -jar /home/mc/paper18.jar nogui # TODO: Use recommended jvm flags
