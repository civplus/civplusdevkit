package plus.civ.layerafterlayer.model

import org.bukkit.Bukkit
import org.bukkit.entity.Player
import vg.civcraft.mc.namelayer.GroupManager
import vg.civcraft.mc.namelayer.NameAPI
import java.util.*

data class Group private constructor(val name: String, val id: Int) {
	 internal constructor(from: vg.civcraft.mc.namelayer.group.Group) : this(from.name, from.groupId)

	internal constructor(id: Int) : this(GroupManager.getGroup(id).name, id)

	companion object {
		fun get(name: String): Group? {
			if (!GroupManager.hasGroup(name)) {
				return null
			} else {
				val group = GroupManager.getGroup(name)
				return Group(group.name, group.groupId)
			}
		}

		fun get(id: Int): Group? {
			if (!GroupManager.hasGroup(id)) {
				return null
			} else {
				val group = GroupManager.getGroup(id)
				return Group(group.name, group.groupId)
			}
		}
	}

	val allOnlinePlayers: List<Player>
		get() {
			return GroupManager.getGroup(id).allMembers.mapNotNull { uuid ->
				Bukkit.getServer().getPlayer(uuid)?.takeIf { player ->
					player.isOnline
				}
			}
		}

	fun includesMember(player: Player): Boolean = includesMember(player.uniqueId)

	fun includesMember(playerId: UUID): Boolean {
		return GroupManager.getGroup(this.id).isCurrentMember(playerId)
	}

	fun addPlayer(player: Player) {
		GroupManager.getGroup(this.id).addMember(player.uniqueId, GroupManager.PlayerType.MEMBERS)
	}

	// TODO need to make a CMC NamespacedKey
	/*fun playerHasLayerPermission(player: UUID, permission: PermissionType): Boolean {
		return NameAPI.getGroupManager().hasAccess(this.name, player, vg.civcraft.mc.namelayer.permission.PermissionType.getPermission(permission.key))
	}*/
}

fun Player.isOnGroup(group: Group): Boolean {
	return group.includesMember(this)
}

val Player.defaultGroup: Group
	get() = Group(GroupManager.getGroup(NameAPI.getGroupManager().getDefaultGroup(this.uniqueId)))
