package plus.civ.layerafterlayer.listeners

import org.bukkit.ChatColor
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent
import plus.civ.layerafterlayer.Config
import plus.civ.layerafterlayer.LayerAfterLayer
import plus.civ.layerafterlayer.model.Group
import java.util.logging.Level

class PlayerJoinListener: Listener {
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	fun onNewPlayerJoin(event: PlayerJoinEvent) {
		if (event.player.hasPlayedBefore()) return

		val group = Group.get(Config.OFFICIAL_PUBLIC_GROUP)
		if (group == null) {
			LayerAfterLayer.instance.logger.log(Level.WARNING, "Group ${Config.OFFICIAL_PUBLIC_GROUP} was put in the config as the public group, but was not found.")
			return
		}
		group.addPlayer(event.player)
		event.player.sendMessage("""
			${ChatColor.YELLOW}You've been automatically added to the group ${ChatColor.BLUE}!${ChatColor.YELLOW}. 
			You can use the command ${ChatColor.GRAY}/g ! ${ChatColor.YELLOW} to talk in that group's chat.
		""".trimIndent())
	}
}
