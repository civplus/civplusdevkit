package plus.civ.layerafterlayer

import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PluginInfo

@PluginInfo(
	name = "LayerAfterLayer",
	author = "sepia",
	description = "NameLayer strangler",
	depends = ["CivModCore", "NameLayer"],
)
class LayerAfterLayer: KotlinPlugin() {
	companion object {
		var _instance: LayerAfterLayer? = null
		val instance: LayerAfterLayer
			get() = _instance!!
	}

	override fun onEnable() {
		super.onEnable()
		_instance = this
	}
}
