package plus.civ.whatisanocttree.event

import org.bukkit.Chunk
import org.bukkit.event.Event
import org.bukkit.event.HandlerList
import org.bukkit.event.world.ChunkEvent

/**
 * Called when the chunk's data is fully loaded and is ready to be accessed.
 *
 * This exists because if you access a chunk's data in ChunkLoadEvent
 * it will force that chunk's data to be loaded on the main thread
 *
 * This event is called even when a chunk with no data has been loaded.
 */
class ChunkDataLoadEvent(chunk: Chunk) : ChunkEvent(chunk) {
	companion object {
		private val _handlers: HandlerList = HandlerList()
		@JvmStatic
		fun getHandlerList(): HandlerList {
			return _handlers
		}
	}
	override fun getHandlers(): HandlerList {
		return _handlers
	}
}
