package plus.civ.whatisanocttree.listeners

import org.bukkit.Bukkit
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerPreLoginEvent
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.scheduler.BukkitTask
import org.litote.kmongo.*
import plus.civ.whatisanocttree.WhatIsAnOcttree
import plus.civ.whatisanocttree.player.LoadingPlayer
import plus.civ.whatisanocttree.player.PlayerData
import java.util.*

object PlayerLoadUnload: Listener {
	val loadingPlayers = mutableMapOf<UUID, BukkitTask>()

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun join(event: AsyncPlayerPreLoginEvent) {
		if (event.loginResult != AsyncPlayerPreLoginEvent.Result.ALLOWED) return

		val uuid = event.uniqueId

		LoadingPlayer(uuid).runTaskAsynchronously(WhatIsAnOcttree.instance)

		// unload the player if they don't actually connect within 1 hour (very long to be extra safe)
		loadingPlayers[uuid] = Bukkit.getScheduler().runTaskLater(WhatIsAnOcttree.instance, {
			if (Bukkit.getServer().getPlayer(uuid) == null) {
				unloadPlayerData(uuid)
			}
		}, 60 * 60 * 20)
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun joinForReal(event: PlayerJoinEvent) {
		loadingPlayers.remove(event.player.uniqueId)?.cancel() // make things a little more deterministic
	}

	fun unloadPlayerData(uuid: UUID) {
		PlayerData.playerLock.lock()
		try {
			for (loadingTask in PlayerData.loadingTasks) {
				if (loadingTask.uuid == uuid) {
					loadingTask.task.getAndSet(null)?.cancel()
				}
			}

			for (playerData in PlayerData.loadedPlayers.values) {
				if (playerData.playerUUID == uuid) {
					if (playerData.isNotEmpty) {
						playerData.saveLater()
					} else {
						playerData.deleteLater()
					}
					PlayerData.loadedPlayers.remove(uuid)
				}
			}
		} finally {
			PlayerData.playerLock.unlock()
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun leave(event: PlayerQuitEvent) {
		unloadPlayerData(event.player.uniqueId)
	}

	fun enable() {
		// probably will never actually load any players, but you never know
		for (player in Bukkit.getServer().onlinePlayers) {
			val data = PlayerData.collection.findOne(PlayerData::playerUUID eq player.uniqueId)
			if (data != null) {
				LoadingPlayer(player.uniqueId).runNow()
			}
		}
	}

	fun disable() {
		// the unlocked reads and writes below are okay, because we first make sure nobody else is reading and writing
		// and we're the only people who can spawn more tasks that can read and write
		PlayerData.loadingTasks.forEach(LoadingPlayer::runNow)
		PlayerData.loadedPlayers.forEach { (_, data) -> data.saveBlocking() }
		PlayerData.loadedPlayers.clear()
	}
}
