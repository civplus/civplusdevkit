package plus.civ.whatisanocttree.listeners

import org.bukkit.Material.*
import org.bukkit.block.Block
import org.bukkit.entity.FallingBlock
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.*
import org.bukkit.event.entity.EntityChangeBlockEvent
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.getBlockData
import plus.civ.whatisanocttree.chunk.tags.ImmovableBlock

object ImmovableBlockListener: Listener {
	@EventHandler(ignoreCancelled = true)
	fun fromTo(event: BlockFromToEvent) {
		// dragon eggs, water/lava

		val immovableBlocks = event.block.location.getBlockData<ImmovableBlock>()
		if (immovableBlocks.isEmpty()) return

		if (immovableBlocks.all(ImmovableBlock::isImmovable))
			event.isCancelled = true
	}

	fun pistonEvent(event: BlockPistonEvent, blocks: List<Block>) {
		for (block in blocks) {
			val immovableBlocks = event.block.location.getBlockData<ImmovableBlock>()
			if (immovableBlocks.isEmpty()) continue

			if (immovableBlocks.all(ImmovableBlock::isImmovable))
				event.isCancelled = true
			return
		}
	}

	@EventHandler(ignoreCancelled = true)
	fun pistonExtend(event: BlockPistonExtendEvent) {
		pistonEvent(event, event.blocks)
	}

	@EventHandler(ignoreCancelled = true)
	fun pistonRetract(event: BlockPistonRetractEvent) {
		pistonEvent(event, event.blocks)
	}

	val fallingBlocks = setOf(SAND, GRAVEL, ANVIL, DRAGON_EGG)

	@EventHandler(ignoreCancelled = true)
	fun physicsEvent(event: BlockPhysicsEvent) {
		if (event.block.type !in fallingBlocks) return

		val immovableBlocks = event.block.location.getBlockData<ImmovableBlock>()
		if (immovableBlocks.isEmpty()) return

		if (immovableBlocks.all(ImmovableBlock::isImmovable))
			event.isCancelled = true

		event.isCancelled = true
	}

	@EventHandler(ignoreCancelled = true)
	fun blockChangeEntity(event: EntityChangeBlockEvent) {
		if (event.entity !is FallingBlock) return
		if (event.to != AIR) return

		val immovableBlocks = event.block.location.getBlockData<ImmovableBlock>()
		if (immovableBlocks.isEmpty()) return

		if (immovableBlocks.all(ImmovableBlock::isImmovable)) {
			event.isCancelled = true
			event.block.state.update(false, false) // fix ghost blocks probably
		}
	}
}
