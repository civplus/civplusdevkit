package plus.civ.whatisanocttree.chunk.tags

import com.fasterxml.jackson.annotation.JsonTypeInfo
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.inventory.ItemStack
import vg.civcraft.mc.civmodcore.event.BlockDropItemEvent
import vg.civcraft.mc.civmodcore.extensions.nbt
import vg.civcraft.mc.civmodcore.extensions.readObject
import vg.civcraft.mc.civmodcore.extensions.writeNBTTag
import java.util.concurrent.ConcurrentLinkedDeque

/**
 * Marks block-level data that should be persisted onto a block's drop(s) when broken
 *
 * If the resulting drop is then placed, the data will be persisted back onto the world
 */
interface ItemSerializable {
	/**
	 * Called when the block is placed in the world.
	 */
	fun onBecomeBlock(event: BlockPlaceEvent) {}

	/**
	 * Called when the block is broken
	 *
	 * @return If WhatIsAnOcttree should add its own data to the item that just dropped.
	 * If you added data to the item manually, you should return false.
	 */
	fun onBecomeItem(event: BlockDropItemEvent): Boolean {
		return true
	}

	data class ItemDataContainer(
		@JsonTypeInfo(
			use = JsonTypeInfo.Id.CLASS,
			include = JsonTypeInfo.As.PROPERTY,
			property = "type")
		val data: ConcurrentLinkedDeque<Any> = ConcurrentLinkedDeque()
	)

	companion object {
		const val ITEM_DATA_TAG = "WAOTPlacableItemData"

		fun ItemStack.addData(data: ItemSerializable) {
			val currentData = nbt?.readObject(ITEM_DATA_TAG, ItemDataContainer::class.java) ?: ItemDataContainer()
			currentData.data.add(data)
			writeNBTTag(ITEM_DATA_TAG, currentData)
		}

		fun ItemStack.addAllData(data: Iterable<ItemSerializable>) {
			val currentData = nbt?.readObject(ITEM_DATA_TAG, ItemDataContainer::class.java) ?: ItemDataContainer()
			currentData.data.addAll(data)
			writeNBTTag(ITEM_DATA_TAG, currentData)
		}

		fun ItemStack.removeData(data: ItemSerializable): Boolean {
			val currentData = nbt?.readObject(ITEM_DATA_TAG, ItemDataContainer::class.java) ?: ItemDataContainer()
			val result = currentData.data.remove(data)
			if (currentData.data.isEmpty()) {
				writeNBTTag(ITEM_DATA_TAG, null)
			} else {
				writeNBTTag(ITEM_DATA_TAG, currentData)
			}
			return result
		}

		inline fun <reified T: Any> ItemStack.getItemData(): List<T> {
			val currentData = nbt?.readObject(ITEM_DATA_TAG, ItemDataContainer::class.java) ?: ItemDataContainer()
			return currentData.data.filterIsInstance<T>()
		}
	}
}
