package plus.civ.whatisanocttree.chunk.tags

import com.fasterxml.jackson.annotation.JsonIgnore

/**
 * If a block has a data that implements this interface, the block will not be affected by physics, and can not be pushed with a piston
 */
interface ImmovableBlock {
	/**
	 * If the block is actually immovable.
	 *
	 * This is intended to be used for blocks that are sometimes immovable and sometimes are movable.
	 */
	@get:JsonIgnore
	val isImmovable: Boolean
		get() = true
}
