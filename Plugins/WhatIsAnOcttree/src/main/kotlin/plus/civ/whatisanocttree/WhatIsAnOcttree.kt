package plus.civ.whatisanocttree

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import de.undercouch.bson4jackson.BsonFactory
import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PluginInfo
import plus.civ.whatisanocttree.listeners.PlayerLoadUnload

@PluginInfo(
		name = "WhatIsAnOcttree",
		description = "A simple chunk/block-level data layer. Also has functionality for player data",
		author = "Amelorate",
		depends = ["CivModCore"],
)
class WhatIsAnOcttree: KotlinPlugin() {
	companion object {
		private var instanceStorage: WhatIsAnOcttree? = null
		val instance: WhatIsAnOcttree
			get() = instanceStorage!!
	}

	override fun onEnable() {
		super.onEnable()
		instanceStorage = this
		PlayerLoadUnload.enable()
	}

	override fun onDisable() {
		super.onDisable()
		PlayerLoadUnload.disable()
	}

	private val validator = BasicPolymorphicTypeValidator.builder().allowIfBaseType(Any::class.java).build() // allow anything to be instantiated, since we're only using internal data
	private val bsonMapper = ObjectMapper(BsonFactory()).setPolymorphicTypeValidator(validator).registerKotlinModule()

	/**
	 * Tries to serialize and then deserialize the passed object using Jackson, the same library that our mongo library uses
	 *
	 * If it fails, then we can assume that inserting it into the database will also fail
	 *
	 * @throws com.fasterxml.jackson.databind.JsonMappingException
	 *
	 * @return If the thing and its deserialized copy are equal. This may be valuable to check, depending on the context
	 */
	fun tryToSerializeAndDeserializeObject(thing: Any): Boolean {
		val clazz = thing.javaClass
		val bytes = bsonMapper.writeValueAsBytes(thing)
		val otherThing = bsonMapper.readValue(bytes, clazz)

		return thing == otherThing
	}
}
