package plus.civ.whatisanocttree.player

import org.bukkit.Bukkit
import org.bukkit.plugin.Plugin
import org.bukkit.scheduler.BukkitRunnable
import org.bukkit.scheduler.BukkitTask
import org.litote.kmongo.eq
import plus.civ.whatisanocttree.WhatIsAnOcttree
import java.util.*
import java.util.concurrent.ConcurrentLinkedDeque
import java.util.concurrent.atomic.AtomicReference
import kotlin.collections.set

class LoadingPlayer(val uuid: UUID): BukkitRunnable() {
	var task: AtomicReference<BukkitTask?> = AtomicReference(null)

	override fun runTaskAsynchronously(plugin: Plugin): BukkitTask {
		PlayerData.loadingTasks.add(this)
		// ^ this unlocked write is safe because of the concurrent data structure and this is being done on the main thread;
		//   nobody can observe a loaded chunk with no loading task

		val task = super.runTaskAsynchronously(plugin)
		this.task.set(task)
		return task
	}

	override fun run() {
		val multipleData = try {
			PlayerData.collection.find(PlayerData::playerUUID eq uuid).toList()
		} catch (e: Exception) {
			WhatIsAnOcttree.instance.logger.warning("Object already on player ($uuid) can't be deserialized!")
			e.printStackTrace()
			Bukkit.getScheduler().runTask(WhatIsAnOcttree.instance) {
				Bukkit.getPlayer(uuid)?.kickPlayer("WhatIsAnOcttree: Corrupt Player Data (no de)! Please contact the admins.")
			}
			return
		}

		val data = mergeData(multipleData)

		PlayerData.playerLock.lock()
		try {
			if (task.getAndSet(null) == null) {
				// the positioning of this check is very specific to prevent the world from changing under us
				return
			}

			if (data != null && !PlayerData.loadedPlayers.contains(data)) {
				PlayerData.loadedPlayers[uuid] = data
			}
			PlayerData.loadingTasks.remove(this)
		} finally {
			PlayerData.playerLock.unlock()
		}

		if (data != null && !data.dataSafeToSave()) {
			Bukkit.getScheduler().runTask(WhatIsAnOcttree.instance) {
				Bukkit.getPlayer(uuid)?.kickPlayer("WhatIsAnOcttree: Corrupt Player Data! Please contact the admins.")
			}
		}
	}

	fun runNow() {
		task.getAndSet(null)?.cancel()

		val multipleData = try {
			PlayerData.collection.find(PlayerData::playerUUID eq uuid).toList()
		} catch (e: Exception) {
			WhatIsAnOcttree.instance.logger.warning("Object already on player ($uuid) can't be deserialized!")
			e.printStackTrace()
			Bukkit.getScheduler().runTask(WhatIsAnOcttree.instance) {
				Bukkit.getPlayer(uuid)?.kickPlayer("WhatIsAnOcttree: Corrupt Player Data (no de)! Please contact the admins.")
			}
			return
		}

		val data = mergeData(multipleData)
		PlayerData.playerLock.lock()
		try {
			if (data != null && !PlayerData.loadedPlayers.contains(data)) {
				PlayerData.loadedPlayers[uuid] = data
			}
			PlayerData.loadingTasks.remove(this)
		} finally {
			PlayerData.playerLock.unlock()
		}

		if (data != null && !data.dataSafeToSave()) {
			Bukkit.getScheduler().runTask(WhatIsAnOcttree.instance) {
				Bukkit.getPlayer(uuid)?.kickPlayer("WhatIsAnOcttree: Corrupt Player Data! Please contact the admins.")
			}
		}
	}

	fun mergeData(manyData: List<PlayerData>): PlayerData? {
		val multipleData = manyData.toList()
		if (multipleData.isEmpty()) return null
		if (multipleData.size == 1) return multipleData.first()

		val chunkLevelData: ConcurrentLinkedDeque<Any> = ConcurrentLinkedDeque()

		for (data in multipleData) {
			chunkLevelData.addAll(data.data)
		}

		val firstData = multipleData.first()
		firstData.data.clear()
		firstData.data.addAll(chunkLevelData)
		firstData.saveLater()

		val tailData = multipleData.drop(1)
		tailData.forEach(PlayerData::deleteLater)

		return firstData
	}
}
