package plus.civ.whatisanocttree.chunk

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonTypeInfo
import org.bukkit.Location
import org.litote.kmongo.Id
import org.litote.kmongo.newId
import plus.civ.whatisanocttree.DataNotLoadedException
import vg.civcraft.mc.civmodcore.extensions.chebyshevDistance
import vg.civcraft.mc.civmodcore.extensions.distanceIgnoringHeight
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedDeque
import kotlin.math.abs

data class ChunkData(
	val location: List<Int>,
	val world: String,
	@JsonTypeInfo(
		use = JsonTypeInfo.Id.CLASS,
		include = JsonTypeInfo.As.PROPERTY,
		property = "type")
	val chunkLevelData: ConcurrentLinkedDeque<Any> = ConcurrentLinkedDeque(),
	val blockLevelData: ConcurrentHashMap<Short, BlockData> = ConcurrentHashMap(),
	val _id: Id<ChunkData> = newId(),
) {
	data class BlockData(
		@JsonTypeInfo(
			use = JsonTypeInfo.Id.CLASS,
			include = JsonTypeInfo.As.PROPERTY,
			property = "type")
		val data: ConcurrentLinkedDeque<Any> = ConcurrentLinkedDeque()
	)
	companion object {
		val loadedChunks = ConcurrentLinkedDeque<ChunkData>()

		val Location.chunkBlockLocation: Short
			get() = ((this.blockX and 0xF) xor ((this.blockZ and 0xF) shl 4) xor ((this.blockY and 0xFF) shl 8)).toShort()

		/**
		 * Converts a short returned by Location.chunkBlockLocation into a proper location in the world.
		 *
		 * @param chunkLocation The location of the corner of the chunk containing the short location.
		 * There are no checks to enforce this location is aligned to 16 blocks.
		 */
		fun shortToLocation(chunkLocation: Location, short: Short): Location {
			val bitfield = short.toInt()
			val x = bitfield and 0x000F
			val z = bitfield and 0x00F0 shr 4
			val y = bitfield and 0xFF00 shr 8

			return chunkLocation.clone().add(x.toDouble(), y.toDouble(), z.toDouble())
		}

		/**
		 * Gets the data for a block at a given location
		 *
		 * If the chunk was very recently loaded, this function may wrongly return an empty list
		 *
		 * @throws DataNotLoadedException
		 */
		inline fun <reified T: Any> getBlock(location: Location): List<T> {
			val chunkData = getChunk(location)
				?: return emptyList() // there's no data in that chunk, so there's no data in that block

			val chunkBlockLocation = location.chunkBlockLocation
			return chunkData.blockLevelData[chunkBlockLocation]?.data?.filterIsInstance<T>() ?: emptyList()
		}

		/**
		 * Gets the data for the block at that location
		 */
		inline fun <reified T: Any> Location.getBlockData(): List<T> = getBlock(this)

		/**
		 * Adds a piece of data to be associated with that specific block
		 */
		fun addBlockData(location: Location, data: Any) {
			var chunkData = getChunk(location)
			if (chunkData == null) {
				chunkData = ChunkData(listOf(location.chunk.x, location.chunk.z), location.world.name)
				loadedChunks.add(chunkData)
				// this unlocked access is okay because we just checked the chunk isn't being loaded,
				// and the only thread who can spawn loading tasks is us
			}

			val clazz = data.javaClass
			for (field in clazz.fields) {
				if (field.name == "type") {
					throw IllegalArgumentException("The object contains a field named type. " +
						"This will cause silent data corruption if it gets into the database.")
				}
			}

			val blockSet = chunkData.blockLevelData.getOrPut(location.chunkBlockLocation) { BlockData() }.data
			blockSet.add(data)
		}

		/**
		 * Adds a piece of data to be associated with that specific block
		 */
		@JvmName("addBlockData1")
		fun Location.addBlockData(data: Any) = addBlockData(this, data)

		/**
		 * Removes a piece of data from the specific block
		 *
		 * @return true if the data was associated with the block
		 */
		fun removeBlockData(location: Location, data: Any): Boolean {
			val chunkData = getChunk(location) ?: return false
			val blockSet = chunkData.blockLevelData[location.chunkBlockLocation]?.data ?: return false
			return blockSet.remove(data)
		}

		/**
		 * Removes a piece of data from the block at the location
		 *
		 * @return true if the data was associated with the block
		 */
		@JvmName("removeBlockData1")
		fun Location.removeBlockData(data: Any): Boolean = removeBlockData(this, data)

		/**
		 * Gets the data associated with the chunk inside the location
		 */
		inline fun <reified T: Any> getChunkLevelData(location: Location): List<T> {
			val chunkData = getChunk(location) ?: return emptyList()
			return chunkData.chunkLevelData.filterIsInstance<T>()
		}

		/**
		 * Gets the chunk data for the chunk inside the location
		 */
		@JvmName("getChunkLevelData1")
		inline fun <reified T: Any> Location.getChunkLevelData(): List<T> = getChunkLevelData(this)

		/**
		 * Adds some data to the chunk inside the location
		 */
		fun addChunkLevelData(location: Location, data: Any) {
			var chunkData = getChunk(location)
			if (chunkData == null) {
				chunkData = ChunkData(listOf(location.chunk.x, location.chunk.z), location.world.name)
				loadedChunks.add(chunkData)
				// this unlocked access is okay because we just checked the chunk isn't being loaded,
				// and the only thread who can spawn loading tasks is us
			}

			val clazz = data.javaClass
			for (field in clazz.fields) {
				if (field.name == "type") {
					throw IllegalArgumentException("The object contains a field named type. " +
						"This will cause silent data corruption if it gets into the database.")
				}
			}

			chunkData.chunkLevelData.add(data)
		}

		/**
		 * Adds some data to the chunk inside the location
		 */
		@JvmName("addChunkLevelData1")
		fun Location.addChunkLevelData(data: Any) = addChunkLevelData(this, data)

		/**
		 * Removes the piece of data from the chunk inside the given location
		 */
		fun removeChunkLevelData(location: Location, data: Any): Boolean {
			val chunkData = getChunk(location) ?: return false
			return chunkData.chunkLevelData.remove(data)
		}

		/**
		 * Removes the piece of data from the chunk inside the location
		 */
		@JvmName("removeChunkLevelData1")
		fun Location.removeChunkLevelData(data: Any): Boolean = removeChunkLevelData(this, data)

		/**
		 * Gets the ChunkData class associated with the chunk inside the given location
		 *
		 * @throws DataNotLoadedException
		 *
		 * @return null if the chunk has no data in it
		 */
		fun getChunk(location: Location): ChunkData? {
			if (!location.chunk.isLoaded) {
				throw DataNotLoadedException()
			}

			val chunkLocation = listOf(location.chunk.x, location.chunk.z)

			return loadedChunks.firstOrNull { it.location == chunkLocation && it.world == location.world.name }
		}

		/**
		 * Gets the ChunkData class for the chunk inside the location
		 */
		fun Location.getChunkData() = getChunk(this)

		fun ___cubicDistance(here: List<Int>, other: List<Int>): Double {
			var maxDistance = 0.0
			for ((i, point) in here.withIndex()) {
				maxDistance = maxOf(maxDistance, abs(point.toDouble() - other[i].toDouble()))
			}

			return maxDistance
		}

		/**
		 * Returns a list of all the blocks within the given radius (spherical) that contain data of the given type.
		 *
		 * If a chunk is not loaded, it will be considered to have no data, so it will return no data.
		 * In other words, a chunk that is not loaded will be ignored for the purposes of this function.
		 */
		inline fun <reified T: Any> Location.getBlocksInRadiusWithData(radius: Double): List<Location> {
			val chunkSearchRadius = (radius + 16) / 16 + 1
			val chunkLocation = listOf(blockX / 16, blockZ / 16)
			val chunks = loadedChunks.filter { it.world == world.name && ___cubicDistance(it.location, chunkLocation) < chunkSearchRadius }

			val result = mutableListOf<Location>()
			for (chunk in chunks) {
				result.addAll(chunk.blockLevelData
					.filterValues { it.data.filterIsInstance<T>().isNotEmpty() }
					.mapKeys { shortToLocation(
						Location(world, chunk.location[0] * 16.0, 0.0, chunk.location[1] * 16.0), it.key) }
					.filter { it.key.distance(this) < radius }
					.keys)
			}

			return result
		}

		/**
		 * Returns a list of all the blocks within the given radius (cubic) that contain data of the given type.
		 *
		 * This function uses taxicab distance to calculate its "radius".
		 *
		 * If a chunk is not loaded, it will be considered to have no data, so it will return no data.
		 * In other words, a chunk that is not loaded will be ignored for the purposes of this function.
		 */
		inline fun <reified T: Any> Location.getBlocksInCubeRadiusWithData(radius: Double): List<Location> {
			val chunkSearchRadius = (radius + 16) / 16 + 1
			val chunkLocation = listOf(blockX / 16, blockZ / 16)
			val chunks = loadedChunks.filter { it.world == world.name && ___cubicDistance(it.location, chunkLocation) < chunkSearchRadius }

			val result = mutableListOf<Location>()
			for (chunk in chunks) {
				result.addAll(chunk.blockLevelData
					.filterValues { it.data.filterIsInstance<T>().isNotEmpty() }
					.mapKeys { shortToLocation(
						Location(world, chunk.location[0] * 16.0, 0.0, chunk.location[1] * 16.0), it.key) }
					.filter { it.key.chebyshevDistance(this) < radius }
					.keys)
			}

			return result
		}

		/**
		 * Returns a list of all the blocks within the given radius (cylindrical) that contain data of the given type.
		 *
		 * If a chunk is not loaded, it will be considered to have no data, so it will return no data.
		 * In other words, a chunk that is not loaded will be ignored for the purposes of this function.
		 */
		inline fun <reified T: Any> Location.getBlocksInCylRadiusWithData(radius: Double, height: Double): List<Location> {
			val chunkSearchRadius = (radius + 16) / 16 + 1
			val chunkLocation = listOf(blockX / 16, blockZ / 16)
			val chunks = loadedChunks.filter { it.world == world.name && ___cubicDistance(it.location, chunkLocation) < chunkSearchRadius }

			val result = mutableListOf<Location>()
			for (chunk in chunks) {
				result.addAll(chunk.blockLevelData
					.filterValues { it.data.filterIsInstance<T>().isNotEmpty() }
					.mapKeys { shortToLocation(
						Location(world, chunk.location[0] * 16.0, 0.0, chunk.location[1] * 16.0), it.key) }
					.filter { it.key.distanceIgnoringHeight(this) < radius
						&& it.key.y - this.y < height }
					.keys)
			}

			return result
		}
	}

	fun garbageCollectBlocks() {
		val garbageBlocks = mutableSetOf<Short>()
		for ((location, data) in blockLevelData) {
			if (data.data.isEmpty()) {
				garbageBlocks.add(location)
			}
		}

		for (location in garbageBlocks) {
			blockLevelData.remove(location)
		}
	}

	@get:JsonIgnore
	val isEmpty: Boolean
		get() {
			garbageCollectBlocks()
			return blockLevelData.isEmpty() && chunkLevelData.isEmpty()
		}

	@get:JsonIgnore
	val isNotEmpty: Boolean
		get() = !isEmpty

	/**
	 * Spawns a bukkit async task to save this chunk soon.
	 *
	 * Calling this function before the previous task has ran is fine, because it will cancel the old task.
	 *
	 * Calling this task after a delete call MAY restore the chunk, however this can not be relied upon
	 */
	fun saveLater() {
		// devkit snip
	}

	/**
	 * Saves this chunk now, blocking the thread.
	 */
	fun saveBlocking() {
		garbageCollectBlocks()
		// devkit snip
	}

	/**
	 * Spawns a bukkit async task to delete this chunk soon.
	 *
	 * Calling this function cancels any previous save tasks that have not ran yet, however this may on occasion fail.
	 */
	fun deleteLater(deleteNotEmptyChunks: Boolean = false) {
		// devkit snip
	}

	/**
	 * Deletes this chunk now, blocking the thread.
	 *
	 * @param deleteNotEmptyChunks If this function should delete a chunk that contains data
	 */
	fun deleteBlocking(deleteNotEmptyChunks: Boolean = false) {
		// devkit snip
	}
}
