package plus.civ.whatisanocttree.player

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonTypeInfo
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import org.bukkit.scheduler.BukkitTask
import org.litote.kmongo.*
import plus.civ.whatisanocttree.DataNotLoadedException
import plus.civ.whatisanocttree.Database
import plus.civ.whatisanocttree.WhatIsAnOcttree
import java.lang.Exception
import java.util.UUID
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedDeque
import java.util.concurrent.locks.ReentrantLock

data class PlayerData(
		val playerUUID: UUID,
		@JsonTypeInfo(
				use = JsonTypeInfo.Id.CLASS,
				include = JsonTypeInfo.As.PROPERTY,
				property = "type")
		val data: ConcurrentLinkedDeque<Any> = ConcurrentLinkedDeque(),
		val _id: Id<PlayerData> = newId(),
) {
	companion object {
		val playerLock = ReentrantLock()
		val loadedPlayers = ConcurrentHashMap<UUID, PlayerData>()
		val loadingTasks = ConcurrentLinkedDeque<LoadingPlayer>()

		val collection = Database.database.getCollection<PlayerData>("Players")

		init {
			collection.ensureIndex(PlayerData::playerUUID)
		}

		/**
		 * Gets the data associated with the player with the given type
		 */
		inline fun <reified T: Any> getPlayerData(player: Player): List<T> {
			val playerData = getPlayer(player) ?: return emptyList()
			return playerData.data.filterIsInstance<T>()
		}

		/**
		 * Gets the player data for the player with the given type
		 */
		@JvmName("getPlayerData1")
		inline fun <reified T: Any> Player.getPlayerData(): List<T> = getPlayerData(this)

		/**
		 * Adds some data to the player
		 */
		fun addPlayerData(player: Player, data: Any) {
			try {
				WhatIsAnOcttree.instance.tryToSerializeAndDeserializeObject(data)
			} catch (e: Exception) {
				WhatIsAnOcttree.instance.logger.warning("Object adding to player ${player.name} (${player.uniqueId}) can't be serialized! Silently discarding data!")
				WhatIsAnOcttree.instance.logger.warning("Object: $data")
				e.printStackTrace()

				return
			}

			val clazz = data.javaClass
			for (field in clazz.fields) {
				if (field.name == "type") {
					throw IllegalArgumentException("The object contains a field named type. " +
						"This will cause silent data corruption if it gets into the database.")
				}
			}

			var playerData = getPlayer(player)
			if (playerData == null) {
				playerData = PlayerData(player.uniqueId)
				loadedPlayers[player.uniqueId] = playerData
				// this unlocked access is okay because we just checked the player isn't being loaded,
				// and the only thread who can spawn loading tasks is us
			}

			playerData.data.add(data)
		}

		/**
		 * Adds some data to the player
		 */
		@JvmName("addPlayerData1")
		fun Player.addPlayerData(data: Any) = addPlayerData(this, data)

		/**
		 * Removes the piece of data from the player
		 */
		fun removePlayerData(player: Player, data: Any): Boolean {
			val chunkData = getPlayer(player) ?: return false
			return chunkData.data.remove(data)
		}

		/**
		 * Removes the piece of data from the player
		 */
		@JvmName("removePlayerData1")
		fun Player.removePlayerData(data: Any): Boolean = removePlayerData(this, data)

		/**
		 * Gets the PlayerData class associated with the player
		 *
		 * @throws DataNotLoadedException
		 *
		 * @return null if the player has no data in it
		 */
		fun getPlayer(player: Player): PlayerData? {
			if (!player.isOnline) {
				throw DataNotLoadedException()
			}

			playerLock.lock()
			try {
				// check if we're currently pending to load the player, and if we are eagerly run it
				for (loadingTask in loadingTasks) {
					if (loadingTask.uuid == player.uniqueId) {
						loadingTask.runNow()
					}
				}

				return loadedPlayers[player.uniqueId]
			} finally {
			    playerLock.unlock()
			}
		}

		/**
		 * Gets the PlayerData class for the chunk inside the location
		 */
		fun Player.getPlayerData() = getPlayer(this)
	}

	@get:JsonIgnore
	val isEmpty: Boolean
		get() {
			return data.isEmpty()
		}

	@get:JsonIgnore
	val isNotEmpty: Boolean
		get() = !isEmpty

	/**
	 * Tries to serialize and deserialize the player's data. Returns false if some data is not serializable
	 *
	 * This function will also print a warning and stack trace if there is any bad data present.
	 *
	 * @return True if the data serializes correctly, false otherwise
	 */
	fun dataSafeToSave(): Boolean {
		var result = true
		for (data in data) {
			try {
				WhatIsAnOcttree.instance.tryToSerializeAndDeserializeObject(data)
			} catch (e: Exception) {
				result = false
				WhatIsAnOcttree.instance.logger.warning("Object already on player ($playerUUID) can't be serialized! Object: $data")
				e.printStackTrace()

				continue // print errors for all other bad data, just in case
			}
		}

		return result
	}

	private var saveTask: BukkitTask? = null

	/**
	 * Spawns a bukkit async task to save this player soon.
	 *
	 * Calling this function before the previous task has ran is fine, because it will cancel the old task.
	 *
	 * Calling this task after a delete call MAY restore the player's data, however this can not be relied upon
	 */
	fun saveLater() {
		saveTask?.cancel()
		saveTask = Bukkit.getScheduler().runTaskAsynchronously(WhatIsAnOcttree.instance) {
			saveBlocking()
		}
	}

	/**
	 * Saves this player now, blocking the thread.
	 */
	fun saveBlocking() {
		if (!dataSafeToSave()) {
			Bukkit.getScheduler().runTask(WhatIsAnOcttree.instance) {
				Bukkit.getPlayer(playerUUID)?.kickPlayer("WhatIsAnOcttree: Player Data Corrupt! If this continues, please contact the admins.")
			}
		} else {
			collection.updateOneById(_id, this, upsert())
		}
	}

	/**
	 * Spawns a bukkit async task to delete this player's data soon.
	 *
	 * Calling this function cancels any previous save tasks that have not ran yet, however this may on occasion fail.
	 */
	fun deleteLater(deleteNotEmptyPlayer: Boolean = false) {
		saveTask?.cancel()
		saveTask = Bukkit.getScheduler().runTaskAsynchronously(WhatIsAnOcttree.instance) {
			deleteBlocking(deleteNotEmptyPlayer)
		}
	}

	/**
	 * Deletes this player's data now, blocking the thread.
	 *
	 * @param deleteNotEmptyPlayer If this function should delete a player that contains data
	 */
	fun deleteBlocking(deleteNotEmptyPlayer: Boolean = false) {
		if (isNotEmpty && !deleteNotEmptyPlayer) {
			return
		}

		collection.deleteOneById(_id)
	}
}
