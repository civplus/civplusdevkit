package plus.civ.whatisanocttree.listeners

import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockPlaceEvent
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.addBlockData
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.getBlockData
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.removeBlockData
import plus.civ.whatisanocttree.chunk.tags.ItemSerializable
import plus.civ.whatisanocttree.chunk.tags.ItemSerializable.Companion.addAllData
import plus.civ.whatisanocttree.chunk.tags.ItemSerializable.Companion.getItemData
import vg.civcraft.mc.civmodcore.event.BlockDropItemEvent

object ItemDataPortabilityListener: Listener {
	@EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
	fun blockPlace(event: BlockPlaceEvent) {
		val datas = event.itemInHand.getItemData<ItemSerializable>()
		if (datas.isEmpty()) return

		for (data in datas) {
			data.onBecomeBlock(event)
			event.blockPlaced.location.addBlockData(data)
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGH)
	fun blockDrop(event: BlockDropItemEvent) {
		val datas = event.block.location.getBlockData<ItemSerializable>().toMutableSet()
		val noDatas = mutableSetOf<ItemSerializable>()
		if (datas.isEmpty()) return

		for (data in datas) {
			event.block.location.removeBlockData(data)
			val shouldAddData = data.onBecomeItem(event)
			if (!shouldAddData) {
				noDatas.add(data)
			}
		}

		datas.removeAll(noDatas)

		for (drop in event.droppedItems!!) {
			drop.addAllData(datas)
		}
	}
}
