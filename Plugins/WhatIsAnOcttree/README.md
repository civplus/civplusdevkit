# WhatIsAnOcttree?

I wrote this plugin without knowing what an octtree is, and that's a good thing.
Instead, this plugin loads and unloads data from the database associated with chunks and blocks in step with vanilla.
On the live server, this involves fancy async access that minimizes lag due to database delays.
In the devkit, there's none of that. Instead, it keeps block data in memory and discards it when the server restarts.
This is to prevent abuse of the devkit's copywritten code.

# Developing plugins using WIAO

For an example plugin, see Stickers and MacGuffin's StoneAnvil, FlyingGravel, and TrapSeeingDispenser.
