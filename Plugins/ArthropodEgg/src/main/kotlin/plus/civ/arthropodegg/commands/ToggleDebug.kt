package plus.civ.arthropodegg.commands

import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import plus.civ.arthropodegg.Config
import plus.civ.kotlinplugin.CommandInfo
import vg.civcraft.mc.civmodcore.extensions.onOrOff

@CommandInfo(
	name = "arthropodEggToggleDebug",
	description = "Admin command to toggle ArthropodEgg debug mode",
	aliases = ["aetd", "aeToggleDebug"],
	usage = "/<command>",
	permission = "arthropodegg.configure",
)
class ToggleDebug: CommandExecutor {
	override fun onCommand(sender: CommandSender, cmd: Command, label: String, args: Array<out String>): Boolean {
		Config.debug = !Config.debug
		sender.sendMessage("${ChatColor.YELLOW}Debug has been turned ${Config.debug.onOrOff}.")
		return true
	}
}
