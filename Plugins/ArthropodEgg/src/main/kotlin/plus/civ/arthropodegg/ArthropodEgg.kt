package plus.civ.arthropodegg

import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PermissionDefault
import plus.civ.kotlinplugin.PluginInfo
import plus.civ.kotlinplugin.PluginPermission

@PluginInfo(
	name = "ArthropodEgg",
	description = "Make monsters drop spawneggs when killed with a Bane of Arthropods sword",
	author = "fndragon",
	authors = ["sepia"],
	depends = ["CivModCore"],
	permissions = [
		PluginPermission(
			name = "arthropodegg.*",
			description = "All ArthropodEgg permissions",
			default = PermissionDefault.OP,
			children = [
				PluginPermission(
					name = "arthropodegg.getDrops",
					description = "Get spawnegg drops from monsters",
					default = PermissionDefault.TRUE,
				),
				PluginPermission(
					name = "arthropodegg.configure",
					description = "Configure various ArthropodEgg settings",
					default = PermissionDefault.OP,
				),
				PluginPermission(
					name = "arthropodegg.info",
					description = "See various ArthropodEgg settings",
					default = PermissionDefault.OP,
				),
			]
		)
	]
)
class ArthropodEgg: KotlinPlugin()
