package plus.civ.arthropodegg.commands

import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import plus.civ.arthropodegg.Config
import plus.civ.kotlinplugin.CommandInfo

@CommandInfo(
	name = "arthropodEggSetDroprate",
	description = "Admin command to set ArthropodEgg droprates",
	aliases = ["aesd", "aeSetDroprate"],
	usage = "/<command> [0..100]",
	permission = "arthropodegg.configure",
)
class SetDroprate: CommandExecutor {
	override fun onCommand(sender: CommandSender, cmd: Command, label: String, args: Array<out String>): Boolean {
		if (args.size != 1) {
			sender.sendMessage("${ChatColor.RED}That command requires one argument (the droprate you want to set, as a percentage from 0 to 100.")
			return false
		}
		val droprate = args[0].toIntOrNull()
		if (droprate == null) {
			sender.sendMessage("${ChatColor.RED}The droprate you tried to enter is misformatted.")
			return false
		} else if (droprate < 0 || droprate > 100) {
			sender.sendMessage("${ChatColor.RED}Droprate must be from 0 to 100.")
			return false
		}
		Config.droprate = droprate / 100.0f
		sender.sendMessage("${ChatColor.YELLOW}Droprate has been set to $droprate%")
		return true
	}
}
