package plus.civ.arthropodegg

import org.bukkit.entity.EntityType

object Config {
	var debug: Boolean = false;
	// final chance to drop = arthropod_level * droprate * LOOTING_MODIFIER^looting_level
	var droprate: Float = 0.03f;
	val LOOTING_MODIFIER = 1.5f;
	// whether normal mob drops should still happen if an egg is dropped
	val NORMAL_DROPS_WITH_EGG_DROP = false;
	val EGG_ENABLED_ENTITIES: Array<EntityType> = arrayOf(
		EntityType.BLAZE,
		EntityType.CHICKEN,
		EntityType.COW,
		EntityType.MUSHROOM_COW,
		EntityType.SPIDER,
		EntityType.ZOMBIE,
		EntityType.SKELETON,
		EntityType.PIG,
		EntityType.BAT,
		EntityType.CREEPER,
		EntityType.SLIME,
		EntityType.SHEEP,
		EntityType.HORSE,
		EntityType.RABBIT,
		EntityType.SQUID,
		EntityType.OCELOT
	)
}
