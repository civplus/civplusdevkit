package plus.civ.arthropodegg.listeners

import net.minecraft.server.v1_8_R3.NBTTagCompound
import org.bukkit.ChatColor
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack
import org.bukkit.enchantments.Enchantment
import org.bukkit.entity.Ageable
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDeathEvent
import org.bukkit.material.SpawnEgg
import plus.civ.arthropodegg.Config
import kotlin.math.pow

class EntityListener: Listener {
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	fun onEntityDeath(event: EntityDeathEvent) {
		// return on non-player (null) killer
		event.entity.killer ?: return
		// return on non-enabled entity type
		if (!Config.EGG_ENABLED_ENTITIES.contains(event.entityType)) return
		// return on baby entity
		if (event.entity is Ageable) {
			if (!(event.entity as Ageable).isAdult) return
		}
		// killer's currently equipped item, otherwise return if none
		val murderWeapon = event.entity.killer.itemInHand ?: return
		//return if no enchantments on the murder weapon
	    if (murderWeapon.enchantments.isEmpty()) return
		// return if the murder weapon doesn't have bane of arthropods
		if (!murderWeapon.containsEnchantment(Enchantment.DAMAGE_ARTHROPODS)) return
		// return if the player doesn't have permission to get drops
		if (!(event.entity.killer as Player).hasPermission("arthropodegg.getDrops")) return

		val lootingLevel = murderWeapon.getEnchantmentLevel(Enchantment.LOOT_BONUS_MOBS)
		val baneLevel = murderWeapon.getEnchantmentLevel(Enchantment.DAMAGE_ARTHROPODS)
		val rollNeeded = 1 - baneLevel * Config.droprate * Config.LOOTING_MODIFIER.pow(lootingLevel)
		val roll = Math.random()
		if (Config.debug) {
			event.entity.killer.sendMessage(
				"""
				|${ChatColor.YELLOW}Bane of Arthropods [${ChatColor.BOLD}$baneLevel${ChatColor.RESET}${ChatColor.YELLOW}]
				|${ChatColor.YELLOW}Looting [${ChatColor.BOLD}$lootingLevel${ChatColor.RESET}${ChatColor.YELLOW}]
				|${ChatColor.YELLOW}Roll needed [${ChatColor.BOLD}$rollNeeded${ChatColor.RESET}${ChatColor.YELLOW}]
				|${ChatColor.YELLOW}Roll rolled [${ChatColor.BOLD}$roll${ChatColor.RESET}${ChatColor.YELLOW}]
				|${ChatColor.YELLOW}Roll ${ChatColor.BOLD}${if (roll >= rollNeeded) "${ChatColor.GREEN}successful" else "${ChatColor.RED}failed"}
				""".trimMargin()
			)
		}
		if (roll >= rollNeeded) {
			val egg = SpawnEgg(event.entityType)
			val nonNmsItem = egg.toItemStack(1)
			// NMS fuckery required for spawn eggs
			val nmsStack: net.minecraft.server.v1_8_R3.ItemStack = CraftItemStack.asNMSCopy(nonNmsItem)
			val eid = NBTTagCompound()
			eid.setString("id", egg.spawnedType.name)
			val nbt = nmsStack.tag ?: NBTTagCompound()
			nbt.set("EntityTag", eid)
			val finishedItem = CraftItemStack.asBukkitCopy(nmsStack)
			if (!Config.NORMAL_DROPS_WITH_EGG_DROP) {
				event.drops.clear()
				event.droppedExp = 0
			}
			event.drops.add(finishedItem)
			if (Config.debug) {
				event.entity.killer.sendMessage("${ChatColor.YELLOW}${event.entityType} egg dropped.")
			}
		}
	}
}
