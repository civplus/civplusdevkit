package plus.civ.arthropodegg.commands

import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import plus.civ.arthropodegg.Config
import plus.civ.kotlinplugin.CommandInfo
import vg.civcraft.mc.civmodcore.extensions.onOrOff

@CommandInfo(
	name = "arthropodEggInfo",
	description = "Admin command to see ArthropodEgg droprates",
	aliases = ["aei", "aeInfo"],
	usage = "/<command>",
	permission = "arthropodEgg.info"
)
class Info: CommandExecutor {
	override fun onCommand(sender: CommandSender, cmd: Command, label: String, args: Array<out String>): Boolean {
		if (args.isNotEmpty()) return false

		sender.sendMessage(
			"""
				|${ChatColor.AQUA}Droprate: ${Config.droprate * 100}%
				|Debug: ${Config.debug.onOrOff}
			""".trimMargin()
		)
		return true
	}
}
