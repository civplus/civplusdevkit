package plus.civ.leer.commands

import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import plus.civ.kotlinplugin.CommandInfo
import plus.civ.layerafterlayer.model.Group
import plus.civ.leer.db.DatabaseManager
import plus.civ.leer.db.model.LeerLog
import plus.civ.leer.db.model.Watcher

@CommandInfo(
	name = "leer",
	description = "The core leer command",
	usage = "/leer help",
)
class LeerCommand: CommandExecutor {

	override fun onCommand(sender: CommandSender, command: Command, name: String, args: Array<out String>): Boolean {
		if (sender !is Player) {
			sender.sendMessage(ChatColor.RED.toString() + "This command only works for players.")
			return true
		}
		if (args.isEmpty()) {
			return false
		}

		when (args[0]) {
			"help" -> {
				cmdHelp(sender, args.sliceArray(1 until args.size))
				return true
			}
			"setgroup" -> {
				cmdSetGroup(sender, args.sliceArray(1 until args.size))
				return true
			}
			"logs" -> {
				cmdLogs(sender, args.sliceArray(1 until args.size))
				return true
			}
			"info" -> {
				cmdInfo(sender, args.sliceArray(1 until args.size))
				return true
			}
			else -> {
				return false
			}
		}
	}

	private fun cmdInfo(sender: Player, args: Array<out String>) {
		if (args.size > 1) {
			sender.sendMessage("${ChatColor.RED}This command only takes one argument (see ${ChatColor.GRAY}/leer help)")
			return
		}
		// The number given, otherwise 0
		val page = if (args.size == 1) {
			try {
				args[0].toInt() - 1
			} catch(e: NumberFormatException) {
				sender.sendMessage("${ChatColor.RED}You've entered an invalid page number. Please enter a number. See ${ChatColor.GRAY}/leer help")
				return
			}
		} else {
			0
		}

		val friendlyWatchers = Watcher.getAllInChunk(sender.world, sender.location.chunk.x, sender.location.chunk.z)
			.filter { it.group.includesMember(sender) }

		val c = ChatColor.YELLOW.toString()
		val h = ChatColor.AQUA.toString()
		val i = ChatColor.ITALIC.toString()

		if (friendlyWatchers.isEmpty()) {
			sender.sendMessage("${c}You're not watching this chunk.")
			return
		}

		val sb = StringBuilder()
		val resultsPerPage = 10
		val totalPages = friendlyWatchers.size / resultsPerPage + 1
		sb.appendLine("$c${i}Page ${page+1}/$totalPages")
		sb.appendLine("${c}-------------------")
		for (w in friendlyWatchers) {
			sb.appendLine("${c}Watched by group: ${h}${w.group.name}")
			sb.appendLine("${c}Time to expiration: ${h}${w.hoursToExpiration} hours")
			sb.appendLine("${c}-------------------")
		}

		sender.sendMessage(sb.toString())
	}

	private fun cmdHelp(sender: Player, args: Array<out String>) {
		val c = ChatColor.RED.toString()
		val b = ChatColor.BOLD.toString()
		val i = ChatColor.ITALIC.toString()

		val sb = StringBuilder()

		sb.appendLine("$c${b}Commands")

		sb.appendLine("$c$i${ChatColor.GRAY}/leer help")
		sb.appendLine(" - ${c}Show command information.")

		sb.appendLine("$c$i/leer setgroup [group]")
		sb.appendLine(" - ${c}Set the Layer group that chunks you throw an Eye of Ender in will be watched on.")

		sb.appendLine("$c$i/leer logs [page]")
		sb.appendLine(" - ${c}Show the logs of actions conducted in the chunk you're currently standing in.")

		sb.appendLine("$c$i/leer info")
		sb.appendLine(" - ${c}Show information about the chunk you're standing in.")

		sender.sendMessage(sb.toString())
	}

	private fun cmdSetGroup(sender: Player, args: Array<out String>) {
		if (args.size != 1) {
			sender.sendMessage("${ChatColor.RED}This command only takes one argument (see ${ChatColor.GRAY}/leer help)")
			return
		}

		val groupName = args[0]
		val group = Group.get(groupName)

		if (group == null) {
			sender.sendMessage("${ChatColor.RED}Either that group doesn't exist, or you're not allowed to watch chunks on it.")
			return
		} else if (!group.includesMember(sender)) {
			sender.sendMessage("${ChatColor.RED}Either that group doesn't exist, or you're not allowed to watch chunks on it.")
			return
		}

		DatabaseManager.setPlayerSelectedGroup(sender, group)
		sender.sendMessage("${ChatColor.GREEN}Your selected group has been set to ${group.name}.")
	}

	private fun cmdLogs(sender: Player, args: Array<out String>) {
		if (args.size > 1) {
			sender.sendMessage("${ChatColor.RED}This command only takes one argument (see ${ChatColor.GRAY}/leer help)")
			return
		}
		// The number given, otherwise 0
		val page = if (args.size == 1) {
			try {
				args[0].toInt() - 1
			} catch(e: NumberFormatException) {
				sender.sendMessage("${ChatColor.RED}You've entered an invalid page number. Please enter a number. See ${ChatColor.GRAY}/leer help")
				return
			}
		} else {
			0
		}

		val friendlyWatchersInChunk = Watcher.getAllInChunk(sender.world, sender.location.chunk.x, sender.location.chunk.z)
			.filter { it.group.includesMember(sender) }
		if (friendlyWatchersInChunk.isEmpty()) {
			sender.sendMessage("${ChatColor.YELLOW}You're not watching this chunk.")
			return
		}

		val allLogs = LeerLog.getAllInChunk(sender.location.world, sender.location.chunk.x, sender.location.chunk.z)
			.filter { log -> friendlyWatchersInChunk.any{ watcher -> log.time > watcher.watchingSince } } // throw away logs from before the chunk was watched
			.sortedBy { log -> -log.time } // sort most recent to least recent
		val totalPages = ((allLogs.size - 1) / 10) + 1

		val c = ChatColor.YELLOW.toString()
		val b = ChatColor.BOLD.toString()
		val i = ChatColor.ITALIC.toString()

		val sb = StringBuilder()
		val resultsPerPage = 10
		sb.appendLine("$c${i}Page ${page+1}/$totalPages")
		sb.appendLine("-------------------")
		for (log in allLogs.slice(page * resultsPerPage until minOf(allLogs.size, page * resultsPerPage + 10))) {
			sb.appendLine("${c}${log.alertMessageTimestamped}")
		}
		sb.appendLine("-------------------")
		sender.sendMessage(sb.toString())
	}
}
