package plus.civ.leer.listeners

import org.bukkit.entity.EntityType
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.block.BlockIgniteEvent
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.event.entity.EntityDeathEvent
import org.bukkit.event.inventory.InventoryOpenEvent
import org.bukkit.event.player.*
import org.bukkit.event.vehicle.VehicleDestroyEvent
import org.bukkit.event.vehicle.VehicleEnterEvent
import org.bukkit.event.vehicle.VehicleExitEvent
import org.bukkit.event.vehicle.VehicleMoveEvent
import org.spigotmc.event.entity.EntityDismountEvent
import org.spigotmc.event.entity.EntityMountEvent
import plus.civ.leer.Leer
import plus.civ.leer.db.model.LoggableActionKind
import vg.civcraft.mc.civmodcore.extensions.blockCoordsString
import vg.civcraft.mc.civmodcore.extensions.displayableName
import vg.civcraft.mc.civmodcore.extensions.location

internal object LoggedActionListener: Listener {

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun enterSnitchProximity(event: PlayerMoveEvent) {
		val from = event.from
		val to = event.to ?: return
		if (from.chunk != to.chunk) {
			Leer.logAction(to, LoggableActionKind.ENTERED_CHUNK, event.player, to.blockCoordsString)
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onPlayerTeleport(event: PlayerTeleportEvent) {
		enterSnitchProximity(PlayerMoveEvent(event.player, event.from, event.to))
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onVehicleMovement(event: VehicleMoveEvent) {
		if (event.vehicle.passenger is Player) {
			enterSnitchProximity(PlayerMoveEvent(event.vehicle.passenger as Player, event.from, event.to))
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onBlockPlace(event: BlockPlaceEvent) {
		val targetString = "${event.block.type.name} at ${event.block.location.blockCoordsString}"
		Leer.logAction(event.block.location, LoggableActionKind.PLACED_BLOCK, event.player, targetString)
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onBlockBreak(event: BlockBreakEvent) {
		val targetString = "${event.block.type.name} at ${event.block.location.blockCoordsString}"
		Leer.logAction(event.block.location, LoggableActionKind.BROKE_BLOCK, event.player, targetString)
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onEntityKill(event: EntityDeathEvent) {
		val victim = event.entity
		val killer = victim.killer ?: return
		if (victim.type == EntityType.PLAYER) {
			Leer.logAction(event.entity.location, LoggableActionKind.KILLED_PLAYER, killer, (victim as Player).displayName)
		} else {
			Leer.logAction(event.entity.location, LoggableActionKind.KILLED_MOB, killer, victim.displayableName)
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onDestroyVehicle(event: VehicleDestroyEvent) {
		if (event.attacker?.type != EntityType.PLAYER) return

		Leer.logAction(event.vehicle.location, LoggableActionKind.DESTROYED_VEHICLE, (event.attacker as Player), event.vehicle.displayableName)
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onEnterVehicle(event: VehicleEnterEvent) {
		if (event.entered.type != EntityType.PLAYER) return

		Leer.logAction(event.vehicle.location, LoggableActionKind.ENTERED_VEHICLE, (event.entered as Player), event.vehicle.type.name)
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onExitVehicle(event: VehicleExitEvent) {
		if (event.exited.type != EntityType.PLAYER) return

		Leer.logAction(event.vehicle.location, LoggableActionKind.EXITED_VEHICLE, (event.exited as Player), event.vehicle.type.name)
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onEmptyBucket(event: PlayerBucketEmptyEvent) {
		val blockLoc = event.blockClicked.getRelative(event.blockFace).location
		Leer.logAction(blockLoc, LoggableActionKind.EMPTIED_BUCKET, event.player, blockLoc.blockCoordsString)
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onFillBucket(event: PlayerBucketFillEvent) {
		Leer.logAction(event.blockClicked.location, LoggableActionKind.EMPTIED_BUCKET, event.player, event.blockClicked.location.blockCoordsString)
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onMountEntity(event: EntityMountEvent) {
		if (event.entityType != EntityType.PLAYER) return

		Leer.logAction(event.mount.location, LoggableActionKind.MOUNTED_ENTITY, (event.entity as Player), event.mount.displayableName)
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onDismountEntity(event: EntityDismountEvent) {
		if (event.entityType != EntityType.PLAYER) return

		Leer.logAction(event.dismounted.location, LoggableActionKind.UNMOUNTED_ENTITY, (event.entity as Player), event.dismounted.displayableName)
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onOpenInventory(event: InventoryOpenEvent) {
		val holder = event.inventory.holder ?: return
		val loc = holder.inventory.location ?: return

		Leer.logAction(loc, LoggableActionKind.OPENED_CONTAINER, (event.player as Player), "${event.inventory.title} at ${loc.blockCoordsString}")
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun playerJoinEvent(event: PlayerJoinEvent) {
		Leer.logAction(event.player.location, LoggableActionKind.LOGGED_IN, event.player, event.player.location.blockCoordsString)
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun playerQuitEvent(event: PlayerQuitEvent) {
		Leer.logAction(event.player.location, LoggableActionKind.LOGGED_IN, event.player, event.player.location.blockCoordsString)
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun playerIgniteBlock(event: BlockIgniteEvent) {
		if (event.cause != BlockIgniteEvent.IgniteCause.FLINT_AND_STEEL) return
		val player = event.player ?: return

		Leer.logAction(event.block.location, LoggableActionKind.IGNITED, player, "${event.block.type.name} at ${event.block.location.blockCoordsString}")
	}
}
