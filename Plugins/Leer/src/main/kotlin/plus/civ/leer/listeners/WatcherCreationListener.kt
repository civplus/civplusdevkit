package plus.civ.leer.listeners

import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent
import plus.civ.leer.Leer
import vg.civcraft.mc.civmodcore.extensions.removeOne

internal object WatcherCreationListener: Listener {

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
	fun onEnderEyeThrow(event: PlayerInteractEvent) {
		if (event.item == null) return
		if (event.item.type != Material.EYE_OF_ENDER) return
		if (event.action != Action.RIGHT_CLICK_AIR && event.action != Action.RIGHT_CLICK_BLOCK) return

		val player = event.player
		val watcher = Leer.watchChunk(player)
		if (watcher != null) {
			player.inventory.removeOne(Material.EYE_OF_ENDER)
		}

		event.isCancelled = true
	}
}
