package plus.civ.leer.db

import org.bukkit.entity.Player
import plus.civ.layerafterlayer.model.Group
import plus.civ.leer.Config
import plus.civ.leer.Leer
import vg.civcraft.mc.civmodcore.dao.ManagedDatasource
import java.sql.Connection
import java.sql.SQLException

internal object DatabaseManager {
	private val database by lazy {
		val db = ManagedDatasource(Leer.instance,
				Config.dbUsername,
				Config.dbPassword,
				Config.dbHost,
				Config.dbPort,
				Config.dbName,
				5,
				6000,
				600000,
				7200000)

		val logs = """
            CREATE TABLE logs (
              id INT NOT NULL AUTO_INCREMENT,
              world varchar(36) NOT NULL,
              chunk_x int NOT NULL,
              chunk_z int NOT NULL,
              action varchar(36) NOT NULL,
			  culprit varchar(36) NOT NULL,
			  target varchar(100) NOT NULL,
			  time bigint,

              PRIMARY KEY (id)
            )
        """.trimIndent()

		val watchers = """
            CREATE TABLE watchers (
              id INT NOT NULL AUTO_INCREMENT,
              world varchar(36) NOT NULL,
              chunk_x int NOT NULL,
              chunk_z int NOT NULL,
			  group_id int NOT NULL,
			  created_time bigint,
			  
              PRIMARY KEY (id)
            )
        """.trimIndent()

		val playerSelectedGroups = """
            CREATE TABLE player_selected_groups (
              player varchar(36) NOT NULL,
			  group_id int NOT NULL,
			  
              PRIMARY KEY (player)
            )
        """.trimIndent()

		db.registerMigration(0, false, logs, watchers, playerSelectedGroups)
		db.updateDatabase()

		db
	}

	fun <T> useConnection(function: (connection: Connection) -> T): T {
		val connection = database.connection
		val ret = function(connection)
		connection.close()
		return ret
	}

	fun getPlayerSelectedGroup(player: Player): Group? {
		return useConnection { connection ->
			val statement = connection.prepareStatement("""
				SELECT group_id FROM player_selected_groups WHERE player=?
			""".trimIndent()
			)
			statement.setString(1, player.uniqueId.toString())
			val result = statement.executeQuery()
			return@useConnection if (!result.first()) {
				null
			} else {
				Group.get(result.getInt("group_id"))
			}
		}
	}

	fun setPlayerSelectedGroup(player: Player, group: Group) {
		useConnection { connection ->
			val statement = connection.prepareStatement("""
				INSERT IGNORE INTO player_selected_groups (player, group_id) VALUES (?, ?)
				ON DUPLICATE KEY UPDATE
				player = VALUES(player),
				group_id = VALUES(group_id)
			""".trimIndent())
			statement.setString(1, player.uniqueId.toString())
			statement.setInt(2, group.id)
			statement.executeUpdate()
		}
	}
}
