package plus.civ.leer.db.model

import org.bukkit.World
import plus.civ.layerafterlayer.model.Group
import plus.civ.leer.Config
import plus.civ.leer.db.DatabaseManager
import vg.civcraft.mc.civmodcore.extensions.millisToMinutes
import vg.civcraft.mc.civmodcore.extensions.minutesToHours
import java.sql.Statement
import kotlin.collections.ArrayList

/**
 * @param group the Layer group that owns the watcher
 * @param watchingSince the time the watcher was created, as a unix timestamp in milliseconds
 * @param id the id in the database
 */
class Watcher private constructor(val group: Group, val watchingSince: Long, id: Int? = null) {
	var id: Int? = id
		private set

	val minutesToExpiration: Long
		get() = Config.WATCHER_EXPIRATION_TIME - (System.currentTimeMillis() - watchingSince).millisToMinutes
	val hoursToExpiration: Long
		get() = minutesToExpiration.minutesToHours
	val isExpired: Boolean
		get() = minutesToExpiration <= 0
	val isNotExpired: Boolean
		get() = !isExpired

	companion object {
		/**
		 * Creates a new Watcher and adds it to the database
		 */
		internal fun create(world: World, chunkX: Int, chunkZ: Int, group: Group, watchingSince: Long): Watcher {
			val watcher = Watcher(group, watchingSince)

			return DatabaseManager.useConnection { connection ->
				val statement = connection.prepareStatement(
					"""
						INSERT INTO watchers	(world,	chunk_x,	chunk_z,	group_id,	created_time)
						VALUES             		(?,		?,			?,			?,		?)
					""".trimIndent(), Statement.RETURN_GENERATED_KEYS
				)
				statement.setString(1, world.name)
				statement.setInt(2, chunkX)
				statement.setInt(3, chunkZ)
				statement.setInt(4, group.id)
				statement.setLong(5, watchingSince)

				statement.executeUpdate()
				val result = statement.generatedKeys
				result.first()
				watcher.id = result.getInt(1)

				return@useConnection watcher
			}
		}

		internal fun getAllInChunk(world: World, chunkX: Int, chunkZ: Int): List<Watcher> {
			return DatabaseManager.useConnection { connection ->
				val statement = connection.prepareStatement(
					"""
						SELECT * FROM watchers WHERE world=? AND chunk_x=? AND chunk_z=?
					""".trimIndent()
				)
				statement.setString(1, world.name)
				statement.setInt(2, chunkX)
				statement.setInt(3, chunkZ)

				val result = statement.executeQuery()
				val ret = ArrayList<Watcher>()
				val expirees = ArrayList<Int>()
				while (result.next()) {
					// TODO this is definitely not a safe !!. There's a bug here
					// the bug is when the group has since been deleted.
					val group = Group.get(result.getInt("group_id"))!!
					val watchingSince = result.getLong("created_time")
					val id = result.getInt("id")
					val watcher = Watcher(group, watchingSince, id)
					if (watcher.isExpired) {
						expirees.add(id)
					} else {
						ret.add(watcher)
					}
				}
				expirees.forEach {
					val statement = connection.prepareStatement(
						"""
						DELETE FROM watchers WHERE id=?
					""".trimIndent()
					)
					statement.setInt(1, it)
					statement.executeUpdate()
				}
				return@useConnection ret
			}
		}
	}
}
