package plus.civ.leer.db.model

enum class LoggableActionKind(val logStringFormat: String) {
	ENTERED_CHUNK("entered chunk at"),
	PLACED_BLOCK("placed block"),
	BROKE_BLOCK("broke block"),
	KILLED_PLAYER("killed player"),
	KILLED_MOB("killed mob"),
	DESTROYED_VEHICLE("destroyed vehicle"),
	ENTERED_VEHICLE("entered vehicle"),
	EXITED_VEHICLE("exited vehicle"),
	MOUNTED_ENTITY("mounted entity"),
	UNMOUNTED_ENTITY("unmounted entity"),
	FILLED_BUCKET("filled bucket"),
	EMPTIED_BUCKET("emptied bucket"),
	OPENED_CONTAINER("opened container"),
	LOGGED_IN("logged in"),
	LOGGED_OUT("logged out"),
	IGNITED("ignited block"),
}
