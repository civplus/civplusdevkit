package plus.civ.leer

import org.bukkit.ChatColor
import org.bukkit.Location
import org.bukkit.World
import org.bukkit.entity.Player
import org.bukkit.potion.PotionEffectType
import plus.civ.layerafterlayer.model.Group
import plus.civ.leer.db.DatabaseManager
import plus.civ.leer.db.model.LeerLog
import plus.civ.leer.db.model.LoggableActionKind
import plus.civ.leer.db.model.Watcher
import de.myzelyam.api.vanish.VanishAPI
import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PluginInfo

@PluginInfo(
	name = "Leer",
	author = "sepia",
	description = "Leer allows players to record the activity of other players within chunks that they've claimed",
	depends = ["CivModCore", "NameLayer"],
	softDepends = ["SuperVanish"],
)
class Leer: KotlinPlugin() {

	companion object {
		private var _instance: Leer? = null
		val instance: Leer
			get() = _instance!!

		fun logAction(world: World, chunkX: Int, chunkZ: Int, action: LoggableActionKind, culprit: Player, target: String, time: Long = System.currentTimeMillis()): LeerLog? {
			val allWatchers = Watcher.getAllInChunk(world, chunkX, chunkZ)
			val enemyWatchers = allWatchers.filter{ !it.group.includesMember(culprit) }

			val culpritIsVanished = if (instance.server.pluginManager.isPluginEnabled("SuperVanish")) {
				VanishAPI.isInvisible(culprit)
			} else {
				false
			}
			val culpritIsInvisible = culprit.hasPotionEffect(PotionEffectType.INVISIBILITY) || culpritIsVanished

			var log: LeerLog? = null
			if (enemyWatchers.isNotEmpty()) {
				log = LeerLog.create(world, chunkX, chunkZ, action, culprit.uniqueId, target, time)
			}
			if (log != null && !culpritIsInvisible) {
				// alert all online watchers
				enemyWatchers.forEach{ watcher -> watcher.group.allOnlinePlayers.forEach { player -> player.sendMessage(ChatColor.YELLOW.toString() + log.alertMessage) } }
			}
			return log
		}

		fun logAction(loc: Location, action: LoggableActionKind, culprit: Player, target: String): LeerLog? {
			return logAction(loc.world, loc.chunk.x, loc.chunk.z, action, culprit, target, System.currentTimeMillis())
		}

		fun watchChunk(player: Player): Watcher? {
			val c = ChatColor.YELLOW.toString()
			val e = ChatColor.RED.toString()
			val h = ChatColor.AQUA.toString()
			val g = ChatColor.GRAY.toString()

			val group = DatabaseManager.getPlayerSelectedGroup(player)
			if (group == null) {
				player.sendMessage("${e}You don't have a group selected. Use /leer setgroup [group]")
				return null
			} else if (!group.includesMember(player)) {
				player.sendMessage("${e}You don't have permission to watch chunks on the group ${c}${group.name}${e}. Change your selected group with ${g}/leer setgroup [group]${e}.")
				return null
			} else {
				player.sendMessage("${c}Now watching this chunk on the group ${h}${group.name}.")
				val world = player.location.world
				val cx = player.location.chunk.x
				val cz = player.location.chunk.z
				return watchChunk(world, cx, cz, group)
			}
		}

		fun watchChunk(world: World, chunkX: Int, chunkY: Int, group: Group): Watcher {
			return Watcher.create(world, chunkX, chunkY, group, System.currentTimeMillis())
		}
	}

	override fun onEnable() {
		_instance = this
		super.onEnable()
	}
}
