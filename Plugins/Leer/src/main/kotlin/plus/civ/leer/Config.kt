package plus.civ.leer

internal object Config {
	const val DAYS_TO_MINUTES = 24 * 60
	// Time, in minutes, that it takes for a watcher to expire
	const val WATCHER_EXPIRATION_TIME = 14 * DAYS_TO_MINUTES

	val database by lazy {
		Leer.instance.config.getConfigurationSection("database")!!
	}
	val dbUsername by lazy { database.getString("username")!! }
	val dbPassword by lazy { database.getString("password")!! }
	val dbHost by lazy { database.getString("host")!! }
	val dbPort by lazy { database.getInt("port") }
	val dbName by lazy { database.getString("dbName")!! }
}
