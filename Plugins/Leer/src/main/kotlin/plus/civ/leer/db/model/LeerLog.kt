package plus.civ.leer.db.model

import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.World
import plus.civ.leer.db.DatabaseManager
import java.sql.Statement
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * A data class representing a logged player action
 * @param action The kind of action
 * @param culprit The player who conducted the action
 * @param target A string with any added information about the target of the action (this will be displayed to users verbatim)
 * @param time The time that the action happened, as a unix timestamp in milliseconds
 */
class LeerLog private constructor(val action: LoggableActionKind, val culprit: UUID, val target: String, val time: Long = System.currentTimeMillis(), id: Int? = null) {
	// I can't do a private setting in a field inside the constructor of a data class and I want to kill the retards at JetBrains who have been arguing for 3 years about what the syntax should be for doing that
	var id: Int? = id
		private set

	val alertMessage: String
		get() {
			val culpritName = Bukkit.getServer().getOfflinePlayer(culprit).name
			return "$culpritName ${action.logStringFormat} $target"
		}
	val alertMessageTimestamped: String
		get() {
			val dateFormatter = SimpleDateFormat("dd-M HH:mm:ss")
			dateFormatter.timeZone = TimeZone.getTimeZone("UTC")
			val displayableTime = dateFormatter.format(Date(time))
			return "$displayableTime    $alertMessage"
		}

	companion object {
		/**
		 * Creates a new LeerLog and adds it to the database
		 */
		internal fun create(world: World, chunkX: Int, chunkZ: Int, action: LoggableActionKind, culprit: UUID, target: String, time: Long = System.currentTimeMillis()): LeerLog {
			val log = LeerLog(action, culprit, target, time)

			return DatabaseManager.useConnection { connection ->
				val statement = connection.prepareStatement(
					"""
						INSERT INTO logs (world, 	chunk_x, 	chunk_z, 	action, 	culprit, 	target, 	time)
						VALUES             (?,		?,			?,			?,				?,			?,			?)
					""".trimIndent(), Statement.RETURN_GENERATED_KEYS
				)
				statement.setString(1, world.name)
				statement.setInt(2, chunkX)
				statement.setInt(3, chunkZ)
				statement.setString(4, action.toString())
				statement.setString(5, culprit.toString())
				statement.setString(6, target)
				statement.setLong(7, time)

				statement.executeUpdate()
				val result = statement.generatedKeys
				result.first()
				log.id = result.getInt(1)

				return@useConnection log
			}
		}

		internal fun getAllInChunk(world: World, chunkX: Int, chunkZ: Int): List<LeerLog> {
			return DatabaseManager.useConnection { connection ->
				val statement = connection.prepareStatement(
					"""
						SELECT * FROM logs WHERE world=? AND chunk_x=? AND chunk_z=?
					""".trimIndent()
				)
				statement.setString(1, world.name)
				statement.setInt(2, chunkX)
				statement.setInt(3, chunkZ)

				val result = statement.executeQuery()
				val ret = ArrayList<LeerLog>()
				while (result.next()) {
					val action = LoggableActionKind.valueOf(result.getString("action"))
					val culprit = UUID.fromString(result.getString("culprit"))
					val target = result.getString("target")
					val time = result.getLong("time")
					val id = result.getInt("id")
					val log = LeerLog(action, culprit, target, time, id)
					ret.add(log)
				}
				return@useConnection ret
			}
		}
	}
}
