package plus.civ.timeofday

import org.bukkit.World
import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PluginInfo
import plus.civ.timeofday.timers.TimeTimer

@PluginInfo(
		name = "TimeOfDay",
		description = "Alters the length of day and night time",
		author = "Amelorate",
		depends = ["CivModCore"],
)
class TimeOfDay: KotlinPlugin() {
	companion object {
		private var instanceStorage: TimeOfDay? = null
		val instance: TimeOfDay
			get() = instanceStorage!!
	}

	var dayTimer: TimeTimer? = null
	var nightTimer: TimeTimer? = null

	override fun onEnable() {
		super.onEnable()
		instanceStorage = this

		dayTimer = TimeTimer(server.getWorld("world"), 1, World::isDayTime)
		nightTimer = TimeTimer(server.getWorld("world"), 1, World::isNightTime)
		dayTimer!!.runEvery(Config.defaultDayMultiplier)
		nightTimer!!.runEvery(Config.defaultNightMultiplier)

		server.getWorld("world").setGameRuleValue("doDaylightCycle", "false")
	}
}

val World.isDayTime: Boolean
	get() = time < 13000
val World.isNightTime: Boolean
    get() = !isDayTime
