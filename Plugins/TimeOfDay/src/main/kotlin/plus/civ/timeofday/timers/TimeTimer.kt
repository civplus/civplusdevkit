package plus.civ.timeofday.timers

import org.bukkit.World
import org.bukkit.plugin.Plugin
import org.bukkit.scheduler.BukkitRunnable
import org.bukkit.scheduler.BukkitTask
import plus.civ.timeofday.TimeOfDay
import java.lang.IllegalStateException

class TimeTimer(val world: World, var skipTicks: Long, val shouldRun: (World) -> Boolean): BukkitRunnable() {
	/**
	 * The number of ticks in between each time skipTicks is added to the world's time
	 */
	var runningEvery: Long? = null

	val dayLengthMultiplier: Double?
		get() {
			return runningEvery?.toDouble()?.div(skipTicks.toDouble())
		}

	override fun run() {
		if (shouldRun(world)) {
			world.fullTime += skipTicks
		}
	}

	override fun cancel() {
		super.cancel()
		runningEvery = null
	}

	override fun runTaskTimer(plugin: Plugin?, delay: Long, period: Long): BukkitTask {
		runningEvery = period
		return super.runTaskTimer(plugin, delay, period)
	}

	fun runEvery(ticks: Long) {
		try {
			cancel()
		} catch (e: IllegalStateException) { }

		runTaskTimer(TimeOfDay.instance, 0, ticks)
	}
}
