package plus.civ.timeofday

/**
 * Config values for the main server. Shards should config the plugin by changing TimeOfDat.day/nightTimer's variables
 */
object Config {
	val defaultDayMultiplier: Long = 12 // two hour days
	val defaultNightMultiplier: Long = 3 // 30 minute nights
}
