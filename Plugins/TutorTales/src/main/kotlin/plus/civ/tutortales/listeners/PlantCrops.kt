package plus.civ.tutortales.listeners

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.BookMeta
import plus.civ.ivy.gardenersGrimoireItem
import plus.civ.tutortales.TutorialTopic
import vg.civcraft.mc.civmodcore.extensions.giveItemOrDropOnGround
import vg.civcraft.mc.civmodcore.extensions.writeNBTTag

object PlantCrops: Listener {
	class IvyTutorial: TutorialTopic {
		override val book: ItemStack
			get() = run {
				val result = ItemStack(Material.WRITTEN_BOOK)
				val meta = result.itemMeta as BookMeta

				meta.author = "civplus"
				meta.title = "Civ+ Crops & Trees"

				meta.addPage("""
					${ChatColor.UNDERLINE}${ChatColor.BOLD}Slow Crop Growth${ChatColor.RESET}

					On Civ+, crops and trees grow at a slower rate than in vanilla Minecraft. However, they will still grow even when you are offline. 
				""".trimIndent())
				meta.addPage("Sugar cane and cactus are the only exceptions and will grow at the normal speed. " +
					"The growth rate of crops is affected by the biome they are in and whether or not they are exposed to sunlight.")

				meta.addPage("""
					${ChatColor.UNDERLINE}${ChatColor.BOLD}Checking Crop Growth Rate${ChatColor.RESET}

					To check the growth rate of a crop, you can use a Gardener's Grimoire. This item is crafted with a book, a carrot, and lapis lazuli. 
				""".trimIndent())
				meta.addPage("Left-clicking on a crop with the Grimoire will display the growth rate of that specific crop.")

				meta.addPage("""
					${ChatColor.UNDERLINE}${ChatColor.BOLD}Biome Impact on Crop Growth${ChatColor.RESET}

					The biome in which a crop is planted can also affect its growth rate. For example, wheat planted in a Birch Forest will grow faster than wheat planted in a Desert.
				""".trimIndent())
				meta.addPage("Make sure to consider the biome when planting your crops to ensure they grow at the optimal rate.")

				meta.addPage("""
					${ChatColor.UNDERLINE}${ChatColor.BOLD}Sunlight Impact on Crop Growth${ChatColor.RESET}

					Exposure to sunlight also plays a role in crop growth. Crops that are fully exposed to sunlight will grow faster than those that are partially shaded or not exposed to sunlight at all. 
				""".trimIndent())
				meta.addPage("Make sure to place your crops in a location that receives plenty of sunlight for optimal growth.")

				meta.addPage("""
					${ChatColor.UNDERLINE}${ChatColor.BOLD}Rare Ingredients${ChatColor.RESET}

					Occasionally, when you harvest a crop, you will receive a Rare Ingredient. These ingredients can be used to craft alcoholic beverages in a Brewing Stand. 
				""".trimIndent())
				meta.addPage("Keep an eye out for these rare items as they can be quite valuable in the brewing process.")

				result.itemMeta = meta
				result
			}

		override val message: String
			get() = "${ChatColor.BLUE}Civ+ changes crops to grow slowly, but they grow while you're offline. For more info, read this book."

		override fun sideEffect(player: Player) {
			player.giveItemOrDropOnGround(gardenersGrimoireItem)
		}
	}

	val seedItems = setOf(
		Material.SEEDS,
		Material.PUMPKIN_SEEDS,
		Material.MELON_SEEDS,
		Material.CARROT_ITEM,
		Material.POTATO_ITEM,
		Material.CACTUS,
		Material.SUGAR_CANE,
		//Material.SAPLING,
		// A player is likely to replant the first tree they cut down, but they probably don't need to know about crop
		// growth yet or why their tree is taking so long to regrow.
	)

	@EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
	fun plantCrop(event: BlockPlaceEvent) {
		if (!seedItems.contains(event.itemInHand.type)) return

		IvyTutorial().giveToPlayer(event.player)
	}
}
