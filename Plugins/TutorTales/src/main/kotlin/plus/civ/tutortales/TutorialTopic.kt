package plus.civ.tutortales

import org.bukkit.Sound
import com.fasterxml.jackson.annotation.JsonIgnore
import org.bukkit.ChatColor
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.BookMeta
import plus.civ.whatisanocttree.player.PlayerData.Companion.addPlayerData
import plus.civ.whatisanocttree.player.PlayerData.Companion.getPlayerData
import vg.civcraft.mc.civmodcore.extensions.giveItemOrDropOnGround
import java.time.LocalDate

interface TutorialTopic {
	/**
	 * A book that is given to the player when they are given the tutorial for the first time
	 */
	@get:JsonIgnore
	val book: ItemStack

	/**
	 * A message that's sent to the player when they are given the tutorial for the first time
	 */
	@get:JsonIgnore
	val message: String

	/**
	 * A function that's ran when the player is given the tutorial for the first time
	 */
	fun sideEffect(player: Player) {}

	/**
	 * If the player hasn't gotten the tutorial before,
	 * sends them the message, plays a sound-effect, gives them a copy of the book, and then runs sideEffect(player)
	 */
	fun giveToPlayer(player: Player) {
		val clazz = this.javaClass
		val thisTutorial = player.getPlayerData()?.data?.firstOrNull { clazz.isInstance(it) }
		if (thisTutorial != null) {
			return
		}

		// timestamp the book
		val book = this.book.clone()
		val bookMeta = book.itemMeta
		if (bookMeta is BookMeta) {
			bookMeta.addPage("\n\n\n\n\n\nThis tutorial was given to " +
				"${ChatColor.ITALIC}${player.displayName}${ChatColor.RESET} on " +
				"${ChatColor.ITALIC}${LocalDate.now()}${ChatColor.RESET}")
		}
		book.itemMeta = bookMeta

		// give the player the tutorial
		player.sendMessage(this.message)
		player.giveItemOrDropOnGround(book)
		player.playSound(player.location, Sound.ORB_PICKUP, 1.0f, 1.0f)
		this.sideEffect(player)

		player.addPlayerData(this)
	}
}
