package plus.civ.tutortales.listeners

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.BookMeta
import plus.civ.tutortales.TutorialTopic

object BastionTutorialGiver: Listener {
	class BastionTutorial : TutorialTopic {
		override val book: ItemStack = run {
			val result = ItemStack(Material.WRITTEN_BOOK)
			val meta = result.itemMeta as BookMeta

			meta.title = "Security Through Durity"
			meta.author = "civplus"

			meta.addPage("\"Security Through Durity\"\n" +
				"\n" +
				"That's the Bastion's motto. With a Bastion, you are afforded the highest level of protection.\n" +
				"\n" +
				"Within a Bastion's 10 block radius, players outside your group can't place blocks or use ender pearls.")
			meta.addPage("A Bastion's protective field extends infinitely upwards from the block," +
				" so nothing below a Bastion will be protected.\n" +
				"\n" +
				"A Bastion can be damaged by trying to place blocks within its radius, trying to pearl into its radius," +
				" or breaking the Bastion block itself.")
			meta.addPage("")
			meta.addPage("   This tutorial was\n      written by:\n\n\n\n\n\n\n\n\n\n\n         ${ChatColor.ITALIC}Parker")

			result.itemMeta = meta

			result
		}

		override val message = "${ChatColor.BLUE}Bastions can protect your property to the highest degree. " +
			"For more info, read this book."
	}

	@EventHandler
	fun placeObsidian(event: BlockPlaceEvent) {
		if (event.itemInHand.type != Material.OBSIDIAN) return

		BastionTutorial().giveToPlayer(event.player)
	}
}
