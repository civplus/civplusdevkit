package plus.civ.tutortales.listeners

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.entity.CreatureSpawnEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.BookMeta
import plus.civ.ivy.shepherdsGrimoireItem
import plus.civ.tutortales.TutorialTopic
import vg.civcraft.mc.civmodcore.extensions.giveItemOrDropOnGround
import vg.civcraft.mc.civmodcore.extensions.writeNBTTag

object BreedAnimal: Listener {
	class AnimalBreedingTutorial: TutorialTopic {
		override val book: ItemStack
			get() = run {
				val result = ItemStack(Material.WRITTEN_BOOK)
				val meta = result.itemMeta as BookMeta

				meta.author = "civplus"
				meta.title = "Civ+ Baby Animals Info"

				meta.addPage("On Civ+, baby animals grow slowly. " +
					"The speed is based on several factors that combine together to create the animal's happiness percentage.")
				meta.addPage( "In this tutorial, we will go over how to ensure your animals are happy" +
					" and how to use the Sheppard's Grimoire to check their happiness levels.")

				meta.addPage("${ChatColor.BOLD}${ChatColor.UNDERLINE}Animal' Happiness${ChatColor.RESET}\n\n" +
					"The happiness of an animal is determined by several factors, including the block they are standing on, " +
					"the number of animals around them, and whether or not they have a \"friend\" animal nearby.")
				meta.addPage("Each type of animal has its own preferred block and friend animal. " +
					"For example, chickens are friends with Mooshrooms, they like standing on Hay Bales," +
					" and their preferred biomes are Forests, Plains, and Extreme Hills.")
				meta.addPage("To ensure your animals are happy, make sure they have plenty of space and are in a biome they prefer. " +
					"Also, try to have a friend animal nearby, such as placing a Mooshroom next to your chickens.")

				meta.addPage("${ChatColor.BOLD}${ChatColor.UNDERLINE}Sheppard's Grimoire${ChatColor.RESET}\n\n" +
					"To check an animal's current happiness level and see what they prefer, left-click them with a Sheppard's Grimoire. " +
					"This item is crafted using a book, a steak, and a piece of lapis lazuli.")
				meta.addPage("The Sheppard's Grimoire will give you detailed information on the animal's happiness and their preferred conditions.")

				meta.addPage("${ChatColor.BOLD}${ChatColor.UNDERLINE}Growing Up${ChatColor.RESET}\n\n" +
					"Animals may grow slowly even when happy, but they grow in real-time and continue to grow when offline. " +
					"Check their happiness levels regularly and be patient, as they will eventually grow.")

				meta.addPage("${ChatColor.BOLD}${ChatColor.UNDERLINE}Horse Breeding${ChatColor.RESET}\n\n" +
					"When breeding horses, a genome system is used to affect their stats using perlin noise. " +
					"Riding the horse will level up its stats up to a local maximum based on each horse's potential.")
				meta.addPage("Children will inherit random stats from each parent," +
					" and Mendelian traits such as health regeneration can be bred for.")

				meta.addPage("${ChatColor.BOLD}${ChatColor.UNDERLINE}Checking Horse Stats${ChatColor.RESET}\n\n" +
					"To view a horse's stats and some limited parts of the genome, use a Sheppard's Grimoire." +
					"Left-clicking a horse with the Sheppard's Grimoire will give you some information on the horse's stats, genome, and breeding potential.")

				meta.addPage("${ChatColor.BOLD}${ChatColor.UNDERLINE}Leveling Up Horse Stats${ChatColor.RESET}\n\n" +
					"The only way to level up a horse's stats is through riding the horse. " +
					"Keep in mind that each horse has its own \"peak\" for each stat, determined by perlin noise.")
				meta.addPage("This means that some horses may have higher potential for certain stats, while others may have lower potential.")

				result.itemMeta = meta
				result
			}

		override val message: String
			get() = "${ChatColor.BLUE}Civ+ changes baby animals to grow slowly, but they grow even when you're offline. " +
				"For more info including how to make them grow faster, read this book."

		override fun sideEffect(player: Player) {
			player.giveItemOrDropOnGround(shepherdsGrimoireItem)
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
	fun breedAnimal(event: CreatureSpawnEvent) {
		if (event.spawnReason != CreatureSpawnEvent.SpawnReason.BREEDING) return

		val closestPlayers = event.location.world.getNearbyEntities(event.location, 10.0, 10.0, 10.0).filterIsInstance<Player>()
		for (player in closestPlayers) {
			AnimalBreedingTutorial().giveToPlayer(player)
		}
	}
}
