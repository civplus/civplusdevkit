package plus.civ.tutortales.listeners

import org.bukkit.*
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.BookMeta
import plus.civ.tutortales.TutorTales
import plus.civ.tutortales.TutorialTopic
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.addChunkLevelData
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.getChunkLevelData
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.removeChunkLevelData
import plus.civ.whatisanocttree.event.ChunkDataLoadEvent
import vg.civcraft.mc.citadel.Citadel
import vg.civcraft.mc.citadel.events.ReinforcementDamageEvent
import vg.civcraft.mc.citadel.reinforcement.PlayerReinforcement
import java.util.*

object LeerTutorialGiver: Listener {
	class LeerTutorial: TutorialTopic {
		override val book: ItemStack = run {
			val result = ItemStack(Material.WRITTEN_BOOK)
			val meta = result.itemMeta as BookMeta

			meta.title = "Leering"
			meta.author = "civplus"

			meta.addPage("If you've ever been griefed or robbed, " +
				"you know what it feels like to have no one to hold responsible. " +
				"With Leer, now you can.")
			meta.addPage("By placing a reinforced \"snitch\", you get chat alerts whenever a player outside your group " +
				"does anything within the snitch radius (11 blocks).\n")
			meta.addPage("To craft a snitch, place either a noteblock or a jukebox in a crafting table with an eye of ender.")
			meta.addPage("A noteblock snitch only sends field entry, exit, and logout alerts. " +
				"A jukebox snitch logs and alerts all player activity within the radius, even when you are offline.\n" +
				"\n" +
				"To check a snitch's logs, stand near it and type /ja info. §0\n")
			meta.addPage("It can be hard to keep track of your snitches. " +
				"To name a snitch, stand near it and type /ja name [name].\n" +
				"\n" +
				"If a snitch is destroyed, its logs are destroyed too. " +
				"It's worth burying snitches so they aren't visible.")
			meta.addPage("To disguise a snitch further, place a snitch and a natural block in a crafting table. " +
				"It will now appear as that block, but retain snitch functionality.\n")
			meta.addPage("")
			meta.addPage("   This tutorial was\n      written by:\n\n\n\n\n\n\n\n\n\n\n         ${ChatColor.ITALIC}J_Soms")

			result.itemMeta = meta
			result
		}

		override val message = "${ChatColor.BLUE}You can protect your items by placing a Snitch near them." +
			" For more info, read this book."
	}

	@EventHandler
	fun placeSnitch(event: BlockPlaceEvent) {
		if (event.itemInHand.type != Material.NOTE_BLOCK) return
		// also give them the book if they try to place a civ2.0-based snitch, to try to right their mistake

		LeerTutorial().giveToPlayer(event.player)
	}

	val valuableBlocks = setOf(Material.CHEST, Material.TRAPPED_CHEST)
	data class BrokenValuableBlockBreadCrumb(val owner: UUID)

	@EventHandler(priority = EventPriority.MONITOR)
	fun enemyBreaksChest(event: ReinforcementDamageEvent) {
		if (event.block.type !in valuableBlocks) return

		val rein = event.reinforcement
		if (rein !is PlayerReinforcement) return
		val group = rein.group ?: return
		val primaryOwner = group.owner ?: return

		Bukkit.getScheduler().runTask(TutorTales.instance) {
			if (Citadel.getReinforcementManager().getReinforcement(event.block.location) != null) return@runTask

			if (Bukkit.getOfflinePlayer(primaryOwner).isOnline) {
				LeerTutorial().giveToPlayer(Bukkit.getPlayer(primaryOwner))
			} else {
				if (event.block.location
						.getChunkLevelData<BrokenValuableBlockBreadCrumb>()
						.any { it.owner == primaryOwner }) return@runTask

				event.block.location.addChunkLevelData(BrokenValuableBlockBreadCrumb(primaryOwner))
			}
		}

	}

	@EventHandler
	fun loadChunkWithBreadCrumb(event: ChunkDataLoadEvent) {
		processBreadcrumbsForChunk(event.chunk)
	}

	@EventHandler
	fun loginCheckForBreadCrumbOnLoadedChunks(event: PlayerJoinEvent) {
		Bukkit.getScheduler().runTaskLater(TutorTales.instance, {
			for (chunk in event.player.world.loadedChunks) {
				processBreadcrumbsForChunk(chunk)
			}
		}, (20L * 20L)) // run 20 seconds after login
	}

	fun processBreadcrumbsForChunk(chunk: Chunk) {
		val chunkLocation = Location(chunk.world, (chunk.x * 16).toDouble(), 0.0, (chunk.z * 16).toDouble())
		val breadcrumbs = chunkLocation
			.getChunkLevelData<BrokenValuableBlockBreadCrumb>()
			.filter { Bukkit.getOfflinePlayer(it.owner).isOnline }

		if (breadcrumbs.isEmpty()) return

		for (breadcrumb in breadcrumbs) {
			chunkLocation.removeChunkLevelData(breadcrumb)
			LeerTutorial().giveToPlayer(Bukkit.getPlayer(breadcrumb.owner))
		}

	}
}
