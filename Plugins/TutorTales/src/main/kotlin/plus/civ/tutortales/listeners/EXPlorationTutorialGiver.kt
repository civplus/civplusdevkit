package plus.civ.tutortales.listeners

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerItemHeldEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.BookMeta
import plus.civ.tutortales.TutorialTopic

object EXPlorationTutorialGiver: Listener {
	class EXPlorationTutorial: TutorialTopic {
		override val book: ItemStack = run {
			val result = ItemStack(Material.WRITTEN_BOOK)
			val meta = result.itemMeta as BookMeta

			meta.title = "EXPloration"
			meta.author = "civplus"

			meta.addPage("In the world of Civ+, there are treasures to be found. " +
				"These will give you the ability to create powerful enchantments, " +
				"or may be sacrificed as an ingredient to create and store EXP.")
			meta.addPage("To seek treasure, you must use a compass or treasure map. " +
				"A treasure map comes from activities like farming, fighting, fishing and mining.")
			meta.addPage("Maps and compasses lead you to a unique location where you find a Relic. " +
				"This Relic can be used to imbue it's enchantment onto an item of your choosing, " +
				"or sacrificed in the crafting recipe for EXP.")
			meta.addPage("Using a compass, click the ground to discern the location of a nearby Relic. " +
				"Right clicking again will tell you your distance from the Relic.")
			meta.addPage("You must wait an hour to reset this location. " +
				"When close, you will see the Relic's exact position by the aura it emits. " +
				"Dig here with a shovel to extract it.")
			meta.addPage("A more advanced treasure hunter may choose to follow treasure maps. " +
				"Maps will be marked by the specific type of Relic they point to. " +
				"X marks the spot.\n" +
				"\n" +
				"To transfer a Relic's enchantment onto another item, combine them in a crafting menu with 5 emeralds.")
			meta.addPage("")
			meta.addPage("   This tutorial was\n      written by:\n\n\n\n\n\n\n\n\n\n\n         ${ChatColor.ITALIC}Parker")

			result.itemMeta = meta
			result
		}

		override val message = "${ChatColor.BLUE}You can follow maps or your compass to find a Relic. " +
			"For more info, read this book."
	}

	@EventHandler
	fun holdCompass(event: PlayerItemHeldEvent) {
		if (event.player.inventory.getItem(event.newSlot)?.type != Material.COMPASS) return

		EXPlorationTutorial().giveToPlayer(event.player)
	}
}
