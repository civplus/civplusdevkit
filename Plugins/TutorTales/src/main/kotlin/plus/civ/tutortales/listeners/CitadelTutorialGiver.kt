package plus.civ.tutortales.listeners

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.BookMeta
import plus.civ.tutortales.TutorialTopic
import vg.civcraft.mc.civmodcore.extensions.writeNBTTag

object CitadelTutorialGiver: Listener {
	class CitadelTutorial: TutorialTopic {
		override val book: ItemStack = run {
			val result = ItemStack(Material.WRITTEN_BOOK)
			val meta = result.itemMeta as BookMeta

			meta.title = "Citadel"
			meta.author = "civplus"

			meta.addPage("Citadel allows you to reinforce your blocks." +
				" You use stone, iron or diamond – and you will protect a block with this material.")
			meta.addPage("If a block is reinforced, it will take a set number of breaks to be destroyed." +
				" If the block is a container, you have to break it the same amount in order to open it.")
			meta.addPage("How do you reinforce a block?" +
				" Hold a reinforcment material in your hand, type /ctr [group] and then," +
				" once the block is clicked, it will be reinforced to the group you type." +
				" Groups are part of another important plugin NameLayer, and can be created using /nlcg [Group].")
			meta.addPage("This is good for reinforcing a block, " +
				"however, when making a large building it becomes tedious. " +
				"This is where /ctf [group] comes in use.")
			meta.addPage("Run the command with a reinforcement material in your hand. " +
				"This command automatically reinforces every block you place, " +
				"provided you have the material to reinforce it.")
			meta.addPage("Type /cto to exit reinforcement mode.\n" +
				"You can check a block is reinforced by typing /cti and clicking, " +
				"this gives you information on how many breaks the block has left.")
			meta.addPage("Using /ctb, you can destroy blocks which you have reinforced and receive " +
				"back the block and reinforcement material.")
			meta.addPage("")
			meta.addPage("   This tutorial was\n      written by:\n\n\n\n\n\n\n\n\n\n\n         ${ChatColor.ITALIC}Pirater")

			result.itemMeta = meta
			result
		}

		override val message = "${ChatColor.BLUE}Civ+ has a unique approach to protecting your property called Citadel." +
			" For more info, read this book."
	}

	val noTutorialBlocks = setOf(Material.STONE, Material.DIRT, Material.GRASS, Material.SAND)

	@EventHandler(ignoreCancelled = true)
	fun placeDoor(event: BlockPlaceEvent) {
		if (!event.blockPlaced.type.name.contains("DOOR")) return
		if (event.blockPlaced.location.add(0.0, -1.0, 0.0).block.type in noTutorialBlocks) return

		CitadelTutorial().giveToPlayer(event.player)
	}
}
