package plus.civ.tutortales

import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PluginInfo

@PluginInfo(
		name = "TutorTales",
		description = "Adds tutorial books when you do certain actions for the first time",
		author = "Amelorate",
		depends = ["CivModCore", "WhatIsAnOcttree", "Ivy", "HiddenOre", "CivChat2"],
)
class TutorTales: KotlinPlugin() {
	companion object {
		private var instanceStorage: TutorTales? = null
		val instance: TutorTales
			get() = instanceStorage!!

	}

	override fun onEnable() {
		super.onEnable()
		instanceStorage = this
	}
}
