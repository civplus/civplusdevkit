package plus.civ.tutortales.listeners

import com.github.devotedmc.hiddenore.event.HiddenOreEvent
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.BookMeta
import plus.civ.tutortales.TutorialTopic
import vg.civcraft.mc.civmodcore.extensions.writeNBTTag


object HiddenOreDropTutorialGiver: Listener {
	class HiddenOreDropTutorial: TutorialTopic {
		override val book: ItemStack
			get() = run {
				val result = ItemStack(Material.WRITTEN_BOOK)
				val meta = result.itemMeta as BookMeta

				meta.addPage("In Civ+ ores and valuable items are hidden within stone and other natural blocks. " +
					"Mine some stone to see what you can find! " +
					"Different levels of tools, and the Y level you’re mining at will affect what can possibly drop.")
				meta.addPage("So make sure you pay attention to where you’re mining, and that you’re using the appropriate tool!\n" +
					"\n" +
					"Q: What level can I find diamonds?\n" +
					"A: Y=16 or below\n" +
					"\n" +
					"Q: What pickaxe is required for good drops?\n" +
					"A: At least an iron pick")
				meta.addPage("Q: Do Fortune enchants increase the drop rate from HiddenOre?\n" +
					"A: No, but Fortune functions normally on ore that you have received as a drop.\n" +
					"\n" +
					"Q: Will Silk Touch activate HiddenOre drops?\n" +
					"A: Yes.")
				meta.addPage("")
				meta.addPage("   This tutorial was\n      written by:\n\n\n\n\n\n\n\n\n\n\n         ${ChatColor.ITALIC}J_Soms")

				meta.title = "Hidden Ores"
				meta.author = "civplus"

				result.itemMeta = meta
				result
			}
		override val message: String
			get() = "${ChatColor.BLUE}Ores on Civ+ drop randomly when you go mining, to prevent x-ray. For more info, read this book."
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
	fun hiddenOreDrop(event: HiddenOreEvent) {
		if (event.item.type == Material.MAP) return // avoid giving tutorials for treasure maps

		HiddenOreDropTutorial().giveToPlayer(event.player)
	}
}
