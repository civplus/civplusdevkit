package plus.civ.tutortales.listeners

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.BookMeta
import plus.civ.tutortales.TutorialTopic
import vg.civcraft.mc.civchat2.event.GlobalChatEvent
import vg.civcraft.mc.civmodcore.extensions.writeNBTTag

object CivChatTutorialGiver: Listener {
	class CivChatTutorial: TutorialTopic {
		override val book: ItemStack
			get() = run {
				val result = ItemStack(Material.WRITTEN_BOOK)
				val meta = result.itemMeta as BookMeta

				meta.title = "Chatting it up"
				meta.author = "civplus"

				meta.addPage("Civ+ has a global and group chat system. \n" +
					"By default, you are just speaking in your local area. \n" +
					"Want to speak to everyone around the world? Join the global channel with “/g !”.")
				meta.addPage("With “g” meaning group, and the “!” being that group (“!” is the global group that everyone is a part of by default).")
				meta.addPage("This system also works if you want to have a private conversation with just your friends or allies" +
					" by joining the group chat for any NameLayer group that you have made personally," +
					" or are a part of, assuming you have permission to access the chat.")
				meta.addPage("If I make a group called “Civ+”, I can speak in that group chat with “/g Civ+”.")
				meta.addPage("Want to message an individual privately? “/msg player” will start a chat with them. " +
					"Just make sure you leave the chat when you’re done speaking, so you don’t message them unintentionally.")
				meta.addPage("If I wish to exit a chat and speak in local, I can “/exit” or “/g” and will no longer be speaking in that chat.")
				meta.addPage("")
				meta.addPage("   This tutorial was\n      written by:\n\n\n\n\n\n\n\n\n\n\n         ${ChatColor.ITALIC}J_Soms")

				result.itemMeta = meta
				result
			}

		override val message: String
			get() = "${ChatColor.BLUE}Civ+ has a local/global chat system, and local is the default. For more info, read this book."
	}

	@EventHandler(priority = EventPriority.MONITOR)
	fun talkInLocalChat(event: GlobalChatEvent) {
		// actually only fires when they talk in local chat

		CivChatTutorial().giveToPlayer(event.player)
	}
}
