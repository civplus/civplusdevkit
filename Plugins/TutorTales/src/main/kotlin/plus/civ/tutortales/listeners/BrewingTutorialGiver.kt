package plus.civ.tutortales.listeners

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.inventory.InventoryOpenEvent
import org.bukkit.event.inventory.InventoryType
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.BookMeta
import plus.civ.tutortales.TutorialTopic
import vg.civcraft.mc.civmodcore.extensions.writeNBTTag

object BrewingTutorialGiver: Listener {
	class BrewingTutorial: TutorialTopic {
		override val book: ItemStack
			get() = run {
				val result = ItemStack(Material.WRITTEN_BOOK)
				val meta = result.itemMeta as BookMeta

				meta.title = "Brewing on the Top Shelf"
				meta.author = "civplus"

				meta.addPage("On Civ+, drinks can get pretty fancy. " +
					"Potion recipes remain the same as normal," +
					" however, adding more of a primary ingredient to a potion once it has been brewed will" +
					" increase the duration of the potion’s effect.")
				meta.addPage("More powerful effects can be discovered and created by brewing" +
					" alcoholic beverages from rare ingredients found by hand harvesting crops.")
				meta.addPage("First add one of these ingredients to a bottle of water in the brewing stand. This will give you a \"Basic Base\".")
				meta.addPage("Then add an ingredient in the brewing stand that is NOT one of the rare crop ingredients" +
					" (e.g Wheat, but this can be almost anything)" +
					" to the “Basic Base”, and you may have discovered something delicious or useful!")
				meta.addPage("However, usually you will just end up with a ruined brew.")
				meta.addPage("More powerful and desirable beverages may require an extra step. " +
					"Add another rare crop ingredient to a “Basic Base” in the brewing stand. " +
					"This will give you a “Complex Base”. " +
					"Finally add an ingredient in the same way as described previously.")
				meta.addPage("Example recipe:\n" +
					"Water + Pumpkin Seed Oil + Cocoa Nibs + Wheat = Beer")
				meta.addPage("Once you’ve made something, enjoy! " +
					"There are many very powerful effects to be found. " +
					"But don’t overindulge, you may find yourself too intoxicated to continue.")
				meta.addPage("If you’re not the experimental type, keep your eye on the sky. " +
					"Rare and secret recipes may be hidden within the contents of the next meteor. " +
					"And with so many possible combinations, this information can be extremely valuable.")
				meta.addPage("")
				meta.addPage("${ChatColor.BOLD}${ChatColor.UNDERLINE}Note From The Admins:${ChatColor.RESET}\n\n" +
					"You aren't really meant to brute-force the recipes by guessing ingredients. " +
					"Instead, you should look for secrets and surprises. " +
					"That's where you'll find the recipes you so desire.")
				meta.addPage("   This tutorial was\n      written by:\n\n\n\n\n\n\n\n\n\n\n         ${ChatColor.ITALIC}J_Soms")

				result.itemMeta = meta
				result
			}

		override val message: String
			get() = "${ChatColor.BLUE}Brewing is a bit different on Civ+. " +
				"You can add more primary ingredients to increase the duration, instead of using redstone. " +
				"For more info, read this book."
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
	fun clickBrewingStand(event: InventoryOpenEvent) {
		if (event.inventory.type != InventoryType.BREWING) return
		val player = event.player
		if (player !is Player) return

		BrewingTutorial().giveToPlayer(player)
	}
}
