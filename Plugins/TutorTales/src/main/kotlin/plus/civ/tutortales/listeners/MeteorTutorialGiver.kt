package plus.civ.tutortales.listeners

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerItemHeldEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.BookMeta
import plus.civ.tutortales.TutorialTopic

object MeteorTutorialGiver: Listener {
	class MeteorTutorial: TutorialTopic {
		override val book: ItemStack = run {
			val result = ItemStack(Material.WRITTEN_BOOK)
			val meta = result.itemMeta as BookMeta

			meta.title = "Meteoric Wealth"
			meta.author = "civplus"

			meta.addPage("Looks like you're starting to amass some wealth. " +
				"There are many ways to make a fortune here. " +
				"One of the best is to keep an eye to the sky.\n" +
				"\n" +
				"The surface of Civ+ is regularly pelted with meteors.")
			meta.addPage("When a meteor is going to land, its approximate location and ETA are announced to the entire server.\n" +
				"\n" +
				"A meteor's contents are often extremely valuable, but beware- each meteor is its own war zone.")
			meta.addPage("A meteor emits a damaging aura, making it difficult to crack one open especially when contested.\n" +
				"\n" +
				"The true value of a meteor comes from more than just its contents. " +
				"Many will visit a meteor for its magical properties instead.")
			meta.addPage("Meteors land weekly, shifting one day forward each time. " +
				"(One week it will land on Monday, the next week on Tuesday…)\n\n" +
				"You can see when a meteor will land next using /meteor.")
			meta.addPage("")
			meta.addPage("   This tutorial was\n      written by:\n\n\n\n\n\n\n\n\n\n\n         ${ChatColor.ITALIC}Parker")

			result.itemMeta = meta
			result
		}

		override val message = "${ChatColor.BLUE}Meteors will fall from the sky once a week. " +
			"For more info, read this book."
	}

	@EventHandler
	fun holdDiamondItem(event: PlayerItemHeldEvent) {
		if (!event.player.itemInHand.type.name.contains("DIAMOND_")) return

		MeteorTutorial().giveToPlayer(event.player)
	}
}
