package plus.civ.tutortales.listeners

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.inventory.InventoryType
import org.bukkit.inventory.AnvilInventory
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.BookMeta
import plus.civ.tutortales.TutorialTopic
import vg.civcraft.mc.civmodcore.extensions.mainInputItem
import vg.civcraft.mc.civmodcore.extensions.resultItem
import vg.civcraft.mc.civmodcore.extensions.writeNBTTag

object WordBankTutorialGiver: Listener {
	class WordBankTutorial: TutorialTopic {
		override val book: ItemStack = run {
			val result = ItemStack(Material.WRITTEN_BOOK)
			val meta = result.itemMeta as BookMeta

			meta.title = "WordBank"
			meta.author = "civplus"

			meta.addPage("Feel like personalizing your items?\n" +
				"\n" +
				"Wordbank can create a cryptographically unique, colored, randomized name for your items.")
			meta.addPage("Rename an item in an anvil to whatever you want as a wordbank seed, " +
				"then right click your renamed item twice on an enchanting table to " +
				"randomly generate a new name based on the seed you created. " +
				"Make sure you have some EXP bottles in your inventory.")
			meta.addPage("Seeds are 1:1 and are case sensitive. " +
				"For example, the seed 'test seed 1' will always output 'Pants Messy.'\n" +
				"\n" +
				"If you kill a player with an item like this, the resulting chat message will read\n" +
				"\"Player1 killed Player2 with Pants Messy\"")
			meta.addPage("")
			meta.addPage("   This tutorial was\n      written by:\n\n\n\n\n\n\n\n\n\n\n         ${ChatColor.ITALIC}Parker")

			result.itemMeta = meta
			result
		}

		override val message = "${ChatColor.BLUE}Civ+ has a fancy system for giving items unique colored names. " +
			"For more info, read this book."
	}

	@EventHandler
	fun anvilClick(event: InventoryClickEvent) {
		if (event.view.topInventory.type != InventoryType.ANVIL) return
		if (event.whoClicked !is Player) return

		val player = event.whoClicked as Player
		val anvilInventory = event.view.topInventory as AnvilInventory

		val resultItem = anvilInventory.resultItem
		val inputItem = anvilInventory.mainInputItem

		if (resultItem?.itemMeta?.displayName == inputItem?.itemMeta?.displayName) return

		if (event.rawSlot == 2) { // if the player clicked the result slot
			WordBankTutorial().giveToPlayer(player)
		}
	}
}
