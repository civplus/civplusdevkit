package plus.civ.stickers.listeners

import org.bukkit.*
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent
import plus.civ.stickers.StickerData
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.addBlockData
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.getBlockData

object Sticker: Listener {
	@EventHandler(ignoreCancelled = true)
	fun stickerApply(event: PlayerInteractEvent) {
		if (event.action != Action.RIGHT_CLICK_BLOCK) {
			return
		}

		val item = event.player.itemInHand

		if (item?.type != Material.INK_SACK) {
			return
		}

		val color = DyeColor.getByDyeData(item.durability.toByte())
		val text = item.itemMeta?.displayName ?: return

		if (text.contains(ChatColor.COLOR_CHAR)) return // <-- avoid applying custom items

		val data = StickerData(text, color)
		event.clickedBlock.location.addBlockData(data)

		item.amount -= 1
		event.player.itemInHand = item

		event.player.sendMessage("${ChatColor.GREEN}Applied a sticker! It says:")
		event.player.world.playSound(event.clickedBlock.location, Sound.SLIME_WALK2, 0.5f, 1.5f)
		event.player.world.playEffect(event.clickedBlock.location, Effect.SLIME, 0)

		event.isCancelled = true
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
	fun stickerInteract(event: PlayerInteractEvent) {
		val stickers = event.clickedBlock?.location?.getBlockData<StickerData>() ?: return

		for (sticker in stickers) {
			event.player.sendMessage(sticker.renderedText)
		}
	}
}
