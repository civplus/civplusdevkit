package plus.civ.stickers

import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PluginInfo

@PluginInfo(
		name = "Stickers",
		description = "Adds stickers you can apply to blocks that show in chat when you interact with them",
		author = "Amelorate",
		depends = ["CivModCore", "WhatIsAnOcttree"],
		softDepends = ["MeansOfProduction"],
)
class Stickers: KotlinPlugin() {
	companion object {
		private var instanceStorage: Stickers? = null
		val instance: Stickers
			get() = instanceStorage!!
	}

	override fun onEnable() {
		super.onEnable()
		instanceStorage = this
	}
}
