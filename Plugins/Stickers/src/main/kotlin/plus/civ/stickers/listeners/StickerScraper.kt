package plus.civ.stickers.listeners

import org.bukkit.ChatColor
import org.bukkit.Effect
import org.bukkit.Material
import org.bukkit.Sound
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.BookMeta
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeCraftInfo
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe
import plus.civ.stickers.StickerData
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.getBlockData
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.removeBlockData
import vg.civcraft.mc.civmodcore.expression.*
import vg.civcraft.mc.civmodcore.extensions.giveItemOrDropOnGround
import vg.civcraft.mc.civmodcore.extensions.writeNBTTag

object StickerScraper: Listener, RecipeProvider {
	val scraper = NBTExpression()
		.id(Material.IRON_AXE)
		.itemName("${ChatColor.RESET}${ChatColor.AQUA}Sticker Scraper")
		.with("tag.stickerscraper", 1)
		.or(NBTExpression()
			.with("tag.stickerscraper", 1)
		)

	val tutorialBook = run {
		val book = ItemStack(Material.WRITTEN_BOOK)
		val meta = book.itemMeta
		if (meta is BookMeta) {
			meta.pages = mutableListOf(
					"""
					How to use Stickers
					-----------------

					You can craft a sticker by renaming any dye in an anvil. You can then stick a sticker to a block by right clicking with the renamed dye.

					Blocks can have multiple stickers.
					""".trimIndent(),
					"""
					The name of a sticker is displayed in chat when you left or right click on the block, or in the case of redstone ore and pressure plates, walk over the block.

					The chat message will be in the color of the sticker.
					""".trimIndent(),
					"""
					You can remove a sticker by right clicking the block with a Sticker Scraper.

					Stickers are removed in a first-in last-out order. In other words, the sticker that will be removed is the sticker that was most recently applied to the block.
					""".trimIndent(),
					"""
					If you break a block with a sticker on it, the item that drops will also have the sticker. Placing the item back as a block will retain the sticker that was originally placed upon the block.
					""".trimIndent()
			)
			meta.title = "How to use Stickers"
			meta.author = "civplus"
		}
		book.itemMeta = meta
		book.writeNBTTag("generation", 3)
		book
	}

	override fun register() {
		val ingredients = mutableMapOf(
			Pair('-', NBTExpression().id(Material.IRON_INGOT).amountGTE(1)),
			Pair('|', NBTExpression().id(Material.STICK).amountGTE(1))
		)

		val shape = """
			-----
			_---_
			__|__
			__|__
		""".trimIndent()

		val recipe = object : ShapedCraftingRecipe(ingredients, shape, "sticker_scraper", scraper.solveToItemStack(), nothingChar = '_') {
			override fun sideEffect(info: RecipeCraftInfo): ItemStack? {
				val result = super.sideEffect(info)
				info.player.giveItemOrDropOnGround(tutorialBook)
				return result
			}
		}

		addRecipeBookRecipe(recipe, RecipeCategory.MACGUFFINS)
	}

	@EventHandler(ignoreCancelled = true)
	fun scrape(event: PlayerInteractEvent) {
		if (event.action != Action.RIGHT_CLICK_BLOCK) {
			return
		}

		if (event.player.itemInHand == null) {
			return
		}

		if (!scraper.matches(event.player.itemInHand)) {
			return
		}

		val stickers = event.clickedBlock.location.getBlockData<StickerData>()
		if (stickers.isEmpty()) {
			return
		}

		val stickerToRemove = stickers.last()
		event.clickedBlock.location.removeBlockData(stickerToRemove)

		event.player.sendMessage("${ChatColor.GREEN}You removed a sticker! It said: ${stickerToRemove.renderedText}")

		event.player.world.playSound(event.clickedBlock.location, Sound.DOOR_CLOSE, 0.5f, 10f)
		event.player.world.playEffect(event.clickedBlock.location, Effect.SLIME, 0)

		event.isCancelled = true
	}
}
