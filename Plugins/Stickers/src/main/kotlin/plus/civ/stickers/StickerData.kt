package plus.civ.stickers

import com.fasterxml.jackson.annotation.JsonIgnore
import org.bukkit.ChatColor
import org.bukkit.DyeColor
import plus.civ.whatisanocttree.chunk.tags.ItemSerializable
import vg.civcraft.mc.civmodcore.extensions.chatColor

data class StickerData(
    val text: String,
    val color: DyeColor,
): ItemSerializable {
	@get:JsonIgnore
	val renderedText: String = "${color.chatColor}${ChatColor.ITALIC}$text"
}
