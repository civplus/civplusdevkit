package vg.civcraft.mc.civchat2.commands;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import vg.civcraft.mc.civchat2.ChatStrings;
import vg.civcraft.mc.civchat2.CivChat2;
import vg.civcraft.mc.civchat2.database.CivChatDAO;
import vg.civcraft.mc.civmodcore.command.PlayerCommand;

public class Ignore implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String commandName, String[] args) {
		Player player = (Player) sender;
		Player ignoredPlayer = Bukkit.getServer().getPlayer(args [0]);
		if (ignoredPlayer == null) {
			player.sendMessage(ChatStrings.chatPlayerNotFound);
			return true;
		}
		if (player == ignoredPlayer) {
			player.sendMessage(ChatStrings.chatCantIgnoreSelf);
			return true;
		}
		player.sendMessage("You are now ignoring that player.");
		return true;
	}
}
