package plus.civ.manxomemeteors

import org.bukkit.scheduler.BukkitRunnable
import plus.civ.coast.Coast
import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PluginInfo
import plus.civ.vorpal.database.VorpalEnchant.Companion.vorpalsInside
import java.util.logging.Level

@PluginInfo(
	name = "ManxomeMeteors",
	author = "sepia",
	description = "Refuel Vorpal swords when players are standing in a meteor aura from Coast",
	depends = ["CivModCore", "Coast", "Vorpal"],
)
class ManxomeMeteors: KotlinPlugin() {
	companion object {
		const val REFUEL_TICK_TIME: Long = 20L * 10L // 10 seconds
		const val FUEL_AMOUNT_PER_REFUEL: Int = 180
	}

	override fun onEnable() {
		super.onEnable()
		runnable.runTaskTimer(this, REFUEL_TICK_TIME, REFUEL_TICK_TIME)
	}

	val runnable = object : BukkitRunnable() {
		override fun run() {
			this@ManxomeMeteors.server.onlinePlayers.filter{ Coast.instance.isPlayerInAura(it) }.forEach { player ->
				player.inventory.vorpalsInside.forEach{
					it.fuelHours += FUEL_AMOUNT_PER_REFUEL
					it.reevaluateItem()
					it.saveLater()
					this@ManxomeMeteors.logger.log(Level.INFO, "Refuelling a vorpal sword held by ${player.displayName}.")
				}
			}
		}
	}
}
