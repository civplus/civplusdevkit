package plus.civ.meansofproduction

import org.bukkit.ChatColor
import org.bukkit.Material
import vg.civcraft.mc.civmodcore.expression.NBTExpression
import vg.civcraft.mc.civmodcore.expression.damage
import vg.civcraft.mc.civmodcore.expression.id
import vg.civcraft.mc.civmodcore.expression.itemName

enum class RecipeCategory(val icon: NBTExpression) {
	EXP(NBTExpression()
			.id(Material.EXP_BOTTLE)
			.itemName("${ChatColor.GREEN}ExP")),
	PVP(NBTExpression()
			.id(Material.DIAMOND_SWORD)
			.itemName("${ChatColor.RED}PvP")),
	BUILDING(NBTExpression()
			.id(Material.BRICK)
			.itemName("${ChatColor.AQUA}Building Blocks")),
	RESOURCES(NBTExpression()
			.id(Material.CLAY_BRICK)
			.itemName("${ChatColor.AQUA}Resources & Materials")),
	MOBS(NBTExpression()
			.id(Material.MONSTER_EGG)
			.damage(5)
			.itemName("${ChatColor.DARK_GREEN}Monsters")),
	VANILLA_TWEAKS(NBTExpression()
			.id(Material.SADDLE)
			.itemName("${ChatColor.AQUA}Vanilla Tweaks")),
	VANILLA_UTILITY(NBTExpression()
			.id(Material.BUCKET)
			.itemName("${ChatColor.AQUA}Often Forgotten Vanilla Recipes")),
	MACGUFFINS(NBTExpression()
			.id(Material.LEATHER_LEGGINGS)
			.with("tag.display.color", 255)
			.itemName("${ChatColor.AQUA}Custom Items")),
}
