package plus.civ.meansofproduction.recipe

import org.bukkit.Material
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.CRAFTING_TABLE_SIZE
import plus.civ.meansofproduction.CraftingGrid
import vg.civcraft.mc.civmodcore.expression.*

open class ShapedCraftingRecipe(val symbols: MutableMap<Char, NBTExpression>, val shape: String, override val key: String, val result: ItemStack, nothingChar: Char = ' '): Recipe {
	init {
		symbols[nothingChar] = NBTExpression.airItem()
	}

	private val shapeArray: CharArray = shape.filter { it != '\n' }.toCharArray()
	private val width = run {
		var width = 0
		var maybeWidth = 0
		for (char in shape) {
			if (char == '\n') {
				maybeWidth = 0
				continue
			}

			maybeWidth++
			if (maybeWidth > width) {
				width = maybeWidth
			}
		}
		width
	}
	val height = shape.count { it == '\n' } + 1

	init {
		if (width * height != shapeArray.size) {
			throw AssertionError("width ($width) * height ($height) != shapeArray.size (${shapeArray.size}) in $key")
		}
	}

	init {
		if (width > CRAFTING_TABLE_SIZE || height > CRAFTING_TABLE_SIZE) {
			throw AssertionError("width / height must not be more than 5 of recipe $key")
		}
	}

	override fun preview(): Pair<CraftingGrid, ItemStack>? {
		val grid = CraftingGrid.empty()
		// center the recipe if possible
		val widthRange = if (width <= 3) { 1 until (width + 1) } else { 0 until width }
		val heightRange = if (height <= 3) { 1 until (height + 1) } else { 0 until height }

		x@for ((gridX, shapeX) in widthRange.zip(0 until width)) {
			for ((gridY, shapeY) in heightRange.zip(0 until height)) {
				val char = shapeArray[width, shapeX, shapeY]
				val expression = symbols[char] ?: error("recipe $key has undefined symbol $char")
				grid[gridX, gridY] = expression.solveToItemStack()
			}
		}
		return Pair(grid, result.clone())
	}

	override fun execute(grid: CraftingGrid): ItemStack? {
		val preGrid = grid.clone() // used because the below function mutates the grid at times when the recipe doesn't completely match, because we don't know it doesn't match yet
		// loop over all the slots in the table
		x@for (xStart in 0 until CRAFTING_TABLE_SIZE) {
			// if the recipe goes outside the table horizontally, fail
			if (xStart + width > CRAFTING_TABLE_SIZE) {
				return null
			}

			// restart@ allows breaking and continuing at a specific for
			y@for (yStart in 0 until CRAFTING_TABLE_SIZE) {
				grid.grid = preGrid.clone().grid // reset the grid from anything that might have been done in previous iterations
				// if the recipe goes outside the table vertically, fail
				if (yStart + height > CRAFTING_TABLE_SIZE) {
					continue@y
				}

				val matches = Array(25) { false }

				// loop over the recipe's slots
				for (x in 0 until width) {
					for (y in 0 until height) {
						val item = grid[x + xStart, y + yStart] // the item to check
						val char = shapeArray[width, x, y]
						val expression = symbols[char] ?: error("recipe $key has undefined symbol $char")
						if (expression.matches(item)) {
							if (item.type != Material.AIR) {
								// remove the specified number of items
								val amount = expression.getAmount()
								item.amount = item.amount - amount
								if (item.amount == 0) {
									// if the item is out in that slot, delete it
									item.type = Material.AIR
								}
								grid[x + xStart, y + yStart] = item
							}

							matches[(y + yStart) * CRAFTING_TABLE_SIZE + (x + xStart)] = true
						} else {
							// start over in finding a recipe
							continue@y
						}
					}
				}

				// check all items not matched over, if they contain an item: this isn't our recipe, return
				for (i in matches.indices) {
					if (!matches[i]) {
						if (preGrid.grid[i].type != Material.AIR) { // use the grid before any mutations have happened, else https://github.com/civplus/CivPlus/issues/112 reoccurs
							return null
						}
					}
				}

				// all the recipe's expressions match, success
				return result.clone()
			}
		}

		// no recipe possible
		return null
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as ShapedCraftingRecipe

		if (symbols != other.symbols) return false
		if (shape != other.shape) return false
		if (key != other.key) return false
		if (result != other.result) return false

		return true
	}

	override fun hashCode(): Int {
		var result1 = symbols.hashCode()
		result1 = 31 * result1 + shape.hashCode()
		result1 = 31 * result1 + key.hashCode()
		result1 = 31 * result1 + result.hashCode()
		return result1
	}

}

operator fun CharArray.get(width: Int, x: Int, y: Int): Char {
	return this[y * width + x]
}

operator fun CharArray.set(width: Int, x: Int, y: Int, to: Char) {
	this[y * width + x] = to
}
