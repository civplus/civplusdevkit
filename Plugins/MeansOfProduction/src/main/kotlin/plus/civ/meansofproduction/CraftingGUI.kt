package plus.civ.meansofproduction

import net.minecraft.server.v1_8_R3.NBTTagCompound
import org.bukkit.ChatColor
import org.bukkit.DyeColor
import org.bukkit.Material
import org.bukkit.Sound
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack
import org.bukkit.entity.HumanEntity
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.HandlerList
import org.bukkit.event.Listener
import org.bukkit.event.inventory.*
import org.bukkit.event.server.PluginDisableEvent
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.ItemMeta
import plus.civ.meansofproduction.listeners.RecipeBookListGUI.showRecipeGUI
import plus.civ.meansofproduction.recipe.RecipeCraftInfo
import vg.civcraft.mc.civmodcore.extensions.giveItemOrDropOnGround

class CraftingGUI(val inventory: Inventory, val player: HumanEntity, var is5x5: Boolean = false): Listener {
	init {
		MeansOfProduction.instance.server.scheduler.runTask(MeansOfProduction.instance) {
			initInventory()
			placeResult()
		}
	}

	val RESULT_SLOT = xyToSlot(7, 2)
	val CLOSE_SLOT = xyToSlot(8, 5)
	val HELP_SLOT = xyToSlot(6, 5)

	/**
	 * Places all the template items into the inventory.
	 */
	fun initInventory() {
		// fill the 6th, 7th, 8th, 9th rows solid since nothing goes there
		for (x in listOf(5, 6, 7, 8)) {
			for (y in 0 until DC_HEIGHT) {
				placePlaceholder(xyToSlot(x, y))
			}
		}

		// fill the bottom solid with black (used to be red, but that was confusing)
		for (x in 0 until DC_WIDTH) {
			placePlaceholder(xyToSlot(x, 5))
		}

		// if 3x3, fill in the unused squares
		if (!is5x5) {
			// top and bottom of square
			for (x in listOf(0, 4)) {
				for (y in 0..4) {
					@Suppress("DEPRECATION")
					placePlaceholder(xyToSlot(x, y), material = Material.STAINED_GLASS_PANE,
							name = "${ChatColor.RESET}Click To Expand Grid",
							materialData = DyeColor.LIGHT_BLUE.data)
				}
			}

			// left and right of square
			for (x in 0..4) {
				for (y in listOf(0, 4)) {
					@Suppress("DEPRECATION")
					placePlaceholder(xyToSlot(x, y), material = Material.STAINED_GLASS_PANE,
							name = "${ChatColor.RESET}Click To Expand Grid",
							materialData = DyeColor.LIGHT_BLUE.data)
				}
			}
		} else {
			// delete placeholder glass

			// top and bottom of square
			for (x in listOf(0, 4)) {
				for (y in 0..4) {
					if (inventory.getItem(xyToSlot(x, y))?.itemMeta?.displayName == "${ChatColor.RESET}Click To Expand Grid") {
						inventory.setItem(xyToSlot(x, y), ItemStack(Material.AIR))
					}
				}
			}

			// left and right of square
			for (x in 0..4) {
				for (y in listOf(0, 4)) {
					if (inventory.getItem(xyToSlot(x, y))?.itemMeta?.displayName == "${ChatColor.RESET}Click To Expand Grid") {
						inventory.setItem(xyToSlot(x, y), ItemStack(Material.AIR))
					}
				}
			}
		}

		// fill result square
		@Suppress("DEPRECATION")
		placePlaceholder(RESULT_SLOT, materialData = DyeColor.SILVER.data)

		// close button
		placePlaceholder(CLOSE_SLOT, Material.BARRIER, "${ChatColor.RESET}Close")

		// help button
		placePlaceholder(HELP_SLOT, Material.BOOK, "${ChatColor.RESET}Recipe Book")

		player.updateInventoryLater()
	}

	fun placePlaceholder(slot: Int, material: Material = Material.STAINED_GLASS_PANE, name: String = " ",
						 materialData: Byte? = if (material == Material.STAINED_GLASS_PANE) { @Suppress("DEPRECATION") DyeColor.BLACK.data } else { null }) {
		var item = ItemStack(material, 1)

		if (material != Material.AIR) {
			val meta: ItemMeta = if (!item.hasItemMeta()) {
				MeansOfProduction.instance.server.itemFactory.getItemMeta(item.type)!!
			} else {
				item.itemMeta!!
			}
			meta.displayName = name
			item.itemMeta = meta

			if (materialData != null) {
				item.durability = materialData.toShort()
			}

			item = item.placeholderClone()
		}

		inventory.setItem(slot, item)
	}

	fun xyToSlot(x: Int, y: Int): Int = y * DC_WIDTH + x

	@EventHandler
	fun onClose(event: InventoryCloseEvent) {
		if (event.inventory != inventory) {
			return
		}

		for (item in grid.grid) {
			if (item.type == Material.AIR) {
				continue
			}

			player.location.world.dropItemNaturally(player.location, item)
		}

		HandlerList.unregisterAll(this)
	}

	@EventHandler
	fun closeOnDisable(event: PluginDisableEvent) {
		if (event.plugin != MeansOfProduction.instance) {
			return
		}

		for (item in grid.grid) {
			if (item.type == Material.AIR) {
				continue
			}

			player.location.world.dropItemNaturally(player.location, item)
		}

		HandlerList.unregisterAll(this)
	}

	@EventHandler
	fun onClick(event: InventoryClickEvent) {
		try {
			if (event.whoClicked != player || event.inventory != inventory) {
				return
			}

			if (event.click == ClickType.DOUBLE_CLICK) {
				// super extra prevent dupes, sorry
				// todo: replace this with checking if the item in the result slot would be taken, and if so consiter the recipe crafted
				event.isCancelled = true
				return
			}

			MeansOfProduction.instance.server.scheduler.runTask(MeansOfProduction.instance) {
				initInventory()
				placeResult()
				maybeMake5x5()
			}

			player.updateInventoryLater()

			// clicked placeholder
			if (event.currentItem?.isPlaceholder() == true && event.slot != RESULT_SLOT) {
				event.isCancelled = true
			}

			if (event.currentItem?.itemMeta?.displayName == "${ChatColor.RESET}Click To Expand Grid") {
				event.isCancelled = true

				is5x5 = true
				initInventory()
				placeResult()
				if (player is Player) {
					player.playSound(player.location, Sound.CLICK, 1.0f, 1.0f)
				}

				return
			}

			// clicked close button
			if (event.currentItem?.let { it.type == Material.BARRIER } == true) {
				event.isCancelled = true
				MeansOfProduction.instance.server.scheduler.runTask(MeansOfProduction.instance, event.view::close)
				return
			}

			if (event.slot == HELP_SLOT && event.rawSlot < 54) {
				event.isCancelled = true
				MeansOfProduction.instance.server.scheduler.runTask(MeansOfProduction.instance) {
					//event.view.close()
					event.whoClicked.showRecipeGUI()
				}
				return
			}

			// click the recipe output slot
			if (event.slot == RESULT_SLOT && event.rawSlot < 54) {
				event.isCancelled = true

				// If shift click, do the recipe in a loop until we can't anymore
				val loopMode = event.click == ClickType.SHIFT_LEFT || event.click == ClickType.SHIFT_RIGHT
				if (loopMode) {
					event.isCancelled = true
				}

				var recipe = grid.evaluate()
				do {
					val oldRecipe = recipe
					recipe = grid.evaluate()
					if (recipe != oldRecipe) {
						return
					}

					if (recipe == null) {
						event.isCancelled = true
						return
					}

					val gridBefore = grid
					val gridAfter = grid
					var result = recipe.execute(gridAfter) ?: return
					// return if out of ingredients, we should stop shift clicking

					var shouldAddToCursor = false
					if (player.itemOnCursor != null && player.itemOnCursor.type != Material.AIR && !loopMode) {
						event.isCancelled = true

						if (!result.isSimilar(player.itemOnCursor)) {
							return
						}

						if (player.itemOnCursor.amount + result.amount > result.maxStackSize) {
							return
						}

						shouldAddToCursor = true
					}

					val maybeResult = recipe.sideEffect(RecipeCraftInfo(event.whoClicked, event.view, gridBefore, gridAfter, result))
					if (maybeResult != null) {
						result = maybeResult
						//inventory.setItem(RESULT_SLOT, result)
					}

					if (shouldAddToCursor) {
						val cursorItem = player.itemOnCursor
						if (!cursorItem.isSimilar(result)) {
							MeansOfProduction.instance.logger.warning("Recipe side effect of ${recipe.key} changed item while similar stacking! See issue #230")

						} else {
							cursorItem.amount += result.amount
							player.itemOnCursor = cursorItem
						}
					} else if (loopMode) {
						player.giveItemOrDropOnGround(result)
					} else {
						player.itemOnCursor = result
					}

					grid = gridAfter


					placeResult()

					player.updateInventoryLater()
				} while (loopMode)
				return
			}

			// clicked into the recipe slots, update
		} catch (ex: Exception) {
			MeansOfProduction.instance.logger.warning("Exception while crafting (${System.currentTimeMillis() / 1000})")
			ex.printStackTrace()
			event.whoClicked.sendMessage("${ChatColor.RED}An exception occurred while crafting. " +
					"Please report this to the admins. The time is ${System.currentTimeMillis() / 1000}.")
			event.isCancelled = true
		}
	}

	/**
	 * Makes the table 5x5 if the center is completely filled with items
	 */
	fun maybeMake5x5() {
		val grid = grid

		if (!is5x5) {
			// check if we should become 5x5 because the crafting grid is full
			var should5x5 = true
			for (x in 1..3) {
				for (y in 1..3) {
					val item = grid[x, y]
					if (item.type == Material.AIR) {
						should5x5 = false
					}
				}
			}

			if (should5x5) {
				is5x5 = true
				initInventory()
				placeResult()
				if (player is Player) {
					player.playSound(player.location, Sound.CLICK, 1.0f, 1.0f)
				}
			}
		}
	}

	@EventHandler
	fun onDrag(event: InventoryDragEvent) {
		if (event.inventory != inventory) {
			return
		}

		if (event.inventorySlots.contains(RESULT_SLOT)) {
			event.isCancelled = true
		}

		MeansOfProduction.instance.server.scheduler.runTask(MeansOfProduction.instance) {
			initInventory()
			placeResult()

			maybeMake5x5()
		}
		player.updateInventoryLater()
	}

	/**
	 * Fills in the result slot.
	 *
	 * @return True if there is a item placed into the slot
	 */
	fun placeResult(): Boolean {
		val recipe = grid.evaluate()
		if (recipe != null) {
			inventory.setItem(RESULT_SLOT, recipe.execute(this.grid)!!.placeholderClone())
			return true
		} else {
			@Suppress("DEPRECATION")
			placePlaceholder(RESULT_SLOT, materialData = DyeColor.SILVER.data)
			return false
		}
	}

	fun ItemStack.isPlaceholder(): Boolean {
		val nmsItem = CraftItemStack.asNMSCopy(this) ?: return false
		val tag = nmsItem.tag ?: NBTTagCompound()
		return tag.hasKey("GUIItem") && tag.getBoolean("GUIItem")
	}

	fun ItemStack.placeholderClone(): ItemStack {
		val nmsItem = CraftItemStack.asNMSCopy(this)
		val tag = nmsItem.tag ?: NBTTagCompound()
		tag.setBoolean("GUIItem", true)
		nmsItem.tag = tag
		return CraftItemStack.asBukkitCopy(nmsItem)
	}

	fun ItemStack.notPlaceholderClone(): ItemStack {
		val nmsItem = CraftItemStack.asNMSCopy(this)
		val tag = nmsItem.tag ?: NBTTagCompound()
		if (tag.hasKey("GUIItem")) {
			tag.remove("GUIItem")
		}
		nmsItem.tag = tag
		return CraftItemStack.asBukkitCopy(nmsItem)
	}

	var grid: CraftingGrid
		get() {
			val grid = CraftingGrid.empty()

			for (x in 0..4) {
				for (y in 0..4) {
					val item = inventory.getItem(xyToSlot(x, y)) ?: ItemStack(Material.AIR)
					if (!item.isPlaceholder()) {
						grid[x, y] = item.clone()
					} else {
						grid[x, y] = ItemStack(Material.AIR)
					}
				}
			}

			return grid
		}
		set(grid) {
			for (x in 0 until CRAFTING_TABLE_SIZE) {
				for (y in 0 until CRAFTING_TABLE_SIZE) {
					inventory.setItem(xyToSlot(x, y), grid[x, y])
				}
			}
		}
}

const val DC_WIDTH = 9
const val DC_HEIGHT = 6

fun HumanEntity.showCraftingGUI() {
	val inv = MeansOfProduction.instance.server.createInventory(null, DC_WIDTH * DC_HEIGHT, "Crafting Table")
	this.openInventory(inv)
	MeansOfProduction.instance.server.pluginManager.registerEvents(CraftingGUI(inv, this), MeansOfProduction.instance)
}

fun HumanEntity.updateInventoryLater() {
	if (this is Player) {
		MeansOfProduction.instance.server.scheduler.runTaskLater(MeansOfProduction.instance, this::updateInventory, 1)
	}
}
