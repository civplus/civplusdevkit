package plus.civ.meansofproduction.recipes

import org.bukkit.Material.*
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe

object FishingRodNotBackwardsRecipe: RecipeProvider {
	override fun register() {
		val stick = item(STICK)
		val string = item(STRING)
		val ingredients = mutableMapOf(Pair('/', stick), Pair('|', string))
		val shape = """
			  /
			 /|
			/ |
		""".trimIndent()
		addUnlistedRecipe(ShapedCraftingRecipe(ingredients, shape, "fishing_rod_good", ItemStack(FISHING_ROD)))
	}
}
