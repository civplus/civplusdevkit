package plus.civ.meansofproduction.recipe.mutator

import org.bukkit.DyeColor
import org.bukkit.block.banner.Pattern
import org.bukkit.block.banner.PatternType
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.BannerMeta
import plus.civ.meansofproduction.CraftingGrid
import plus.civ.meansofproduction.recipe.Recipe
import vg.civcraft.mc.civmodcore.expression.NBTExpression
import vg.civcraft.mc.civmodcore.expression.matches

class BannerPatternMutator(val mutates: Recipe, val dye: NBTExpression, val banner: NBTExpression, val pattern: PatternType): Recipe {
	override val key = mutates.key

	override fun preview(): Pair<CraftingGrid, ItemStack>? {
		return mutates.preview()
	}

	override fun execute(grid: CraftingGrid): ItemStack? {
		var color: DyeColor? = null
		grid.grid.filter(dye::matches)
				.map { @Suppress("DEPRECATION") DyeColor.getByDyeData(it.durability.toByte()) }
				.forEach { if (color == null) color = it else if (color != it) return null }

		val banners = grid.grid.filter(banner::matches)
		if (banners.size != 1) {
			return null
		}
		val banner = banners[0].clone()

		mutates.execute(grid) ?: return null

		val resultMeta = banner.itemMeta
		if (resultMeta !is BannerMeta) {
			return null
		}

		if (resultMeta.numberOfPatterns() >= 16) {
			return null
		}

		resultMeta.addPattern(Pattern(color,  pattern))
		banner.itemMeta = resultMeta

		banner.amount = 1

		return banner
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as BannerPatternMutator

		if (mutates != other.mutates) return false
		if (dye != other.dye) return false
		if (banner != other.banner) return false
		if (pattern != other.pattern) return false
		if (key != other.key) return false

		return true
	}

	override fun hashCode(): Int {
		var result = mutates.hashCode()
		result = 31 * result + dye.hashCode()
		result = 31 * result + banner.hashCode()
		result = 31 * result + pattern.hashCode()
		result = 31 * result + key.hashCode()
		return result
	}
}
