package plus.civ.meansofproduction.listeners

import org.bukkit.ChatColor
import org.bukkit.DyeColor
import org.bukkit.Material
import org.bukkit.Sound
import org.bukkit.entity.HumanEntity
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.inventory.InventoryCloseEvent
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.BookMeta
import plus.civ.meansofproduction.*
import plus.civ.meansofproduction.listeners.RecipeBookListGUI.showRecipeGUI
import plus.civ.meansofproduction.listeners.RecipePreview.showRecipePreview
import plus.civ.meansofproduction.recipe.Recipe
import vg.civcraft.mc.civmodcore.expression.*
import vg.civcraft.mc.civmodcore.extensions.normalized
import vg.civcraft.mc.civmodcore.extensions.prettyName
import java.util.*

object RecipeBookListGUI: Listener {
	val entries = mutableListOf<RecipeBookEntry>()
	val openPlayers = mutableMapOf<UUID, RecipeCategory>()

	val exitButton = NBTExpression()
			.id(Material.BARRIER)
			.itemName("${ChatColor.RESET}Close")
	val separator = NBTExpression()
			.id(Material.STAINED_GLASS_PANE)
			.damage(DyeColor.GRAY.woolData.toShort())
			.itemName("${ChatColor.RESET}")
	val selectionArrow = NBTExpression()
			.id(Material.STAINED_GLASS_PANE)
			.damage(DyeColor.LIGHT_BLUE.woolData.toShort())
			.itemName("${ChatColor.RESET}")

	@EventHandler
	fun onClose(event: InventoryCloseEvent) {
		if (!openPlayers.containsKey(event.player.uniqueId)) {
			return
		}

		openPlayers.remove(event.player.uniqueId)
	}

	@EventHandler
	fun onClick(event: InventoryClickEvent) {
		if (!openPlayers.containsKey(event.whoClicked.uniqueId)) {
			return
		}

		event.isCancelled = true

		val item = event.currentItem ?: ItemStack(Material.AIR)
		val player = event.whoClicked
		val inventory = event.clickedInventory

		if (exitButton.matches(item)) {
			if (player is Player) {
				player.playSound(player.location, Sound.CLICK, 1.0f, 1.0f)
			}
			player.showCraftingGUI()
			return
		}

		if (event.slot / 9 == 5) {
			val categoryIndex = event.slot % 9
			val category = RecipeCategory.values()[categoryIndex]
			if (player is Player) {
				player.playSound(player.location, Sound.CLICK, 1.0f, 1.0f)
			}
			openPlayers[player.uniqueId] = category
			showCategory(inventory, category)
			return
		}

		for (recipe in entries) {
			if (recipe.icon == item) {
				if (player is Player) {
					player.playSound(player.location, Sound.CLICK, 1.0f, 1.0f)
				}
				player.showRecipePreview(recipe, openPlayers[player.uniqueId]!!)
				return
			}
		}
	}

	fun showCategory(inv: Inventory, category: RecipeCategory) {
		for (viewer in inv.viewers) {
			if (viewer is Player) {
				viewer.playSound(viewer.location, Sound.CLICK, 1.0f, 1.0f)
			}
		}

		// add/reset the separating bar
		for (x in 0 until DC_WIDTH) {
			val y = DC_HEIGHT - 2
			val item = separator.solveToItemStack()
			inv.setItem(xyToSlot(x, y), item)
		}

		// set the selection arrow
		for ((x, maybeCategory) in RecipeCategory.values().withIndex()) {
			if (category != maybeCategory) {
				continue
			}

			val y = DC_HEIGHT - 2
			val item = selectionArrow.solveToItemStack()
			inv.setItem(xyToSlot(x, y), item)
		}

		// clear the old category recipes
		for (x in 0 until DC_WIDTH) {
			for (y in 0..DC_HEIGHT - 3) {
				inv.setItem(xyToSlot(x, y), ItemStack(Material.AIR))
			}
		}

		// add the new category recipes
		var recipeSlot = 0
		for (entry in entries) {
			if (entry.category == category) {
				inv.setItem(recipeSlot++, entry.icon)
			}
		}
	}

	fun HumanEntity.showRecipeGUI(startingCategory: RecipeCategory = RecipeCategory.BUILDING) {
		MeansOfProduction.instance.server.scheduler.runTask(MeansOfProduction.instance) {
			val inv = MeansOfProduction.instance.server.createInventory(null, DC_WIDTH * DC_HEIGHT, "Recipe Book")
			this.openInventory(inv)
			openPlayers[uniqueId] = startingCategory

			// set the categories
			for ((x, category) in RecipeCategory.values().withIndex()) {
				inv.setItem(xyToSlot(x, DC_HEIGHT - 1), category.icon.solveToItemStack())
			}

			// add the exit button
			inv.setItem(xyToSlot(DC_WIDTH - 1, DC_HEIGHT - 1), exitButton.solveToItemStack())

			// set the default category
			showCategory(inv, startingCategory)
		}
	}

	fun xyToSlot(x: Int, y: Int): Int = y * DC_WIDTH + x
}

object RecipePreview: Listener {
	val openPlayers = mutableMapOf<UUID, RecipeCategory>()

	val exitButton = NBTExpression()
			.id(Material.BARRIER)
			.itemName("${ChatColor.RESET}Close")
	val resultSlot = xyToSlot(7, 2)
	val exitSlot = xyToSlot(8, 5)

	@EventHandler
	fun onClose(event: InventoryCloseEvent) {
		if (!openPlayers.containsKey(event.player.uniqueId)) {
			return
		}

		openPlayers.remove(event.player.uniqueId)
	}

	@EventHandler
	fun onClick(event: InventoryClickEvent) {
		if (!openPlayers.containsKey(event.whoClicked.uniqueId)) {
			return
		}

		event.isCancelled = true

		val item = event.currentItem ?: ItemStack(Material.AIR)
		val player = event.whoClicked

		if (exitButton.matches(item)) {
			player.showRecipeGUI(startingCategory = openPlayers[player.uniqueId]!!)
			return
		}
	}

	fun HumanEntity.showRecipePreview(recipe: RecipeBookEntry, oldCategory: RecipeCategory) {
		MeansOfProduction.instance.server.scheduler.runTask(MeansOfProduction.instance) {
			val inv = MeansOfProduction.instance.server.createInventory(null, DC_WIDTH * DC_HEIGHT, recipe.prettyRecipeName)
			this.openInventory(inv)
			openPlayers[uniqueId] = oldCategory

			val grid = recipe.grid
			var is3x3 = true

			loop@ for (i in 0 until CRAFTING_TABLE_SIZE) {
				if (grid[i, 0].type != Material.AIR ||
						grid[0, i].type != Material.AIR ||
						grid[4, i].type != Material.AIR ||
						grid[i, 4].type != Material.AIR) {
					is3x3 = false
					break@loop
				}
			}

			// don't 3x3 if the 3x3 grid is completely full
			var hasOneAir = false
			if (is3x3) {
				x@for (x in 1..3) {
					for (y in 1..3) {
						if (grid[x, y].type == Material.AIR) {
							hasOneAir = true
							break@x
						}
					}
				}
			}
			if (!hasOneAir) {
				is3x3 = false
			}

			val placeholder = ItemStack(Material.STAINED_GLASS_PANE)
			placeholder.durability = DyeColor.GRAY.woolData.toShort()
			val placeholderMeta = placeholder.itemMeta
			placeholderMeta.displayName = "${ChatColor.RESET}"
			placeholder.itemMeta = placeholderMeta

			// fill in every slot with a placeholder
			for (x in 0 until DC_WIDTH) {
				for (y in 0 until DC_HEIGHT) {
					inv.setItem(xyToSlot(x, y), placeholder)
				}
			}

			// remove placeholders from the 3x3/5x5
			val xRange = if (is3x3) 2..4 else 1..5
			val yRange = if (is3x3) 1..3 else 0..4
			for (x in xRange) {
				for (y in yRange) {
					inv.setItem(xyToSlot(x, y), ItemStack(Material.AIR))
				}
			}

			// fill in the items from the grid
			for (x in 0 until CRAFTING_TABLE_SIZE) {
				for (y in 0 until CRAFTING_TABLE_SIZE) {
					val item = grid[x, y]
					if (item.type == Material.AIR) {
						continue
					}

					inv.setItem(xyToSlot(x + 1, y), item)
				}
			}

			// add the result
			inv.setItem(resultSlot, recipe.result)

			// add the close button
			inv.setItem(exitSlot, exitButton.solveToItemStack())
		}
	}

	fun xyToSlot(x: Int, y: Int): Int = y * DC_WIDTH + x
}

data class RecipeBookEntry(val recipe: Recipe, val category: RecipeCategory, val recipeName: String = "") {
	private val preview = recipe.preview() ?: error("For recipe ${recipe.key}: Can't add a recipe without a preview to the recipe book")
	val icon = recipe.icon ?: preview.second.normalized // gets mutated below in init {}
	val result = preview.second.normalized
	val grid = preview.first
	val prettyRecipeName = when {
		recipeName != "" -> {
			recipeName
		}
		result.itemMeta.hasDisplayName() -> {
			result.itemMeta.displayName
		}
		result.itemMeta is BookMeta -> {
			(result.itemMeta as BookMeta).title
		}
		else -> {
			result.type.prettyName
		}
	}

	init {
		if (recipeName != "" && recipe.icon != null) {
			val iconMeta = icon.itemMeta
			iconMeta.displayName = "${ChatColor.RESET}$recipeName"
			icon.itemMeta = iconMeta
		}
	}
}
