package plus.civ.meansofproduction.recipes

import org.bukkit.Material.*
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapelessCraftingRecipe

object ClayRecipe: RecipeProvider {
	override fun register() {
		val dirt = item(DIRT, 32)
		val gravel = item(GRAVEL, 32)
		val ingredients = listOf(dirt, gravel)
		addRecipeBookRecipe(ShapelessCraftingRecipe(ingredients, "clay", ItemStack(CLAY, 64)), RecipeCategory.RESOURCES)
	}
}
