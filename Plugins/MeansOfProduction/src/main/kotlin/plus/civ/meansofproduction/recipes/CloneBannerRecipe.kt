package plus.civ.meansofproduction.recipes

import org.bukkit.Material.BANNER
import plus.civ.meansofproduction.recipe.CloneItemRecipe
import plus.civ.meansofproduction.recipe.RecipeProvider

object CloneBannerRecipe: RecipeProvider {
	override fun register() {
		addUnlistedRecipe(CloneItemRecipe(listOf(item(BANNER), item(BANNER)), "cloneBanner", item(BANNER)))
	}
}
