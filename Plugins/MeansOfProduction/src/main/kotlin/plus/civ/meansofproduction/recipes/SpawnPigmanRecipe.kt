package plus.civ.meansofproduction.recipes

import org.bukkit.Material.GOLD_INGOT
import org.bukkit.Material.LAVA_BUCKET
import org.bukkit.entity.EntityType
import org.bukkit.material.SpawnEgg
import plus.civ.meansofproduction.*
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe
import vg.civcraft.mc.civmodcore.expression.NBTExpression
import vg.civcraft.mc.civmodcore.expression.damage
import vg.civcraft.mc.civmodcore.expression.id

object SpawnPigmanRecipe: RecipeProvider {
	override fun register() {
		val lava = item(LAVA_BUCKET)

		@Suppress("DEPRECATION")
		val pig = NBTExpression.empty()
				.id("minecraft:spawn_egg")
				.amount(1)
				.damage(EntityType.PIG.typeId)
		val gold = item(GOLD_INGOT)
		val ingredients = mutableMapOf(Pair('l', lava), Pair('p', pig), Pair('g', gold))
		val shape = """
			 l 
			ppp
			 g 
		""".trimIndent()
		addRecipeBookRecipe(ShapedCraftingRecipe(ingredients, shape, "spawn_pigman", SpawnEgg(EntityType.PIG_ZOMBIE).toItemStack(1)), RecipeCategory.MOBS)
	}
}
