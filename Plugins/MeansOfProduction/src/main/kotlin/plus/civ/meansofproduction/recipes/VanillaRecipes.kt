package plus.civ.meansofproduction.recipes

import org.bukkit.Material
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack
import org.bukkit.inventory.FurnaceRecipe
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.ShapedRecipe
import org.bukkit.inventory.ShapelessRecipe
import plus.civ.meansofproduction.MeansOfProduction
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.disabledVanillaRecipes
import plus.civ.meansofproduction.oftenForgottenVanillaRecipes
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe
import plus.civ.meansofproduction.recipe.ShapelessCraftingRecipe
import plus.civ.meansofproduction.recipe.mutator.DurabilityOuchMutator
import vg.civcraft.mc.civmodcore.expression.*
import vg.civcraft.mc.civmodcore.extensions.nbt
import vg.civcraft.mc.civmodcore.extensions.vanillaId

object VanillaRecipes: RecipeProvider {
	override fun register() {
		// Devkit snip to make things harder to make your own server using MOP
	}
}
