package plus.civ.meansofproduction.recipes

import org.bukkit.Material.*
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe
import vg.civcraft.mc.civmodcore.expression.NBTExpression
import vg.civcraft.mc.civmodcore.expression.id

object SaddleRecipe: RecipeProvider {
	override fun register() {
		val leather = NBTExpression.empty().id(LEATHER).amount(1)
		val goldNugget = NBTExpression.empty().id(GOLD_NUGGET).amount(1)
		val ingredients = mutableMapOf(Pair('l', leather), Pair('n', goldNugget))
		val saddleShape = """
		  n n 
		 lllll
		 l n l
		""".trimIndent()
		addRecipeBookRecipe(ShapedCraftingRecipe(ingredients, saddleShape, "saddle", ItemStack(SADDLE, 1)), RecipeCategory.VANILLA_TWEAKS)
	}
}
