@file:Suppress("UnstableApiUsage")

package plus.civ.meansofproduction.recipe

import com.google.common.reflect.ClassPath
import org.bukkit.Bukkit
import org.bukkit.plugin.java.JavaPlugin
import plus.civ.meansofproduction.MeansOfProduction

object RecipeManager {
	val recipes = mutableSetOf<Recipe>()

	val recipeProviders = mutableSetOf<RecipeProvider>()

	internal fun registerRecipes() {
		for (plugin in Bukkit.getPluginManager().plugins) {
			if (plugin !is MeansOfProduction) {
				if (!plugin.description.depend.contains("MeansOfProduction") && !plugin.description.softDepend.contains("MeansOfProduction")) {
					continue
				}
			}

			val packageName = plugin.javaClass.`package`.name

			val getClassLoader = JavaPlugin::class.java.getDeclaredMethod("getClassLoader")
			getClassLoader.isAccessible = true

			val classLoader = getClassLoader.invoke(plugin) as ClassLoader
			val classpath = ClassPath.from(classLoader)

			for (classInfo in classpath.allClasses) {
				if (!classInfo.name.startsWith(packageName)) {
					continue
				}
				val clazz = try { classInfo.load() } catch (e: Exception) { continue }
				if (!RecipeProvider::class.java.isAssignableFrom(clazz)) {
					continue
				}
				val recipeProvider = clazz.kotlin.objectInstance ?: clazz.kotlin.constructors.firstOrNull { it.parameters.isEmpty() }?.call() ?: continue
				if (recipeProvider !is RecipeProvider) {
					continue
				}

				MeansOfProduction.instance.logger.info("Found RecipeProvider ${classInfo.simpleName}")

				if (recipeProvider !in recipeProviders) {
					try {
						recipeProvider.register()
					} catch (e: Exception) {
						MeansOfProduction.instance.logger.info("RecipeProvider ${classInfo.simpleName} had an exception while registering")
						e.printStackTrace()
						continue
					}
				}

				recipeProviders.add(recipeProvider)
			}
		}
	}
}
