package plus.civ.meansofproduction.recipes

import org.bukkit.Material.*
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe
import vg.civcraft.mc.civmodcore.expression.NBTExpression
import vg.civcraft.mc.civmodcore.expression.id

object DiamondArmorRecipe: RecipeProvider {
	override fun register() {
		val diamond = NBTExpression.empty().id(DIAMOND).amount(7)
		val ingredients = mutableMapOf(Pair('d', diamond))

		val helmetShape = """
			ddd
			d d
		""".trimIndent()
		val chestShape = """
			d d
			ddd
			ddd
		""".trimIndent()
		val leggingsShape = """
			ddd
			d d
			d d
		""".trimIndent()
		val bootsShape = """
			d d
			d d
		""".trimIndent()

		val helmetRecipe = ShapedCraftingRecipe(ingredients, helmetShape, "diamond_helmet", ItemStack(DIAMOND_HELMET, 1))
		val chestRecipe = ShapedCraftingRecipe(ingredients, chestShape, "diamond_chest", ItemStack(DIAMOND_CHESTPLATE, 1))
		val legsRecipe = ShapedCraftingRecipe(ingredients, leggingsShape, "diamond_leggings", ItemStack(DIAMOND_LEGGINGS, 1))
		val bootsRecipe = ShapedCraftingRecipe(ingredients, bootsShape, "diamond_boots", ItemStack(DIAMOND_BOOTS, 1))

		addRecipeBookRecipe(helmetRecipe, RecipeCategory.PVP)
		addRecipeBookRecipe(chestRecipe, RecipeCategory.PVP)
		addRecipeBookRecipe(legsRecipe, RecipeCategory.PVP)
		addRecipeBookRecipe(bootsRecipe, RecipeCategory.PVP)
		addRecipeBookRecipe(helmetRecipe, RecipeCategory.VANILLA_TWEAKS)
		addRecipeBookRecipe(chestRecipe, RecipeCategory.VANILLA_TWEAKS)
		addRecipeBookRecipe(legsRecipe, RecipeCategory.VANILLA_TWEAKS)
		addRecipeBookRecipe(bootsRecipe, RecipeCategory.VANILLA_TWEAKS)
	}
}
