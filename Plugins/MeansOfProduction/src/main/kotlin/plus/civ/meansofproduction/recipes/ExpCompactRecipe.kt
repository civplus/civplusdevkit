package plus.civ.meansofproduction.recipes

import org.bukkit.Material
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe
import plus.civ.meansofproduction.recipe.ShapelessCraftingRecipe

object ExpCompactRecipe: RecipeProvider {
	override fun register() {
		val expBottle = item(Material.EXP_BOTTLE)

		val ingredientMap = mutableMapOf(Pair('b', expBottle))
		val recipe = """
		 b 
		bbb
		bbb
		bbb
		 b 
		""".trimIndent()

		addRecipeBookRecipe(ShapedCraftingRecipe(ingredientMap, recipe, "emerald", ItemStack(Material.EMERALD)),
				RecipeCategory.EXP, "Compress ExP Bottles into Emeralds")

		val ingredients = listOf(item(Material.EMERALD))
		addRecipeBookRecipe(ShapelessCraftingRecipe(ingredients, "unemerald", ItemStack(Material.EXP_BOTTLE, 11)),
				RecipeCategory.EXP, "Decompress Emeralds into ExP Bottles")
	}
}
