package plus.civ.meansofproduction.recipes

import net.minecraft.server.v1_8_R3.NBTTagCompound
import org.bukkit.Material
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.CRAFTING_TABLE_SIZE
import plus.civ.meansofproduction.CraftingGrid
import plus.civ.meansofproduction.recipe.Recipe
import plus.civ.meansofproduction.recipe.RecipeProvider

object CloneWrittenBookRecipe: RecipeProvider, Recipe {
	override fun register() {
		addUnlistedRecipe(this)
	}

	override val key: String = "cloneWrittenBook"

	override fun preview(): Pair<CraftingGrid, ItemStack> {
		val grid = CraftingGrid.empty()
		grid[0, 0] = ItemStack(Material.WRITTEN_BOOK)
		grid[0, 1] = ItemStack(Material.BOOK_AND_QUILL)

		return Pair(grid, ItemStack(Material.WRITTEN_BOOK))
	}

	override fun execute(grid: CraftingGrid): ItemStack? {
		val writtenBooks = grid.grid.filter { it.type == Material.WRITTEN_BOOK }
		if (writtenBooks.size != 1) {
			return null
		}
		val writtenBook = writtenBooks[0].clone()
		val bookAndQuillCount = grid.grid.filter { it.type == Material.BOOK_AND_QUILL }.size
		if (bookAndQuillCount == 0) {
			return null
		}

		for (x in 0 until CRAFTING_TABLE_SIZE) {
			for (y in 0 until CRAFTING_TABLE_SIZE) {
				if (grid[x, y].type !in setOf(Material.BOOK_AND_QUILL, Material.WRITTEN_BOOK, Material.AIR)) {
					return null
				}

				if (grid[x, y].type == Material.BOOK_AND_QUILL) {
					grid[x, y] = ItemStack(Material.AIR)
				}
			}
		}

		writtenBook.amount = bookAndQuillCount
		val nmsItem = CraftItemStack.asNMSCopy(writtenBook)
		val tag = nmsItem.tag ?: NBTTagCompound()
		val generation = tag.getInt("generation")
		if (generation >= 2) {
			// copy of a copy
			return null
		}
		tag.setInt("generation", generation + 1)
		nmsItem.tag = tag
		return CraftItemStack.asBukkitCopy(nmsItem)
	}
}
