package plus.civ.meansofproduction.recipes

import org.bukkit.Material.*
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapelessCraftingRecipe

object GravelRecipe: RecipeProvider {
	override fun register() {
		val wheat = item(WHEAT, 4)
		val dirt = item(DIRT, 32)
		val cobble = item(COBBLESTONE, 32)
		val ingredients = listOf(wheat, dirt, cobble)
		addRecipeBookRecipe(ShapelessCraftingRecipe(ingredients, "gravel", ItemStack(GRAVEL, 64)), RecipeCategory.RESOURCES)
	}
}
