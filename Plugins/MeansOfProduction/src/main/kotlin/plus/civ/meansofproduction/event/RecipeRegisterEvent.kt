package plus.civ.meansofproduction.event

import org.bukkit.event.Cancellable
import org.bukkit.event.Event
import org.bukkit.event.HandlerList
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.Recipe

/**
 * Called when a recipe gets registered, by MeansOfProduction or some other plugin
 */
class RecipeRegisterEvent(val recipe: Recipe, val category: RecipeCategory?): Event(), Cancellable {
	companion object {
		@JvmStatic
		private val handlers = HandlerList()

		@JvmStatic
		fun getHandlerList(): HandlerList {
			return handlers
		}
	}

	override fun getHandlers(): HandlerList {
		return Companion.handlers
	}

	private var cancelledStatus = false

	override fun isCancelled(): Boolean = cancelledStatus

	override fun setCancelled(cancelled: Boolean) {
		cancelledStatus = cancelled
	}
}
