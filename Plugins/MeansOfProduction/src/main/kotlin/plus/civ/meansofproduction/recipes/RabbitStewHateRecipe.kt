package plus.civ.meansofproduction.recipes

import org.bukkit.Material.*
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe

object RabbitStewHateRecipe: RecipeProvider {
	override fun register() {
		// Parker wants me to add a recipe book item for rabbit stew
		// Vanilla defines two recipes for vanilla stew, one with a red mushroom and one with a brown mushroom
		// Those two recipes show up in the interface.
		// This is to make that not happen.

		val rabbit = item(COOKED_RABBIT)
		val carrot = item(CARROT_ITEM)
		val potato = item(POTATO_ITEM)
		val mushroom = item(BROWN_MUSHROOM).or(item(RED_MUSHROOM))
		val bowl = item(BOWL)

		val shape = """
			 r 
			cpm
			 b 
		""".trimIndent()
		val altShape = """
			 r 
			mpc
			 b 
		""".trimIndent()

		val ingredients = mutableMapOf(Pair('r', rabbit), Pair('c', carrot), Pair('p', potato), Pair('m', mushroom), Pair('b', bowl))

		addRecipeBookRecipe(ShapedCraftingRecipe(ingredients, shape, "rabbit_stew", ItemStack(RABBIT_STEW)), RecipeCategory.VANILLA_UTILITY)
		addUnlistedRecipe(ShapedCraftingRecipe(ingredients, altShape, "rabbit_stew_alt", ItemStack(RABBIT_STEW)))
	}
}
