package plus.civ.meansofproduction.listeners

import net.minecraft.server.v1_8_R3.PacketPlayOutSetSlot
import org.bukkit.GameMode
import org.bukkit.Material
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.inventory.ClickType
import org.bukkit.event.inventory.InventoryAction
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.inventory.InventoryType
import org.bukkit.inventory.CraftingInventory
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.ItemMeta
import org.bukkit.scheduler.BukkitRunnable
import plus.civ.meansofproduction.MeansOfProduction
import plus.civ.meansofproduction.showCraftingGUI


object OpenCraftingGUIInvButton: Listener, BukkitRunnable() {
	val openCraftingTableItem by lazy {
		val item = ItemStack(Material.WORKBENCH)
		val meta: ItemMeta = if (!item.hasItemMeta()) {
			MeansOfProduction.instance.server.itemFactory.getItemMeta(item.type)!!
		} else {
			item.itemMeta!!
		}
		meta.lore = listOf("Click to open a 3x3 / 5x5 Crafting Table")
		item.itemMeta = meta

		item
	}

	@EventHandler
	fun onClickWorkbench(event: InventoryClickEvent) {
		if (!openCraftingTableItem.isSimilar(event.currentItem)) {
			if (event.clickedInventory !is CraftingInventory) {
				return
			}

			if (event.slotType != InventoryType.SlotType.CRAFTING) {
				return
			}
		}

		event.isCancelled = true

		val badActions = setOf(
				InventoryAction.DROP_ALL_CURSOR,
				InventoryAction.DROP_ALL_SLOT,
				InventoryAction.DROP_ONE_CURSOR,
				InventoryAction.DROP_ONE_SLOT)

		if (event.action in badActions) {
			return
		}

		val badClick = setOf(ClickType.CONTROL_DROP, ClickType.DROP)

		if (event.click in badClick) {
			return
		}

		if (event.whoClicked !is Player) {
			return
		}

		(event.whoClicked as Player).showCraftingGUI()
	}

	override fun run() {
		for (player in MeansOfProduction.instance.server.onlinePlayers) {
			if (player.gameMode != GameMode.SURVIVAL) {
				for (i in 1..4) {
					val packet = PacketPlayOutSetSlot(0, i, null)
					(player as CraftPlayer).handle.playerConnection.sendPacket(packet)
				}
				continue
			}

			for (i in 1..4) {
				val packet = PacketPlayOutSetSlot(0, i, CraftItemStack.asNMSCopy(openCraftingTableItem))
				(player as CraftPlayer).handle.playerConnection.sendPacket(packet)
			}
		}
	}
}
