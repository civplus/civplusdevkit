package plus.civ.meansofproduction.recipes

import org.bukkit.DyeColor
import org.bukkit.Material.*
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe
import vg.civcraft.mc.civmodcore.expression.damage

object HellStoneRecipe: RecipeProvider {
	override fun register() {
		val netherbrick = item(NETHER_BRICK_ITEM)
		val dirt = item(DIRT, 32)
		val melon = item(MELON, 32)

		@Suppress("DEPRECATION")
		val cocoa = item(INK_SACK, 4).damage(DyeColor.BROWN.dyeData.toShort())
		val wart = item(NETHER_STALK, 4)
		val ingredients = mutableMapOf(Pair('b', netherbrick), Pair('d', dirt), Pair('m', melon), Pair('c', cocoa), Pair('w', wart))
		val shape = """
			 b 
			cdw
			 m 
		""".trimIndent()
		val recipe = ShapedCraftingRecipe(ingredients, shape, "hell_stone", ItemStack(NETHERRACK, 48))
		addRecipeBookRecipe(recipe, RecipeCategory.RESOURCES)
	}
}
