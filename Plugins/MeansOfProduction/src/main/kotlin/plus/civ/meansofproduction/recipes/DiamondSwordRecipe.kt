package plus.civ.meansofproduction.recipes

import org.bukkit.Material.*
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.MeansOfProduction
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe
import vg.civcraft.mc.civmodcore.expression.NBTExpression
import vg.civcraft.mc.civmodcore.expression.id

object DiamondSwordRecipe: RecipeProvider {
	override fun register() {
		val diamond = NBTExpression.empty().id(DIAMOND).amount(9)
		val stick = NBTExpression.empty().id(STICK).amount(1)
		val ingredients = mutableMapOf(Pair('d', diamond), Pair('/', stick))
		val swordShape = """
			d
			d
			/
		""".trimIndent()
		val swordRecipe = ShapedCraftingRecipe(ingredients, swordShape, "diamond_sword", ItemStack(DIAMOND_SWORD, 1))
		addRecipeBookRecipe(swordRecipe, RecipeCategory.PVP)
		addRecipeBookRecipe(swordRecipe, RecipeCategory.VANILLA_TWEAKS)
	}
}
