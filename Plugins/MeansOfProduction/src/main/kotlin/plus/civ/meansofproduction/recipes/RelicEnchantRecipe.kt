package plus.civ.meansofproduction.recipes

import org.bukkit.Material.EMERALD
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.EnchantmentAddRecipe
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.remnant.items.Relic
import vg.civcraft.mc.civmodcore.expression.NBTExpression
import vg.civcraft.mc.civmodcore.expression.id

object RelicEnchantRecipe: RecipeProvider {
	override fun register() {
		addRecipeBookRecipe(EnchantmentAddRecipe(Relic.expression, NBTExpression.empty().id(EMERALD), 5, "relic_enchant"),
				RecipeCategory.EXP, "Enchant an Item using Relics")
	}
}
