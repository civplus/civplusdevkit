package plus.civ.meansofproduction.recipes

import org.bukkit.Material.FLINT
import org.bukkit.Material.GRAVEL
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapelessCraftingRecipe

object FlintFromGravelRecipe: RecipeProvider {
	override fun register() {
		val gravel = item(GRAVEL)
		val ingredients = listOf(gravel, gravel)
		addRecipeBookRecipe(ShapelessCraftingRecipe(ingredients, "flint", ItemStack(FLINT, 1)), RecipeCategory.RESOURCES)
	}
}
