package plus.civ.meansofproduction.recipes

import org.bukkit.Material.*
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.*
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapelessCraftingRecipe
import plus.civ.remnant.items.Relic

object BasicExpRecipe: RecipeProvider {
	override fun register() {
		val potato = item(POTATO_ITEM)
		val cookie = item(COOKIE)
		val pumpkin = item(PUMPKIN)
		val melon = item(MELON_BLOCK)
		val carrot = item(CARROT_ITEM)
		val cake = item(CAKE)
		val wart = item(NETHER_STALK)
		val hay = item(HAY_BLOCK)
		val ingredients = listOf(potato, cookie, pumpkin, melon, carrot, cake, wart, hay, Relic.expression)
		addRecipeBookRecipe(ShapelessCraftingRecipe(ingredients, "basic_exp", ItemStack(EXP_BOTTLE, 64)),
				RecipeCategory.EXP, "Make ExP from Crops (Basic)")
	}
}
