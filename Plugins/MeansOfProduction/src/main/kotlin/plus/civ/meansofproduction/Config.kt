@file:Suppress("unused")

package plus.civ.meansofproduction

import org.bukkit.Material.*

val disabledVanillaRecipes = mutableSetOf(DIAMOND_HELMET, DIAMOND_CHESTPLATE, DIAMOND_LEGGINGS, DIAMOND_BOOTS, DIAMOND_SWORD,
		BOAT,
		RABBIT_STEW, BEACON, IRON_FENCE, BOOKSHELF)
val oftenForgottenVanillaRecipes = setOf(STONE, PRISMARINE, SEA_LANTERN, CAKE, PUMPKIN_PIE, LEASH)
