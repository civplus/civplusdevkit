package plus.civ.meansofproduction

import org.bukkit.Bukkit
import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PluginInfo
import plus.civ.meansofproduction.event.RecipeRegisterEvent
import plus.civ.meansofproduction.listeners.OpenCraftingGUIInvButton
import plus.civ.meansofproduction.listeners.RecipeBookEntry
import plus.civ.meansofproduction.listeners.RecipeBookListGUI
import plus.civ.meansofproduction.recipe.Recipe
import plus.civ.meansofproduction.recipe.RecipeManager

@PluginInfo(
	name = "MeansOfProduction",
	description = "Adds a 5x5 crafting table with custom recipes",
	author = "Amelorate",
	depends = ["CivModCore"],
)
class MeansOfProduction: KotlinPlugin() {
	companion object {
		val instance: MeansOfProduction
			get() = instanceStorage!!
		private var instanceStorage: MeansOfProduction? = null
	}

	override fun onEnable() {
		super.onEnable()
		instanceStorage = this

		OpenCraftingGUIInvButton.runTaskTimer(this, 20, 20)

		server.scheduler.runTaskLater(this, {
			RecipeManager.registerRecipes()
		}, 1)
	}

	fun addUnlistedRecipe(recipe: Recipe, category: RecipeCategory? = null) {
		if (!RecipeManager.recipes.contains(recipe)) {
			val event = RecipeRegisterEvent(recipe, category)
			Bukkit.getPluginManager().callEvent(event)
			if (event.isCancelled) return

			RecipeManager.recipes.add(recipe)
		}
	}

	fun addRecipeBookRecipe(recipe: Recipe, category: RecipeCategory, recipeName: String = "") {
		addUnlistedRecipe(recipe)
		RecipeBookListGUI.entries.add(RecipeBookEntry(recipe, category, recipeName))
	}
}
