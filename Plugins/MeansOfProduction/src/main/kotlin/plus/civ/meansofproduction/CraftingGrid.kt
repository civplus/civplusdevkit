package plus.civ.meansofproduction

import org.bukkit.Material
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.recipe.Recipe
import plus.civ.meansofproduction.recipe.RecipeManager

data class CraftingGrid(var grid: MutableList<ItemStack>) {
	companion object {
		fun empty(): CraftingGrid {
			return CraftingGrid(mutableListOf(*Array(CRAFTING_TABLE_AREA) { ItemStack(Material.AIR) }))
		}
	}

	init {
		assert(grid.size == CRAFTING_TABLE_AREA)
	}

	operator fun get(x: Int, y: Int): ItemStack {
		return grid[y * CRAFTING_TABLE_SIZE + x].clone()
	}

	operator fun set(x: Int, y: Int, to: ItemStack) {
		grid[y * CRAFTING_TABLE_SIZE + x] = to
	}

	fun remove(item: ItemStack): Boolean {
		val grid = grid.map { it.clone() }.toMutableList()

		@Suppress("NAME_SHADOWING")
		var itemAmount = item.amount
		for (x in 0 until CRAFTING_TABLE_SIZE) {
			for (y in 0 until CRAFTING_TABLE_SIZE) {
				val there = grid[y * CRAFTING_TABLE_SIZE + x]
				if (there.isSimilar(item)) {
					val thereAmount = there.amount

					there.amount -= itemAmount
					itemAmount -= thereAmount

					if (itemAmount <= 0) {
						return true
					}
				}
			}
		}

		return false
	}

	val nullGrid: List<ItemStack?>
		get() {
			return grid.map { if (it.type == Material.AIR || it.amount <= 0) { null } else { it } }
		}

	fun setNullable(grid: List<ItemStack?>) {
		if (grid.size > CRAFTING_TABLE_AREA) {
			error("grid too large in setNullable: ${grid.size}, $grid")
		}

		this.grid = grid.map { it ?: ItemStack(Material.AIR) }.toMutableList()

		if (this.grid.size > CRAFTING_TABLE_AREA) {
			for (i in 0..(CRAFTING_TABLE_AREA - this.grid.size)) {
				this.grid.add(ItemStack(Material.AIR))
			}

			assert(this.grid.size == CRAFTING_TABLE_AREA)
		}
	}

	/**
	 * Goes through all the recipes and returns the first one that matches.
	 */
	fun evaluate(): Recipe? {
		for (recipe in RecipeManager.recipes) {
			val result = recipe.execute(this.clone())
			if (result != null) {
				return recipe
			}
		}

		return null
	}

	fun clone(): CraftingGrid {
		return CraftingGrid(grid.map { it.clone() }.toMutableList())
	}
}

const val CRAFTING_TABLE_SIZE = 5
const val CRAFTING_TABLE_AREA = CRAFTING_TABLE_SIZE * CRAFTING_TABLE_SIZE
