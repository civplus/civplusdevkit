package plus.civ.meansofproduction.recipe.mutator

import org.bukkit.enchantments.Enchantment
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.CRAFTING_TABLE_SIZE
import plus.civ.meansofproduction.CraftingGrid
import plus.civ.meansofproduction.recipe.Recipe
import plus.civ.meansofproduction.recipe.RecipeCraftInfo
import vg.civcraft.mc.civmodcore.expression.NBTExpression
import vg.civcraft.mc.civmodcore.expression.matches

/**
 * Reduces the durability of the item(s) that match toolMatcher by durabilityOuch
 */
class DurabilityOuchMutator(val mutates: Recipe, val toolMatcher: NBTExpression, val durabilityOuch: Short, val respectUnbreaking: Boolean = true): Recipe {
	override fun preview(): Pair<CraftingGrid, ItemStack>? {
		return mutates.preview()
	}

	override fun execute(grid: CraftingGrid): ItemStack? {
		return mutates.execute(grid)
	}

	override fun sideEffect(info: RecipeCraftInfo): ItemStack? {
		for (x in 0 until CRAFTING_TABLE_SIZE) {
			for (y in 0 until CRAFTING_TABLE_SIZE) {
				if (toolMatcher.matches(info.gridBefore[x, y])) {
					val item = info.gridBefore[x, y]

					if (!item.hasItemMeta() || !item.itemMeta.spigot().isUnbreakable) {
						val unbreakingLevel = item.getEnchantmentLevel(Enchantment.DURABILITY)
						val ouchChance = 1.0 / (unbreakingLevel + 1)
						val random = if (respectUnbreaking) Math.random() else 0.0
						if (ouchChance >= random) {
							if (item.durability == item.type.maxDurability) {
								continue
							}
							item.durability = (item.durability + durabilityOuch).toShort()
						}
					}

					info.gridAfter[x, y] = item
				}
			}
		}

		return super.sideEffect(info)
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as DurabilityOuchMutator

		if (mutates != other.mutates) return false
		if (toolMatcher != other.toolMatcher) return false
		if (durabilityOuch != other.durabilityOuch) return false
		if (respectUnbreaking != other.respectUnbreaking) return false

		return true
	}

	override fun hashCode(): Int {
		var result = mutates.hashCode()
		result = 31 * result + toolMatcher.hashCode()
		result = 31 * result + durabilityOuch
		result = 31 * result + respectUnbreaking.hashCode()
		return result
	}

	override val key: String
		get() = mutates.key
}
