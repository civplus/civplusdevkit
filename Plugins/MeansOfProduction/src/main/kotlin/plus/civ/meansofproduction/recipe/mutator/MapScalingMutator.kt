package plus.civ.meansofproduction.recipe.mutator

import net.minecraft.server.v1_8_R3.WorldMap
import org.bukkit.Bukkit
import org.bukkit.craftbukkit.v1_8_R3.map.CraftMapView
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.MapMeta
import org.bukkit.map.MapView.Scale.*
import plus.civ.meansofproduction.CraftingGrid
import plus.civ.meansofproduction.recipe.Recipe
import plus.civ.meansofproduction.recipe.RecipeCraftInfo
import vg.civcraft.mc.civmodcore.expression.NBTExpression
import vg.civcraft.mc.civmodcore.expression.matches

class MapScalingMutator(val mutates: Recipe, val map: NBTExpression): Recipe {
	override val key = mutates.key

	override fun preview(): Pair<CraftingGrid, ItemStack>? {
		return mutates.preview()
	}

	override fun execute(grid: CraftingGrid): ItemStack? {
		val maps = grid.grid.filter(map::matches)
		if (maps.size != 1) {
			return null
		}
		val map = maps[0]

		mutates.execute(grid) ?: return null

		val resultMeta = map.itemMeta
		if (resultMeta !is MapMeta) {
			return null
		}

		resultMeta.lore = listOf("(scaled)")

		map.itemMeta = resultMeta

		return map
	}

	override fun sideEffect(info: RecipeCraftInfo): ItemStack? {
		val grid = info.gridBefore

		val maps = grid.grid.filter(map::matches)
		if (maps.size != 1) {
			error("Multiple maps: Did the grid change since execute was called?")
		}
		val map = maps[0]

		mutates.sideEffect(info)

		val view = Bukkit.getMap(map.durability) as CraftMapView
		view.scale = when (view.scale) {
			CLOSEST -> CLOSE
			CLOSE -> NORMAL
			NORMAL -> FAR
			FAR -> FARTHEST
			FARTHEST -> FARTHEST
			else -> CLOSEST
		}

		val worldMap: WorldMap = view.javaClass.getDeclaredField("worldMap")
				.also { it.isAccessible = true }
				.get(view) as WorldMap

		worldMap.colors = ByteArray(16384)

		return map
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as MapScalingMutator

		if (mutates != other.mutates) return false
		if (map != other.map) return false
		if (key != other.key) return false

		return true
	}

	override fun hashCode(): Int {
		var result = mutates.hashCode()
		result = 31 * result + map.hashCode()
		result = 31 * result + key.hashCode()
		return result
	}
}
