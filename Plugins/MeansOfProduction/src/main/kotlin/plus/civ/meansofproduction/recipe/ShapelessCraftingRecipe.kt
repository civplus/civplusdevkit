package plus.civ.meansofproduction.recipe

import org.bukkit.Material
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.CRAFTING_TABLE_SIZE
import plus.civ.meansofproduction.CraftingGrid
import vg.civcraft.mc.civmodcore.expression.NBTExpression
import vg.civcraft.mc.civmodcore.expression.getAmount
import vg.civcraft.mc.civmodcore.expression.matches
import vg.civcraft.mc.civmodcore.expression.solveToItemStack

open class ShapelessCraftingRecipe(val ingredients: List<NBTExpression>, override val key: String, val result: ItemStack): Recipe {
	private enum class PreviewSpiralDirection(val modX: Int, val modY: Int) {
		UP(0, -1),
		DOWN(0, 1),
		LEFT(-1, 0),
		RIGHT(1, 0);

		val clockwise: PreviewSpiralDirection
		get() = when(this) {
			UP -> RIGHT
			DOWN -> LEFT
			LEFT -> UP
			RIGHT -> DOWN
		}
	}

	override fun preview(): Pair<CraftingGrid, ItemStack>? {
		val grid = CraftingGrid.empty()

		var x = 2
		var y = 2
		var i = 0
		var direction = PreviewSpiralDirection.LEFT

		while (ingredients.size > i) {
			grid[x, y] = ingredients[i++].solveToItemStack()

			val newDirection = direction.clockwise
			val newX = x + newDirection.modX
			val newY = y + newDirection.modY

			if (grid[newX, newY].type == Material.AIR) {
				direction = newDirection
			}

			x += direction.modX
			y += direction.modY
		}

		return Pair(grid, result.clone())
	}

	override fun execute(grid: CraftingGrid): ItemStack? {
		/*
		 * The algorithm used for Shapeless Recipes is like so:
		 *
		 * Loop over all the ingredients passed to this recipe.
		 *
		 * For every ingredient, loop over all the slots in the crafting grid.
		 *
		 * If the slot is empty, skip it.
		 * If the slot has been matched by another ingredient, skip it.
		 * If the item in the slot does not match the ItemExpression of the ingredient, skip it.
		 * If the item in the slot does match the ItemExpression, subtract the amount of the ItemExpression from the
		 *   item's amount, and mark the slot that it has been matched by this ItemExpression, and go to the next ingredient.
		 *
		 * If no items in the slots matched this ingredient, return that the items in the crafting grid do not match this recipe.
		 *
		 * If all of the ingredients matched an item:
		 *
		 * Loop over all the slots, and if there is a item in a slot that has not been matched,
		 * return that this is not our recipe.
		 *
		 * Create a copy of the list of ingredients.
		 * For every slot that has been matched by an ingredient, remove that ingredient from the copy.
		 * If the copy is not empty once all slots have been looped over, return that this is not our recipe.
		 *
		 * If the end of this algorithm has been reached, return the result of this recipe.
		 */

		// We start by creating a list of the slots that have been successfully matched for an item in the recipe.
		// This prevents us from using two ItemExpressions on the same slot.
		// This isn't a boolean because we check over it at the end to make extra sure that every item in the grid
		// has been matched by exactly one expression
		val matches = Array<NBTExpression?>(25) { null }

		// We loop over all of the ingredients, with the xy grid on the inside
		ingredient@for (ingredient in ingredients) {
			// We use until instead of .. because until is not inclusive over the maximum value
      		// Using .. would go outside the bounds of the crafting grid
			for (x in 0 until CRAFTING_TABLE_SIZE) {
				for (y in 0 until CRAFTING_TABLE_SIZE) {
					// save the item at the position of the grid for conveince's sake
					val item = grid[x, y]

					// If this item here has been matched before, skip it.
					if (matches[y * CRAFTING_TABLE_SIZE + x] != null) {
						continue
					}

					// If the item matches the ItemExpression we're checking, decrement its amount and
					// set that we've matched it in the matches array
					if (ingredient.matches(item)) {
						// Get the amount that matches
						val amount = ingredient.getAmount()

						// Decrement the amount only if it's not an empty slot
						// We might be able to get away with not checking if it's AIR, but best to be sure.
						if (item.type != Material.AIR) {
							// .also allows us to change the amount as a one-liner
							grid[x, y] = item.also { it.amount -= amount }
						}

						// Set that this ItemExpression matched this slot
						matches[y * CRAFTING_TABLE_SIZE + x] = ingredient

						// We've successfully matched with this ItemExpression, so go to the next ItemExpression.
						// We don't care about the x and y for loops.
						// If we didn't use this continue@loop syntax, the bug would be that one ItemExpression would
						// try to match over multiple items and decrement their amount.
						continue@ingredient
					}

					// if it doesn't match, go onto the next slot / ingredient
				}
			}

			 // no item in the grid matched this ingredient, this isn't the recipe
			return null
		}

		// check all items not matched over, if they contain an item that wasn't matched: this isn't our recipe, return
		for (i in matches.indices) {
			if (matches[i] == null) {
				if (grid.grid[i].type != Material.AIR) {
					return null
				}
			}
		}

		// We make a new list of ingredients in order to be able to mutate it.
		val notMatchedIngredients = mutableListOf(*ingredients.toTypedArray())

		// If a ItemExpression matched in the slot, remove it from the list of ingredients
		for (i in matches.indices) {
			if (matches[i] != null) {
				notMatchedIngredients.remove(matches[i])
			}
		}

		if (notMatchedIngredients.isNotEmpty()) {
			// there were ingredients that were not used in this recipe: it's not our recipe
			return null
		}

		// This is our recipe. Return the proper result ItemStack.
		return result.clone()
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as ShapelessCraftingRecipe

		if (ingredients != other.ingredients) return false
		if (key != other.key) return false
		if (result != other.result) return false

		return true
	}

	override fun hashCode(): Int {
		var result1 = ingredients.hashCode()
		result1 = 31 * result1 + key.hashCode()
		result1 = 31 * result1 + result.hashCode()
		return result1
	}
}
