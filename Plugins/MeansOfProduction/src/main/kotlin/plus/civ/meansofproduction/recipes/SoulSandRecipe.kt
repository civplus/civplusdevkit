package plus.civ.meansofproduction.recipes

import org.bukkit.Material.*
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapelessCraftingRecipe

object SoulSandRecipe: RecipeProvider {
	override fun register() {
		val wart = item(NETHER_STALK, 4)
		val rack = item(NETHERRACK, 8)
		val gravel = item(GRAVEL, 8)
		val ingredients = listOf(wart, rack, gravel)
		addRecipeBookRecipe(ShapelessCraftingRecipe(ingredients, "soul_sand", ItemStack(SOUL_SAND, 16)), RecipeCategory.RESOURCES)
	}
}
