package plus.civ.meansofproduction.recipes

import org.bukkit.Material.*
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe

object RailsRecipe: RecipeProvider {
	override fun register() {
		poweredRail()
		ironRail()
	}

	fun poweredRail() {
		val gold = item(GOLD_INGOT)
		val stick = item(STICK)
		val carrots = item(CARROT_ITEM, 16)
		val redstone = item(REDSTONE_BLOCK)
		val ingredients = mutableMapOf(Pair('_', gold), Pair('|', stick), Pair('c', carrots), Pair('r', redstone))
		val shape = """
			_c_
			_|_
			_r_
		""".trimIndent()
		val altShape = """
			_c_
			_|_
			_r_
		""".trimIndent()
		addRecipeBookRecipe(ShapedCraftingRecipe(ingredients, shape, "powered_rail", ItemStack(POWERED_RAIL, 16)), RecipeCategory.BUILDING)
		addUnlistedRecipe(ShapedCraftingRecipe(ingredients, altShape, "powered_rail_alt", ItemStack(POWERED_RAIL, 16)))
	}

	fun ironRail() {
		val iron = item(IRON_INGOT)
		val stick = item(STICK, 1)
		val carrots = item(CARROT_ITEM, 16)
		val potatos = item(POTATO_ITEM, 32)
		val ingredients = mutableMapOf(Pair('_', iron), Pair('|', stick), Pair('c', carrots), Pair('p', potatos))
		val shape = """
			_c_
			_|_
			_p_
		""".trimIndent()
		val altShape = """
			_p_
			_|_
			_c_
		""".trimIndent()
		addRecipeBookRecipe(ShapedCraftingRecipe(ingredients, shape, "rails", ItemStack(RAILS, 64)), RecipeCategory.BUILDING)
		addUnlistedRecipe(ShapedCraftingRecipe(ingredients, altShape, "rails_alt", ItemStack(RAILS, 64)))
	}
}
