package plus.civ.meansofproduction.recipes

import org.bukkit.Material.BOAT
import org.bukkit.Material.WOOD
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.recipe.RecipeCraftInfo
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe

object BoatBuggedRecipe: RecipeProvider {
	override fun register() {
		val wood = item(WOOD)
		val shape = """
			w w
			www
		""".trimIndent()
		val ingredients = mutableMapOf(Pair('w', wood))
		val boat = ItemStack(BOAT)
		val boatMeta = boat.itemMeta
		boatMeta.lore = listOf("Boats are a little buggy in 1.8.", "You should craft a Speedo instead")
		boat.itemMeta = boatMeta
		val recipe = object : ShapedCraftingRecipe(ingredients, shape, "boat_bugged", boat) {
			override fun sideEffect(info: RecipeCraftInfo): ItemStack {
				return ItemStack(BOAT)
			}
		}
		addUnlistedRecipe(recipe)
	}
}
