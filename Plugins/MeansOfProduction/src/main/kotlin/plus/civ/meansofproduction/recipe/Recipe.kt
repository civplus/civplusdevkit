package plus.civ.meansofproduction.recipe

import org.bukkit.Material
import org.bukkit.entity.HumanEntity
import org.bukkit.inventory.InventoryView
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.CraftingGrid
import vg.civcraft.mc.civmodcore.extensions.giveItemOrDropOnGround
import java.lang.Integer.min

interface Recipe {
	/**
	 * Shows a preview of how the recipe can be crafted.
	 */
	fun preview(): Pair<CraftingGrid, ItemStack>?

	fun execute(grid: CraftingGrid): ItemStack?

	/**
	 * Ran after the recipe has been successfully crafted.
	 *
	 * The player has not yet been given the result of this craft.
	 *
	 * @return What the recipe should actually craft, or null if it should craft the result of execute()
	 */
	fun sideEffect(info: RecipeCraftInfo): ItemStack? {
		val bucketList = listOf(Material.WATER_BUCKET, Material.LAVA_BUCKET, Material.MILK_BUCKET)

		var matchCount = 0

		for (item in info.gridBefore.grid) {
			if (item.type in bucketList) {
				matchCount += 1 // giving back one bucket isn't perfect, because if there is a recipe that takes 2 stacked buckets, this will eat one of the buckets
			}
		}

		if (matchCount == 0) {
			return null
		}

		val buckets = mutableListOf<ItemStack>()
		do {
			buckets.add(ItemStack(Material.BUCKET, min(matchCount, 16)))
			matchCount -= 16
		} while (matchCount >= 0)

		info.player.giveItemOrDropOnGround(*buckets.toTypedArray())

		return null
	}

	val key: String

	/**
	 * The icon that's used to represent this recipe in the crafting book
	 *
	 * If left as null the recipe book will instead use the result of the preview
	 */
	val icon: ItemStack?
		get() = null
}

data class RecipeCraftInfo(val player: HumanEntity,
						   val inventoryView: InventoryView,
						   val gridBefore: CraftingGrid,
						   val gridAfter: CraftingGrid,
						   val result: ItemStack)
