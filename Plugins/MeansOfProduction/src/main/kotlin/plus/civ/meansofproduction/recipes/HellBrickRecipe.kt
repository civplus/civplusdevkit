package plus.civ.meansofproduction.recipes

import org.bukkit.Material.*
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapelessCraftingRecipe
import plus.civ.meansofproduction.recipe.mutator.DurabilityOuchMutator

object HellBrickRecipe: RecipeProvider {
	override fun register() {
		val wart = item(NETHER_STALK, 4)
		val flintSteel = item(FLINT_AND_STEEL)
		val brick = item(CLAY_BRICK, 16)
		val ingredients = listOf(wart, flintSteel, brick)
		val recipe = ShapelessCraftingRecipe(ingredients, "hell_brick", ItemStack(NETHER_BRICK_ITEM, 16))
		addRecipeBookRecipe(DurabilityOuchMutator(recipe, flintSteel, 1), RecipeCategory.RESOURCES)
	}
}
