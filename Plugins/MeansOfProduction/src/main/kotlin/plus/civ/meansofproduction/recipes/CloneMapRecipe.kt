package plus.civ.meansofproduction.recipes

import org.bukkit.Material.EMPTY_MAP
import org.bukkit.Material.MAP
import plus.civ.meansofproduction.recipe.CloneItemRecipe
import plus.civ.meansofproduction.recipe.RecipeProvider

object CloneMapRecipe: RecipeProvider {
	override fun register() {
		val emptyMap = item(EMPTY_MAP)
		val fullMap = item(MAP)
		addUnlistedRecipe(CloneItemRecipe(listOf(emptyMap, fullMap), "cloneMap", fullMap))
	}
}
