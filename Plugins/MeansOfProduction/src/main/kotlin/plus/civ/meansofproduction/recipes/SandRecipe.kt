package plus.civ.meansofproduction.recipes

import org.bukkit.Material.*
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe

object SandRecipe: RecipeProvider {
	override fun register() {
		val reeds = item(SUGAR_CANE)
		val dirt = item(DIRT, 16)
		val cactus = item(CACTUS)
		val cobble = item(COBBLESTONE, 64)
		val ingredients = mutableMapOf(Pair('r', reeds), Pair('d', dirt), Pair('c', cactus), Pair('s', cobble))
		val shape = """
			rdr
			ccc
			rsr
		""".trimIndent()
		addRecipeBookRecipe(ShapedCraftingRecipe(ingredients, shape, "sand", ItemStack(SAND, 64)), RecipeCategory.RESOURCES)
	}
}
