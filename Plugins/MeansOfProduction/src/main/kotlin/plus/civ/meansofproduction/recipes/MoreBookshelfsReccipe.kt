package plus.civ.meansofproduction.recipes

import org.bukkit.Material
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe

object MoreBookshelfsReccipe: RecipeProvider {
	override fun register() {
		val wood = item(Material.WOOD)
		val book = item(Material.BOOK)

		val ingredients = mutableMapOf(Pair('#', wood), Pair('b', book))
		val shape = """
			###
			bbb
			###
		""".trimIndent()

		val recipe = ShapedCraftingRecipe(ingredients, shape, "better_bookshelf", ItemStack(Material.BOOKSHELF, 16))
		addUnlistedRecipe(recipe)
	}
}
