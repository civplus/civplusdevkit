package plus.civ.meansofproduction.recipe

import org.bukkit.Material
import org.bukkit.enchantments.Enchantment
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.CRAFTING_TABLE_SIZE
import plus.civ.meansofproduction.CraftingGrid
import vg.civcraft.mc.civmodcore.expression.NBTExpression
import vg.civcraft.mc.civmodcore.expression.matches
import vg.civcraft.mc.civmodcore.expression.solveToItemStack

class EnchantmentAddRecipe(val enchantHolder: NBTExpression, val catalystItem: NBTExpression, val catalystCount: Int, override val key: String): Recipe {
	override fun preview(): Pair<CraftingGrid, ItemStack>? {
		val grid = CraftingGrid.empty()

		val catalyst = catalystItem.solveToItemStack()
		catalyst.amount = catalystCount
		val holder = enchantHolder.solveToItemStack()
		val tool = ItemStack(Material.DIAMOND_HOE)

		val enchantedTool = ItemStack(Material.DIAMOND_HOE)
		enchantedTool.addEnchantment(Enchantment.DURABILITY, 3)

		grid[1, 2] = holder
		grid[2, 2] = catalyst
		grid[3, 2] = tool

		return Pair(grid, enchantedTool)
	}

	override fun execute(grid: CraftingGrid): ItemStack? {
		var resultItem = ItemStack(Material.AIR)
		val holderItems = mutableListOf<ItemStack>()
		var catalystCount = 0

		for (x in 0 until CRAFTING_TABLE_SIZE) {
			for (y in 0 until CRAFTING_TABLE_SIZE) {
				val item = grid[x, y]
				if (item.type == Material.AIR) {
					continue
				}
				if (catalystItem.matches(item)) {
					catalystCount += item.amount
				} else if (enchantHolder.matches(item)) {
					holderItems.add(item)
				} else {
					if (resultItem.type == Material.AIR) {
						resultItem = item
					} else {
						return null
					}
				}

				grid[x, y] = ItemStack(Material.AIR)
			}
		}

		if (resultItem.type == Material.AIR) {
			return null
		}

		if (holderItems.isEmpty()) {
			return null
		}

		if (catalystCount != this.catalystCount) {
			return null
		}

		for (holderItem in holderItems) {
			resultItem.addUnsafeEnchantments(holderItem.enchantments)
		}

		return resultItem
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as EnchantmentAddRecipe

		if (enchantHolder != other.enchantHolder) return false
		if (catalystItem != other.catalystItem) return false
		if (catalystCount != other.catalystCount) return false
		if (key != other.key) return false

		return true
	}

	override fun hashCode(): Int {
		var result = enchantHolder.hashCode()
		result = 31 * result + catalystItem.hashCode()
		result = 31 * result + catalystCount
		result = 31 * result + key.hashCode()
		return result
	}
}
