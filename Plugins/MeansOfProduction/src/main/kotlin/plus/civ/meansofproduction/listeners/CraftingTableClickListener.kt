package plus.civ.meansofproduction.listeners

import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent
import plus.civ.meansofproduction.MeansOfProduction
import plus.civ.meansofproduction.showCraftingGUI

object CraftingTableClickListener: Listener {
	@EventHandler
	fun onRightClick(event: PlayerInteractEvent) {
		if (event.action != Action.RIGHT_CLICK_BLOCK) {
			return
		}

		if (event.clickedBlock?.type != Material.WORKBENCH) {
			return
		}

		if (event.player.isSneaking) {
			return
		}

		event.isCancelled = true

		MeansOfProduction.instance.server.scheduler.runTask(MeansOfProduction.instance) {
			event.player.showCraftingGUI()
		}
	}
}
