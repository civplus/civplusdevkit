package plus.civ.meansofproduction.recipes

import org.bukkit.Material
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe

object MoreIronFenceRecipe: RecipeProvider {
	override fun register() {
		val ironIngot = item(Material.IRON_INGOT)

		val ingredients = mutableMapOf(Pair('-', ironIngot))
		val shape = """
			---
			---
		""".trimIndent()

		val recipe = ShapedCraftingRecipe(ingredients, shape, "better_iron_fence", ItemStack(Material.IRON_FENCE, 64))
		addUnlistedRecipe(recipe)
	}
}
