package plus.civ.meansofproduction.recipe

import net.minecraft.server.v1_8_R3.NBTTagByte
import org.bukkit.Material
import plus.civ.meansofproduction.MeansOfProduction
import plus.civ.meansofproduction.RecipeCategory
import vg.civcraft.mc.civmodcore.expression.NBTExpression
import vg.civcraft.mc.civmodcore.expression.NumberMatcher
import vg.civcraft.mc.civmodcore.expression.amount
import vg.civcraft.mc.civmodcore.expression.id

interface RecipeProvider {
	fun register()

	fun item(material: Material, amount: Byte): NBTExpression = NBTExpression.empty().id(material).amount(amount)
	fun item(material: Material): NBTExpression = NBTExpression.empty().id(material).amount(1)
	fun NBTExpression.amount(amount: Byte): NBTExpression = amount(NumberMatcher.greaterThanOrEqual(NBTTagByte(amount)))

	fun addUnlistedRecipe(recipe: Recipe) {
		MeansOfProduction.instance.addUnlistedRecipe(recipe)
	}

	fun addRecipeBookRecipe(recipe: Recipe, category: RecipeCategory, recipeName: String = "") {
		MeansOfProduction.instance.addRecipeBookRecipe(recipe, category, recipeName)
	}
}
