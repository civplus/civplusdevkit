package plus.civ.meansofproduction.recipe

import org.bukkit.Material
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.CraftingGrid
import vg.civcraft.mc.civmodcore.expression.NBTExpression
import vg.civcraft.mc.civmodcore.expression.matches

class CloneItemRecipe(ingredients: List<NBTExpression>, key: String, val itemToClone: NBTExpression) : ShapelessCraftingRecipe(ingredients, key, ItemStack(Material.WRITTEN_BOOK)) {
	override fun execute(grid: CraftingGrid): ItemStack? {
		val book: ItemStack = grid.grid.filter { itemToClone.matches(it) }.getOrElse(0) { return null }.clone()
		super.execute(grid) ?: return null

		book.amount = 2
		return book
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false
		if (!super.equals(other)) return false

		other as CloneItemRecipe

		if (itemToClone != other.itemToClone) return false

		return true
	}

	override fun hashCode(): Int {
		var result = super.hashCode()
		result = 31 * result + itemToClone.hashCode()
		return result
	}
}
