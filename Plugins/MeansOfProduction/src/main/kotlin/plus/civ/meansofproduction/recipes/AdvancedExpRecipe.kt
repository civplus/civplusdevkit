package plus.civ.meansofproduction.recipes

import org.bukkit.Material.*
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.*
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe
import plus.civ.remnant.items.Relic

object AdvancedExpRecipe: RecipeProvider {
	override fun register() {
		val potato = item(POTATO_ITEM, 32)
		val cookie = item(COOKIE, 32)
		val pumpkin = item(PUMPKIN, 32)
		val melon = item(MELON_BLOCK, 32)
		val carrot = item(CARROT_ITEM, 32)
		val cake = item(CAKE)
		val wart = item(NETHER_STALK, 32)
		val hay = item(HAY_BLOCK, 16)
		val ingredients = mutableMapOf(Pair('p', potato), Pair('c', cookie), Pair('u', pumpkin),
				Pair('m', melon), Pair('a', carrot), Pair('k', cake),
				Pair('w', wart), Pair('h', hay), Pair('r', Relic.expression))
		val shape = """
			  k  
			kprck
			kuhmk
			karwk
			  k  
		""".trimIndent()
		val altshape = """
			  k  
			kprck
			kmhuk
			karwk
			  k  
		""".trimIndent()
		addRecipeBookRecipe(ShapedCraftingRecipe(ingredients, shape, "advanced_exp", ItemStack(EMERALD, 48)),
				RecipeCategory.EXP, "Make ExP from Crops (Advanced)")
		addUnlistedRecipe(ShapedCraftingRecipe(ingredients, altshape, "advanced_exp_alt", ItemStack(EMERALD, 48)))
	}
}
