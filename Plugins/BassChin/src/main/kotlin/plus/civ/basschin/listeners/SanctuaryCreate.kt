package plus.civ.basschin.listeners

import org.bukkit.ChatColor
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockPlaceEvent
import plus.civ.basschin.Config
import plus.civ.basschin.database.Sanctuary
import plus.civ.basschin.sanctuaryExplainBook
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.getBlockData
import vg.civcraft.mc.citadel.PlayerState
import vg.civcraft.mc.citadel.ReinforcementMode
import vg.civcraft.mc.citadel.events.ReinforcementCreationEvent
import vg.civcraft.mc.civmodcore.database.serializable
import vg.civcraft.mc.civmodcore.extensions.blockCoordsString
import vg.civcraft.mc.civmodcore.extensions.giveItemOrDropOnGround

class SanctuaryCreate: Listener {
	@EventHandler(ignoreCancelled = true)
	fun blockPlace(event: ReinforcementCreationEvent) {
		if (event.block.location.getBlockData<SanctuaryBlock.SanctuaryTag>().isEmpty()) return

		val sanctuary = Sanctuary(whoPlaced = event.player.uniqueId, location = event.block.location.serializable, isWIAOTUpdated = true)
		sanctuary.saveLater()

		Sanctuary.sanctuaries.add(sanctuary)

		event.player.sendMessage("${ChatColor.AQUA}Created a new sanctuary at ${event.block.location.blockCoordsString}. Add some fuel to power it up.")
	}
}
