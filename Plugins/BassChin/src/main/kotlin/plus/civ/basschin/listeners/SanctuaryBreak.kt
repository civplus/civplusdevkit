package plus.civ.basschin.listeners

import org.bukkit.ChatColor
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.block.BlockExplodeEvent
import plus.civ.basschin.Config
import plus.civ.basschin.database.Sanctuary
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.getBlockData
import vg.civcraft.mc.civmodcore.expression.matches
import vg.civcraft.mc.civmodcore.extensions.blockCoordsString
import vg.civcraft.mc.civmodcore.extensions.giveItemOrDropOnGround
import kotlin.math.floor
import kotlin.math.min

class SanctuaryBreak: Listener {
	@EventHandler(ignoreCancelled = true)
	fun sanctuaryBreak(event: BlockBreakEvent) {
		if (event.block.location.getBlockData<SanctuaryBlock.SanctuaryTag>().isEmpty()) {
			return
		}

		val sanctuary = Sanctuary.sanctuaries.firstOrNull { it.location.asLocation() == event.block.location } ?: return
		giveRefund(sanctuary, event.player)
		sanctuary.power = 0.0
		Sanctuary.sanctuaries.remove(sanctuary)
		sanctuary.deleteLater()
		event.player.sendMessage("${ChatColor.AQUA}Destroyed ${sanctuary.name} at ${sanctuary.location.asLocation().blockCoordsString}")
	}

	fun giveRefund(sanctuary: Sanctuary, player: Player) {
		val power = sanctuary.power * Config.refundPercentage
		var powerPerItem = 0.0

		for ((itemPower, item) in Config.powerItems) {
			if (item.matches(Config.refundItem)) {
				powerPerItem = itemPower
				break
			}
		}

		var items = floor(power / powerPerItem).toInt()
		while (items > 0) {
			val giveItem = Config.refundItem.clone()
			val giveAmount = min(items, giveItem.maxStackSize)
			items -= giveAmount
			giveItem.amount = giveAmount
			player.giveItemOrDropOnGround(giveItem)
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
	fun sanctuaryExplode(event: BlockExplodeEvent) {
		for (block in event.blockList()) {
			if (block.location.getBlockData<SanctuaryBlock.SanctuaryTag>().isEmpty()) {
				continue
			}

			val sanctuary = Sanctuary.sanctuaries.firstOrNull { it.location.asLocation() == block.location } ?: continue
			sanctuary.power = 0.0
			Sanctuary.sanctuaries.remove(sanctuary)
			sanctuary.deleteLater()
		}
	}
}
