package plus.civ.basschin.listeners

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.inventory.ItemStack
import plus.civ.basschin.Config
import plus.civ.basschin.database.Sanctuary
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.getBlockData
import vg.civcraft.mc.civmodcore.expression.matches
import kotlin.math.roundToInt

class SanctuaryPower: Listener {
	@EventHandler(ignoreCancelled = true)
	fun playerInteract(event: PlayerInteractEvent) {
		if (event.action != Action.RIGHT_CLICK_BLOCK) {
			return
		}

		if (event.clickedBlock.location.getBlockData<SanctuaryBlock.SanctuaryTag>().isEmpty()) {
			return
		}

		val sanctuary = Sanctuary.sanctuaries.firstOrNull { it.location.asLocation() == event.clickedBlock.location }
		if (sanctuary == null) {
			return
		}

		val item = event.item ?: return
		for ((power, expression) in Config.powerItems) {
			if (!expression.matches(item)) {
				continue
			}

			val powerIncrease = power * item.amount
			val oldRadius = sanctuary.radius

			sanctuary.power += powerIncrease
			sanctuary.saveLater()
			val newRadius = sanctuary.radius
			val increase = (newRadius - oldRadius).roundToInt()
			event.player.itemInHand = ItemStack(Material.AIR)
			event.player.sendMessage("${ChatColor.AQUA}Added $increase blocks of radius to ${sanctuary.name}")
			event.isCancelled = true
			break
		}
	}
}
