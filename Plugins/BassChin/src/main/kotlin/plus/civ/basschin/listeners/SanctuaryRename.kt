package plus.civ.basschin.listeners

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent
import plus.civ.basschin.Config
import plus.civ.basschin.database.Sanctuary
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.getBlockData
import vg.civcraft.mc.civmodcore.extensions.prettyName

class SanctuaryRename: Listener {
	@EventHandler(ignoreCancelled = true)
	fun interact(event: PlayerInteractEvent) {
		if (event.action != Action.RIGHT_CLICK_BLOCK) {
			return
		}

		if (event.clickedBlock.location.getBlockData<SanctuaryBlock.SanctuaryTag>().isEmpty()) {
			return
		}

		val sanctuary = Sanctuary.sanctuaries.firstOrNull { it.location.asLocation() == event.clickedBlock.location }
		if (sanctuary == null) {
			event.player.sendMessage("${ChatColor.RED}That beacon isn't a Sanctuary. Try breaking and replacing it.")
			return
		}

		val item = event.item ?: return
		if (item.type != Config.sanctuaryRenameItem) {
			return
		}

		val name = item.itemMeta.displayName
		if (name == null || name == "") {
			event.player.sendMessage("${ChatColor.RED}You need to rename the ${item.type.prettyName} before you can use it to rename a Sanctuary.")
			return
		}

		sanctuary.name = name
		sanctuary.saveLater()

		item.amount -= 1
		if (item.amount == 0) {
			item.type = Material.AIR
		}
		event.player.itemInHand = item
		event.isCancelled = true
	}
}
