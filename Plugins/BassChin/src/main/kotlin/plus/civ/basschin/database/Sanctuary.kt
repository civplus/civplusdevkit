package plus.civ.basschin.database

import com.fasterxml.jackson.annotation.JsonIgnore
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.block.Block
import org.bukkit.entity.Player
import org.bukkit.scheduler.BukkitTask
import org.litote.kmongo.*
import plus.civ.basschin.BassChin
import plus.civ.basschin.Config
import plus.civ.basschin.listeners.SanctuaryBlock
import plus.civ.layerafterlayer.model.Group
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.addBlockData
import vg.civcraft.mc.citadel.Citadel
import vg.civcraft.mc.citadel.reinforcement.PlayerReinforcement
import vg.civcraft.mc.civmodcore.database.SerializableLocation
import vg.civcraft.mc.civmodcore.extensions.safeDistance
import vg.civcraft.mc.namelayer.GroupManager
import java.time.Instant
import java.util.*
import kotlin.math.PI
import kotlin.math.sqrt

data class Sanctuary(
		val _id: Id<Sanctuary> = newId(),
		val whoPlaced: UUID,
		var power: Double = 0.0,
		val location: SerializableLocation,
		var name: String = "A Sanctuary",
		/**
		 * If the sanctuary has been updated to have a WhatIsAnOcttree block tag placed on its location
		 */
		var isWIAOTUpdated: Boolean = false,
) {
	companion object {
		val collection = Database.database.getCollection<Sanctuary>("Sanctuary")
		var sanctuaries: MutableSet<Sanctuary> = collection.find().toMutableSet()

		fun getClosestSanctuary(location: Location): Sanctuary? {
			var closestSanctuary: Sanctuary? = null

			for (sanctuary in sanctuaries) {

				sanctuary.migrateToUseSanctuaryBlock()

				if (closestSanctuary == null) {
					closestSanctuary = sanctuary
					continue
				}

				if (sanctuary.location.asLocation().safeDistance(location) < closestSanctuary.location.asLocation().safeDistance(location)) {
					closestSanctuary = sanctuary
				}
			}

			return closestSanctuary
		}

		fun getSanctuariesIntersecting(location: Location): List<Sanctuary> {
			return sanctuaries.filter { it.isInside(location) }
		}

		fun getSanctuariesIntersectingCity(location: Location): List<Sanctuary> {
			return sanctuaries.filter { it.isInsideCityArea(location) }
		}

		fun getSanctuariesIntersectingVault(location: Location): List<Sanctuary> {
			return sanctuaries.filter { it.isInsideVaultArea(location) }
		}

		fun areaToRadius(area: Double): Double = sqrt(area / PI)
	}

	fun migrateToUseSanctuaryBlock() {
		if (isWIAOTUpdated) return
		if (!location.asLocation().world.isChunkLoaded(location.x.toInt() shr 4, location.z.toInt() shr 4)) return

		location.asLocation().addBlockData(SanctuaryBlock.SanctuaryTag())
		isWIAOTUpdated = true
		saveLater()
	}

	private var saveTask: BukkitTask? = null

	fun saveLater() {
		saveTask?.cancel()
		saveTask = Bukkit.getScheduler().runTaskAsynchronously(BassChin.instance) {
			saveBlocking()
		}
	}

	fun saveBlocking() {
		collection.updateOneById(_id, this, upsert())
	}

	fun deleteLater() {
		saveTask?.cancel()
		saveTask = Bukkit.getScheduler().runTaskAsynchronously(BassChin.instance) {
			deleteBlocking()
		}
	}

	fun deleteBlocking() {
		collection.deleteOneById(_id)
	}

	fun isInside(location: Location, radius: Double = this.radius): Boolean {
		val clone = location.clone()
		clone.y = 0.0

		val thisLocationClone = this.location.asLocation()
		thisLocationClone.y = 0.0

		return thisLocationClone.safeDistance(clone) < radius
	}

	fun isInsideCityArea(location: Location): Boolean {
		return isInside(location)
	}

	fun isInsideVaultArea(location: Location): Boolean {
		return isInside(location, areaToRadius(area * Config.vaultAreaPercentage))
	}

	private val damageCooldown = mutableMapOf<UUID, Instant>()

	fun damage(amount: Double, player: Player) {
		val lastDamage = damageCooldown.getOrDefault(player.uniqueId, Instant.EPOCH)
		if (lastDamage.plusMillis(Config.damageCooldownMilliseconds).isAfter(Instant.now())) {
			return
		}

		power -= amount
		if (power < 0.0) {
			power = 0.0
		}
	}

	@get:JsonIgnore
	val group: Group?
		get() {
			val reinforcement = Citadel.getReinforcementManager().getReinforcement(location.asLocation())
			if (reinforcement !is PlayerReinforcement) {
				return null
			}

			return Group.get(reinforcement.groupId)
		}

	@get:JsonIgnore
	val namelayerGroup: vg.civcraft.mc.namelayer.group.Group?
		get() {
			return GroupManager.getGroup(group?.id ?: return null)
		}

	@get:JsonIgnore
	val radius: Double
		get() = areaToRadius(area)

	@get:JsonIgnore
	val area: Double
		get() = power * Config.areaBlocksPerPower

	@get:JsonIgnore
	val block: Block
		get() = location.asLocation().block
}
