package plus.civ.basschin.listeners

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.inventory.BeaconInventory
import org.bukkit.inventory.ItemStack
import plus.civ.basschin.BassChin
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.getBlockData
import plus.civ.whatisanocttree.chunk.tags.ImmovableBlock
import plus.civ.whatisanocttree.chunk.tags.ItemSerializable
import vg.civcraft.mc.civmodcore.event.BlockDropItemEvent
import vg.civcraft.mc.civmodcore.expression.damage
import vg.civcraft.mc.civmodcore.extensions.location

object SanctuaryBlock: Listener, RecipeProvider {
	class SanctuaryTag: ItemSerializable, ImmovableBlock {
		override fun onBecomeItem(event: BlockDropItemEvent): Boolean {
			if (event.droppedItems != null && event.droppedItems!!.isNotEmpty()) {
				BassChin.instance.logger.warning("Breaking a sanctuary with drops: ${event.droppedItems}! Overriding them.")
			}

			event.droppedItems = mutableListOf(sanctuaryIten.clone())

			return false
		}
	}

	val sanctuaryIten = run {
		val result = ItemStack(Material.BEACON)
		val meta = result.itemMeta
		meta.displayName = "${ChatColor.AQUA}Sanctuary"
		result.itemMeta = meta
		result
	}

	override fun register() {
		val emerald = item(Material.EMERALD)
		val grass = item(Material.GRASS)
		val stone = item(Material.STONE).damage(0)
		val enchantingTable = item(Material.ENCHANTMENT_TABLE)
		val rose = item(Material.RED_ROSE).damage(0)
		val yellowFlower = item(Material.YELLOW_FLOWER).damage(0)

		val ingredients = mutableMapOf(Pair('e', emerald), Pair('#', grass), Pair('=', stone), Pair('.', enchantingTable), Pair('[', rose), Pair(']', yellowFlower))
		val recipe = """
			eeeee
			e[.]e
			e###e
			e===e
			eeeee
		""".trimIndent()
		val altRecipe = """
			eeeee
			e].[e
			e###e
			e===e
			eeeee
		""".trimIndent()

		addRecipeBookRecipe(ShapedCraftingRecipe(ingredients, recipe, "sanctuary", sanctuaryIten.clone()),
			RecipeCategory.MACGUFFINS)
		addUnlistedRecipe(ShapedCraftingRecipe(ingredients, altRecipe, "sanctuary_alt", sanctuaryIten.clone()))
	}

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	fun stopOpeningBeaconInventory(event: PlayerInteractEvent) {
		if (event.action != Action.RIGHT_CLICK_BLOCK) return
		if (event.clickedBlock.location.getBlockData<SanctuaryTag>().isEmpty()) return

		event.isCancelled = true
	}

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	fun stopBeaconStartup(event: InventoryClickEvent) {
		if (event.inventory !is BeaconInventory) return
		if (event.inventory.location?.getBlockData<SanctuaryTag>()?.isEmpty() == true) return

		event.isCancelled = true
	}
}

