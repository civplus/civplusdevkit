package plus.civ.basschin

import org.bukkit.Material
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.BookMeta
import vg.civcraft.mc.civmodcore.extensions.writeNBTTag

fun sanctuaryExplainBook(): ItemStack {
	val book = ItemStack(Material.WRITTEN_BOOK)
	val meta = book.itemMeta
	if (meta !is BookMeta) {
		error("expected bookmeta, got $meta")
	}

	meta.title = "How to use a Sanctuary"
	meta.author = "civplus"

	meta.addPage("A Sanctuary protects a wide area from those who wish to do you ill. " +
			"The inner 20% of the Sanctuary's radius has the most protections, and can be used to protect your most important infrastructure. " +
			"The outer 80% has a lesser level of protection, and is best for cities.")
	meta.addPage("Within the inner 20%, it is impossible to place blocks, use flying machines, place paintings, " +
			"and do other restricted actions unless you are on the NameLayer group with the proper permissions set.")
	meta.addPage("Within the 80% radius, it is impossible to reinforce blocks, " +
			"unless you are on the NameLayer group of the Sanctuary.")
	meta.addPage("Also, within the full radius of the Sanctuary, all man-made blocks are 20 times as hard to break. " +
			"Natural blocks, such as Hardened Clay (including coloured), Stone, Dirt, and other such blocks are not protected.")
	meta.addPage("A Sanctuary requires ExP to function." +
			" The area the Sanctuary has effect upon is based upon how much ExP is inside it. " +
			"Preventing restricted actions will take away a small amount of ExP.")
	meta.addPage("To add ExP, right click the Sanctuary with any ExP-containing item, like ExP bottles, Emeralds," +
			" or Emerald Blocks. Bastions (Sponges) also work, if you have any laying around.")
	meta.addPage("""
		A Sanctuary also has a name. It'll be displayed as a title card like below when you enter its radius:

		 Entering A Sanctuary

		To change this name, right click it with a Name Tag renamed to the name that you want.
	""".trimIndent())

	book.itemMeta = meta

	book.writeNBTTag("generation", 3)
	return book
}
