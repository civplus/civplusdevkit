package plus.civ.basschin

import org.bukkit.Material.*
import org.bukkit.inventory.ItemStack
import vg.civcraft.mc.civmodcore.expression.NBTExpression
import vg.civcraft.mc.civmodcore.expression.id

object Config {
	var vaultAreaPercentage = 0.2
	var areaBlocksPerPower = 1

	private const val ONE_EXP_BOTTLE_POWER = 10.0

	val powerItems: Set<Pair<Double, NBTExpression>> = mutableSetOf(
			Pair(ONE_EXP_BOTTLE_POWER, NBTExpression()
					.id(EXP_BOTTLE)),
			Pair(ONE_EXP_BOTTLE_POWER * 11.0, NBTExpression()
					.id(EMERALD)),
			Pair(ONE_EXP_BOTTLE_POWER * 11.0 * 9.0, NBTExpression()
					.id(EMERALD_BLOCK)),
	)

	var refundItem = ItemStack(EMERALD_BLOCK)
	var refundPercentage = 0.95

	var canceledActionPowerCost = 1.76
	var damageCooldownMilliseconds: Long = 1000

	var restrictedActionPlayerCheckRadius = 7

	var cityBastionProtectedBlockBreaks = 20
	val cityProtectedBlocksOverride = mutableSetOf(
			Pair(STONE, 2), Pair(STONE, 4), Pair(STONE, 6),
			Pair(SANDSTONE, 1), Pair(SANDSTONE, 2),
			Pair(RED_SANDSTONE, 1), Pair(RED_SANDSTONE, 2),
	)
	val cityUnprotectedBlocks = mutableSetOf(
			// underground blocks
			BEDROCK, STONE, CLAY, DIRT, GRASS, GRAVEL, HARD_CLAY, STAINED_CLAY, SAND, SANDSTONE, RED_SANDSTONE,

			// crops
			CACTUS, CARROT, POTATO, NETHER_STALK, NETHER_WARTS, CROPS, PUMPKIN_STEM, PUMPKIN, MELON_STEM, MELON_BLOCK,

			// ores
			COAL_ORE, IRON_ORE, GOLD_ORE, DIAMOND_ORE, QUARTZ_ORE, LAPIS_ORE, REDSTONE_ORE,

			// plants
			DEAD_BUSH, LOG, LOG_2, LEAVES, LEAVES_2, DOUBLE_PLANT, LONG_GRASS, RED_ROSE, YELLOW_FLOWER, RED_MUSHROOM, BROWN_MUSHROOM, HUGE_MUSHROOM_2, HUGE_MUSHROOM_1, COCOA,

			// end blocks
			ENDER_STONE, OBSIDIAN,

			// ice
			ICE, PACKED_ICE, SNOW, SNOW_BLOCK
	)

	var sanctuaryRenameItem = NAME_TAG

	var horseJumpBlocksHeightInVaultProtections = 2.5
}
