package plus.civ.basschin.listeners

import org.bukkit.ChatColor
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent
import plus.civ.basschin.Config
import plus.civ.basschin.database.Sanctuary
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.getBlockData
import kotlin.math.roundToInt

class SanctuaryCheck: Listener {
	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	fun playerInteract(event: PlayerInteractEvent) {
		if (event.action != Action.RIGHT_CLICK_BLOCK) {
			return
		}

		if (event.clickedBlock.location.getBlockData<SanctuaryBlock.SanctuaryTag>().isEmpty()) {
			return
		}

		val sanctuary = Sanctuary.sanctuaries.firstOrNull { it.location.asLocation() == event.clickedBlock.location }
		if (sanctuary == null) {
			event.player.sendMessage("${ChatColor.RED}That beacon isn't a sanctuary. Try breaking and replacing it.")
			return
		}

		event.player.sendMessage("${ChatColor.AQUA}${sanctuary.name} has ${sanctuary.radius.roundToInt()} blocks of radius")
	}
}
