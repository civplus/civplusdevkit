package plus.civ.basschin

import plus.civ.basschin.database.Sanctuary
import plus.civ.basschin.listeners.CityProtections
import plus.civ.basschin.listeners.VaultProtections
import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PluginInfo
import vg.civcraft.mc.namelayer.GroupManager
import vg.civcraft.mc.namelayer.permission.PermissionType

@PluginInfo(
		name = "BassChin",
		description = "Adds sanctuaries from sovasc to replace bastions.",
		author = "Amelorate",
		depends = ["CivModCore", "LayerAfterLayer", "Citadel", "JustATest"],
		softDepends = ["BumHug", "CombatTagPlus"]
)
class BassChin: KotlinPlugin() {
	companion object {
		private var instanceStorage: BassChin? = null
		val instance: BassChin
			get() = instanceStorage!!
	}

	override fun onLoad() {
		System.setProperty("org.litote.mongo.test.mapping.service", "org.litote.kmongo.jackson.JacksonClassMappingTypeService")
		super.onLoad()
	}

	override fun onEnable() {
		super.onEnable()
		instanceStorage = this

		PermissionType.registerPermission(VaultProtections.SANCTUARY_PLACE,
				mutableListOf(GroupManager.PlayerType.ADMINS, GroupManager.PlayerType.OWNER),
				"Allows placing blocks inside the vault-type radius of a sanctuary.")

		PermissionType.registerPermission(CityProtections.SANCTUARY_REINFORCE,
				mutableListOf(GroupManager.PlayerType.MODS, GroupManager.PlayerType.ADMINS, GroupManager.PlayerType.OWNER),
				"Allows reinforcing blocks inside the city-type radius of a sanctuary.")
	}
}
