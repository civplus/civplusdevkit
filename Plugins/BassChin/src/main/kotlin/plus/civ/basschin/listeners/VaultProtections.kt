package plus.civ.basschin.listeners

import dev.civmc.bumhug.hacks.PearlCooldown
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.Location
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEnderPearl
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity
import org.bukkit.entity.Player
import org.bukkit.event.Cancellable
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockMultiPlaceEvent
import org.bukkit.event.block.BlockPistonExtendEvent
import org.bukkit.event.block.BlockPistonRetractEvent
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.event.entity.HorseJumpEvent
import org.bukkit.event.entity.ProjectileLaunchEvent
import org.bukkit.event.hanging.HangingPlaceEvent
import org.bukkit.event.player.PlayerTeleportEvent
import plus.civ.basschin.Config
import plus.civ.basschin.database.Sanctuary
import plus.civ.ivy.nms.livestock.IvyHorse
import vg.civcraft.mc.civmodcore.extensions.safeDistance
import vg.civcraft.mc.namelayer.permission.GroupPermission
import vg.civcraft.mc.namelayer.permission.PermissionType

class VaultProtections: Listener {
	companion object {
		const val SANCTUARY_PLACE = "SANCTUARY_PLACE"
	}

	@EventHandler(ignoreCancelled = true)
	fun playerPlace(event: BlockPlaceEvent) {
		handleEvent(event, event.player, event.block.location)
	}

	@EventHandler(ignoreCancelled = true)
	fun blockMultiPlace(event: BlockMultiPlaceEvent) {
		for (block in event.replacedBlockStates) {
			handleEvent(event, event.player, block.location)
			if (event.isCancelled) {
				return
			}
		}
	}

	@EventHandler(ignoreCancelled = true)
	fun hangingPlace(event: HangingPlaceEvent) {
		handleEvent(event, event.player, event.block.location.add(event.blockFace.modX.toDouble(), event.blockFace.modY.toDouble(), event.blockFace.modZ.toDouble()))
	}

	@EventHandler(ignoreCancelled = true)
	fun pistonPush(event: BlockPistonExtendEvent) {
		for (block in event.blocks) {
			handleNonPlayer(event, block.location)
		}
	}

	@EventHandler(ignoreCancelled = true)
	fun pistonPull(event: BlockPistonRetractEvent) {
		for (block in event.blocks) {
			handleNonPlayer(event, block.location)
		}
	}

	@EventHandler(ignoreCancelled = true)
	fun pearlThrow(event: ProjectileLaunchEvent) {
		val pearl = event.entity
		if (pearl !is CraftEnderPearl) {
			return
		}

		val player = pearl.getShooter()
		if (player !is Player) {
			return
		}

		val isCanceled = handleEvent(event, player, player.location)
		if (isCanceled) {
			if (Bukkit.getPluginManager().isPluginEnabled("Bumhug")) {
				PearlCooldown.lastThrownTimestamps[player.uniqueId] = System.currentTimeMillis()
			}
		}
	}

	@EventHandler(ignoreCancelled = true)
	fun pearlLand(event: PlayerTeleportEvent) {
		if (event.cause != PlayerTeleportEvent.TeleportCause.ENDER_PEARL) {
			return
		}

		handleEvent(event, event.player, event.to)
	}

	private fun handleEvent(event: Cancellable, player: Player, location: Location): Boolean {
		var cancelledEvent = false

		for (sanctuary in Sanctuary.getSanctuariesIntersectingVault(location)) {
			val group = sanctuary.namelayerGroup ?: continue

			val perms = GroupPermission(group)
			val type = group.getPlayerType(player.uniqueId)
			if (!perms.hasPermission(type, PermissionType.getPermission(SANCTUARY_PLACE))) {
				event.isCancelled = true
				sanctuary.damage(Config.canceledActionPowerCost, player)
				sanctuary.saveLater()

				player.sendMessage("${ChatColor.RED}${sanctuary.name} doesn't allow you to do that here.")
				cancelledEvent = true
			}
		}

		return cancelledEvent
	}

	private fun handleNonPlayer(event: Cancellable, location: Location) {
		for (sanctuary in Sanctuary.getSanctuariesIntersectingVault(location)) {
			val group = sanctuary.namelayerGroup ?: continue
			val perms = GroupPermission(group)

			for (player in Bukkit.getOnlinePlayers()) {
				if (player.location.safeDistance(location) < Config.restrictedActionPlayerCheckRadius) {
					val type = group.getPlayerType(player.uniqueId)
					if (perms.hasPermission(type, PermissionType.getPermission(SANCTUARY_PLACE))) {
						continue
					}
				}
			}

			event.isCancelled = true
		}
	}

	@EventHandler(ignoreCancelled = true)
	fun horseJump(event: HorseJumpEvent) {
		if (Sanctuary.getSanctuariesIntersectingVault(event.entity.location).isEmpty()) return

		if (Config.horseJumpBlocksHeightInVaultProtections != 0.0) {
			// The below code is flawed because the jump power bar is non-linear, I think
			val bukkitHorse = event.entity
			if (bukkitHorse !is CraftEntity) return
			val ivyHorse = bukkitHorse.handle
			if (ivyHorse !is IvyHorse) return

			val jump = ivyHorse.genome?.jumpHeightBlocks ?: 0.0
			if (jump < Config.horseJumpBlocksHeightInVaultProtections) return

			event.power = (Config.horseJumpBlocksHeightInVaultProtections / jump).toFloat() * event.power
		} else {
			event.isCancelled = true
		}
	}
}
