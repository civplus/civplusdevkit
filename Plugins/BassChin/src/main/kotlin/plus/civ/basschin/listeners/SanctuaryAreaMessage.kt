package plus.civ.basschin.listeners

import org.bukkit.ChatColor
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerMoveEvent
import org.bukkit.event.player.PlayerQuitEvent
import plus.civ.basschin.database.Sanctuary
import java.util.*

class SanctuaryAreaMessage: Listener {
	val playerSanctuaries = mutableMapOf<UUID, String>()

	@EventHandler
	fun playerMove(event: PlayerMoveEvent) {
		if (event.to.blockX == event.from.blockX && event.to.blockZ == event.from.blockZ) {
			return
		}

		val currentSanctuary = playerSanctuaries[event.player.uniqueId]
		val insideSanctuary = Sanctuary.getClosestSanctuary(event.to) ?: return
		if (!insideSanctuary.isInside(event.to)) {
			playerSanctuaries.remove(event.player.uniqueId)
			if (currentSanctuary != null) {
				event.player.sendTitle("", "${ChatColor.GRAY}Leaving $currentSanctuary")
			}
			return
		}

		if (insideSanctuary.name == currentSanctuary) {
			return
		}

		playerSanctuaries[event.player.uniqueId] = insideSanctuary.name
		event.player.sendTitle("", "${ChatColor.GRAY}Entering ${insideSanctuary.name}")
	}

	@EventHandler
	fun playerLogout(event: PlayerQuitEvent) {
		playerSanctuaries.remove(event.player.uniqueId)
	}
}
