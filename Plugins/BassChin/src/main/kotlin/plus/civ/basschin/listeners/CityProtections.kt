package plus.civ.basschin.listeners

import net.minelink.ctplus.event.CombatLogEvent
import org.bukkit.ChatColor
import org.bukkit.Effect
import org.bukkit.Location
import org.bukkit.entity.EntityType
import org.bukkit.entity.Player
import org.bukkit.event.Cancellable
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.entity.CreatureSpawnEvent
import org.bukkit.event.entity.EntitySpawnEvent
import org.bukkit.event.player.PlayerBucketEmptyEvent
import org.bukkit.event.player.PlayerBucketFillEvent
import plus.civ.basschin.Config
import plus.civ.basschin.database.Sanctuary
import vg.civcraft.mc.citadel.Citadel
import vg.civcraft.mc.citadel.events.ReinforcementCreationEvent
import vg.civcraft.mc.namelayer.permission.GroupPermission
import vg.civcraft.mc.namelayer.permission.PermissionType

class CityProtections: Listener {
	companion object {
		const val SANCTUARY_REINFORCE = "SANCTUARY_REINFORCE"
	}

	val brokenBlocks = mutableMapOf<Location, Int>()

	@EventHandler(ignoreCancelled = true)
	fun breakBlock(event: BlockBreakEvent) {
		val materialPair = Pair(event.block.type, event.block.data.toInt())
		if (event.block.type in Config.cityUnprotectedBlocks && materialPair !in Config.cityProtectedBlocksOverride) {
			return
		}

		if (Citadel.getReinforcementManager().isReinforced(event.block)) {
			return
		}

		val intersecting = Sanctuary.getSanctuariesIntersectingCity(event.block.location)
		if (intersecting.isEmpty()) {
			return
		}

		if (intersecting.all { sanctuary ->
					val group = sanctuary.namelayerGroup ?: return@all true
					val perms = GroupPermission(group)
					val type = group.getPlayerType(event.player.uniqueId)
					perms.hasPermission(type, PermissionType.getPermission(SANCTUARY_REINFORCE))
				}) {
			return
		}

		val breaks = brokenBlocks.getOrDefault(event.block.location, 0)
		if (breaks >= Config.cityBastionProtectedBlockBreaks) {
			brokenBlocks[event.block.location] = 0
			return
		}

		val nearestSanctuary = Sanctuary.getClosestSanctuary(event.block.location) ?: return

		if (breaks == 0) {
			event.player.sendMessage("${ChatColor.AQUA}${nearestSanctuary.name} doesn't allow you to break this block. Mine it ${Config.cityBastionProtectedBlockBreaks - 1} more times to break it.")
		}

		brokenBlocks[event.block.location] = breaks + 1
		event.isCancelled = true

		for (i in 0..12) {
			event.player.world.playEffect(event.block.location.add(0.5, 0.5, 0.5), Effect.INSTANT_SPELL, 0)
		}
	}

	@EventHandler
	fun unsafeLogout(event: CombatLogEvent) {
		if (event.reason != CombatLogEvent.Reason.UNSAFE_LOGOUT) {
			return
		}

		var inOneOrMoreSanctuaries = false
		var shouldCancel = true

		for (sanctuary in Sanctuary.getSanctuariesIntersectingCity(event.player.location)) {
			if (!shouldCancel) continue
			inOneOrMoreSanctuaries = true

			val group = sanctuary.namelayerGroup ?: continue

			val perms = GroupPermission(group)
			val type = group.getPlayerType(event.player.uniqueId)
			if (!perms.hasPermission(type, PermissionType.getPermission(SANCTUARY_REINFORCE))) {
				shouldCancel = false
			}
		}

		if (inOneOrMoreSanctuaries && shouldCancel) {
			event.isCancelled = true
		}
	}

	private val reasons = setOf(CreatureSpawnEvent.SpawnReason.NATURAL, CreatureSpawnEvent.SpawnReason.NETHER_PORTAL, CreatureSpawnEvent.SpawnReason.REINFORCEMENTS)
	private val passiveMobs = setOf(EntityType.BAT, EntityType.CHICKEN, EntityType.COW, EntityType.HORSE,
		EntityType.MUSHROOM_COW, EntityType.OCELOT, EntityType.PIG, EntityType.RABBIT, EntityType.SHEEP,
		EntityType.SQUID, EntityType.WOLF)

	@EventHandler
	fun mobSpawn(event: CreatureSpawnEvent) {
		if (event.spawnReason !in reasons) {
			return
		}

		if (event.entityType in passiveMobs) {
			return
		}

		if (Sanctuary.getSanctuariesIntersectingCity(event.location).isNotEmpty()) {
			event.isCancelled = true
		}
	}

	@EventHandler
	fun reinforce(event: ReinforcementCreationEvent) {
		handleEvent(event, event.player, event.block.location)
	}

	@EventHandler(ignoreCancelled = true)
	fun emptyBucket(event: PlayerBucketEmptyEvent) {
		handleEvent(event, event.player, event.blockClicked.location.add(event.blockFace.modX.toDouble(), event.blockFace.modY.toDouble(), event.blockFace.modZ.toDouble()))
	}

	@EventHandler(ignoreCancelled = true)
	fun fillBucket(event: PlayerBucketFillEvent) {
		handleEvent(event, event.player, event.blockClicked.location.add(event.blockFace.modX.toDouble(), event.blockFace.modY.toDouble(), event.blockFace.modZ.toDouble()))
	}

	private fun handleEvent(event: Cancellable, player: Player, location: Location) {
		for (sanctuary in Sanctuary.getSanctuariesIntersectingCity(location)) {
			val group = sanctuary.namelayerGroup ?: continue

			val perms = GroupPermission(group)
			val type = group.getPlayerType(player.uniqueId)
			if (!perms.hasPermission(type, PermissionType.getPermission(SANCTUARY_REINFORCE))) {
				event.isCancelled = true
				player.sendMessage("${ChatColor.RED}${sanctuary.name} doesn't allow you to do that here.")
			}
		}
	}
}
