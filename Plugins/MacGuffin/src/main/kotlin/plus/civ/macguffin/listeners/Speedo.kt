package plus.civ.macguffin.listeners

import org.bukkit.ChatColor
import org.bukkit.DyeColor
import org.bukkit.Material
import org.bukkit.enchantments.Enchantment
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerMoveEvent
import org.bukkit.potion.PotionEffect
import org.bukkit.potion.PotionEffectType
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe
import vg.civcraft.mc.civmodcore.expression.*

object Speedo : Listener, RecipeProvider {
	val basicSpeedo = NBTExpression().id(Material.LEATHER_LEGGINGS)
			.with("tag.macguffin", "speedo")
	val speedo = NBTExpression().id(Material.LEATHER_LEGGINGS)
			.amountGTE(1)
			.with("tag.macguffin", "speedo")
			.with("tag.display.Name", "${ChatColor.RESET}${ChatColor.AQUA}Speedo")
			.with("tag.display.color", 255) // blue
			.with("tag.ench[_]", EnchantmentMatcher.enchant(Enchantment.DEPTH_STRIDER, 10))
			.or(basicSpeedo)

	override fun register() {
		val leather = NBTExpression().id(Material.LEATHER).amountGTE(1)
		val dye = NBTExpression().dye(DyeColor.BLUE).amountGTE(1)

		val ingredients = mutableMapOf(Pair('.', dye), Pair('l', leather))

		val shape = """
				.....
				.lll.
				.l.l.
				.l.l.
				.....
			""".trimIndent()

		addRecipeBookRecipe(ShapedCraftingRecipe(ingredients, shape, "speedo", speedo.solveToItemStack()), RecipeCategory.MACGUFFINS)
	}

	@EventHandler
	fun onPlayerMove(event: PlayerMoveEvent) {
		if (event.from.blockX == event.to.blockX &&
				event.from.blockY == event.to.blockY &&
				event.from.blockZ == event.to.blockZ) {
			return
		}

		if (event.player.inventory.leggings == null) {
			return
		}

		// just the speedo. none of that fat kid with swimshirt shit
		if (event.player.inventory.chestplate != null || event.player.inventory.boots != null || event.player.inventory.helmet != null) {
			return
		}

		if (!speedo.matches(event.player.inventory.leggings)) {
			return
		}

		val feetBlock = event.player.location.block.type

		if (feetBlock == Material.WATER || feetBlock == Material.STATIONARY_WATER) {
			event.player.addPotionEffect(PotionEffect(PotionEffectType.SPEED, 2 * 20, 3, false, false), true)
		} else if (event.player.activePotionEffects.any { it.type == PotionEffectType.SPEED && it.amplifier == 3 }) {
			// if the player is not in water but has effect, remove it
			event.player.removePotionEffect(PotionEffectType.SPEED)
		}
	}
}
