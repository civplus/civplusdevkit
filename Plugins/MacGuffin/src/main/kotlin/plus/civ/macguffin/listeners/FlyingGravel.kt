package plus.civ.macguffin.listeners

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabExecutor
import org.bukkit.entity.Player
import org.bukkit.event.Listener
import org.bukkit.inventory.ItemStack
import plus.civ.kotlinplugin.CommandInfo
import plus.civ.kotlinplugin.PermissionDefault
import plus.civ.kotlinplugin.PluginPermission
import plus.civ.whatisanocttree.chunk.tags.ImmovableBlock
import plus.civ.whatisanocttree.chunk.tags.ItemSerializable
import plus.civ.whatisanocttree.chunk.tags.ItemSerializable.Companion.addData
import vg.civcraft.mc.civmodcore.event.BlockDropItemEvent
import vg.civcraft.mc.civmodcore.extensions.giveItemOrDropOnGround

@CommandInfo(
		name = "flyinggravel",
		description = "Spawn in flying sand/gravel",
		permission = "macguffin.flyinggravel",
		pluginPermission = PluginPermission(
				name = "macguffin.flyinggravel",
				description = "Allows spawning in flying sand/gravel items",
				default = PermissionDefault.OP
		),
		usage = "/<command> (sand/gravel)",
)
class FlyingGravel: Listener, TabExecutor {
	class FlyingTag : ItemSerializable, ImmovableBlock {
		override fun onBecomeItem(event: BlockDropItemEvent): Boolean {
			if (event.droppedItems.isNullOrEmpty()) {
				event.setToDefaultDrops()
			}

			for (item in event.droppedItems!!) {
				val meta = item.itemMeta
				val lore = meta.lore ?: mutableListOf()
				lore.add(0, "${ChatColor.RESET}${ChatColor.GRAY}Flying")
				meta.lore = lore
				item.itemMeta = meta
			}

			return false
		}
	}

	val allowedItems = listOf("sand", "gravel")

	override fun onCommand(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): Boolean {
		if (sender !is Player) {
			sender.sendMessage("This command must be ran by a player")
			return true
		}

		val type = args.getOrNull(0) ?: "gravel"
		val material = Material.matchMaterial(type.uppercase())
		val item = ItemStack(material, 64)
		item.addData(FlyingTag())
		val meta = item.itemMeta
		meta.lore = listOf("${ChatColor.RESET}${ChatColor.GRAY}Flying")
		item.itemMeta = meta

		sender.giveItemOrDropOnGround(item)

		return true
	}

	override fun onTabComplete(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): MutableList<String> {
		if (args.isEmpty()) {
			return allowedItems.toMutableList()
		} else {
			return allowedItems.filter { it.startsWith(args[0].lowercase()) }
					.toMutableList()
		}
	}
}
