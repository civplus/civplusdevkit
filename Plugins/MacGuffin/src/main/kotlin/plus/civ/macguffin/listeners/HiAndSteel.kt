package plus.civ.macguffin.listeners

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.Sound
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.util.Vector
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe
import vg.civcraft.mc.civmodcore.expression.*
import xyz.xenondevs.particle.ParticleBuilder
import xyz.xenondevs.particle.ParticleEffect

class HiAndSteel: Listener, RecipeProvider {
	val hi = NBTExpression()
			.id(Material.FLINT_AND_STEEL)
			.itemName("${ChatColor.RESET}${ChatColor.DARK_RED}火")
			.with("tag.Unbreakable", 1)

	override fun register() {
		val flintAndSteel = NBTExpression().id(Material.FLINT_AND_STEEL).amountGTE(1)
		val coal = NBTExpression().id(Material.COAL_BLOCK).amountGTE(64)

		val ingredients = mutableMapOf(Pair('f', flintAndSteel), Pair('c', coal))

		val shape = """
				ccccc
				ccccc
				ccfcc
				ccccc
				ccccc
			""".trimIndent()

		addRecipeBookRecipe(ShapedCraftingRecipe(ingredients, shape, "hi_and_steel", hi.solveToItemStack()), RecipeCategory.MACGUFFINS)
	}

	@EventHandler
	fun hiLeftClick(event: PlayerInteractEvent) {
		val item = event.item ?: return

		if (!hi.matches(item)) {
			return
		}

		if (event.action != Action.LEFT_CLICK_AIR && event.action != Action.LEFT_CLICK_BLOCK) {
			return
		}

		event.player.location.world.playSound(event.player.location, Sound.FIRE_IGNITE, 0.5f, 1.0f)

		// generate a vector that's around the player's head
		val dx = Math.random().toFloat() - 0.5f
		val dy = Math.random().toFloat() - 0.5f
		val dz = Math.random().toFloat() - 0.5f
		val offsetVec = Vector(dx, dy, dz).normalize()

		// generate a random velocity that's mostly upward
		val vx = Math.random().toFloat() - 0.5f
		val vy = Math.random().toFloat() * 2f
		val vz = Math.random().toFloat() - 0.5f

		ParticleBuilder(ParticleEffect.FLAME, event.player.location)
				.setSpeed(0.05f)
				.setLocation(event.player.location.clone().add(offsetVec))
				.setOffset(vx, vy, vz)
				.display()
	}

}
