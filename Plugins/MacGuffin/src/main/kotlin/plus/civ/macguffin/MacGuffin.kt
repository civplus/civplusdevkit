package plus.civ.macguffin

import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PluginInfo

@PluginInfo(
		name = "MacGuffin",
		description = "Adds special items with special effects",
		author = "Amelorate",
		depends = ["CivModCore", "WhatIsAnOcttree"],
		softDepends = ["MeansOfProduction"],
		commandsPackage = "listeners",
)
class MacGuffin: KotlinPlugin()
