package plus.civ.macguffin.listeners

import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapedCraftingRecipe
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.getBlockData
import plus.civ.whatisanocttree.chunk.tags.ImmovableBlock
import plus.civ.whatisanocttree.chunk.tags.ItemSerializable
import plus.civ.whatisanocttree.chunk.tags.ItemSerializable.Companion.addData
import vg.civcraft.mc.civmodcore.event.BlockDropItemEvent

object StoneAnvil: Listener, RecipeProvider {
	class StoneAnvilTag: ItemSerializable, ImmovableBlock {
		override fun onBecomeItem(event: BlockDropItemEvent): Boolean {
			if (event.droppedItems != null && event.droppedItems!!.isNotEmpty()) {
				Bukkit.getPluginManager().getPlugin("MacGuffin").logger.warning("Breaking a stone anvil with drops: ${event.droppedItems}! Overriding them.")
			}

			event.droppedItems = mutableListOf(anvilItem.clone())

			return false
		}
	}

	val anvilItem = run {
		val result = ItemStack(Material.ANVIL)
		val meta = result.itemMeta
		meta.displayName = "${ChatColor.RESET}Stone Anvil"
		meta.lore = listOf("Decorative Item")
		result.itemMeta = meta

		result.addData(StoneAnvilTag())
		result
	}

	override fun register() {
		val shape = """
			bbb
			_-_
			---
		""".trimIndent()

		val stoneBrick = item(Material.SMOOTH_BRICK)
		val halfSlab = item(Material.STEP)

		val ingredients = mutableMapOf(Pair('b', stoneBrick), Pair('-', halfSlab))

		val recipe = ShapedCraftingRecipe(ingredients, shape, "stone_anvil", anvilItem.clone(), nothingChar = '_')
		addRecipeBookRecipe(recipe, RecipeCategory.BUILDING)
	}

	@EventHandler(ignoreCancelled = true)
	fun stoneAnvilInteract(event: PlayerInteractEvent) {
		if (event.clickedBlock.type != Material.ANVIL) return
		if (event.action != Action.RIGHT_CLICK_BLOCK) return
		if (event.clickedBlock.location.getBlockData<StoneAnvilTag>().isEmpty()) return

		event.isCancelled = true
	}
}
