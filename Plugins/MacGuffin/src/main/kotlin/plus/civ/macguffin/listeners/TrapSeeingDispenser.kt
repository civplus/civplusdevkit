package plus.civ.macguffin.listeners

import org.bukkit.*
import org.bukkit.block.BlockFace
import org.bukkit.block.Dispenser
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerMoveEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.material.FurnaceAndDispenser
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapelessCraftingRecipe
import plus.civ.whatisanocttree.chunk.ChunkData.Companion.getBlocksInRadiusWithData
import plus.civ.whatisanocttree.chunk.tags.ImmovableBlock
import plus.civ.whatisanocttree.chunk.tags.ItemSerializable
import plus.civ.whatisanocttree.chunk.tags.ItemSerializable.Companion.addData
import vg.civcraft.mc.citadel.Citadel
import vg.civcraft.mc.citadel.reinforcement.PlayerReinforcement
import vg.civcraft.mc.civmodcore.event.BlockDropItemEvent
import vg.civcraft.mc.civmodcore.extensions.safeDistance
import vg.civcraft.mc.namelayer.GroupManager
import vg.civcraft.mc.namelayer.permission.GroupPermission
import vg.civcraft.mc.namelayer.permission.PermissionType
import java.util.*
import kotlin.math.sign

object TrapSeeingDispenser: RecipeProvider, Listener {
	class TrapSeeingDispenserTag : ItemSerializable, ImmovableBlock {
		override fun onBecomeItem(event: BlockDropItemEvent): Boolean {
			if (event.droppedItems != null && event.droppedItems!!.isNotEmpty()) {
				Bukkit.getPluginManager().getPlugin("MacGuffin").logger.warning("Breaking a seeing dispenser with drops: ${event.droppedItems}! Overriding them.")
			}

			event.droppedItems = mutableListOf(dispenserItem.clone())

			return false
		}
	}

	const val TRAP_IGNORE_PERMISSION = "TRAP_IGNORE"

	init {
		PermissionType.registerPermission(TRAP_IGNORE_PERMISSION,
			listOf(GroupManager.PlayerType.MEMBERS, GroupManager.PlayerType.MODS, GroupManager.PlayerType.ADMINS, GroupManager.PlayerType.OWNER),
		"Players with this permission will be ignored by Seeing-Trap Dispensers reinforced to this group")
	}

	val seenPlayers = mutableMapOf<Location, MutableSet<UUID>>()

	var seeingRange = 16.0

	val dispenserItem = run {
		val result = ItemStack(Material.DISPENSER)
		val meta = result.itemMeta
		meta.displayName = "${ChatColor.RESET}${ChatColor.AQUA}Seeing-Trap Dispenser"
		meta.lore = listOf("Dispenses an item when it sees a player.",
			"Sees through walls. Maximum range: ${seeingRange.toInt()} blocks.")
		result.itemMeta = meta
		result.addData(TrapSeeingDispenserTag())

		result
	}

	override fun register() {
		val dispenser = item(Material.DISPENSER)
		val enderEye = item(Material.EYE_OF_ENDER)

		val ingredients = listOf(dispenser, enderEye)

		val recipe = ShapelessCraftingRecipe(ingredients, "trap_seeing_dispenser", dispenserItem)
		addRecipeBookRecipe(recipe, RecipeCategory.PVP)
		addRecipeBookRecipe(recipe, RecipeCategory.MACGUFFINS)
	}

	@EventHandler(ignoreCancelled = true)
	fun playerMove(event: PlayerMoveEvent) {
		if (event.from.blockX == event.to.blockX &&
			event.from.blockY == event.to.blockY &&
			event.from.blockZ == event.to.blockZ) {
			return
		}

		if (event.player.gameMode == GameMode.SPECTATOR) return

		val dispenserLocations = event.to.getBlocksInRadiusWithData<TrapSeeingDispenserTag>(seeingRange + 1)
		for (dispenserLocation in dispenserLocations) {
			val block = dispenserLocation.block
			val dispenserState = block.state
			if (dispenserState !is Dispenser) continue
			val dispenserData = dispenserState.data
			if (dispenserData !is FurnaceAndDispenser) continue
			val dispenserFacing = dispenserData.facing

			if (isWithinField(event.to, dispenserLocation, dispenserFacing)) {
				if (seenPlayers[dispenserLocation]?.contains(event.player.uniqueId) == true) {
					continue
				} else {
					var shouldDispense = true

					val citadel = Bukkit.getPluginManager().getPlugin("Citadel")
					if (citadel is Citadel) {
						val reinforcement = Citadel.getReinforcementManager().getReinforcement(dispenserLocation)
						if (reinforcement is PlayerReinforcement) {
							val perms = GroupPermission(reinforcement.group)
							val permissionType = reinforcement.group.getPlayerType(event.player.uniqueId)
							if (perms.hasPermission(permissionType, PermissionType.getPermission(TRAP_IGNORE_PERMISSION))) {
								shouldDispense = false // gosh i hate namelayer
							}
						}
					}

					if (shouldDispense) {
						dispenserState.dispense()
					}
					seenPlayers.getOrPut(dispenserLocation) { mutableSetOf() }.add(event.player.uniqueId)
				}
			} else {
				seenPlayers[dispenserLocation]?.remove(event.player.uniqueId)
			}
		}
	}

	fun isWithinField(playerLocation: Location, dispenserLocation: Location, dispenserFacing: BlockFace): Boolean {
		if (playerLocation.safeDistance(dispenserLocation) > seeingRange) {
			return false
		}

		if (dispenserFacing == BlockFace.UP || dispenserFacing == BlockFace.DOWN) {
			// up and down is special because it doesn't need to think about heads. only feet are in the dispenser's head
			if (playerLocation.blockX != dispenserLocation.blockX || playerLocation.blockZ != dispenserLocation.blockZ) {
				return false
			}

			if (dispenserFacing == BlockFace.UP && dispenserLocation.y < playerLocation.y) {
				return true
			} else if (dispenserFacing == BlockFace.DOWN && dispenserLocation.y > playerLocation.y) {
				return true
			} else {
				@Suppress("RedundantIf")
				return false
			}
		}

		// now we start thinking about the N E S W

		if (dispenserLocation.blockY != playerLocation.blockY && dispenserLocation.blockY - 1 != playerLocation.blockY) {
			return false
		}

		val modX = sign((playerLocation.blockX - dispenserLocation.blockX).toDouble()).toInt()
		val modZ = sign((playerLocation.blockZ - dispenserLocation.blockZ).toDouble()).toInt()

		val disModX = dispenserFacing.modX
		val disModZ = dispenserFacing.modZ


		if (modX == disModX && modZ == disModZ) {
			if (modX != 0 && modZ != 0) {
				return false
			}

			return true
		} else {
			return false
		}
	}
}
