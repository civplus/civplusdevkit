package plus.civ.ivy.config

import org.bukkit.DyeColor
import org.bukkit.Material
import org.bukkit.block.Biome
import org.bukkit.inventory.ItemStack
import org.bukkit.material.Dye
import vg.civcraft.mc.civmodcore.expression.NBTExpression
import vg.civcraft.mc.civmodcore.expression.id
import vg.civcraft.mc.civmodcore.expression.toItemGenerator
import vg.civcraft.mc.civmodcore.loottable.LootTable
import vg.civcraft.mc.civmodcore.loottable.LootTableEntry
import vg.civcraft.mc.civmodcore.loottable.TieredLootTable
import vg.civcraft.mc.civmodcore.random.NormalItemAmount
import vg.civcraft.mc.civmodcore.random.SECURE_RANDOM
import vg.civcraft.mc.civmodcore.random.UniformByte
import vg.civcraft.mc.civmodcore.random.selectOneOf

object FishingConfig {
	// NMS interprets this number a particular way. vanilla uses 100..900
	val FISHING_TIME = 100..500
	// amount to reduce the roll per lure level
	val LURE_MODIFIER = 100

	// Bad loot for all biomes
	private val badGenericFishingLoot = arrayOf(
		LootTableEntry(
			generator = NBTExpression().id(Material.RAW_FISH).toItemGenerator(UniformByte(1, 5)),
			chance = 10.0f,
		),
		LootTableEntry(
			generator = NBTExpression().id(Material.LEATHER_BOOTS).toItemGenerator()
				.let { { it().also {
						item -> item.durability = SECURE_RANDOM.nextInt(item.type.maxDurability.toInt()).toShort()
				} } },
			chance = 1.0f,
		),
		LootTableEntry(
			generator = NBTExpression().id(Material.FISHING_ROD).toItemGenerator() // fishing rod with random durability
				.let { { it().also {
						item -> item.durability = SECURE_RANDOM.nextInt(item.type.maxDurability.toInt()).toShort()
				} } },
			chance = 1.0f,
		),
		LootTableEntry(
			generator = NBTExpression().id(Material.WATER_LILY).toItemGenerator(NormalItemAmount(5, 3.0f)),
			chance = 1.0f,
		),
		LootTableEntry(
			generator = NBTExpression().id(Material.STICK).toItemGenerator(NormalItemAmount(5, 5.0f)),
			chance = 1.0f,
		),
		LootTableEntry(
			generator = NBTExpression().id(Material.GLASS_BOTTLE).toItemGenerator(NormalItemAmount(4, 3.0f)),
			chance = 1.0f,
		),
		LootTableEntry(
			generator = NBTExpression().id(Material.BONE).toItemGenerator(UniformByte(1, 4)),
			chance = 1.0f,
		),
		LootTableEntry(
			generator = NBTExpression().id(Material.STRING).toItemGenerator(NormalItemAmount(5, 3.0f)),
			chance = 1.0f,
		),
		LootTableEntry(
			generator = NBTExpression().id(Material.NETHER_STALK).toItemGenerator(),
			chance = 1.0f,
		),
	)
	// Good loot for generic biomes
	private val goodGenericFishingLoot = arrayOf(
		LootTableEntry(
			generator = NBTExpression().id(Material.INK_SACK).toItemGenerator(NormalItemAmount(3, 2.0f))
					.let { { it().also{ item ->
						val data = (item.data as Dye)
						data.color = SECURE_RANDOM.selectOneOf(DyeColor.values())
						item.data = data
					} } },
			chance = 3.0f,
		),
		LootTableEntry(
			generator = {
				val records = Material.values().filter{ it.name.contains("RECORD") }.toTypedArray()
				ItemStack(SECURE_RANDOM.selectOneOf(records))
			},
			chance = 0.5f,
		),
		LootTableEntry(
			generator = NBTExpression().id(Material.SEA_LANTERN).toItemGenerator(UniformByte(1, 3)),
			chance = 1.0f,
		),
		LootTableEntry(
			generator = NBTExpression().id(Material.WEB).toItemGenerator(UniformByte(1, 5)),
			chance = 1.0f,
		),
		LootTableEntry(
			generator = NBTExpression().id(Material.PACKED_ICE).toItemGenerator(NormalItemAmount(15, 10.0f)),
			chance = 1.0f,
		),
		LootTableEntry(
			generator = {
				val chainPieces = arrayOf(
					Material.CHAINMAIL_HELMET,
					Material.CHAINMAIL_CHESTPLATE,
					Material.CHAINMAIL_LEGGINGS,
					Material.CHAINMAIL_BOOTS
				)
				ItemStack(SECURE_RANDOM.selectOneOf(chainPieces))
			},
			chance = 1.0f,
		),
		LootTableEntry(
			generator = NBTExpression().id(Material.MAGMA_CREAM).toItemGenerator(NormalItemAmount(4, 2.0f)),
			chance = 1.0f,
		),
		LootTableEntry(
			generator = NBTExpression().id(Material.IRON_BARDING).toItemGenerator(),
			chance = 0.25f,
		),
		LootTableEntry(
			generator = NBTExpression().id(Material.GOLD_BARDING).toItemGenerator(),
			chance = 0.15f,
		),
		LootTableEntry(
			generator = NBTExpression().id(Material.DIAMOND_BARDING).toItemGenerator(),
			chance = 0.05f,
		),
		LootTableEntry(
			generator = NBTExpression().id(Material.NAME_TAG).toItemGenerator(),
			chance = 0.75f,
		),
	)

	private val genericFishingLootTable = TieredLootTable(LootTable(*badGenericFishingLoot), LootTable(*goodGenericFishingLoot))

	// Defaults to the generic loot table unless the given biome is configured specially
	val FISHING_LOOT = HashMap<Biome, LootTable>().withDefault{ genericFishingLootTable }
	// Devkit snip: All of the biome-specific loot
}
