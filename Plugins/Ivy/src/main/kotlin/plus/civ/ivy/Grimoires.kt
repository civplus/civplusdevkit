package plus.civ.ivy

import org.bukkit.ChatColor
import org.bukkit.DyeColor
import org.bukkit.Material
import org.bukkit.inventory.meta.BookMeta
import plus.civ.meansofproduction.MeansOfProduction
import plus.civ.meansofproduction.RecipeCategory
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapelessCraftingRecipe
import vg.civcraft.mc.civmodcore.expression.*

val shepherdsGrimoireExpression = NBTExpression.empty()
	.id(Material.WRITTEN_BOOK)
	.with("tag.display.Lore[_]", "${ChatColor.DARK_PURPLE}Grimoire")
	.with("tag.display.Name", "${ChatColor.GOLD}Shepherd's Grimoire")
	.with("tag.author", "civplus")
	.with("tag.generation", 3)
val shepherdsGrimoireItem = run {
	val shepherdsGrimoireItem = shepherdsGrimoireExpression.solveToItemStack()
	val shepherdsBookMeta = shepherdsGrimoireItem.itemMeta as BookMeta
	shepherdsBookMeta.pages = listOf(
		"${ChatColor.GOLD}${ChatColor.BOLD}Shepherd's Grimoire\n${ChatColor.RESET}Leftclick a livestock mob to fill this book with information about it."
	)
	shepherdsGrimoireItem.itemMeta = shepherdsBookMeta
	shepherdsGrimoireItem
}

val gardenersGrimoireExpression = NBTExpression.empty()
	.id(Material.WRITTEN_BOOK)
	.with("tag.display.Lore[_]", "${ChatColor.DARK_PURPLE}Grimoire")
	.with("tag.display.Name", "${ChatColor.DARK_GREEN}Gardener's Grimoire")
	.with("tag.author", "civplus")
	.with("tag.generation", 3)
val gardenersGrimoireItem = run {
	val gardenersGrimoireItem = gardenersGrimoireExpression.solveToItemStack()
	val gardenersBookMeta = gardenersGrimoireItem.itemMeta as BookMeta
	gardenersBookMeta.pages = listOf(
		"${ChatColor.GREEN}${ChatColor.BOLD}Gardener's Grimoire\n${ChatColor.RESET}Leftclick a crop to fill this book with information about it."
	)
	gardenersGrimoireItem.itemMeta = gardenersBookMeta
	gardenersGrimoireItem
}

object GrimoireRecipe: RecipeProvider {
	override fun register() {
		val book = NBTExpression.empty().id(Material.BOOK).amountGTE(1)
		val beef = NBTExpression.empty().id(Material.COOKED_BEEF).amountGTE(1)
		val carrot = NBTExpression.empty().id(Material.CARROT_ITEM).amountGTE(1)
		val lapis = NBTExpression.empty().dye(DyeColor.BLUE).amountGTE(1)

		MeansOfProduction.instance.addRecipeBookRecipe(ShapelessCraftingRecipe(listOf(book, beef, lapis), "shepherds_grimore", shepherdsGrimoireItem), RecipeCategory.EXP)
		MeansOfProduction.instance.addRecipeBookRecipe(ShapelessCraftingRecipe(listOf(book, carrot, lapis), "gardeners_grimore", gardenersGrimoireItem), RecipeCategory.EXP)
	}
}
