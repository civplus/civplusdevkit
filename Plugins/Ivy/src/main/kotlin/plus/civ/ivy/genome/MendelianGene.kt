package plus.civ.ivy.genome

import plus.civ.ivy.Ivy
import kotlin.random.Random
import kotlin.random.nextInt

/**
 * A simple (and bad) simulation of a single gene.
 *
 * A value of true is considered dominant, and a false value is recessive.
 */
data class MendelianGene(
	var geneOne: Boolean,
	var geneTwo: Boolean,
	var letter: Char = 'g',
) {
	companion object {
		/**
		 * Returns a gene with the chance of one of the alleles being recessive, but with a 0% chance that both are.
		 */
		fun randomGeneMaybeMixed(recessiveChance: Double): MendelianGene {
			return MendelianGene(Math.random() > recessiveChance, true)
		}

		/**
		 * Returns a gene with the chance of either alleles being recessive.
		 *
		 * The chance of both alleles being recessive is n^2.
		 */
		fun randomGeneMaybeRecessive(recessiveChance: Double): MendelianGene {
			val oneAlleleChance = recessiveChance / 2
			return MendelianGene(Math.random() > oneAlleleChance, Math.random() > oneAlleleChance)
		}
	}

	constructor(string: String): this(string[0].isUpperCase(), string[1].isUpperCase(), letter = string[0].lowercaseChar()) {
		if (string.length != 2) {
			throw IllegalArgumentException("Passed string $string of length not two to MendelianGene")
		}

		if (string[0].lowercaseChar() != string[1].lowercaseChar()) {
			throw IllegalArgumentException("Passed string $string that isn't two upper/lowercase of the same character to MendelianGene")
		}
	}

	val isDominantActive: Boolean
		get() = geneOne || geneTwo

	val isRecessiveActive: Boolean
		get() = !isDominantActive

	val isMixedActive: Boolean
		get() = geneOne != geneTwo

	fun breedWith(partner: MendelianGene): MendelianGene {
		if (letter != partner.letter) {
			Ivy.instance.logger.warning("Mixing gene with letter $letter with gene of ${partner.letter}! Are we mixing the wrong genes?")
		}

		val pool = mutableListOf(geneOne, geneTwo, partner.geneOne, partner.geneTwo)

		val geneOne = pool.removeAt(Random.nextInt(0..3))
		val geneTwo = pool.removeAt(Random.nextInt(0..2))

		return MendelianGene(geneOne, geneTwo, letter)
	}

	override fun toString(): String {
		return (if (geneOne) letter.uppercase() else letter).toString() + (if (geneTwo) letter.uppercase() else letter)
	}
}
