package plus.civ.ivy.nms.livestock

import net.minecraft.server.v1_8_R3.EntityAgeable
import net.minecraft.server.v1_8_R3.EntityPig
import net.minecraft.server.v1_8_R3.NBTTagCompound
import net.minecraft.server.v1_8_R3.World
import org.bukkit.entity.LivingEntity
import plus.civ.ivy.config.LivestockConfig
import plus.civ.ivy.config.LivestockHappinessConfig

class IvyPig(world: World): EntityPig(world), IvyLivestock {
	override var happiness: Float = 0.0f
	override var happinessChecks: Int = 0
	override var birthday: Long = 0L
	override val config: LivestockHappinessConfig
		get() = LivestockConfig.LIVESTOCK_CONFIGS[this.bukkitEntity.type]!!
	override val self: LivingEntity
		get() = this.bukkitEntity as LivingEntity

	// this function is called when the entity loads in, with the nbt data that it had stored in the world file
	override fun a(nbt: NBTTagCompound) {
		super.a(nbt)
		loadFromNBT(nbt)
	}

	// this function is called right before `nbt` is saved, whenever the entity is saved to the world file
	override fun e(nbt: NBTTagCompound) {
		super.e(nbt)
		saveToNBT(nbt)
	}

	// this is the tick function, i think. it does some ticking, at least. there's also a `t_` that might be better,
	// but `t_` calls this anyway (in fact, in `Entity`, that's all `t_` does)
	override fun K() {
		super.K()
		tickLivestock()
	}

	// this is called to make a baby one
	override fun createChild(partner: EntityAgeable): EntityAgeable {
		return IvyPig(this.world)
	}
}
