package plus.civ.ivy.genome

import net.minecraft.server.v1_8_R3.EntityHorse
import net.minecraft.server.v1_8_R3.GenericAttributes
import net.minecraft.server.v1_8_R3.NBTTagCompound
import org.bukkit.ChatColor
import org.bukkit.Location
import org.bukkit.entity.Horse
import org.bukkit.util.noise.SimplexOctaveGenerator
import plus.civ.ivy.config.HorseConfig
import plus.civ.ivy.nms.livestock.IvyHorse
import kotlin.math.pow
import kotlin.math.roundToLong
import kotlin.random.Random

data class HorseGenome(
	var speedSeed: Int,
	var jumpSeed: Int,
	var healthSeed: Int,

	var regenSpeedSeed: Int,
	var regenerating: MendelianGene,

	var noLevelUp: MendelianGene,

	var expressedAppearance: HorseAppearance,
	var retainedAppearance: HorseAppearance,
) {
	companion object {
		fun randomHorse(): HorseGenome {
			return HorseGenome(
				speedSeed = Random.nextInt(),
				jumpSeed = Random.nextInt(),
				healthSeed = Random.nextInt(),

				regenSpeedSeed = Random.nextInt(),
				regenerating = MendelianGene.randomGeneMaybeRecessive(HorseConfig.regeneratingAlleleRate),

				noLevelUp = MendelianGene.randomGeneMaybeMixed(HorseConfig.noLevelUpAlleleRate),

				expressedAppearance = HorseAppearance.randomHorse(),
				retainedAppearance = HorseAppearance.randomHorse(),
			)
		}
	}

	constructor(compound: NBTTagCompound): this(
		speedSeed = compound.getInt("speedSeed"),
		jumpSeed = compound.getInt("jumpSeed"),
		healthSeed = compound.getInt("healthSeed"),

		regenSpeedSeed = compound.getInt("regenSpeedSeed"),
		regenerating = MendelianGene(compound.getString("regenerating")).also { it.letter = 'r' },

		noLevelUp = if (compound.hasKey("noLevelUp"))
			MendelianGene(compound.getString("noLevelUp")).also { it.letter = 'l' }
		else
			MendelianGene.randomGeneMaybeMixed(HorseConfig.noLevelUpAlleleRate),

		expressedAppearance = HorseAppearance(compound.getCompound("expressedAppearance")),
		retainedAppearance = HorseAppearance(compound.getCompound("retainedAppearance")),
	)

	fun saveToNBT(): NBTTagCompound {
		val compound = NBTTagCompound()

		compound.setInt("speedSeed", speedSeed)
		compound.setInt("jumpSeed", jumpSeed)
		compound.setInt("healthSeed", healthSeed)

		compound.setInt("regenSpeedSeed", regenSpeedSeed)
		compound.setString("regenerating", regenerating.toString())

		compound.setString("noLevelUp", noLevelUp.toString())

		compound.set("expressedAppearance", expressedAppearance.saveToNBT())
		compound.set("retainedAppearance", retainedAppearance.saveToNBT())

		return compound
	}

	fun breedWith(partner: HorseGenome): HorseGenome {
		val color = arrayOf(expressedAppearance.color, retainedAppearance.color, partner.expressedAppearance.color, partner.retainedAppearance.color)
			.random()
		var retainedColor = arrayOf(expressedAppearance.color, retainedAppearance.color, partner.expressedAppearance.color, partner.retainedAppearance.color)
			.random()

		if (Math.random() < HorseConfig.appearanceMutationRate) {
			retainedColor = Horse.Color.values().random()
		}

		val style = arrayOf(expressedAppearance.style, retainedAppearance.style, partner.expressedAppearance.style, partner.retainedAppearance.style)
			.random()
		var retainedStyle = arrayOf(expressedAppearance.style, retainedAppearance.style, partner.expressedAppearance.style, partner.retainedAppearance.style)
			.random()

		if (Math.random() < HorseConfig.appearanceMutationRate) {
			retainedStyle = Horse.Style.values().random()
		}

		val variant = arrayOf(expressedAppearance.variant, retainedAppearance.variant, partner.expressedAppearance.variant, partner.retainedAppearance.variant)
			.random()
		var retainedVariant = arrayOf(expressedAppearance.variant, retainedAppearance.variant, partner.expressedAppearance.variant, partner.retainedAppearance.variant)
			.random()

		if (Math.random() < HorseConfig.appearanceMutationRate) {
			retainedVariant = Horse.Variant.values().random()
		}

		val expressedAppearance = HorseAppearance(color, style, variant)
		val retainedAppearance = HorseAppearance(retainedColor, retainedStyle, retainedVariant)

		return HorseGenome(
			speedSeed = if (Random.nextBoolean()) speedSeed else partner.speedSeed,
			jumpSeed = if (Random.nextBoolean()) jumpSeed else partner.jumpSeed,
			healthSeed = if (Random.nextBoolean()) healthSeed else partner.healthSeed,

			regenSpeedSeed = if (Random.nextBoolean()) regenSpeedSeed else partner.regenSpeedSeed,
			regenerating = regenerating.breedWith(partner.regenerating),

			noLevelUp = noLevelUp.breedWith(partner.noLevelUp),

			expressedAppearance = expressedAppearance,
			retainedAppearance = retainedAppearance
		)
	}

	object NoiseGenerators {
		// Devkit: Seeds changed from live server
		val speed = SimplexOctaveGenerator(1325036409103859001L, HorseConfig.noiseOctaves)
		val jumpHeight = SimplexOctaveGenerator(5238594386936458691L, HorseConfig.noiseOctaves)
		val health = SimplexOctaveGenerator(6536958456895430111L, HorseConfig.noiseOctaves)
		val regenSpeed = SimplexOctaveGenerator(46938690321012317L, HorseConfig.noiseOctaves)

		init {
			speed.setScale(HorseConfig.noiseScale)
			jumpHeight.setScale(HorseConfig.noiseScale)
			health.setScale(HorseConfig.noiseScale)
			regenSpeed.setScale(HorseConfig.noiseScale)
		}
	}

	private fun SimplexOctaveGenerator.minMaxNoise(x: Int, min: Double, max: Double): Double {
		val negativeOneToOne = noise(x.toDouble(), 0.5, 0.5, true)
		val zeroToOne = (negativeOneToOne + 1) / 2
		val range = max - min
		val rangeAdjustedNoise = range * zeroToOne
		return rangeAdjustedNoise + min
	}

	/**
	 * Measured in internal minecraft values, see https://minecraft.fandom.com/wiki/Horse#Statistics
	 */
	val speed: Double
		get() = NoiseGenerators.speed.minMaxNoise(speedSeed, HorseConfig.minSpeed, HorseConfig.maxSpeed)

	val speedBlocks: Double
		get() = speed * 43.178 - 0.02141

	/**
	 * Measured in internal minecraft values, see https://minecraft.fandom.com/wiki/Horse#Statistics
	 */
	val jumpHeight: Double
		get() = NoiseGenerators.jumpHeight.minMaxNoise(jumpSeed, HorseConfig.minJump, HorseConfig.maxJump)

	val jumpHeightBlocks: Double
		get() = jumpHeight.pow(1.7) * 5.293

	/**
	 * Maximum health of the horse in half-hearts
	 */
	val health: Double
		get() = NoiseGenerators.health.minMaxNoise(healthSeed, HorseConfig.minHealth, HorseConfig.maxHealth)

	/**
	 * How often a horse will regenerate a heart in ticks
	 */
	val regenSpeed: Double
		get() = if (regenerating.isRecessiveActive)
			NoiseGenerators.regenSpeed.minMaxNoise(regenSpeedSeed, HorseConfig.minRegenSpeed, HorseConfig.maxRegenSpeed)
		else
			Double.POSITIVE_INFINITY

	fun applyToHorse(horse: IvyHorse) {
		val bukkitEntity = horse.bukkitEntity
		if (bukkitEntity is Horse) {
			expressedAppearance.applyToHorse(bukkitEntity)
		}

		horse.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).value = speed
		horse.getAttributeInstance(EntityHorse.attributeJumpStrength).value = jumpHeight
		horse.getAttributeInstance(GenericAttributes.maxHealth).value = health
	}

	fun bookSummary(): String {
		val page = StringBuilder()

		val speedColor = estimateColor(speed, HorseConfig.minSpeed, HorseConfig.maxSpeed)
		page.append("Speed: $speedColor${(speedBlocks * 100).roundToLong().toDouble() / 100}${ChatColor.RESET} m/s\n")
		page.append("($speedSeed)\n")

		val jumpColor = estimateColor(jumpHeight, HorseConfig.minJump, HorseConfig.maxJump)
		page.append(
			"Jump: $jumpColor${
				(jumpHeightBlocks * 100).roundToLong().toDouble() / 100
			}${ChatColor.RESET} blocks\n")
		page.append("($jumpSeed)\n")

		val healthColor = estimateColor(health, HorseConfig.minHealth, HorseConfig.maxHealth)
		page.append("Health: $healthColor${(health.roundToLong().toDouble() / 2)}${ChatColor.RESET} hearts\n")
		page.append("($healthSeed)\n")

		if (regenerating.isRecessiveActive) {
			val regenColor = estimateColor(regenSpeed, HorseConfig.minRegenSpeed, HorseConfig.maxRegenSpeed)
			page.append(
				"Regen: 1 heart every $regenColor${
					regenSpeed.roundToLong().toDouble() / 20
				}${ChatColor.RESET} seconds\n")
			page.append("($regenSpeedSeed)\n")
		}

		if (noLevelUp.isRecessiveActive) {
			page.append("${ChatColor.BOLD}${ChatColor.RED}Incorrigible${ChatColor.RESET}\n")
		}

		return page.toString()
	}

	private fun estimateColor(value: Double, min: Double, max: Double): ChatColor {
		val adjustedValue = value - min
		val percentage = adjustedValue / max

		return if (percentage > 0.666666) {
			ChatColor.GREEN
		} else if (percentage > 0.333333) {
			ChatColor.GOLD
		} else {
			ChatColor.RED
		}
	}

	//private var hologram: Hologram? = null

	fun displayHologram(horseLocation: Location) {
		//if (!Bukkit.getPluginManager().isPluginEnabled("HolographicDisplays")) {
		//	return
		//}

		//val holographicDisplays = HolographicDisplaysAPI.get(Ivy.instance)

		//this.hologram?.delete()
		//val hologram = holographicDisplays.createHologram(horseLocation.add(0.0, 3.0, 0.0))
		//this.hologram = hologram

		//val lines = hologram.lines
		//lines.clear()

		//val speedColor = estimateColor(speed, HorseConfig.minSpeed, HorseConfig.maxSpeed)
		//lines.appendText("Speed: $speedColor${(speedBlocks * 100).roundToLong().toDouble() / 100}${ChatColor.RESET} m/s")

		//val jumpColor = estimateColor(jumpHeight, HorseConfig.minJump, HorseConfig.maxJump)
		//lines.appendText("Jump: $jumpColor${(jumpHeightBlocks * 100).roundToLong().toDouble() / 100}${ChatColor.RESET} blocks")

		//val healthColor = estimateColor(health, HorseConfig.minHealth, HorseConfig.maxHealth)
		//lines.appendText("Health: $healthColor${(health.roundToLong().toDouble() / 2)}${ChatColor.RESET} hearts")

		//if (regenerating.isRecessiveActive) {
		//	val regenColor = estimateColor(regenSpeed, HorseConfig.minRegenSpeed, HorseConfig.maxRegenSpeed)
		//	lines.appendText(
		//		"Regen: 1 heart every $regenColor${
		//			regenSpeed.roundToLong().toDouble() / 20
		//		}${ChatColor.RESET} seconds")
		//}

		//object : BukkitRunnable() {
		//	override fun run() {
		//		hologram.delete()
		//	}
		//}.runTaskLater(Ivy.instance, 5L * 20L)
	}

	fun meritSpeed(points: Int) {
		val oldSpeed = speed

		// lazy algorithm using mutation
		speedSeed += points
		val plusSpeed = speed
		speedSeed -= points

		speedSeed -= points
		val minusSpeed = speed
		speedSeed += points

		if (oldSpeed > plusSpeed && oldSpeed > minusSpeed) {
			return
		} else if (plusSpeed > minusSpeed) {
			speedSeed += points
		} else {
			speedSeed -= points
		}
	}

	fun meritJump(points: Int) {
		val oldJump = jumpHeight

		// lazy algorithm using mutation
		jumpSeed += points
		val plusJump = jumpHeight
		jumpSeed -= points

		jumpSeed -= points
		val minusJump = jumpHeight
		jumpSeed += points

		if (oldJump > plusJump && oldJump > minusJump) {
			return
		} else if (plusJump > minusJump) {
			jumpSeed += points
		} else {
			jumpSeed -= points
		}
	}

	fun meritHealth(points: Int) {
		val oldHealth = health

		// lazy algorithm using mutation
		healthSeed += points
		val plusHealth = health
		healthSeed -= points

		healthSeed -= points
		val minusHealth = health
		healthSeed += points

		if (oldHealth > plusHealth && oldHealth > minusHealth) {
			return
		} else if (plusHealth > minusHealth) {
			healthSeed += points
		} else {
			healthSeed -= points
		}
	}
}

data class HorseAppearance(
	val color: Horse.Color,
	val style: Horse.Style,
	val variant: Horse.Variant,
) {
	companion object {
		fun randomHorse(): HorseAppearance {
			return HorseAppearance(
				color = Horse.Color.values().random(),
				style = Horse.Style.values().random(),
				variant = listOf(Horse.Variant.HORSE, Horse.Variant.HORSE, Horse.Variant.HORSE, Horse.Variant.HORSE,
					Horse.Variant.HORSE, Horse.Variant.HORSE, Horse.Variant.HORSE, Horse.Variant.HORSE,
					Horse.Variant.DONKEY, Horse.Variant.MULE).random(),
			)
		}
	}

	constructor(nbt: NBTTagCompound): this(
		color = Horse.Color.values()[nbt.getInt("color")],
		style = Horse.Style.values()[nbt.getInt("style")],
		variant = Horse.Variant.values()[nbt.getInt("variant")]
	)

	fun saveToNBT(): NBTTagCompound {
		val compound = NBTTagCompound()
		compound.setInt("color", color.ordinal)
		compound.setInt("style", style.ordinal)
		compound.setInt("variant", variant.ordinal)
		return compound
	}

	fun applyToHorse(horse: Horse) {
		horse.color = color
		horse.style = style
		horse.variant = variant
	}
}
