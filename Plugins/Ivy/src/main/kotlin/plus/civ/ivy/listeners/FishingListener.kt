package plus.civ.ivy.listeners

import net.minecraft.server.v1_8_R3.EntityFishingHook
import org.bukkit.ChatColor
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity
import org.bukkit.enchantments.Enchantment
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerFishEvent
import plus.civ.ivy.config.FishingConfig
import vg.civcraft.mc.civmodcore.loottable.TieredLootTable
import vg.civcraft.mc.civmodcore.random.SECURE_RANDOM
import vg.civcraft.mc.civmodcore.random.nextIntUniform
import java.security.SecureRandom

class FishingListener: Listener {
	private val rng = SecureRandom()

	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
	fun onPlayerCast(event: PlayerFishEvent) {
		if (event.state != PlayerFishEvent.State.FISHING) return

		val lureLevel = event.player.itemInHand.getEnchantmentLevel(Enchantment.LURE)
		val lureModifier = lureLevel * FishingConfig.LURE_MODIFIER
		val fishingTime = SECURE_RANDOM.nextIntUniform(FishingConfig.FISHING_TIME.first, FishingConfig.FISHING_TIME.last)
		val modifiedFishingTime = (fishingTime - lureModifier).coerceAtLeast(1)

		val nmsHook = (event.hook as CraftEntity).handle as EntityFishingHook
		nmsHook.javaClass.getDeclaredField("aw")
			.also{ it.isAccessible = true }
			.setInt(nmsHook, modifiedFishingTime)
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	fun onPlayerHookFish(event: PlayerFishEvent) {
		if (event.state != PlayerFishEvent.State.CAUGHT_FISH) return

		val lightLevel = event.hook.location.block.lightFromSky
		if (lightLevel < 10) {
			event.player.sendMessage("${ChatColor.YELLOW}You cannot fish in an area that's not exposed to the sky.")
			event.isCancelled = true
			return
		}
	}

	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	fun onPlayerCatchFish(event: PlayerFishEvent) {
		if (event.state != PlayerFishEvent.State.CAUGHT_FISH) return

		val biome = event.hook.location.block.biome
		val loottable = FishingConfig.FISHING_LOOT.getValue(biome)
		val chosenItem = if (loottable is TieredLootTable) {
			loottable.getRandomDrop(event.player.itemInHand.getEnchantmentLevel(Enchantment.LUCK))
		} else {
			loottable.getRandomDrop()
		}
		event.caught.remove()
		event.player.location.world.dropItemNaturally(event.player.location, chosenItem)
	}
}
