package plus.civ.ivy.commands

import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Ageable
import org.bukkit.entity.Player
import plus.civ.kotlinplugin.CommandInfo

@CommandInfo(
	name = "growbaby",
	description = "Grows all baby entities within 5 blocks instantly",
	permission = "ivy.growbaby",
	usage = "/<command>",
)
class GrowBaby: CommandExecutor {
	override fun onCommand(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): Boolean {
		if (sender !is Player) {
			sender.sendMessage("${ChatColor.RED}This command must be ran by a player")
			return true
		}

		sender.location.world.getNearbyEntities(sender.location, 5.0, 5.0, 5.0)
			.filterIsInstance<Ageable>()
			.filter { !it.isAdult }
			.forEach { it.setAdult() }

		sender.sendMessage("${ChatColor.GREEN}All entities within 5 blocks are now adults.")
		return true
	}
}
