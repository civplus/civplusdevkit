package plus.civ.ivy.config

object HorseConfig {
	/**
	 * How often a horse will be bred with a appearance trait that matches none of its ancestors
	 */
	var appearanceMutationRate = 0.01

	/**
	 * How often a recessive allele for regeneration shows up on a horse that's naturally spawned
	 */
	var regeneratingAlleleRate = 0.2

	var noLevelUpAlleleRate = 0.1

	/**
	 * The perlin noise scale factor for horse attributes like speed, jump, etc.
	 *
	 * Changing this value to be smaller will make it easier but slower to get an exact value.
	 *
	 * Changing this value will scramble the statistics of all horses that exist on the live map.
	 * It shall only be changed at SOTW.
	 */
	val noiseScale = 0.001

	/**
	 * How many octaves should be used for the attribute noise generators
	 *
	 * More octaves will result in more false peaks, because the signal will be rougher
	 */
	val noiseOctaves = 1 // currently a bad idea to have false peaks when we're doing our own gradient ascent

	val minSpeed = 0.112
	val maxSpeed = 0.338

	val minJump = 0.3
	val maxJump = 1.1

	val minHealth = 10.0
	val maxHealth = 35.0

	val minRegenSpeed = 1.0
	val maxRegenSpeed = 12000.0 // 10 minutes in ticks

	val blocksPerSpeedPoint = 500
	val pointsPerJump = 1
	val pointsPerHalfHeartDamage = 1

	val minDistanceWalkedBetweenJumpsForLevelUp = 16.0
}
