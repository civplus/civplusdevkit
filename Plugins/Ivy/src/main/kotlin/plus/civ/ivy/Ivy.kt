package plus.civ.ivy

import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.material.Tree
import plus.civ.ivy.config.PlantsConfig
import plus.civ.ivy.config.growthRate
import plus.civ.ivy.nms.registerCustomEntities
import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PermissionDefault
import plus.civ.kotlinplugin.PluginInfo
import plus.civ.kotlinplugin.PluginPermission

@PluginInfo(
	name = "Ivy",
	author = "sepia",
	description = "Control farming growth/loot rates and give custom farming drops",
	depends = ["CivModCore", "Remnant"],
	softDepends = ["MeansOfProduction", "EXPloration", "HolographicDisplays"],
	loadEarly = true, // i think maybe we need this to register custom entities
	permissions = [
		PluginPermission(
			name = "ivy.growbaby",
			description = "Allows using the growbaby command to grow entities instantly",
			default = PermissionDefault.OP
		)
	]
)
class Ivy: KotlinPlugin() {
	companion object {
		private var _instance: Ivy? = null
		val instance: Ivy
			get() = _instance!!
	}

	override fun onEnable() {
		_instance = this
		super.onEnable()

		registerCustomEntities()
	}
}

/**
 * @return the configured rate the crop or tree ought to grow at
 */
fun Block.growthRate(): Float {
	val material = this.type
	if (material == Material.SAPLING) {
		val species = (this.state.data as Tree).species
		val conf = PlantsConfig.TREE_CONFIGS[species] ?: return 1.0f
		return conf.growthRate(this.biome, this.lightFromSky)
	} else {
		val conf = PlantsConfig.PLANT_CONFIGS[this.type] ?: return 1.0f
		return conf.growthRate(this.biome, this.lightFromSky)
	}
}
