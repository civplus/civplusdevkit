package plus.civ.ivy.config

import org.bukkit.Material
import org.bukkit.TreeSpecies
import org.bukkit.block.Biome
import org.bukkit.block.Block
import org.bukkit.material.Tree
import plus.civ.ivy.config.PlantsConfig.PLANT_CONFIGS
import plus.civ.ivy.config.PlantsConfig.TREE_CONFIGS
import plus.civ.remnant.items.CustomItem
import plus.civ.remnant.items.QuarkIngredient

/**
 * Growth rates are multipliers on the vanilla growth speed of the given crop
 * NOTE: Growth rates are always supposed to be in the range [0.0, 1.0]
 *
 * @param defaultGrowthRate the growth rate (from 0.0 to 1.0) of the crop in biomes that aren't configured otherwise
 * @param biomeGrowthRate a map of growth rates (from 0.0 to 1.0) for specific biomes
 * @param darknessMultiplier the multiplier to apply if the crop is in darkness (this is applied linearly depending on how dark)
 */
data class TickedGrowthConfig(
	val defaultGrowthRate: Float,
	val biomeGrowthRate: Map<Biome, Float> = mapOf(),
	val darknessMultiplier: Float? = null
)

data class PlantDropConfig(
	val material: Material,
	val chance: Double,
	val drop: CustomItem,
)

object PlantsConfig {
	const val DEFAULT_SUNLIGHT_MULTIPLIER = 0.05f

	/*** PLANTS ***/
	val PLANT_CONFIGS: Map<Material, TickedGrowthConfig> = mapOf(
		Pair(Material.CROPS, TickedGrowthConfig(0.001f, mapOf(
			Pair(Biome.PLAINS, 0.05f),
		))),
		Pair(Material.RED_MUSHROOM, TickedGrowthConfig(0.001f, mapOf(
			Pair(Biome.MUSHROOM_ISLAND, 0.1f),
		))),
		Pair(Material.BROWN_MUSHROOM, TickedGrowthConfig(0.001f, mapOf(
			Pair(Biome.MUSHROOM_ISLAND, 0.1f),
		))),
		Pair(Material.POTATO, TickedGrowthConfig(0.001f, mapOf(
			Pair(Biome.ICE_PLAINS, 0.1f),
			Pair(Biome.EXTREME_HILLS, 0.05f)
		))),
		Pair(Material.COCOA, TickedGrowthConfig(0.001f, mapOf(
			Pair(Biome.JUNGLE, 0.2f)
		))),
		Pair(Material.CACTUS, TickedGrowthConfig(0.001f, mapOf(
			Pair(Biome.DESERT, 0.05f),
			Pair(Biome.BEACH, 0.01f),
			Pair(Biome.MESA, 0.01f)
		))),
		Pair(Material.SUGAR_CANE_BLOCK, TickedGrowthConfig(0.001f, mapOf(
			Pair(Biome.JUNGLE, 0.1f),
			Pair(Biome.MESA, 0.1f)
		))),
		Pair(Material.PUMPKIN, TickedGrowthConfig(0.001f, mapOf(
			Pair(Biome.ICE_PLAINS, 0.1f),
			Pair(Biome.MEGA_SPRUCE_TAIGA, 0.1f)
		))),
		Pair(Material.PUMPKIN_STEM, TickedGrowthConfig(0.001f, mapOf(
			Pair(Biome.ICE_PLAINS, 0.2f),
			Pair(Biome.EXTREME_HILLS, 0.5f),
			Pair(Biome.MEGA_SPRUCE_TAIGA, 0.5f)
		))),
		Pair(Material.MELON_BLOCK, TickedGrowthConfig(0.001f, mapOf(
			Pair(Biome.ICE_PLAINS, 0.1f),
			Pair(Biome.EXTREME_HILLS, 0.1f),
			Pair(Biome.OCEAN, 0.025f)
		))),
		Pair(Material.MELON_STEM, TickedGrowthConfig(0.001f, mapOf(
			Pair(Biome.ICE_PLAINS, 0.1f),
			Pair(Biome.OCEAN, 0.025f)
		))),
		Pair(Material.NETHER_WARTS, TickedGrowthConfig(0.001f, mapOf(
			Pair(Biome.ICE_PLAINS, 0.05f),
			Pair(Biome.PLAINS, 0.025f),
			Pair(Biome.DESERT, 0.1f),
		))),
		Pair(Material.CARROT, TickedGrowthConfig(0.001f, mapOf(
			Pair(Biome.ICE_PLAINS, 0.05f),
			Pair(Biome.SWAMPLAND, 0.15f)
		))),
	)

	val PLANT_LOOT_BASE_CHANCE = 0.0314159 // Devkit snip: Changed from live server to encourage codemaker despair

	val PLANT_LOOT: Set<PlantDropConfig> = setOf(
		PlantDropConfig(
			material = Material.CROPS,
			chance = PLANT_LOOT_BASE_CHANCE,
			drop = QuarkIngredient.GoldenFlour
		),
		// Devkit snip
	)

	val PLANT_LOOT_MATERIALS = PLANT_LOOT.map { it.material }.toSet()

	/*** TREES ***/
	val TREE_CONFIGS: Map<TreeSpecies, TickedGrowthConfig> = mapOf(
		Pair(TreeSpecies.GENERIC, TickedGrowthConfig(0.01f, mapOf(
			Pair(Biome.FOREST, 1.0f),
			Pair(Biome.PLAINS, 1.0f),
		))),
		Pair(TreeSpecies.REDWOOD, TickedGrowthConfig(0.01f, mapOf(
			Pair(Biome.TAIGA, 1.0f),
			Pair(Biome.ICE_PLAINS, 1.0f),
		))),
		Pair(TreeSpecies.JUNGLE, TickedGrowthConfig(0.01f, mapOf(
			Pair(Biome.JUNGLE, 1.0f),
		))),
		Pair(TreeSpecies.BIRCH, TickedGrowthConfig(0.01f, mapOf(
			Pair(Biome.BIRCH_FOREST, 1.0f),
		))),
		Pair(TreeSpecies.ACACIA, TickedGrowthConfig(0.01f, mapOf(
			Pair(Biome.MESA, 1.0f),
		))),
		Pair(TreeSpecies.DARK_OAK, TickedGrowthConfig(0.01f, mapOf(
			Pair(Biome.FOREST, 1.0f),
		))),
	)
}

fun TickedGrowthConfig.growthRate(biome: Biome, sunlightPresent: Byte): Float {
	// the multiplier to apply if the crop is in complete darkness
	val multiplierInDarkness = this.darknessMultiplier ?: PlantsConfig.DEFAULT_SUNLIGHT_MULTIPLIER
	// lerp from darkness multiplier up to 100%, depending on light level present (note that max lightlevel is 16)
	val lightMultiplier = multiplierInDarkness + (1.0f - multiplierInDarkness) * (sunlightPresent / 15.0f)
	// return the coefficient for the given biome if there is one, otherwise return the default coefficient for the plant
	return (this.biomeGrowthRate[biome] ?: this.defaultGrowthRate) * lightMultiplier
}

val Block.blockGrowthConfig: TickedGrowthConfig?
	get() {
		if (this.state.data is Tree && this.type == Material.SAPLING) {
			return TREE_CONFIGS[(this.state.data as Tree).species]
		} else {
			return PLANT_CONFIGS[this.type]
		}
	}

val TickedGrowthConfig.maxGrowthRate: Float
	get() {
		val maxBiome = this.biomeGrowthRate.values.maxOrNull() ?: this.defaultGrowthRate
		return maxBiome.coerceAtLeast(this.defaultGrowthRate)
	}
