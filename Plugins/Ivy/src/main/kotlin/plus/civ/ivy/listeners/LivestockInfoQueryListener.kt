package plus.civ.ivy.listeners

import org.bukkit.Material
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.inventory.meta.BookMeta
import plus.civ.ivy.nms.livestock.IvyHorse
import plus.civ.ivy.nms.livestock.IvyLivestock
import plus.civ.ivy.shepherdsGrimoireExpression
import vg.civcraft.mc.civmodcore.expression.matches

class LivestockInfoQueryListener: Listener {
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = false)
	fun onEntityDamageByPlayerWithGrimoire(event: EntityDamageByEntityEvent) {
		if (event.damager !is Player) return
		val player = event.damager as Player
		if (player.itemInHand.type != Material.WRITTEN_BOOK) return
		if ((event.entity as CraftEntity).handle !is IvyLivestock) return
		val book = player.itemInHand
		if (!shepherdsGrimoireExpression.matches(book)) return

		val bookMeta = book.itemMeta as BookMeta
		val animal = (event.entity as CraftEntity).handle as IvyLivestock
		val pages = animal.getHappinessProfile().toMutableList()

		if (animal is IvyHorse) {
			animal.genome?.bookSummary()?.let { pages.add(it) }
			animal.genome?.displayHologram(animal.bukkitEntity.location)
			animal.genome?.applyToHorse(animal)
		}

		bookMeta.pages = pages
		book.itemMeta = bookMeta

		event.isCancelled = true
	}
}
