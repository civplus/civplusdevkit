package plus.civ.ivy.listeners

import net.minecraft.server.v1_8_R3.*
import org.bukkit.craftbukkit.v1_8_R3.CraftChunk
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.world.ChunkLoadEvent
import org.bukkit.event.world.ChunkUnloadEvent
import org.bukkit.scheduler.BukkitRunnable
import plus.civ.ivy.Ivy
import plus.civ.ivy.growthRate
import java.lang.reflect.InvocationTargetException
import java.util.*
import kotlin.math.pow

/**
 * Whenever a chunk loads, all crops in that chunk experience simulated growth based on the timestamp the chunk was last
 * saved at. We don't directly simulate a real amount of growth ticks. Instead, we call a few growth ticks, supplying a fake random
 * (for some crops), or (for other crops) we derive mathematically the chance that it should have grown and do a single roll.
 *
 * Ticking every block in every chunk that loads is pretty expensive, so we have to do it piecemeal over time. A single
 * Runnable manages all of the chunks currently being ticked. That way, if the chunk unloads, we can finish it all at once.
 */

private val CROP_CLASSES = arrayOf(BlockCrops::class, BlockStem::class, BlockNetherWart::class, BlockMushroom::class, BlockSapling::class, BlockPotatoes::class, BlockCarrots::class)

private data class TimeMachineChunk(val nmsChunk: Chunk, val ticksPassed: Int, var y: Int)
object CropTickerTimeMachine: BukkitRunnable() {
	private val activeChunks = HashMap<ChunkCoordIntPair, TimeMachineChunk>()

	override fun run() {
		for (_i in 0..(activeChunks.size)) { // try to run through more sections the more full the list gets
			if (activeChunks.isNotEmpty()) {
				val currentChunk = activeChunks.values.first()
				continueChunk(currentChunk)
			}
		}
	}

	private fun continueChunk(chunk: TimeMachineChunk) {
		for (x in 0..15) {
			for (z in 0..15) {
				val position = BlockPosition(16 * chunk.nmsChunk.locX + x, chunk.y, 16 * chunk.nmsChunk.locZ + z)
				val nmsBlock = chunk.nmsChunk.getType(position)
				if (nmsBlock::class in CROP_CLASSES) {
					simulateTicksOnPlant(chunk.nmsChunk, nmsBlock, position, chunk.ticksPassed)
				}
			}
		}
		if (chunk.y == 255) {
			activeChunks.remove(chunk.nmsChunk.j()) // .j() is the chunk coordinate pair (i think)
		}
		chunk.y++
	}

	fun isChunkHot(coords: ChunkCoordIntPair): Boolean {
		return coords in activeChunks
	}

	fun getInTheDelorean(chunk: Chunk, ticksPassed: Int) {
		val coord = chunk.j()
		if (activeChunks[coord] == null) {
			activeChunks[chunk.j()] = TimeMachineChunk(chunk, ticksPassed, 0)
		}
	}

	fun exhaustChunk(chunk: Chunk) {
		val coord = chunk.j()
		val tmcChunk = activeChunks[coord] ?: return
		while (tmcChunk.y < 255) {
			continueChunk(tmcChunk)
		}
	}

	private fun simulateTicksOnPlant(chunk: Chunk, block: Block, blockPosition: BlockPosition, ticksPassed: Int) {
		val nmsWorld = chunk.world
		val blockData = chunk.getBlockData(blockPosition)
		// we need the growth modifier that ivy applies, for our math
		val ivyGrowthModifier = chunk.bukkitChunk.getBlock(blockPosition.x, blockPosition.y, blockPosition.z).growthRate()
		// create the rigged random object
		val sovietRandom = SovietRandom(ticksPassed, ivyGrowthModifier)
		// and call the random update function, passing the rigged random object
		// this method is going to take a Random object, which the block uses to roll whether it should grow.
		// we give it a fake random object, which will send it rigged numbers depending on how many ticks have passed
		block.b(nmsWorld, blockPosition, blockData, sovietRandom)
		// and potentially repeat for multiple growth ticks
		var i = 0
		while (sovietRandom.repeat && i <= 8) {
			// IBlockData is immutable. if we don't refetch, we end up repeating the same growth stage over and over
			val currentBlockData: IBlockData = chunk.getBlockData(blockPosition)
			try {
				block.b(nmsWorld, blockPosition, currentBlockData, sovietRandom)
			} catch (e: InvocationTargetException) { // this can happen when a sappling becomes a log, for obvious reasons
				break
			}
			i++
		}
	}
}

class ChunkLoadListener: Listener {
	init {
		CropTickerTimeMachine.runTaskTimer(Ivy.instance, 0, 1)
	}

	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	fun onChunkLoad(event: ChunkLoadEvent) {
		if (event.isNewChunk) return

		val nmsChunk = (event.chunk as CraftChunk).handle
		val lastSaved = getChunkLastUpdated(event.world, event.chunk.x, event.chunk.z)
		val ticksPassed = (nmsChunk.world.time - lastSaved).toInt()

		CropTickerTimeMachine.getInTheDelorean(nmsChunk, ticksPassed)
	}

	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	fun onChunkUnload(event: ChunkUnloadEvent) {
		val nmsChunk = (event.chunk as CraftChunk).handle
		CropTickerTimeMachine.exhaustChunk(nmsChunk)
	}

	private fun getChunkLastUpdated(world: org.bukkit.World, x: Int, z: Int): Long {
		// the private `lastSaved` variable on Chunk is a lie.
		// when Spigot loads a Chunk, it does not populate that field with the real `lastSaved`
		// value. however, the LastUpdate really is saved in the NBT file. therefore, we must get
		// the Chunk's NBT file. however, Chunk itself doesn't save a reference to this. instead,
		// we have to manually load the chunk again and get the NBT data (even if it was just loaded).
		// spigot is bad btw
		val worldServer: WorldServer = (world as CraftWorld).handle
		val chunkProviderServer: ChunkProviderServer = worldServer.chunkProviderServer
		val chunkLoader: ChunkRegionLoader = chunkProviderServer.javaClass.getDeclaredField("chunkLoader")
			.also { it.isAccessible = true }
			.get(chunkProviderServer)
				as ChunkRegionLoader
		val loadedChunkData = chunkLoader.loadChunk(worldServer, x, z)
		val chunkNBT = loadedChunkData[1] as NBTTagCompound
		return chunkNBT.getCompound("Level").getLong("LastUpdate")
	}
}

private class SovietRandom(val ticksPassed: Int, val ivyGrowthModifier: Float): Random() {
	var repeat = false

	// in vanilla, m is passed as a max, and if the roll is 0, the crop grows. any non'zero return means no growth
	override fun nextInt(m: Int): Int {
		// the 946 number may look magic. the function that we're about to call (`b`) is the random block tick function
		// the random block tick function is called on a few randomly selected blocks in the chunk every tick. on average
		// it's called every 946.03 ticks for a given block, therefore the real amount of rolls we want to simulate is the
		// ticks that have elapsed / 946, not just the raw ticks that have elapsed. if we wanted to be fancy, we would
		// actually get the randomTickSpeed gamerule (which defaults to 3, which is the number for which the result is 946)
		// and adjust this accordingly. i don't actually know how to grab that, though, and Civ+ uses the default one anyway
		val chanceOfGrowth = 1.0 - (1.0 - ivyGrowthModifier * 1.0 / m).pow(ticksPassed / 946)
		// we roll for growth
		val roll = Math.random()
		if (roll < chanceOfGrowth) {
			// if we are going to grow, we roll again until we miss, to maybe grow even more
			repeat = true
			return 0
		} else {
			repeat = false
			return 1
		}
	}
}
