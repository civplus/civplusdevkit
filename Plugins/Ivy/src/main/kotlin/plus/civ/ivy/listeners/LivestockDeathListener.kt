package plus.civ.ivy.listeners

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDeathEvent
import plus.civ.ivy.config.LivestockConfig
import plus.civ.ivy.nms.livestock.IvyLivestock

class LivestockDeathListener: Listener {
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = false)
	fun onLivestockDeath(event: EntityDeathEvent) {
		val nmsEntity = (event.entity as CraftEntity).handle
		if (nmsEntity !is IvyLivestock) return

		val lootTable = LivestockConfig.LIVESTOCK_LOOT[nmsEntity.javaClass] ?: return
		event.drops.clear()
		event.drops.addAll(lootTable.getRandomDrops())
	}
}
