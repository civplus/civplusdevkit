package plus.civ.ivy.config

import org.bukkit.Material
import org.bukkit.block.Biome
import org.bukkit.entity.EntityType
import plus.civ.ivy.nms.livestock.IvyCow
import vg.civcraft.mc.civmodcore.expression.*
import vg.civcraft.mc.civmodcore.extensions.hoursToSeconds
import vg.civcraft.mc.civmodcore.loottable.LootTable
import vg.civcraft.mc.civmodcore.loottable.LootTableEntry
import vg.civcraft.mc.civmodcore.loottable.TieredLootTable
import vg.civcraft.mc.civmodcore.random.NormalItemAmount
import vg.civcraft.mc.civmodcore.random.UniformByte

data class LivestockHappinessConfig(
	val defaultGrowthTime: Long, // time to grow up, in seconds
	val favoriteBiomes: List<Biome> = listOf(), // biomes the livestock likes to be in
	val favoriteBlocks: List<Material> = listOf(Material.GRASS, Material.DIRT), // blocks the livestock likes stand on
	val favoriteFriends: List<EntityType> = listOf(), // EntityTypes that the livestock wants to be around
	val biomeWeight: Float = 0.6f, // hapiness added for being in a liked biome
	val blockWeight: Float = 0.1f, // happiness added for standing on a liked block
	val friendsWeight: Float = 0.1f, // happiness added for each friend within 25 blocks
	val overcrowdingWeight: Float = 0.05f, // happiness deducted for each non'friend LivingEntity within 3 blocks
	val maxFriends: Int = 3, // more friends than this will not contribute to happiness
)

object LivestockConfig {
	val LIVESTOCK_CONFIGS: HashMap<EntityType, LivestockHappinessConfig> = HashMap()
	init {
		// Devkit snip: All of the config for blocks, biomes, and friends
		LIVESTOCK_CONFIGS[EntityType.COW] = LivestockHappinessConfig(
			defaultGrowthTime = (24L).hoursToSeconds,
			favoriteBiomes = listOf(Biome.PLAINS),
			favoriteBlocks = listOf(Material.GRASS),
			favoriteFriends = listOf(EntityType.PLAYER),
			overcrowdingWeight = 0.01f,
		)
		LIVESTOCK_CONFIGS[EntityType.PIG] = LivestockHappinessConfig(
			defaultGrowthTime = (24L).hoursToSeconds,
			favoriteBiomes = listOf(Biome.PLAINS),
			favoriteBlocks = listOf(Material.GRASS),
			favoriteFriends = listOf(EntityType.PLAYER),
			overcrowdingWeight = 0.01f,
		)
		LIVESTOCK_CONFIGS[EntityType.SHEEP] = LivestockHappinessConfig(
			defaultGrowthTime = (24L).hoursToSeconds,
			favoriteBiomes = listOf(Biome.PLAINS),
			favoriteBlocks = listOf(Material.GRASS),
			favoriteFriends = listOf(EntityType.PLAYER),
		)
		LIVESTOCK_CONFIGS[EntityType.HORSE] = LivestockHappinessConfig(
			defaultGrowthTime = (24L).hoursToSeconds,
			favoriteBiomes = listOf(Biome.PLAINS),
			favoriteBlocks = listOf(Material.GRASS),
			favoriteFriends = listOf(EntityType.PLAYER),
			overcrowdingWeight = 0.1f,
		)
		LIVESTOCK_CONFIGS[EntityType.CHICKEN] = LivestockHappinessConfig(
			defaultGrowthTime = (24L).hoursToSeconds,
			favoriteBiomes = listOf(Biome.PLAINS),
			favoriteBlocks = listOf(Material.GRASS),
			favoriteFriends = listOf(EntityType.PLAYER),
			overcrowdingWeight = 0.01f,
		)
		LIVESTOCK_CONFIGS[EntityType.RABBIT] = LivestockHappinessConfig(
			defaultGrowthTime = (24L).hoursToSeconds,
			favoriteBiomes = listOf(Biome.PLAINS),
			favoriteBlocks = listOf(Material.GRASS),
			favoriteFriends = listOf(EntityType.PLAYER),
			overcrowdingWeight = 0.01f,
		)
		LIVESTOCK_CONFIGS[EntityType.MUSHROOM_COW] = LivestockHappinessConfig(
			defaultGrowthTime = (24L).hoursToSeconds,
			favoriteBiomes = listOf(Biome.PLAINS),
			favoriteBlocks = listOf(Material.GRASS),
			favoriteFriends = listOf(EntityType.PLAYER),
			overcrowdingWeight = 0.01f,
		)
	}

	val LIVESTOCK_LOOT = HashMap<Class<out net.minecraft.server.v1_8_R3.Entity>, LootTable>()
	init {
		val normalCowLootTable = LootTable(
			LootTableEntry(
				generator = NBTExpression().id(Material.LEATHER).toItemGenerator(UniformByte(1, 2)),
				chance = 0.8f,
			),
			LootTableEntry(
				generator = NBTExpression().id(Material.RAW_BEEF).toItemGenerator(NormalItemAmount(2, 1.0f)),
				chance = 1.0f,
			),
		)
		val rareCowLootTable = LootTable(
			LootTableEntry(
				generator = NBTExpression().id(Material.LEATHER).toItemGenerator(UniformByte(3, 8)),
				chance = 1.0f,
			),
			LootTableEntry(
				generator = NBTExpression().id(Material.RAW_BEEF).toItemGenerator(NormalItemAmount(3, 1.0f)),
				chance = 1.0f,
			),
		)
		LIVESTOCK_LOOT[IvyCow::class.java] = TieredLootTable(normalCowLootTable, rareCowLootTable)
	}
}
