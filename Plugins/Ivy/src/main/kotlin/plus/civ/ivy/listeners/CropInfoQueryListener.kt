package plus.civ.ivy.listeners

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockDamageEvent
import org.bukkit.inventory.meta.BookMeta
import plus.civ.ivy.config.blockGrowthConfig
import plus.civ.ivy.config.maxGrowthRate
import plus.civ.ivy.gardenersGrimoireExpression
import plus.civ.ivy.growthRate
import vg.civcraft.mc.civmodcore.expression.matches
import vg.civcraft.mc.civmodcore.extensions.prettyName
import java.util.*
import kotlin.math.roundToInt

class CropInfoQueryListener: Listener {
	private val lastMessagedTimestamps: HashMap<UUID, Long> = HashMap()

	/**
	 * Fill the player's grimoire when they hit a crop
	 */
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
	fun onPlayerHitBlock(event: BlockDamageEvent) {
		if (event.itemInHand.type != Material.WRITTEN_BOOK) return
		val book = event.itemInHand
		if (!gardenersGrimoireExpression.matches(book)) return
		// return if the plant is not configured with a special growth rate
		event.block.blockGrowthConfig ?: return

		val pages = getCropInfoPages(event.block)
		val bookMeta = book.itemMeta as BookMeta
		bookMeta.pages = pages
		book.itemMeta = bookMeta

		event.isCancelled = true
	}

	private fun getCropInfoPages(block: Block): List<String> {
		val conf = block.blockGrowthConfig!!
		val sun = block.lightFromSky
		val maxGrowthRate = conf.maxGrowthRate
		val biomeGrowthRate = (conf.biomeGrowthRate[block.biome] ?: 0.0f)
		val bestBiome = biomeGrowthRate >= maxGrowthRate - 0.00001f
		val okayBiome = biomeGrowthRate >= conf.defaultGrowthRate
		val growthRatePercent = ((block.growthRate() / maxGrowthRate) * 100).roundToInt()

		val cropPage = StringBuilder()
		cropPage.appendLine("${ChatColor.GOLD}${ChatColor.BOLD}${block.type.prettyName}")
		cropPage.append("${ChatColor.RESET}Sunlight: ")
		cropPage.appendLine("${if (sun == 15.toByte()) ChatColor.GREEN else if (sun > 8) ChatColor.GOLD else ChatColor.RED}$sun/15")
		cropPage.append("${ChatColor.RESET}Biome: ")
		cropPage.appendLine("${if (bestBiome) ChatColor.GREEN else if (okayBiome) ChatColor.GOLD else ChatColor.RED}${block.biome.prettyName}")
		cropPage.append("${ChatColor.RESET}Growthrate: ")
		cropPage.appendLine("${if (growthRatePercent > 95) ChatColor.GREEN else if (growthRatePercent > 50) ChatColor.GOLD else ChatColor.RED}$growthRatePercent%")

		val infoPage = StringBuilder()
		infoPage.appendLine("${ChatColor.GOLD}${ChatColor.BOLD}Biome Growth Rates")
		for (pair in conf.biomeGrowthRate) {
			infoPage.append("${ChatColor.RESET}${pair.key.prettyName}: ")
			val biomePercent = ((pair.value / maxGrowthRate) * 100).roundToInt()
			infoPage.appendLine("${if (biomePercent > 95) ChatColor.GREEN else if (biomePercent > 50) ChatColor.GOLD else ChatColor.RED}$biomePercent%")
		}
		infoPage.append("${ChatColor.RESET}Anywhere else: ")
		val defaultPercent = ((conf.defaultGrowthRate / maxGrowthRate) * 100).roundToInt()
		infoPage.appendLine("${if (defaultPercent > 95) ChatColor.GREEN else if (defaultPercent > 50) ChatColor.GOLD else ChatColor.RED}$defaultPercent%")

		return listOf(cropPage.toString(), infoPage.toString())
	}
}
