package plus.civ.ivy.nms.livestock

import net.minecraft.server.v1_8_R3.EntityAgeable
import net.minecraft.server.v1_8_R3.EntityHorse
import net.minecraft.server.v1_8_R3.NBTTagCompound
import net.minecraft.server.v1_8_R3.World
import org.bukkit.craftbukkit.v1_8_R3.event.CraftEventFactory
import org.bukkit.entity.LivingEntity
import plus.civ.ivy.Ivy
import plus.civ.ivy.config.LivestockConfig
import plus.civ.ivy.config.LivestockHappinessConfig
import plus.civ.ivy.genome.HorseGenome
import kotlin.math.roundToInt

class IvyHorse(world: World): EntityHorse(world), IvyLivestock {
	override var happiness: Float = 0.0f
	override var happinessChecks: Int = 0
	override var birthday: Long = 0L
	override val config: LivestockHappinessConfig
		get() = LivestockConfig.LIVESTOCK_CONFIGS[this.bukkitEntity.type]!!
	override val self: LivingEntity
		get() = this.bukkitEntity as LivingEntity

	var tickCounter = 0

	var genome: HorseGenome? = null

	// this function is called when the entity loads in, with the nbt data that it had stored in the world file
	override fun a(nbt: NBTTagCompound) {
		super.a(nbt)
		loadFromNBT(nbt)

		if (nbt.hasKey("genome")) {
			val genomeNBT = nbt.getCompound("genome")
			try {
				genome = HorseGenome(genomeNBT)
			} catch (e: Exception) {
				Ivy.instance.logger.warning("While loading genome $genomeNBT for a horse at [$locX $locY $locZ] encountered an exception:")
				e.printStackTrace()
			}
		} else {
			genome = HorseGenome.randomHorse()
		}

		genome?.applyToHorse(this)
	}

	// this function is called right before `nbt` is saved, whenever the entity is saved to the world file
	override fun e(nbt: NBTTagCompound) {
		super.e(nbt)
		saveToNBT(nbt)

		try {
			nbt.set("genome", genome?.saveToNBT())
		} catch (e: Exception) {
			Ivy.instance.logger.warning("While saving genome $genome a horse at [$locX $locY $locZ] encountered an exception:")
			e.printStackTrace()
		}
	}

	// this is the tick function, i think. it does some ticking, at least. there's also a `t_` that might be better,
	// but `t_` calls this anyway (in fact, in `Entity`, that's all `t_` does)
	override fun K() {
		super.K()
		tickLivestock()

		if (tickCounter % (5 * 20) == 0) {
			if (genome == null) {
				genome = HorseGenome.randomHorse()
			}

			genome!!.applyToHorse(this)
		}

		if (genome?.regenerating?.isRecessiveActive == true) {
			if (tickCounter % genome!!.regenSpeed.roundToInt() == 0) {
				heal(1.0f)
			}
		}

		tickCounter++
	}

	// bugfix for HorseJumpEvent never actually changing the power of a jump, despite it suggesting so
	override fun v(i: Int) {
		var myI = i
		if (this.cG()) {
			if (myI < 0) {
				myI = 0
			}

			var power = if (myI >= 90) { 1.0F } else { 0.4F + 0.4F * myI.toFloat() / 90.0F }

			val event = CraftEventFactory.callHorseJumpEvent(this, power)
			power = event.power
			if (!event.isCancelled) {
				val bG = EntityHorse::class.java.getDeclaredField("bG").also { it.isAccessible = true }
				bG.setBoolean(this, true)
				val dh = EntityHorse::class.java.getDeclaredMethod("dh").also { it.isAccessible = true }
				dh.invoke(this)
				this.br = power
			}
		}
	}

	// this is called to make a baby one
	override fun createChild(partner: EntityAgeable): EntityAgeable {
		if (partner !is IvyHorse) {
			throw java.lang.AssertionError("Expected a ivy horse as a breeding partner! Are there non-ivy horses in the world?")
		}

		val genome = if (this.genome != null && partner.genome != null) {
			try {
				this.genome!!.breedWith(partner.genome!!)
			} catch (e: Exception) {
				Ivy.instance.logger.warning("While breeding a horse with genome $genome and partner genome ${partner.genome} encountered an exception:")
				e.printStackTrace()
				null
			}
		} else {
			null
		}

		val baby = IvyHorse(this.world)
		baby.genome = genome
		genome?.applyToHorse(baby)

		return baby
	}
}
