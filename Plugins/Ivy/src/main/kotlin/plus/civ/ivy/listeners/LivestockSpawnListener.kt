package plus.civ.ivy.listeners

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity
import org.bukkit.entity.Ageable
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.entity.CreatureSpawnEvent
import plus.civ.ivy.nms.livestock.IvyLivestock

class LivestockSpawnListener: Listener {
	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
	fun onIvyLivestockSpawn(event: CreatureSpawnEvent) {
		if ((event.entity as CraftEntity).handle !is IvyLivestock) return
		val animal = event.entity as Ageable
		val nmsAnimal = (animal as CraftEntity).handle as IvyLivestock
		animal.ageLock = true // babies will only grow up when i tell them to
		nmsAnimal.onBirth()
	}
}
