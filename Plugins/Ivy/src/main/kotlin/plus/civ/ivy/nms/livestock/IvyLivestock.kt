package plus.civ.ivy.nms.livestock

import net.minecraft.server.v1_8_R3.NBTTagCompound
import org.bukkit.ChatColor
import org.bukkit.entity.Ageable
import org.bukkit.entity.LivingEntity
import plus.civ.ivy.config.LivestockHappinessConfig
import vg.civcraft.mc.civmodcore.extensions.displayableName
import vg.civcraft.mc.civmodcore.extensions.prettyName

interface IvyLivestock {
	var happiness: Float
	var happinessChecks: Int
	var birthday: Long
	val config: LivestockHappinessConfig
	val self: LivingEntity

	// this function is called when the entity loads in, with the nbt data that it had stored in the world file
	fun loadFromNBT(nbt: NBTTagCompound) {
		happiness = nbt.getFloat("happiness")
		happinessChecks = nbt.getInt("happinessChecks")
		birthday = nbt.getLong("birthday")
	}

	// this function is called right before `nbt` is saved, whenever the entity is saved to the world file
	fun saveToNBT(nbt: NBTTagCompound) {
		nbt.setFloat("happiness", happiness)
		nbt.setInt("happinessChecks", happinessChecks)
		nbt.setLong("birthday", birthday)
	}

	fun onBirth() {
		birthday = System.currentTimeMillis() / 1000
	}

	fun tickLivestock() {
		var newHappiness: Float = 0.0f
		if (self.location.block.biome in config.favoriteBiomes) {
			newHappiness += config.biomeWeight
		}
		if (self.location.add(0.0, -1.0, 0.0).block.type in config.favoriteBlocks) {
			newHappiness += config.blockWeight
		}
		val friends = self
			.getNearbyEntities(25.0, 25.0, 25.0)
			.filter{ it.type in config.favoriteFriends }
			.size
		newHappiness += config.friendsWeight * friends.coerceAtMost(config.maxFriends)
		val overcrowders = self
			.getNearbyEntities(4.0, 4.0, 4.0)
			.filter{ it.type !in config.favoriteFriends && it is LivingEntity }
			.size
		newHappiness -= overcrowders * config.overcrowdingWeight
		newHappiness = newHappiness.coerceIn(0.0f, 1.0f)

		if (happinessChecks <= 0) {
			happiness = newHappiness
			happinessChecks++
		} else {
			val updatedAverageHappiness: Float = (happinessChecks * happiness + newHappiness) / (happinessChecks + 1)
			happiness = updatedAverageHappiness
			happinessChecks++
		}

		// if not ageable, we're done (the rest of the function pertains to ageing the animal)
		if (self !is Ageable) return
		val selfAgeable = self as Ageable
		// if an adult, we're done
		if (selfAgeable.isAdult) return
		// otherwise we calculate whether the livestock may grow up
		val now = System.currentTimeMillis() / 1000
		val timeAlive = now - birthday
		// defaultGrowthTime / happiness, with a divide by 0 guard
		val timeToAdult =  (if (happiness != 0.0f) (config.defaultGrowthTime / happiness) else Long.MAX_VALUE).toLong()
		val timeLeftToAdult = timeToAdult - timeAlive
		if (timeLeftToAdult < 0) {
			selfAgeable.setAdult()
		}
	}

	fun getHappinessProfile(): List<String> {
		val biome = self.location.block.biome
		val standingBlock = self.location.add(0.0, -1.0, 0.0).block.type
		val friends = self
			.getNearbyEntities(25.0, 25.0, 25.0)
			.filter{ it.type in config.favoriteFriends }
			.size
		val overcrowders = self
			.getNearbyEntities(4.0, 4.0, 4.0)
			.filter{ it.type !in config.favoriteFriends && it is LivingEntity }
			.size

		val statusPageBuilder = StringBuilder()
		statusPageBuilder.appendLine("${ChatColor.RESET}${ChatColor.BOLD}${self.displayableName}")
		statusPageBuilder.append("${ChatColor.RESET}Biome: ")
		statusPageBuilder.appendLine("${if (biome in config.favoriteBiomes) ChatColor.GREEN else ChatColor.RED}${biome.prettyName}")
		statusPageBuilder.append("${ChatColor.RESET}Standing on: ")
		statusPageBuilder.appendLine("${if (standingBlock in config.favoriteBlocks) ChatColor.GREEN else ChatColor.RED}${standingBlock.prettyName}")
		statusPageBuilder.appendLine("${ChatColor.RESET}Friends: ${if (friends >= config.maxFriends) ChatColor.GREEN else if (friends > 0) ChatColor.GOLD else ChatColor.GREEN}$friends")
		statusPageBuilder.appendLine("${ChatColor.RESET}Overcrowding: ${if (overcrowders == 0) ChatColor.GREEN else ChatColor.RED}$overcrowders")
		statusPageBuilder.appendLine("${ChatColor.RESET}Happiness: ${if (happiness > 0.75f) ChatColor.GREEN else if (happiness > 0.25f) ChatColor.GOLD else  ChatColor.RED}${(happiness * 100).toInt()}%")
		val statusPage = statusPageBuilder.toString()

		val infoPageBuilder = StringBuilder()
		infoPageBuilder.appendLine("${ChatColor.RESET}${ChatColor.BOLD}${self.type.prettyName}")
		infoPageBuilder.append("${ChatColor.RESET}")
		if (config.favoriteBiomes.isNotEmpty()) {
			infoPageBuilder.appendLine("Favorite biomes:")
			for (b in config.favoriteBiomes) {
				infoPageBuilder.appendLine("- ${b.prettyName}")
			}
		}
		if (config.favoriteBlocks.isNotEmpty()) {
			infoPageBuilder.appendLine("Favorite blocks:")
			for (b in config.favoriteBlocks) {
				infoPageBuilder.appendLine("- ${b.prettyName}")
			}
		}
		if (config.favoriteFriends.isNotEmpty()) {
			infoPageBuilder.appendLine("Favorite friends:")
			for (f in config.favoriteFriends) {
				infoPageBuilder.appendLine("- ${f.prettyName}")
			}
		}
		infoPageBuilder.setLength(infoPageBuilder.length.coerceAtMost(256))
		val infoPage = infoPageBuilder.toString() // max page is 256

		return listOf(statusPage, infoPage)
	}
}
