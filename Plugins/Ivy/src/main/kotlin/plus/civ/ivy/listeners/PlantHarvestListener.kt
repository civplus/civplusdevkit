package plus.civ.ivy.listeners

import org.bukkit.Bukkit
import org.bukkit.CropState
import org.bukkit.Material
import org.bukkit.NetherWartsState
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.material.CocoaPlant
import org.bukkit.material.Crops
import org.bukkit.material.NetherWarts
import plus.civ.ivy.Ivy
import plus.civ.ivy.config.PlantsConfig
import vg.civcraft.mc.civmodcore.extensions.dropItemAsIfBroken

class PlantHarvestListener: Listener {
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	fun onCropBreak(event: BlockBreakEvent) {
		if (event.block.type !in PlantsConfig.PLANT_LOOT_MATERIALS) return

		// make sure it's fully grown
		val state = event.block.state.data
		if (state is Crops) {
			if (state.state != CropState.RIPE) {
				return
			}
		} else if (state is CocoaPlant) {
			if (state.size != CocoaPlant.CocoaPlantSize.LARGE) {
				return
			}
		} else if (state is NetherWarts) {
			if (state.state != NetherWartsState.RIPE) {
				return
			}
		} else if (event.block.type == Material.MELON_STEM || event.block.type == Material.PUMPKIN_STEM) {
			@Suppress("DEPRECATION")
			val age = state.data
			if (age != 7.toByte()) { // Spigot 1.8.8 does not have a Stem class like later version do.
				return
			}
		}
		// sugar cane and cactus: things are probably okay because they don't have growth states
		// maybe in the future we can have a fancy WhatIsAnOcttree tag that tells us if it's grown naturally
		// or rather just placing a cane or cactus places a not-natural tag

		val dropTables = PlantsConfig.PLANT_LOOT.filter { it.material == event.block.type }
		val drops = mutableListOf<ItemStack>()

		for (table in dropTables) {
			if (Math.random() < table.chance) {
				drops.add(table.drop.example)
			}
		}

		if (drops.isNotEmpty()) {
			Bukkit.getScheduler().runTaskLater(Ivy.instance, {
				for (drop in drops) {
					event.block.world.dropItemAsIfBroken(event.block.location, drop)
				}
			}, 1L)
		}
	}
}
