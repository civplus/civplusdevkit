package plus.civ.ivy.listeners

import net.minecraft.server.v1_8_R3.ChunkCoordIntPair
import org.bukkit.Bukkit
import org.bukkit.DyeColor
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.block.BlockGrowEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.event.world.StructureGrowEvent
import plus.civ.ivy.Ivy
import plus.civ.ivy.growthRate
import java.security.SecureRandom

class PlantGrowthListener: Listener {
	private val rng = SecureRandom()

	/**
	 * Cancel plant growth some percentage of the time to get the desired growth rate
	 */
	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	fun onPlantGrow(event: BlockGrowEvent) {
		if (event.block.location in boneMeals) return // allow bonemealing crops

		val chunkCoords = event.block.chunk.let{ ChunkCoordIntPair(it.x, it.z) }
		// if the chunk is currently undergoing forced growth ticks from when it was loaded, we don't
		// want to block any growth. growth rates are handled on their own over there
		if (CropTickerTimeMachine.isChunkHot(chunkCoords)) return

		val growthRate = event.newState.block.growthRate()
		val roll = rng.nextFloat()
		if (roll > growthRate) {
			event.isCancelled = true
		}
	}

	/**
	 * Cancel tree growth some percentage of the time to get the desired growth rate
	 */
	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	fun onStructureGrow(event: StructureGrowEvent) {
		if (event.isFromBonemeal) return // allow bonemealing trees

		val growthRate = event.location.block.growthRate()
		val roll = rng.nextFloat()
		if (roll > growthRate) {
			event.isCancelled = true
		}
	}

	val boneMeals = mutableSetOf<Location>()

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onBonemealBlock(event: PlayerInteractEvent) {
		val bonemeal = event.player.itemInHand
		if (bonemeal.type != Material.INK_SACK) return
		if (bonemeal.durability != DyeColor.WHITE.dyeData.toShort()) return
		if (event.action != Action.RIGHT_CLICK_BLOCK) return

		val location = event.clickedBlock.location
		boneMeals.add(location)
		Bukkit.getScheduler().runTask(Ivy.instance) {
			boneMeals.remove(location)
		}
	}
}
