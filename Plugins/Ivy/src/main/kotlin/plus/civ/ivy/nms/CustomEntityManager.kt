@file:Suppress("UNCHECKED_CAST", "PLATFORM_CLASS_MAPPED_TO_KOTLIN")

package plus.civ.ivy.nms

import net.minecraft.server.v1_8_R3.*
import plus.civ.ivy.nms.livestock.*
import plus.civ.ivy.nms.livestock.IvyCow
import java.lang.reflect.Field
import java.util.Map

private data class CustomEntityData(val clazz: Class<Entity>, val name: String, val id: Int)
private val CUSTOM_ENTITIES = arrayOf(
	// NOTE: the name strings need to be exactly as they are in vanilla
	CustomEntityData(IvyPig::class.java as Class<Entity>, "Pig", 90),
	CustomEntityData(IvySheep::class.java as Class<Entity>, "Sheep", 91),
	CustomEntityData(IvyCow::class.java as Class<Entity>, "Cow", 92),
	CustomEntityData(IvyChicken::class.java as Class<Entity>, "Chicken", 93),
	CustomEntityData(IvyMooshroom::class.java as Class<Entity>, "MushroomCow", 96),
	CustomEntityData(IvyHorse::class.java as Class<Entity>, "EntityHorse", 100),
	CustomEntityData(IvyRabbit::class.java as Class<Entity>, "Rabbit", 101),
)

/**
 * if we were to use the NMS EntityType function `a`, which is normally how to register a custom entity,
 * it would throw `IllegalArgumentException`s about overwriting IDs that already exist. however, we want
 * to overwrite those so that NMS uses our entities in place of the vanilla ones anytime the vanilla ones
 * would spawn. therefore, we have to recreate the `a` function without all of those security checks.
 * esentially all the function does is add the entity's information to a lot of maps, which we get
 * through reflection and add our entity info to manually.
 */
fun registerCustomEntities() {
	val c: Field = EntityTypes::class.java.getDeclaredField("c")
	c.isAccessible = true
	val stringToClassMap = c.get(null) as Map<String, Class<Entity>>
	val d: Field = EntityTypes::class.java.getDeclaredField("d")
	d.isAccessible = true
	val classToStringMap = d.get(null) as Map<Class<Entity>, String>
	val e: Field = EntityTypes::class.java.getDeclaredField("e")
	e.isAccessible = true
	val idToClassMap = e.get(null) as Map<Int, Class<Entity>>
	val f: Field = EntityTypes::class.java.getDeclaredField("f")
	f.isAccessible = true
	val classToIdMap = f.get(null) as Map<Class<Entity>, Int>
	val g: Field = EntityTypes::class.java.getDeclaredField("e")
	g.isAccessible = true
	val stringToIdMap = g.get(null) as Map<String, Int>

	for (entity in CUSTOM_ENTITIES) {
		// first we replace the old entity with the new entity in all biome spawning lists, if the id is used
		val biomeMetaListFields = arrayOf("au", "at", "av", "aw")
			.map{ fieldName -> BiomeBase::class.java.getDeclaredField(fieldName).also{ it.isAccessible = true } }
		val biomeMetaEntityClassField = BiomeBase.BiomeMeta::class.java.getDeclaredField("b")
			.also{ it.isAccessible = true }
		// there are 4 lists because they're used for spawning different kinds of things
		// au is passives, at is hostiles, av is waterthings and aw is cavethings
		for (biome in BiomeBase.getBiomes()) {
			if (biome == null) continue // why are there nulls in the list? i know notch's address
			for (field in biomeMetaListFields) {
				val metaList = field.get(biome) as ArrayList<BiomeBase.BiomeMeta>
				for (meta in metaList) {
					val entityClazz = biomeMetaEntityClassField.get(meta) as Class<EntityInsentient>
					if (classToIdMap.get(entityClazz) == entity.id) {
						biomeMetaEntityClassField.set(meta, entity.clazz)
					}
				}
			}
		}

		// now add the entity to the mappings in EntityType
		stringToClassMap.put(entity.name, entity.clazz)
		classToStringMap.put(entity.clazz, entity.name)
		idToClassMap.put(entity.id, entity.clazz)
		classToIdMap.put(entity.clazz, entity.id)
		stringToIdMap.put(entity.name, entity.id)
	}
}
