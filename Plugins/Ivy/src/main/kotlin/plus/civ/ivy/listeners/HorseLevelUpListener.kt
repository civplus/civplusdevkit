package plus.civ.ivy.listeners

import org.bukkit.Location
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity
import org.bukkit.entity.EntityType
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageEvent
import org.bukkit.event.entity.HorseJumpEvent
import org.spigotmc.event.entity.EntityDismountEvent
import org.spigotmc.event.entity.EntityMountEvent
import plus.civ.ivy.config.HorseConfig
import plus.civ.ivy.nms.livestock.IvyHorse
import vg.civcraft.mc.civmodcore.extensions.safeDistance
import java.util.*
import kotlin.math.roundToInt

class HorseLevelUpListener: Listener {
	val mountLocations = mutableMapOf<UUID, Location>()

	@EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
	fun mountHorse(event: EntityMountEvent) {
		if (event.entity.type != EntityType.PLAYER) return
		if (event.mount.type != EntityType.HORSE) return

		mountLocations[event.entity.uniqueId] = event.mount.location
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
	fun dismountHorse(event: EntityDismountEvent) {
		if (event.entity.type != EntityType.PLAYER) return
		if (event.dismounted.type != EntityType.HORSE) return

		val mountLocation = mountLocations[event.entity.uniqueId] ?: return
		val currentLocation = event.entity.location

		val distance = mountLocation.safeDistance(currentLocation)
		if (distance == Double.POSITIVE_INFINITY) return

		val horse = (event.dismounted as CraftEntity).handle
		if (horse !is IvyHorse) return
		if (horse.genome?.noLevelUp?.isRecessiveActive == true) return

		horse.genome?.meritSpeed((distance / HorseConfig.blocksPerSpeedPoint).toInt())
	}

	val lastJumpLocation = mutableMapOf<UUID, Location>()

	@EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
	fun horseJump(event: HorseJumpEvent) {
		val lastJump = lastJumpLocation.replace(event.entity.uniqueId, event.entity.location)
		if (lastJump != null && lastJump.safeDistance(event.entity.location) < HorseConfig.minDistanceWalkedBetweenJumpsForLevelUp)
			return

		val horse = (event.entity as CraftEntity).handle
		if (horse !is IvyHorse) return
		if (horse.genome?.noLevelUp?.isRecessiveActive == true) return

		horse.genome?.meritJump(HorseConfig.pointsPerJump)
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
	fun horseDamage(event: EntityDamageEvent) {
		val horse = (event.entity as CraftEntity).handle
		if (horse !is IvyHorse) return
		if (horse.genome?.noLevelUp?.isRecessiveActive == true) return

		horse.genome?.meritHealth((event.finalDamage * HorseConfig.pointsPerHalfHeartDamage).roundToInt())
	}
}
