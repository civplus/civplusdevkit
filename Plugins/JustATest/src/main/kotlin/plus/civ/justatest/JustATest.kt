package plus.civ.justatest

import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PluginInfo
import java.io.File

@PluginInfo(
		name = "JustATest",
		description = "Allows the test server to know if it's the test server or the live server, and for shards to know what their name is",
		author = "Amelorate",
		depends = ["CivModCore"],
)
class JustATest: KotlinPlugin() {
	companion object {
		/**
		 * If the running server is specifically a Test Server. This includes the server at /home/mctest, but also any test shards if they are marked as such.
		 * Dream shards have this set to FALSE.
		 */
		var isTesting = File("./plugins/JustATest/testing").exists()

		/**
		 * The name of the directory the running server is in.
		 */
		var shardName: String = File("").absoluteFile.name
	}
}
