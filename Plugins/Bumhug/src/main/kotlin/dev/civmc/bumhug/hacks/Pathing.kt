package dev.civmc.bumhug.hacks

import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.Sound
import org.bukkit.block.BlockFace
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent
import vg.civcraft.mc.citadel.Citadel
import xyz.xenondevs.particle.ParticleBuilder
import xyz.xenondevs.particle.ParticleEffect
import xyz.xenondevs.particle.data.ParticleData
import xyz.xenondevs.particle.data.texture.BlockTexture

class Pathing: Listener {
	val shovels = listOf(Material.WOOD_SPADE, Material.STONE_SPADE, Material.IRON_SPADE, Material.GOLD_SPADE, Material.DIAMOND_SPADE)

	@EventHandler
	@Suppress("DEPRECATION")
	fun onShovelClick(event: PlayerInteractEvent) {
		if (!event.hasItem()) {
			return
		}

		if (event.item.type !in shovels) {
			return
		}

		if (event.action != Action.RIGHT_CLICK_BLOCK) {
			return
		}

		if (event.clickedBlock.type != Material.GRASS && event.clickedBlock.type != Material.DIRT) {
			return
		}

		if (event.blockFace == BlockFace.DOWN) {
			return
		}

		val citadel = Bukkit.getPluginManager().getPlugin("Citadel")
		if (citadel is Citadel) {
			if (Citadel.getReinforcementManager().isReinforced(event.clickedBlock)) {
				return
			}
		}

		val oldType = event.clickedBlock.type

		event.clickedBlock.type = Material.DIRT

		if (oldType == Material.DIRT) {
			when (event.clickedBlock.data) {
				0.toByte() -> {
					event.clickedBlock.data = 1
				}
				1.toByte() -> {
					event.clickedBlock.data = 0
				}
				else -> {
					// do nothing but make some noise
				}
			}
		}

		event.clickedBlock.world.playSound(event.clickedBlock.location, Sound.STEP_GRAVEL, 0.1f, 0.075f)

		fun velocity(): Float = (Math.random().toFloat() * 2 - 1) * 0.075f

		for (i in 0..10) {
			val location = event.clickedBlock.location
			location.y += 1.0
			location.x += Math.random()
			location.z += Math.random()

			ParticleBuilder(ParticleEffect.BLOCK_DUST, location)
					.setParticleData(BlockTexture(Material.DIRT))
					.setOffset(velocity(), velocity(), velocity())
					.display()
		}
	}
}
