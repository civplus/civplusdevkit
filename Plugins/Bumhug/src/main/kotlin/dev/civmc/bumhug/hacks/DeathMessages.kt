package dev.civmc.bumhug.hacks

import org.bukkit.ChatColor
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.entity.PlayerDeathEvent

class DeathMessages: Listener {
	private val SEND_PLAYER_COORDS_ON_DEATH = true
	private val MAKE_DEATH_MESSAGES_RED = true
	private val ANNOUNCE_DEATHS = true
	
	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
	fun onPlayerDeath(event: PlayerDeathEvent) {
		val player = event.entity
		if (SEND_PLAYER_COORDS_ON_DEATH) {
			val loc = player.location
			player.sendMessage("" + ChatColor.RED + "You died at %d, %d, %d!".format(loc.blockX, loc.blockY, loc.blockZ))
		}
		if (ANNOUNCE_DEATHS) {
			if (MAKE_DEATH_MESSAGES_RED) {
				event.deathMessage = "" + ChatColor.RED + event.deathMessage
			}
		} else {
			event.deathMessage = null
		}
	}
}
