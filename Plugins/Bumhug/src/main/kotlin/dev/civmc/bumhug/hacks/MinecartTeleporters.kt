package dev.civmc.bumhug.hacks

import dev.civmc.bumhug.Bumhug
import org.bukkit.Bukkit
import org.bukkit.entity.Minecart
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.event.vehicle.VehicleDestroyEvent
import org.bukkit.event.vehicle.VehicleExitEvent
import org.bukkit.plugin.Plugin
import vg.civcraft.mc.civmodcore.extensions.tryToTeleportVertically
import java.util.logging.Level

class MinecartTeleporters: Listener {
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	fun onPlayerQuitInMinecart(event: PlayerQuitEvent) {
		val vehicle = event.player.vehicle ?: return
		if (vehicle !is Minecart) {
			return
		}

		val vehicleLocation = vehicle.location
		event.player.leaveVehicle()

		if (!event.player.tryToTeleportVertically(vehicleLocation)) {
			event.player.health = 0.000000
			Bumhug.instance.logger.log(Level.INFO, "Player '${event.player.name}' logged out in vehicle: killed")
		}
	}

	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	fun onVehicleExitTeleport(event: VehicleExitEvent) {
		val vehicle = event.vehicle
		if (vehicle !is Minecart) {
			return
		}

		val player: Player
		val passenger = event.exited
		if (passenger is Player) player = passenger else return

		val runnable: Runnable = Runnable {
			if (!player.tryToTeleportVertically(vehicle.location)) {
				player.health = 0.000000
				Bumhug.instance.logger.log(Level.INFO, "Player '${player.name}' exiting vehicle: killed")
			}
		}
		Bukkit.getScheduler().runTaskLater(Bumhug.instance as Plugin, runnable, 2)
	}

	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
	fun onVehicleDestroyTeleport(event: VehicleDestroyEvent) {
		val vehicle = event.vehicle
		if (vehicle !is Minecart) {
			return
		}
		if (vehicle.passenger !is Player) return

		val player = vehicle.passenger as Player
		val runnable: Runnable = Runnable {
			if (!player.tryToTeleportVertically(vehicle.location)) {
				player.health = 0.0
				Bumhug.instance.logger.log(Level.INFO, "Player '${player.name}' exiting vehicle: killed")
			}
		}
		Bukkit.getScheduler().runTaskLater(Bumhug.instance as Plugin, runnable, 2)
	}
}
