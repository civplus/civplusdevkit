package dev.civmc.bumhug

import dev.civmc.bumhug.hacks.SpaceGuard
import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PermissionDefault
import plus.civ.kotlinplugin.PluginInfo
import plus.civ.kotlinplugin.PluginPermission

@PluginInfo(
	name = "Bumhug",
	author = "sepia",
	authors = ["ProgrammerDan55"],
	softDepends = [
		"CombatTagPlus",
		"Citadel",
		"ProtocolLib",
		"MacGuffin",
	],
	permissions = [
		PluginPermission(
			name = "bumhug.broadcastCombat",
			description = "Receive broadcasts when a player is combat-tagged",
			default = PermissionDefault.OP,
		),
		PluginPermission(
			name = "bumhug.bypassChunkLimits",
			description = "Place blocks in chunks that have exceeded the chunk limit for that type of block",
			default = PermissionDefault.OP,
		),
		PluginPermission(
			name = "bumhug.broadcastRaiding",
			description = "Receive broadcasts when a player is breaking a reinforced chest",
			default = PermissionDefault.OP,
		),
		PluginPermission(
			name = "bumhug.reloadInventory",
			description = "Reload a player's inventoy from before the last time they died",
			default = PermissionDefault.OP,
		),
		PluginPermission(
			name = "bumhug.seeInventory",
			description = "View a player's inventory",
			default = PermissionDefault.OP,
		),
		PluginPermission(
			name = "bumhug.ping",
			description = "Get your own ping with a command",
			default = PermissionDefault.TRUE,
		),
		PluginPermission(
			name = "bumhug.intervalAnnouncement.everyone",
			description = "Receive generic announcements",
			default = PermissionDefault.TRUE,
		),
		PluginPermission(
			name = "bumhug.setRepairCost",
			description = "Set the repair cost of items",
			default = PermissionDefault.OP,
		),
		PluginPermission(
			name = "bumhug.gamma",
			description = "Use the /gamma command",
			default = PermissionDefault.TRUE,
		),
		PluginPermission(
				name = "bumhug.cardinal",
				description = "Use the /cardinal command",
				default = PermissionDefault.TRUE,
		),
		PluginPermission(
			name = "bumhug.ghostblockblock",
			description = "Use the /ghostblockblock command",
			default = PermissionDefault.TRUE,
		),
	],
	commandsPackage = "hacks",
	listenersPackage = "hacks",
)
class Bumhug: KotlinPlugin() {
	companion object {
		private var _instance: Bumhug? = null

		internal val instance: Bumhug
			get() = _instance!!
	}

	override fun onEnable() {
		_instance = this
		super.onEnable()

		server.scheduler.runTaskTimer(this, SpaceGuard, 60L * 20L, 60L * 20L)
	}
}
