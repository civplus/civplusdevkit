package dev.civmc.bumhug.hacks

import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import plus.civ.kotlinplugin.CommandInfo
import kotlin.math.round

@CommandInfo(
		name = "cardinal",
		description = "Snap to the nearest 45 degrees",
		permission = "bumhug.cardinal",
		usage = "/<command>",
)
class Cardinal: CommandExecutor {
	override fun onCommand(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): Boolean {
		if (sender !is Player) {
			sender.sendMessage("This command must be ran by a player")
			return true
		}

		val location = sender.location
		location.yaw = (round(location.yaw / 45) * 45)
		location.pitch = 0.0f

		sender.teleport(location)
		return true
	}
}
