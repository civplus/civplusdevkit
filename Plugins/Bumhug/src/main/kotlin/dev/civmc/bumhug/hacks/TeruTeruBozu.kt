package dev.civmc.bumhug.hacks

import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.weather.WeatherChangeEvent

class TeruTeruBozu: Listener {
	val rainChance = 0.3 // 30% of vanilla

	@EventHandler
	fun weatherChange(event: WeatherChangeEvent) {
		if (!event.toWeatherState()) {
			// is going to stop raining
			return
		}

		if (Math.random() > rainChance) {
			event.isCancelled = true
		}
	}
}
