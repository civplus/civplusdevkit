package dev.civmc.bumhug.hacks

import dev.civmc.bumhug.Bumhug
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.event.Listener
import org.bukkit.scheduler.BukkitRunnable
import java.time.LocalDateTime

class IntervalAnnouncement: Listener { // making it a listener tricks KotlinPlugin into loading it on startup, even though it's not listening for anything
	private val HOURS_PER_DAY: Long = 24
	private val MINUTES_PER_HOUR: Long = 60
	private val SECONDS_PER_MINUTE: Long = 60
	private val HOURS_TO_SECONDS: Long = MINUTES_PER_HOUR * SECONDS_PER_MINUTE

    private var ANNOUNCEMENTS = HashSet<Announcement>()
    init {
		ANNOUNCEMENTS.add(Announcement(
			1 * HOURS_TO_SECONDS,
			"${ChatColor.BLUE}Join our discord at https://discord.gg/3qXYz99Z9V"
		))
		ANNOUNCEMENTS.add(Announcement(
			1 * HOURS_TO_SECONDS,
			"${ChatColor.GOLD}Become a patron at https://www.patreon.com/civ_plus",
			// offset so the announcements happen at different times
			lastAnnouncementTime = LocalDateTime.now().minusMinutes(30)
		))
    }
	init {
		object : BukkitRunnable() {
			override fun run() {
				this@IntervalAnnouncement.makeAnnouncementsAsNeeded()
			}
		}.runTaskTimerAsynchronously(Bumhug.instance, 1, 20)
	}

    /**
     * Scans over the announcements set, and if any of the announcements are due to be made, makes those announcements.
     */
	private fun makeAnnouncementsAsNeeded() {
        for (announcement in ANNOUNCEMENTS) {
            // if the announcement is due to be made
            if (announcement.lastAnnouncementTime
                            .plusSeconds(announcement.interval)
                            .isBefore(LocalDateTime.now())) {
                // make the announcement
                Bukkit.broadcast(announcement.message, announcement.permission)

                // update time
                announcement.lastAnnouncementTime = LocalDateTime.now()
            }
        }
    }

	data class Announcement(
		/**
		 * How often, in seconds, the announcement should be made
		 */
		val interval: Long,

		/**
		 * The content of the announcement
		 */
		val message: String,

		/**
		 * The permission the announcement should be broadcasted to
		 */
		val permission: String = "bumhug.intervalAnnouncement.everyone",

		/**
		 * The last time the announcement was made.
		 */
		var lastAnnouncementTime: LocalDateTime = LocalDateTime.now()
	)
}
