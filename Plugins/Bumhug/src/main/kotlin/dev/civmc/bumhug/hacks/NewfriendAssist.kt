package dev.civmc.bumhug.hacks

import dev.civmc.bumhug.Bumhug
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.inventory.ItemStack
import plus.civ.macguffin.listeners.Speedo
import vg.civcraft.mc.civmodcore.expression.solveToItemStack

class NewfriendAssist: Listener {
	private val FIRST_JOIN_MESSAGE = "&6%Name% is new to the server!"
	private val NEWFRIEND_KIT = ArrayList<ItemStack>()
	init {
		val cookies = ItemStack(Material.COOKIE, 64)
		val cookiesMeta = cookies.itemMeta
		cookiesMeta.lore = listOf("${ChatColor.AQUA}Welcome to Civ+!")
		cookies.itemMeta = cookiesMeta
		NEWFRIEND_KIT.add(cookies)

		val bed = ItemStack(Material.BED)
		val bedMeta = bed.itemMeta
		bedMeta.lore = listOf("${ChatColor.AQUA}Sweet dreams :)")
		bedMeta.displayName = "${ChatColor.LIGHT_PURPLE}Sleeping Bag"
		bed.itemMeta = bedMeta
		NEWFRIEND_KIT.add(bed)

		if (Bukkit.getPluginManager().isPluginEnabled("MacGuffin")) {
			NEWFRIEND_KIT.add(Speedo.speedo.solveToItemStack())
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onNewfriendJoin(event: PlayerJoinEvent) {
		if (event.player.hasPlayedBefore()) {
			return
		}

		val cleanMessage = ChatColor.translateAlternateColorCodes('&',
			FIRST_JOIN_MESSAGE.replace("%Name%", event.player.displayName)
		)
		for (player in Bumhug.instance.server.onlinePlayers) {
			player.sendMessage(cleanMessage)
		}
		if (NEWFRIEND_KIT.size > 0) {
			for (stack in NEWFRIEND_KIT) {
				event.player.inventory.addItem(ItemStack(stack))
			}
		}
	}
}
