package dev.civmc.bumhug.hacks

import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.inventory.*
import org.bukkit.inventory.AnvilInventory
import vg.civcraft.mc.civmodcore.extensions.nbt
import vg.civcraft.mc.civmodcore.extensions.writeNBTTag
import vg.civcraft.mc.civmodcore.extensions.resultItem

class AnvilScaling: Listener {
	val DEFAULT_REPAIR_COST_CAP: Int? = 15
	// these materials act like vanilla. everything else has scaling disabled
	val REPAIR_COST_CAPS: Map<Material, Int?> = mapOf(
		Pair(Material.DIAMOND_CHESTPLATE, null),
		Pair(Material.DIAMOND_LEGGINGS, null),
		Pair(Material.DIAMOND_BOOTS, null),
		Pair(Material.DIAMOND_HELMET, null),
		Pair(Material.DIAMOND_SWORD, null)
	)
	// these materials are completely free to repair or rename
	val FREE_REPAIR_MATERIALS: Array<Material> = arrayOf(
		Material.NAME_TAG
	)

	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	fun onAnvilUse(event: InventoryClickEvent) {
		if (event.view.topInventory.type != InventoryType.ANVIL) return
		if (event.whoClicked !is Player) return

		val player = event.whoClicked as Player
		val anvilInventory = event.view.topInventory as AnvilInventory

		// cap the repair cost of outputs
		val resultItem = anvilInventory.resultItem
		if (resultItem != null) {
			val cap = REPAIR_COST_CAPS.getOrDefault(resultItem.type, DEFAULT_REPAIR_COST_CAP)
			val nbt = resultItem.nbt
			if (cap != null && nbt != null) {
				val hypotheticalRepairCost = nbt.getInt("RepairCost")
				// this is the part where we actually cap it
				if (hypotheticalRepairCost > cap) {
					resultItem.writeNBTTag("RepairCost", cap)
				}
			}
		}

		// if it's a free repair material, we give the xp to repair it
		if (anvilInventory.resultItem?.type in FREE_REPAIR_MATERIALS && event.currentItem == anvilInventory.resultItem) {
			anvilInventory.resultItem?.writeNBTTag("RepairCost", 0)
			player.level += 1
		}
	}
}
