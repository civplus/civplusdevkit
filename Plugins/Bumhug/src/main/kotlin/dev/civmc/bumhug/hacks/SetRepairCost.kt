package dev.civmc.bumhug.hacks

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import plus.civ.kotlinplugin.CommandInfo
import vg.civcraft.mc.civmodcore.extensions.writeNBTTag

@CommandInfo(
	name = "setRepairCost",
	description = "Set the repair cost of the held item",
	permission = "bumhug.setRepairCost",
	usage = "/<command> [new cost]",
)
class SetRepairCost: CommandExecutor {
	private val DEFAULT_COST: Int = 15

	override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
		if (sender !is Player) {
			sender.sendMessage("${ChatColor.RED}Only players can run this command.")
			return true
		}
		if (args.size > 1) {
			return false
		}

		val newCost = if (args.isEmpty()) DEFAULT_COST else try {
			Integer.valueOf(args[0])
		} catch (e: Exception) {
			sender.sendMessage("${ChatColor.RED}New repair cost must be an integer.")
			return false
		}
		if (newCost < 0) {
			sender.sendMessage("${ChatColor.RED}New repair cost must be a positve number.")
			return false
		}

		val item = sender.itemInHand
		if (item == null || item.type == Material.AIR) {
			sender.sendMessage("${ChatColor.RED}You must be holding an item.")
			return true
		}

		item.writeNBTTag("RepairCost", newCost)
		return true
	}
}
