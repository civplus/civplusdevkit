package dev.civmc.bumhug.hacks

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.block.Biome
import org.bukkit.entity.*
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.block.BlockPistonExtendEvent
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.event.entity.CreatureSpawnEvent
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason
import org.bukkit.event.entity.EntityChangeBlockEvent
import org.bukkit.event.inventory.InventoryMoveItemEvent
import org.bukkit.event.inventory.InventoryType
import org.bukkit.event.player.*
import org.bukkit.event.vehicle.VehicleDestroyEvent
import org.bukkit.event.vehicle.VehicleExitEvent
import org.bukkit.inventory.meta.FireworkMeta
import org.bukkit.plugin.Plugin
import java.util.logging.Level

class GameFeatures: Listener {
	private val PISTONS = true
	private val HOPPERS = true
	private val PACKED_ICE_IN_HELL = true
	private val VILLAGER_TRADING = false
	private val WITHER_SPAWNING = false
	private val USE_ENDER_CHESTS = false
	private val PLACE_ENDER_CHESTS = true
	private val ELYTRA_FIREWORKS = false
	private val USE_ENCHANTING_TABLE = false
	private val NATURAL_XP = false // from any source other than bottles
	private val USE_NETHER_PORTAL = false
	private val IRON_GOLEM_VILLAGER_SPAWNING = false

	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	fun onPistonActivate(event: BlockPistonExtendEvent) {
		if (!PISTONS) {
			event.isCancelled = true
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	fun onHopperMoveItem(event: InventoryMoveItemEvent) {
		if (!HOPPERS) {
			if (event.initiator.type == InventoryType.HOPPER) {
				event.isCancelled = true
			}
		}
	}

	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	fun onPackedIcePlace(event: BlockPlaceEvent) {
		if (!PACKED_ICE_IN_HELL && event.block.type == Material.PACKED_ICE && event.block.biome == Biome.HELL) {
			event.player.sendMessage("" + ChatColor.RED + "Packed ice cannot be placed in hell.")
			event.isCancelled = true
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	fun onVillagerTrade(event: PlayerInteractEntityEvent) {
		if (!VILLAGER_TRADING) {
			val npc = event.rightClicked
			if (npc.type == EntityType.VILLAGER) {
				event.isCancelled = true
			}
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	fun onWitherSpawn(event: CreatureSpawnEvent) {
		if (!WITHER_SPAWNING) {
			if (event.entityType == EntityType.WITHER && event.spawnReason == SpawnReason.BUILD_WITHER) {
				event.isCancelled = true
			}
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	fun onEnderChestPlacement(event: BlockPlaceEvent) {
		if (!PLACE_ENDER_CHESTS) {
			if (event.block.type == Material.ENDER_CHEST) {
				event.isCancelled = true
			}
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true) 
	fun onEnderChestUse(event: PlayerInteractEvent) {
		if (!USE_ENDER_CHESTS) {
			val clickedBlock = event.clickedBlock ?: return
			if (event.action == Action.RIGHT_CLICK_BLOCK && clickedBlock.type == Material.ENDER_CHEST) {
				event.isCancelled = true
			}
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	fun onPlayerFirework(event: PlayerInteractEvent) {
		val item = event.item ?: return
		if (!ELYTRA_FIREWORKS && item.itemMeta is FireworkMeta) {
			// cancelled if the player is flying or the firework has no effects, otherwise not cancelled
			event.isCancelled = event.player.isFlying || !(item.itemMeta as FireworkMeta).hasEffects()
		}
	}

	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	fun onEnchantmentTableClick(event: PlayerInteractEvent) {
		if (this.USE_ENCHANTING_TABLE) {
			return
		}
		if (event.action != Action.RIGHT_CLICK_BLOCK) {
			return
		}
		if (event.clickedBlock?.type != Material.ENCHANTMENT_TABLE) {
			return
		}
		event.isCancelled = true
	}

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	fun onExperienceGain(event: PlayerExpChangeEvent) {
		if (!this.NATURAL_XP) {
			event.amount = 0
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onExperienceBottleThrow(event: PlayerInteractEvent) {
		if (event.item?.type != Material.EXP_BOTTLE) {
			return
		}
		if (!(event.action == Action.RIGHT_CLICK_AIR || event.action == Action.RIGHT_CLICK_BLOCK)) {
			return
		}
		event.player.giveExp(7)
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	fun onNetherPortaluse(event: PlayerTeleportEvent) {
		if (this.USE_NETHER_PORTAL) {
			return
		}
		if (event.cause == PlayerTeleportEvent.TeleportCause.NETHER_PORTAL) {
			event.isCancelled = true
		}
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	fun onSpawnIronGolem(event: CreatureSpawnEvent) {
		if (IRON_GOLEM_VILLAGER_SPAWNING) {
			return
		}

		if (event.entityType != EntityType.IRON_GOLEM) {
			return
		}

		if (event.spawnReason != SpawnReason.VILLAGE_DEFENSE && event.spawnReason != SpawnReason.VILLAGE_INVASION) {
			return
		}

		event.isCancelled = true
	}
}
