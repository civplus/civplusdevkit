package dev.civmc.bumhug.hacks

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.entity.EntityType
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.entity.ProjectileLaunchEvent
import org.bukkit.inventory.ItemStack
import java.util.*
import kotlin.collections.HashMap

class PearlCooldown: Listener {
	companion object {
		var pearlCooldownSeconds = 15 // seconds
		val lastThrownTimestamps: HashMap<UUID, Long> = HashMap()
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	fun onPlayerThrowPearl(event: ProjectileLaunchEvent) {
		if (event.entityType != EntityType.ENDER_PEARL) return
		if (event.entity.shooter !is Player) return

		val player = event.entity.shooter as Player
		val now = System.currentTimeMillis()
		val lastThrown = lastThrownTimestamps[player.uniqueId] ?: 0

		if (now - lastThrown < pearlCooldownSeconds * 1000) {
			val timeRemaining = pearlCooldownSeconds - (now - lastThrown) / 1000
			player.sendMessage("${ChatColor.YELLOW}You may not pearl for another ${ChatColor.GREEN}$timeRemaining${ChatColor.YELLOW} seconds.")
			event.isCancelled = true
			// cancelling the event doesn't do this, for some reason
			player.inventory.addItem(ItemStack(Material.ENDER_PEARL))
		} else {
			lastThrownTimestamps[player.uniqueId] = now
		}
	}
}
