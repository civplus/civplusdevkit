package dev.civmc.bumhug.hacks

import dev.civmc.bumhug.Bumhug
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import vg.civcraft.mc.citadel.Citadel
import org.bukkit.Bukkit

class RaiderAnnounce: Listener {
	private val MESSAGE = "&4%Name% is raiding a chest at %X%, %Y%, %Z%."
	private val MESSAGE_DELAY = 10000 // seconds

	private val lastAlertSent = HashMap<Player, Long>()

	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	fun onReinforcementBreak(event: BlockBreakEvent) {
		if (!Bumhug.instance.server.pluginManager.isPluginEnabled("Citadel")) return

		val man = Citadel.getReinforcementManager() ?: return
		val isReinforced = man.getReinforcement(event.block) != null
		if (isReinforced && (event.block.type == Material.CHEST || event.block.type == Material.TRAPPED_CHEST)) {
			val last: Long? = lastAlertSent[event.player]
			val now = System.currentTimeMillis()
			if (last == null) {
				lastAlertSent[event.player] = now
			} else if (now - last < MESSAGE_DELAY) {
				return
			} else {
				lastAlertSent[event.player] = now
			}

			val cleanMessage = ChatColor.translateAlternateColorCodes('&',
				MESSAGE
				.replace("%Name%", event.player.displayName)
				.replace("%X%", event.block.location.x.toString())
				.replace("%Y%", event.block.location.y.toString())
				.replace("%Z%", event.block.location.z.toString())
			)
			Bukkit.broadcast(cleanMessage, "bumhug.broadcastRaiding")
		}
	}
}
