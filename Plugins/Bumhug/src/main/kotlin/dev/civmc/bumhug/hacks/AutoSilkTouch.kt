package dev.civmc.bumhug.hacks

import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.inventory.ItemStack
import vg.civcraft.mc.civmodcore.event.BlockDropItemEvent

class AutoSilkTouch: Listener {
	var blocks = setOf(
		Material.BOOKSHELF,
	)

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = false)
	fun breakBlock(event: BlockDropItemEvent) {
		if (event.block.type !in blocks) return
		event.droppedItems = mutableListOf(ItemStack(event.block.type))
	}
}
