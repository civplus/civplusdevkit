package dev.civmc.bumhug.hacks

import org.bukkit.GameMode
import org.bukkit.Material
import org.bukkit.World.Environment
import org.bukkit.block.Biome
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.event.entity.EntityPortalEvent
import org.bukkit.event.entity.EntityTeleportEvent
import org.bukkit.event.player.PlayerMoveEvent
import org.bukkit.inventory.InventoryHolder
import vg.civcraft.mc.civmodcore.extensions.tryToTeleportVertically

class GameFixes: Listener {
	private val PREVENT_STORAGE_TELEPORT = true
	private val PREVENT_BED_BOMBING = true
	private val PREVENT_FALLING_THROUGH_BEDROCK = true

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	fun onStorageTeleport(event: EntityTeleportEvent) {
		if (event.entity is InventoryHolder && PREVENT_STORAGE_TELEPORT) {
			event.isCancelled = true
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	fun onStoragePortal(event: EntityPortalEvent) {
		if (event.entity is InventoryHolder && PREVENT_STORAGE_TELEPORT) {
			event.isCancelled = true
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	fun onBedPlace(event: BlockPlaceEvent) {
		if (!PREVENT_BED_BOMBING || event.block.type == Material.BED) {
			return
		}
		val world = event.block.location.world ?: return
		val env = world.environment
		val biome = event.block.biome
		if (env == Environment.NETHER || env == Environment.THE_END || biome == Biome.HELL) {
			event.isCancelled = true
		}
	}

	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	fun onPlayerFallThroughBedrock(event: PlayerMoveEvent) {
		if (PREVENT_FALLING_THROUGH_BEDROCK) {
			val to = event.to ?: return
			if (to.y >= 0)
				return

			if (event.from.y <= -3) {
				// prevent excessive calls to tryToTeleportVertically
				// this uses from rather than to because in lag to may jump from greater than 0 to less than negative 3,
				// while from will never jump like that
				return
			}

			if (event.player.gameMode != GameMode.SURVIVAL)
				return

			event.player.tryToTeleportVertically(to)
		}
	}
}
