package dev.civmc.bumhug.hacks

import dev.civmc.bumhug.Bumhug
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import java.io.File

object SpaceGuard: Runnable {
	override fun run() {
		val file = File(".")
		if (file.usableSpace < 500 * 1000 * 1000) {
			// if usable space is less than 500 megabytes
			Bukkit.broadcastMessage("${ChatColor.RED}The server is running low on HDD space. " +
					"To prevent world corruption, the server will be going down for maintenance in 60 seconds.")

			Bukkit.getScheduler().runTaskLater(Bumhug.instance, {
				// shutdown the server after 60 seconds
				Bukkit.shutdown()
			} as Runnable, 60L * 20L)
		}
	}
}
