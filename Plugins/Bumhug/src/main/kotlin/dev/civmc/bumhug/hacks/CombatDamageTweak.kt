package dev.civmc.bumhug.hacks

import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.event.entity.EntityDamageEvent

class CombatDamageTweak: Listener {
	private val DAMAGE_COEFFICIENT = 1.0f // All damage between players is multiplied by this number
	private val HIT_DAMAGE_COEFFICIENT = 0.75f
	private val BOW_DAMAGE_COEFFICIENT = 1.0f

	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	fun onPlayerDamagePlayer(e: EntityDamageByEntityEvent) {
		if (e.entity !is Player || e.damager !is Player) return

		e.damage *= DAMAGE_COEFFICIENT
		if (e.cause == EntityDamageEvent.DamageCause.ENTITY_ATTACK) {
			e.damage *= HIT_DAMAGE_COEFFICIENT
		} else if (e.cause == EntityDamageEvent.DamageCause.PROJECTILE) {
			e.damage *= BOW_DAMAGE_COEFFICIENT
		}
	}
}
