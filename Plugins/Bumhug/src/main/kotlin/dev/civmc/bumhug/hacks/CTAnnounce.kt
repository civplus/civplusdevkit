package dev.civmc.bumhug.hacks

import dev.civmc.bumhug.Bumhug
import net.minelink.ctplus.event.PlayerCombatTagEvent
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import java.util.UUID
import java.util.logging.Level

class CTAnnounce: Listener {
	private val DELAY = 10000 // seconds
	private val MESSAGE = "&4%Victim% was combat tagged by %Attacker%"
	
	private val lastCTAnnounce = HashMap<UUID, Long>()
	
	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
	fun onCombatTag(event: PlayerCombatTagEvent) {
		if (event.victim == null || event.attacker == null) return
		
		// Throttle broadcast frequency
		val victimId = event.victim.uniqueId
		val lastTag = lastCTAnnounce[victimId]
		val now = System.currentTimeMillis()
		if (lastTag != null && now - lastTag < DELAY) {
			return
		}
		lastCTAnnounce[victimId] = now

		val cleanMessage = ChatColor.translateAlternateColorCodes('&',
			MESSAGE
			.replace("%Victim%", event.victim.displayName)
			.replace("%Attacker%", event.attacker.displayName)
		)
		Bumhug.instance.logger.log(Level.INFO, cleanMessage)
		Bukkit.broadcast(cleanMessage, "bumhug.broadcastCombat")
	}
}
