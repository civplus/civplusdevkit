package dev.civmc.bumhug.hacks

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent

class SetBedSpawn: Listener {
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onBedRightClick(event: PlayerInteractEvent) {
		if (event.action != Action.RIGHT_CLICK_BLOCK) return
		if (event.clickedBlock.type != Material.BED_BLOCK) return

		event.player.bedSpawnLocation = event.clickedBlock.location
		event.player.sendMessage("${ChatColor.YELLOW}Your spawn has been set.")
	}
}
