package dev.civmc.bumhug.hacks

import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockPlaceEvent
import java.util.*

class ChunkLimits: Listener {
	private val LIMITS: EnumMap<Material, Int> = EnumMap(org.bukkit.Material::class.java)
	init {
		LIMITS[Material.REDSTONE_WIRE] = 16
		LIMITS[Material.PISTON_BASE] = 10
		LIMITS[Material.PISTON_STICKY_BASE] = 10
		LIMITS[Material.HOPPER] = 16
	}
	private val CHUNK_LIMIT_EXCEEDED_MESSAGE: String = "&9You've exceeded the chunk limit for that block type!"
	
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	fun onBlockPlace(event: BlockPlaceEvent) {
		if (event.player.hasPermission("bumhug.bypassChunkLimits")) {
			return
		}
		val bl = event.block
		val lim = LIMITS[bl.type] ?: return

		var count = 0
		for (state in bl.chunk.tileEntities) {
			if (bl.type == state.type) {
				if (++count > lim) {
					event.isCancelled = true
					event.player.sendMessage(CHUNK_LIMIT_EXCEEDED_MESSAGE)
					return
				}
			}
		}
	}
}
