package dev.civmc.bumhug.hacks

import dev.civmc.bumhug.Bumhug
import vg.civcraft.mc.civmodcore.extensions.getPlayerByString
import net.minecraft.server.v1_8_R3.WorldNBTStorage
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.craftbukkit.v1_8_R3.CraftServer
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftInventoryPlayer
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.PlayerInventory
import plus.civ.kotlinplugin.CommandInfo
import java.util.*
import java.util.logging.Level

@CommandInfo(
	name = "invSee",
	description = "View a player's inventory",
	permission = "bumhug.seeInventory",
	usage = "/<command> [playername:UUID]",
)
class InvSee: CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<String>): Boolean {
        if (args.size != 1) {
            return false
        }
        val playerNameOrUUID = args[0]

		/**
		 * Try to get the player and inventory the happy way
		 */
		val player: Player? = getPlayerByString(playerNameOrUUID)
		if (player != null) {
			val playerInv = player.inventory
			invSee(sender, playerInv, player.health, player.foodLevel, playerNameOrUUID)
			return true
		}

		/**
		 * If the happy way didn't work, now we have to try the sad NMS way (they're maybe offline)
		 */
        val playerUUID: UUID = try {
			UUID.fromString(playerNameOrUUID)
		} catch(iae: IllegalArgumentException) {
			sender.sendMessage("Player $playerNameOrUUID does not exist or cannot be opened.")
			return true
		}
        // Go deep into NBT for offline players
		val storage = (Bumhug.instance.server as CraftServer).server.worlds[0].dataManager as WorldNBTStorage
		val rawPlayer = storage.getPlayerData(playerUUID.toString())

		if (rawPlayer != null) {
			Bumhug.instance.logger.log(Level.INFO, "Player $playerNameOrUUID found in NBT data, read-only access enabled.")
			sender.sendMessage("Player found via alternate lookup, read-only access enabled.")
		} else {
			sender.sendMessage("Player $playerNameOrUUID does not exist or cannot be opened.")
			return true
		}

		val health = rawPlayer.getFloat("Health")
		val food = rawPlayer.getInt("foodLevel")
		// Fun NMS inventory reconstruction from file data.
		val nms_pl_inv: net.minecraft.server.v1_8_R3.PlayerInventory = net.minecraft.server.v1_8_R3.PlayerInventory(null)
		val inv = rawPlayer.getList("Inventory", rawPlayer.typeId.toInt())
		nms_pl_inv.b(inv) // We use this to bypass the Craft code which requires a player object, unlike NMS.

		invSee(sender, CraftInventoryPlayer(nms_pl_inv), health.toDouble(), food, playerNameOrUUID)
		return true
    }

    private fun invSee(sender: CommandSender, playerInv: PlayerInventory, health: Double, food: Int, playerName: String) {
        if (sender !is Player) { // send text only.
            val sb = StringBuffer()
            sb.append(playerName).append("'s\n   Health: ").append(health.toInt() * 2)
            sb.append("\n   Food: ").append(food)
            sb.append("\n   Inventory: ")
            sb.append("\n      Helmet: ").append(playerInv.helmet)
            sb.append("\n      Chest: ").append(playerInv.chestplate)
            sb.append("\n      Legs: ").append(playerInv.leggings)
            sb.append("\n      Feet: ").append(playerInv.boots)

            for (slot in 0..35) {
                val item = playerInv.getItem(slot)
                sb.append("\n      ").append(slot).append(":").append(item).append(ChatColor.RESET)
            }

            sender.sendMessage(sb.toString())
        } else {
            val inv = Bukkit.createInventory(sender, 45, "$playerName's Inventory")

            for (slot in 0..35) {
                val item = playerInv.getItem(slot)
                inv.setItem(slot, item)
            }
            inv.setItem(38, playerInv.helmet)
            inv.setItem(39, playerInv.chestplate)
            inv.setItem(40, playerInv.leggings)
            inv.setItem(41, playerInv.boots)

            val healthItem = ItemStack(Material.APPLE, health.toInt() * 2)
            val healthData = healthItem.itemMeta!!
            healthData.setDisplayName("Player Health")
            healthItem.itemMeta = healthData
            inv.setItem(43, healthItem)

            val hungerItem = ItemStack(Material.COOKED_BEEF, food)
            val hungerData = hungerItem.itemMeta!!
            hungerData.setDisplayName("Player Hunger")
            hungerItem.itemMeta = hungerData
            inv.setItem(44, hungerItem)
            sender.openInventory(inv)
            sender.updateInventory()
        }
    }
}
