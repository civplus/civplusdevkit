package dev.civmc.bumhug.hacks

import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.potion.PotionEffect
import org.bukkit.potion.PotionEffectType
import plus.civ.kotlinplugin.CommandInfo

@CommandInfo(
		name = "gamma",
		description = "Toggle your night vision",
		permission = "bumhug.gamma",
		usage = "/<command>",
)
class Gamma: CommandExecutor {
	override fun onCommand(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): Boolean {
		if (sender !is Player) {
			sender.sendMessage("This command must be ran by a player")
			return true
		}

		if (sender.hasPotionEffect(PotionEffectType.NIGHT_VISION)) {
			sender.removePotionEffect(PotionEffectType.NIGHT_VISION)
		} else {
			sender.addPotionEffect(PotionEffect(PotionEffectType.NIGHT_VISION, Int.MAX_VALUE, 0, false, false))
		}

		return true
	}
}
