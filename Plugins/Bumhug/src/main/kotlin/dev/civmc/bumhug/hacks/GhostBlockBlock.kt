package dev.civmc.bumhug.hacks

import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import plus.civ.kotlinplugin.CommandInfo

@CommandInfo(
	name = "ghostblockblock",
	description = "Dispell any nearby ghost blocks",
	permission = "bumhug.ghostblockblock",
	usage = "/<command>",
	aliases = ["ghostblock", "ghost"]
)
class GhostBlockBlock: CommandExecutor {
	override fun onCommand(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): Boolean {
		if (sender !is Player) {
			return false
		}

		for (x in -5..5) {
			for (y in -5..5) {
				for (z in -5..5) {
					val location = sender.location.add(x.toDouble(), y.toDouble(), z.toDouble())
					val block = location.block
					sender.sendBlockChange(location, block.type, block.data)
				}
			}
		}

		sender.sendMessage("${ChatColor.GREEN}Ghost blocks within 5 blocks cleared!")

		return true
	}
}
