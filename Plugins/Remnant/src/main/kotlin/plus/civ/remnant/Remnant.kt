package plus.civ.remnant

import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PluginInfo

@PluginInfo(
	name = "Remnant",
	author = "sepia",
	description = "Configure TopShelf and Ivy with Civ+'s custom items",
	depends = ["CivModCore"],
	commandsPackage = "items",
	listenersPackage = "items",
)
class Remnant: KotlinPlugin() {
	companion object {
		private var _instance: Remnant? = null
		val instance: Remnant
			get() = _instance!!
	}

	override fun onEnable() {
		_instance = this
		super.onEnable()
	}
}
