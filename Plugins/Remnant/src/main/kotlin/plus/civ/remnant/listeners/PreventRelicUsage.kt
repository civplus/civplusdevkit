package plus.civ.remnant.listeners

import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.event.player.PlayerInteractEvent
import vg.civcraft.mc.civmodcore.extensions.nbt

/**
 * Prevents stuff like placing relics as blocks, opening books, and so on
 */
object PreventRelicUsage: Listener {
	@EventHandler
	fun openRelicBook(event: PlayerInteractEvent) {
		if (event.item.nbt?.hasKey("relicId") != true) return
		if (event.item.type == Material.WRITTEN_BOOK || event.item.type == Material.BOOK_AND_QUILL) return

		event.isCancelled = true
	}

	@EventHandler
	fun placeRelicBlock(event: BlockPlaceEvent) {
		if (event.itemInHand.nbt?.hasKey("relicId") != true) return
		event.isCancelled = true
	}
}
