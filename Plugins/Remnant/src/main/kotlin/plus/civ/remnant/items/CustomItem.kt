package plus.civ.remnant.items

import org.bukkit.inventory.ItemStack
import vg.civcraft.mc.civmodcore.expression.NBTExpression
import vg.civcraft.mc.civmodcore.expression.matches
import java.lang.AssertionError

abstract class CustomItem {
	init {
		//assertCorrect()
	}

	abstract val expression: NBTExpression
	abstract val example: ItemStack

	fun assertCorrect() {
		if (!expression.matches(example)) {
			throw AssertionError("Example $example ItemStack in CustomItem does not match expression $expression")
		}
	}
}
