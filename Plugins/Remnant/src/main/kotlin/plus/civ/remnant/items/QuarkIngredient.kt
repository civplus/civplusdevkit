package plus.civ.remnant.items

import org.bukkit.ChatColor
import org.bukkit.DyeColor
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.potion.PotionEffectType
import vg.civcraft.mc.civmodcore.expression.*

object QuarkIngredient: CustomItem(), Listener {
	override val example: ItemStack = QuarkIngredientLoud.example
	override val expression: NBTExpression = NBTExpression().with("tag.Quark", RegexMatcher.new("(LOUD)|(NUANCED)|(SMOOTH)|(SPARKLING)|(LOVELY)|(GROSS)"))

	@EventHandler(ignoreCancelled = true)
	fun placeIngredient(event: BlockPlaceEvent) {
		if (expression.matches(event.itemInHand))
			event.isCancelled = true
	}

	// Devkit snip: All quark ingredients that aren't used for examples

	object ThunderousPotato: CustomItem() {
		override val expression = QuarkIngredientLoud.expression.clone()
			.id(Material.POTATO_ITEM)
			.itemName("${ChatColor.AQUA}Thunderous Tuber")
		override val example: ItemStack = expression.solveToItemStack()
	}

	object GoldenFlour: CustomItem() {
		override val expression = QuarkIngredientLovely.expression.clone()
			.id(Material.SUGAR)
			.itemName("${ChatColor.AQUA}Golden Flour")
		override val example: ItemStack = expression.solveToItemStack()
	}

	object Reeds: CustomItem() {
		override val expression = QuarkIngredientNuanced.expression.clone()
			.id(Material.SUGAR_CANE)
			.itemName("${ChatColor.AQUA}Reeds")
		override val example: ItemStack = expression.solveToItemStack()
	}

	object RainbowRind: CustomItem() {
		override val expression = QuarkIngredientSparkling.expression.clone()
			.id(Material.MELON)
			.itemName("${ChatColor.AQUA}Rainbow Rind")
		override val example: ItemStack = expression.solveToItemStack()
	}

	object PumpkinSeedOil: CustomItem() {
		override val expression = QuarkIngredientSmooth.expression.clone()
			.id(Material.POTION)
			.potionColor(PotionEffectType.FIRE_RESISTANCE)
			.potionHasNoEffects()
			.itemName("${ChatColor.AQUA}Pumpkin Seed Oil")
		override val example: ItemStack = expression.solveToItemStack()
	}

	object PumpkinGuts: CustomItem() {
		override val expression = QuarkIngredientGross.expression.clone()
			.id(Material.LAVA_BUCKET)
			.itemName("${ChatColor.AQUA}Pumpkin Guts")
		override val example: ItemStack = expression.solveToItemStack()
	}
}

object QuarkIngredientLoud: CustomItem() {
	override val expression: NBTExpression = NBTExpression().with("tag.Quark", "LOUD")
	override val example: ItemStack = QuarkIngredient.ThunderousPotato.example
}

object QuarkIngredientNuanced: CustomItem() {
	override val expression: NBTExpression = NBTExpression().with("tag.Quark", "NUANCED")
	override val example: ItemStack = QuarkIngredient.Reeds.example
}

object QuarkIngredientSmooth: CustomItem() {
	override val expression: NBTExpression = NBTExpression().with("tag.Quark", "SMOOTH")
	override val example: ItemStack = QuarkIngredient.PumpkinSeedOil.example
}

object QuarkIngredientSparkling: CustomItem() {
	override val expression: NBTExpression = NBTExpression().with("tag.Quark", "SPARKLING")
	override val example: ItemStack = QuarkIngredient.RainbowRind.example
}

object QuarkIngredientLovely: CustomItem() {
	override val expression: NBTExpression = NBTExpression().with("tag.Quark", "LOVELY")
	override val example: ItemStack = QuarkIngredient.GoldenFlour.example
}

object QuarkIngredientGross: CustomItem() {
	override val expression: NBTExpression = NBTExpression().with("tag.Quark", "GROSS")
	override val example: ItemStack = QuarkIngredient.PumpkinGuts.example
}
