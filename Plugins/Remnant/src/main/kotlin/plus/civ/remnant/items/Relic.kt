package plus.civ.remnant.items

import org.bukkit.Material
import org.bukkit.enchantments.Enchantment
import vg.civcraft.mc.civmodcore.expression.*

object Relic: CustomItem() {
	override val expression = NBTExpression.empty()
		.id(Material.NETHER_STAR)
		.itemName("Neutron Star")
		.loreLine("Relic")
		.with("tag.ench[_]", EnchantmentMatcher.enchant(Enchantment.DURABILITY, 3))
		.amountGTE(1)
		.or(NBTExpression.empty().loreLine("Relic").amountGTE(1))

	override val example = expression.solveToItemStack()

	// Devkit snip: All of the concrete relics and how to generate new ones

}
