package com.github.devotedmc.hiddenore.event;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.inventory.ItemStack;

public class HiddenOreEvent extends PlayerEvent {
	public HiddenOreEvent(Player who, Block block, ItemStack item, Boolean doEffects) {
		super(who);
		this.block = block;
		this.item = item;
		this.doEffects = doEffects;
	}

	private final Block block;
	private final ItemStack item;
	private final boolean doEffects;

	private static final HandlerList handlers = new HandlerList();

	public static HandlerList getHandlerList() {
		return handlers;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public Block getBlock() {
		return block;
	}

	public ItemStack getItem() {
		return item;
	}

	public boolean isDoEffects() {
		return doEffects;
	}
}
