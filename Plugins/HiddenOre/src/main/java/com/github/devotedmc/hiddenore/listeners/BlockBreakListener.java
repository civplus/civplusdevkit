package com.github.devotedmc.hiddenore.listeners;
import com.github.devotedmc.hiddenore.event.HiddenOreEvent;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import java.util.logging.Level;

import com.github.devotedmc.hiddenore.BlockConfig;
import com.github.devotedmc.hiddenore.DropConfig;
import com.github.devotedmc.hiddenore.HiddenOre;
import com.github.devotedmc.hiddenore.Config;
import vg.civcraft.mc.civmodcore.extensions.EntityKt;

public class BlockBreakListener implements Listener {
	
    @SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
		if (event.getBlock().getType() == Material.STONE ||
				event.getBlock().getType() == Material.HARD_CLAY ||
				event.getBlock().getType() == Material.STAINED_CLAY ||
				event.getBlock().getType() == Material.CLAY ||
				event.getBlock().getType() == Material.DIRT ||
				event.getBlock().getType() == Material.SAND ||
				event.getBlock().getType() == Material.SANDSTONE ||
				event.getBlock().getType() == Material.RED_SANDSTONE ||
				event.getBlock().getType() == Material.GRAVEL) {
			if(!HiddenOre.getTracking().trackBreak(event.getBlock().getLocation())) {
				debug("Drop skipped at {0} - layer break max met", event.getBlock().getLocation());
				return;
			}
		}
    	
        Block b = event.getBlock();
        String blockName = b.getType().name();
        Byte sb = b.getData();
        BlockConfig bc = Config.isDropBlock(blockName, sb);
        
        if (bc == null) return;
        debug("Break of tracked type {0}", blockName);
        
        Player p = event.getPlayer();

		boolean hasDrop = false;

		StringBuffer alertUser = null;
		StringBuffer customAlerts = null;

    	String biomeName = b.getBiome().name();
        
    	if (bc.dropMultiple){
	    	for(String drop : bc.getDrops()) {
	    		DropConfig dc = bc.getDropConfig(drop);
	    		
	    		if (!dc.dropsWithTool(biomeName, p.getItemInHand().getType().name())) {
	    			debug("Cannot drop {0} - wrong tool", drop);
	    			continue;
	    		}
	    		
	    		if	(b.getLocation().getBlockY() > dc.getMaxY(biomeName) || 
	    				b.getLocation().getBlockY() < dc.getMinY(biomeName)) {
	    			debug("Cannot drop {0} - wrong Y", drop);
	    			continue;
	    		}
	    		
	    		double dropChance = dc.getChance(biomeName);
	    		
	    		//Random check to decide whether or not the special drop should be dropped
	    		if(dropChance > Math.random()) {
					ItemStack item = dc.renderDrop(drop, biomeName);
	    			givePlayerDrop(p, b, item, dropChance < 0.002);
					b.setType(Material.AIR);
					event.setCancelled(true);

					log("For {5} at {6} replacing {0}:{1} with {2} {3} at dura {4}", blockName, sb, 
							item.getAmount(), drop, item.getDurability(), p.getDisplayName(), p.getLocation());
					if (Config.isAlertUser()) {
						if (alertUser == null) {
							alertUser = new StringBuffer().append(Config.instance.defaultPrefix);
						}
						if (bc.hasCustomPrefix(drop)) {
							customAlerts = new StringBuffer();
							customAlerts.append(bc.getPrefix(drop))
									.append(" ").append(item.getAmount()).append(" ").append(Config.getPrettyName(drop, item.getDurability()));
							event.getPlayer().sendMessage(ChatColor.GOLD + customAlerts.toString());
							customAlerts = null;
						} else{
							if (Config.isListDrops()) {
								alertUser.append(" ").append(item.getAmount()).append(" ").append(Config.getPrettyName(drop, item.getDurability())).append(",");
							}
							hasDrop = true;
						}
					}
	    		}
	    	}
    	} else {
    		String drop = bc.getDropConfig(Math.random(), biomeName, 
    				p.getItemInHand().getType().name(), b.getLocation().getBlockY());
    		
    		if (drop != null) {
        		DropConfig dc = bc.getDropConfig(drop);
				ItemStack item = dc.renderDrop(drop, biomeName);
				givePlayerDrop(p, b, item, false);
				b.setType(Material.AIR);
				event.setCancelled(true);

    			log("For {5} at {6} replacing {0}:{1} with {2} {3} at dura {4}", blockName, sb, 
						item.getAmount(), drop, item.getDurability(), p.getDisplayName(), p.getLocation());
				if (Config.isAlertUser()) {
					if (alertUser == null) {
						alertUser = new StringBuffer().append(Config.instance.defaultPrefix);
					}
					if (bc.hasCustomPrefix(drop)) {
						customAlerts = new StringBuffer();
						customAlerts.append(bc.getPrefix(drop))
								.append(" ").append(item.getAmount()).append(" ").append(Config.getPrettyName(drop, item.getDurability()));
						event.getPlayer().sendMessage(ChatColor.GOLD + customAlerts.toString());
						customAlerts = null;
					} else{
						if (Config.isListDrops()) {
							alertUser.append(" ").append(item.getAmount()).append(" ").append(Config.getPrettyName(drop, item.getDurability())).append(",");
						}
						hasDrop = true;
					}
				}
    		}
    	}
		if (Config.isAlertUser() && hasDrop) {
			if (Config.isListDrops()) {
				alertUser.deleteCharAt(alertUser.length() - 1);
			}
			
			event.getPlayer().sendMessage(ChatColor.GOLD + alertUser.toString());
		}
    }

	private void givePlayerDrop(Player p, Block b, ItemStack item, boolean doEffects) {
		Bukkit.getPluginManager().callEvent(new HiddenOreEvent(p, b, item, doEffects));
		EntityKt.giveItemOrDropOnGround(p, item);
		if (doEffects) {
			p.playSound(b.getLocation(), Sound.FIREWORK_TWINKLE, 50, 1.0f);
			p.playSound(b.getLocation(), Sound.LEVEL_UP, 100, 0.85f);
			p.playEffect(b.getLocation(), Effect.EXPLOSION, null);
		} else {
			p.playSound(b.getLocation(), Sound.ORB_PICKUP, 100, 1.2f);
		}
		p.playEffect(b.getLocation(), Effect.MOBSPAWNER_FLAMES, null);
	}

	private void log(String message, Object...replace) {
		HiddenOre.getPlugin().getLogger().log(Level.INFO, message, replace);
	}
	
	private void debug(String message, Object...replace) {
		if (Config.isDebug) {
			HiddenOre.getPlugin().getLogger().log(Level.INFO, message, replace);
		}
	}
}
