package plus.civ.templateplugin

import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PluginInfo

@PluginInfo(
		name = "TemplatePlugin",
		description = "Replace this with a description of your plugin",
		author = "",
		depends = ["CivModCore"],
)
class TemplatePlugin: KotlinPlugin() {
	companion object {
		private var instanceStorage: TemplatePlugin? = null
		val instance: TemplatePlugin
			get() = instanceStorage!!
	}

	override fun onEnable() {
		super.onEnable()
		instanceStorage = this
	}
}
