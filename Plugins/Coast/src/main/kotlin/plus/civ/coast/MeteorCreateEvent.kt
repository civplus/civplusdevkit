package plus.civ.coast

import org.bukkit.event.Event
import org.bukkit.event.HandlerList

class MeteorCreateEvent(val meteor: Meteor): Event() {
	companion object {
		private val _handlers: HandlerList = HandlerList()
		@JvmStatic
		fun getHandlerList(): HandlerList {
			return _handlers
		}
	}
	override fun getHandlers(): HandlerList {
		return _handlers
	}
}
