package plus.civ.coast.commands

import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.HandlerList
import org.bukkit.event.Listener
import org.bukkit.event.inventory.InventoryCloseEvent
import plus.civ.coast.Coast
import plus.civ.coast.Meteor
import plus.civ.kotlinplugin.CommandInfo
import vg.civcraft.mc.civmodcore.extensions.isEmpty

@CommandInfo(
	name = "meteor",
	description = "Summon a meteor where you're standing",
	aliases = ["crash"],
	usage = "/<command> [delay (minutes)] [radius]",
	permission = "coast.summon",
)
class MeteorCommand: CommandExecutor {
	val maxRadius = 25

	override fun onCommand(sender: CommandSender, command: Command, s: String, args: Array<String>): Boolean {
		if (sender !is Player) {
			sender.sendMessage("${ChatColor.RED}Only players can run this command!")
			return true
		}

		if (args.size != 2) return false

		val delay = runCatching{ args[0].toFloat() }.getOrElse {throwable ->
			if (throwable is ArrayIndexOutOfBoundsException) {
				return false
			} else {
				sender.sendMessage("${ChatColor.RED}Delay must be a number!")
				return true
			}
		}
		val radius = runCatching{ args[1].toFloat() }.getOrElse { throwable ->
			if (throwable is ArrayIndexOutOfBoundsException) {
				return false
			} else {
				sender.sendMessage("${ChatColor.RED}Radius must be a number!")
				return true
			}
		}
		if (radius > maxRadius) {
			sender.sendMessage("${ChatColor.RED}Max radius is $maxRadius")
			return true
		}
		val loc = sender.location

		// Create the inventory that will go in the meteor
		val meteorChestInv = Coast.instance.server.createInventory(sender, 27, "Meteor")
		sender.openInventory(meteorChestInv)
		// Register a listener for when the player closes the inv, and unregister it after it goes off
		val invCloseListener: Listener = object : Listener {
			@EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
			fun onInventoryClose(event: InventoryCloseEvent) {
				if (event.player != sender) {
					return
				}
				if (event.inventory != meteorChestInv) {
					return
				}
				if (event.inventory.isEmpty) {
					return
				}
				Meteor(loc, radius, delay, meteorChestInv).scheduleSummon()
				HandlerList.unregisterAll(this)
			}
		}
		Bukkit.getServer().pluginManager.registerEvents(invCloseListener, Coast.instance)
		return true
	}
}
