package plus.civ.coast

import org.bukkit.*
import org.bukkit.entity.Player
import org.bukkit.scheduler.BukkitRunnable
import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PermissionDefault
import plus.civ.kotlinplugin.PluginInfo
import plus.civ.kotlinplugin.PluginPermission
import vg.civcraft.mc.civmodcore.extensions.safeDistance
import java.util.*
import kotlin.collections.ArrayList

@PluginInfo(
	name = "Coast",
	description = "Summon meteors full of loot",
	author = "sepia",
	authors = ["1machinemaker1"],
	depends = ["CivModCore"],
	permissions = [
		PluginPermission(
			name = "coast.summon",
			default = PermissionDefault.OP,
		)
	]
)
class Coast: KotlinPlugin() {
	companion object {
		internal var _instance: Coast? = null
		val instance: Coast
			get() = _instance!!
	}

	// A list of fallen meteor auras
	internal val fallenMeteorAuras: MutableList<FallenMeteorAura> = Collections.synchronizedList(ArrayList())
	// A list of meteors that are scheduled to fall later
	internal val scheduledMeteors: MutableList<Pair<Meteor, Float>> = ArrayList()

	override fun onEnable() {
		_instance = this
		super.onEnable()
	}

	fun isPlayerInAura(player: Player): Boolean {
		return isLocationInAura(player.location)
	}

	fun isLocationInAura(location: Location): Boolean {
		return fallenMeteorAuras.any{ it.location.safeDistance(location) < Config.AURA_RADIUS }
	}

	/**
	 * Creates a fallen meteor aura at the given location, which will be set to expire after the configed amount of time
	 */
	internal fun createFallenMeteorAura(loc: Location) {
		val aura = FallenMeteorAura(loc)
		fallenMeteorAuras.add(aura)
		aura.startEffects()
		// Remove the aura from the list when it expires, and kill its particle spawner
		object : BukkitRunnable() {
			override fun run() {
				fallenMeteorAuras.remove(aura)
				aura.stopEffects()
			}
		}.runTaskLater(instance, Config.AURA_EXPIRATION_TIME)
	}
}
