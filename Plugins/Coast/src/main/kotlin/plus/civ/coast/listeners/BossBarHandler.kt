package plus.civ.coast.listeners

import com.connorlinfoot.actionbarapi.ActionBarAPI
import net.md_5.bungee.api.ChatColor
import net.md_5.bungee.api.chat.TextComponent
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerMoveEvent
import org.inventivetalent.bossbar.BossBarAPI
import plus.civ.coast.Coast
import plus.civ.coast.Config
import plus.civ.coast.MeteorCreateEvent
import vg.civcraft.mc.civmodcore.extensions.safeDistance

class BossBarHandler: Listener {
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onPlayerMove(event: PlayerMoveEvent) {
		if (!Coast.instance.isLocationInAura(event.from) && Coast.instance.isLocationInAura(event.to)) {
			giveBars(event.player)
		} else if (!Coast.instance.isLocationInAura(event.to)) {
			BossBarAPI.removeAllBars(event.player)
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onNewMeteor(event: MeteorCreateEvent) {
		val playersInAura = event.meteor.location.world.players.filter{ it.location.safeDistance(event.meteor.location) < Config.AURA_RADIUS }
		for (player in playersInAura) {
			giveBars(player)
		}
	}

	private fun giveBars(player: Player) {
		BossBarAPI.addBar(
			player,
			TextComponent("${ChatColor.DARK_PURPLE}In Meteor Aura"),
			BossBarAPI.Color.PURPLE,
			BossBarAPI.Style.PROGRESS,
			1.0f,
			BossBarAPI.Property.DARKEN_SKY,
			BossBarAPI.Property.CREATE_FOG,
		)
		ActionBarAPI.sendActionBar(player, "${ChatColor.DARK_PURPLE}The air stings.")
	}
}
