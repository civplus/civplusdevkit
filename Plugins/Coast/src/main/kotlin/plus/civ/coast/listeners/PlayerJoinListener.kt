package plus.civ.coast.listeners

import org.bukkit.ChatColor
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent
import plus.civ.coast.Coast

class PlayerJoinListener: Listener {
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onPlayerJoin(event: PlayerJoinEvent) {
		// Get soonest meteor, or return if there are none scheduled
		val soonestMeteorPair = Coast.instance.scheduledMeteors.minByOrNull{ it.second } ?: return
		// Time until meteor in minutes
		val timeUntilSoonestMeteor = soonestMeteorPair.second - (System.currentTimeMillis() / 60_000).toFloat()
		// Location of the meteor
		val soonestMeteor = soonestMeteorPair.first

		event.player.sendMessage("""
			${ChatColor.YELLOW}A meteor is flying overhead. It will fall in about ${ChatColor.GREEN}$timeUntilSoonestMeteor${ChatColor.YELLOW} minutes 
			at ${ChatColor.GREEN}${soonestMeteor.roundedLocation}${ChatColor.YELLOW}.
		""".trimIndent().replace("\n", "")
		)
	}
}
