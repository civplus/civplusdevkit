package plus.civ.coast

import org.bukkit.ChatColor
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.Chest
import org.bukkit.inventory.Inventory
import org.bukkit.scheduler.BukkitRunnable
import vg.civcraft.mc.civmodcore.extensions.isEmpty
import kotlin.math.ceil
import kotlin.math.pow
import kotlin.math.sqrt
import kotlin.random.Random
import kotlin.random.nextInt

/**
 * A meteor, which may or may not have fallen yet
 * @param location The center location of the meteor
 * @param radius The radius of the meteor in blocks (decimals can be used to mess with the exact shape)
 * @param delay The amount of minutes to wait before launching the meteor
 * @param inv The Inventory that will be put in the meteor's chest after it lands
 */
class Meteor(val location: Location, val radius: Float = Config.DEFAULT_RADIUS, val delay: Float = Config.DEFAULT_DELAY, val inv: Inventory? = null) {
	val roundedLocation: String
		get() {
			// Rounded coordinates for notification
			val xRounded = location.blockX / 100 * 100
			val zRounded = location.blockZ / 100 * 100
			return "($xRounded, $zRounded)"
		}

	private fun summonNow() {
		// A box of sidelength radius*2, rounding radius up to make sure we get everything
		val startX = location.blockX - ceil(radius.toDouble()).toInt()
		val endX = location.blockX + ceil(radius.toDouble()).toInt()
		val startY = location.blockY - ceil(radius.toDouble()).toInt()
		val endY = location.blockY + ceil(radius.toDouble()).toInt()
		val startZ = location.blockZ - ceil(radius.toDouble()).toInt()
		val endZ = location.blockZ + ceil(radius.toDouble()).toInt()
		// For all blocks in the box
		for (i in startX..endX) {
			for (j in startY..endY) {
				for (k in startZ..endZ) {
					// If distance from center is within radius, we set the block
					val distFromCenter: Double = sqrt((i - location.blockX).toDouble().pow(2.0) + (j - location.blockY).toDouble().pow(2.0) + (k - location.blockZ).toDouble().pow(2.0))
					if (distFromCenter < radius) {
						location.world.getBlockAt(i, j, k).type = Config.METEOR_BLOCKS[Random.nextInt(Config.METEOR_BLOCKS.indices)]
					}
				}
			}
		}

		// Create chest in center of meteor, if there will be one
		if (inv != null && !inv.isEmpty) {
			val chestLoc = location
			val chestBlock = chestLoc.block
			chestBlock.type = Material.CHEST
			(chestBlock.state as Chest).inventory.contents = inv.contents
		}

		// Create the aura
		Coast.instance.createFallenMeteorAura(location)

		Coast.instance.server.pluginManager.callEvent(MeteorCreateEvent(this))
	}

	fun scheduleSummon() {
		// Notify the players
		val preAlertMessage = "${ChatColor.YELLOW}A meteor has been spotted in the sky above ${ChatColor.GREEN}$roundedLocation${ChatColor.YELLOW}! It will fall in ${ChatColor.GREEN}${delay.toInt()}${ChatColor.YELLOW} minutes!"
		location.world.players.forEach { it.sendMessage(preAlertMessage) }
		// Summon the meteor when it's time
		val delayTicks = (delay * 60 * 20).toInt()
		// Needed for explicit reference inside the runnable scope
		val selfMeteor = this
		object : BukkitRunnable() {
			override fun run() {
				// Summon the meteor
				summonNow()
				// Notify the players
				val message = "${ChatColor.YELLOW}A meteor is falling near ${ChatColor.GREEN}$roundedLocation${ChatColor.YELLOW}!"
				location.world.players.forEach { it.sendMessage(message) }
				// Remove self from scheduled meteors list
				Coast.instance.scheduledMeteors.removeIf { it.first == selfMeteor }
			}
		}.runTaskLater(Coast.instance, delayTicks.toLong())
		// Add to scheduled meteors list
		Coast.instance.scheduledMeteors.add(Pair(this, (System.currentTimeMillis() / 60_000).toFloat() + delay))
	}
}

