package plus.civ.coast

import org.bukkit.Material
import kotlin.math.pow

internal object Config {
	// Blocks that a meteor is made of. You can add duplicates to mess with ratios
    val METEOR_BLOCKS = arrayOf(Material.OBSIDIAN, Material.OBSIDIAN, Material.NETHERRACK, Material.ENDER_STONE, Material.ENDER_STONE)

	// Default time in minutes to wait before summoning a meteor when using the command
	const val DEFAULT_DELAY: Float = 0.25f

	// Default radius of meteors if no other radius is provided in the command
	const val DEFAULT_RADIUS: Float = 2.5f

	// Time in ticks that a fallen meteor aura sticks around for
	const val AURA_EXPIRATION_TIME: Long = 20L * 60L * 30L // 30 minutes

	// The radius in meters of a fallen meteor aura
	const val AURA_RADIUS: Float = 40.0f

	// How many particles to spawn per particle update around meteor auras
	// NOTE: the cute formula i'm doing makes it x particles per cubic meter (x being that first number)
	val FLAME_PARTICLE_DENSITY: Int = (0.000285 * (4/3) * Math.PI * AURA_RADIUS.toDouble().pow(3)).toInt()
	// How many purple particles to spawn per perticle update around meteor auras
	val PURPLE_PARTICLE_DENSITY: Int = FLAME_PARTICLE_DENSITY / 2
}
