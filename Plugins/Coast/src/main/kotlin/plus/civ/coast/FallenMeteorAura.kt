package plus.civ.coast

import org.bukkit.Location
import org.bukkit.potion.PotionEffect
import org.bukkit.potion.PotionEffectType
import org.bukkit.scheduler.BukkitRunnable
import org.bukkit.util.Vector
import vg.civcraft.mc.civmodcore.extensions.safeDistance
import xyz.xenondevs.particle.ParticleBuilder
import xyz.xenondevs.particle.ParticleEffect
import xyz.xenondevs.particle.utils.ParticleUtils

class FallenMeteorAura(val location: Location) {
	private var task: BukkitRunnable? = null

	fun startEffects() {
		if (task != null) {
			stopEffects()
		}

		task = object : BukkitRunnable() {
			override fun run() {
				tickParticles()
				afflictMaliceUponPlayers()
			}
		}
		task!!.runTaskTimerAsynchronously(Coast.instance, 20, 3)
	}

	fun stopEffects() {
		task?.cancel()
		task = null
	}

	private fun tickParticles() {
		val particlePackets: ArrayList<Any> = ArrayList()
		val flameBuilder = ParticleBuilder(ParticleEffect.FLAME, location).setSpeed(0.05f)
		for (i in 0..Config.FLAME_PARTICLE_DENSITY) {
			// generate a random offset within a sphere, by getting a random direction and distance
			val dx = Math.random().toFloat() - 0.5f
			val dy = Math.random().toFloat() - 0.5f
			val dz = Math.random().toFloat() - 0.5f
			val dist = Math.random().toFloat() * Config.AURA_RADIUS
			val offsetVec = Vector(dx, dy, dz).normalize().multiply(dist)

			// generate a random velocity that's mostly upward
			val vx = Math.random().toFloat() - 0.5f
			val vy = Math.random().toFloat() * 2f
			val vz = Math.random().toFloat() - 0.5f

			val packet = flameBuilder
				.setLocation(location.clone().add(offsetVec))
				.setOffset(vx, vy, vz)
				.toPacket()
			particlePackets.add(packet)
		}
		val purpleBuilder = ParticleBuilder(ParticleEffect.PORTAL, location).setSpeed(0.08f)
		for (i in 0..Config.PURPLE_PARTICLE_DENSITY) {
			// generate a random offset within a sphere, by getting a random direction and distance
			val dx = Math.random().toFloat() - 0.5f
			val dy = Math.random().toFloat() - 0.5f
			val dz = Math.random().toFloat() - 0.5f
			val dist = Math.random().toFloat() * Config.AURA_RADIUS
			val offsetVec = Vector(dx, dy, dz).normalize().multiply(dist)

			// generate a random velocity
			val vx = Math.random().toFloat() - 0.5f
			val vy = Math.random().toFloat() - 0.5f
			val vz = Math.random().toFloat() - 0.5f

			val packet = purpleBuilder
				.setLocation(location.clone().add(offsetVec))
				.setOffset(vx, vy, vz)
				.toPacket()
			particlePackets.add(packet)
		}
		ParticleUtils.sendBulk(particlePackets, location.world.players)
	}

	private fun afflictMaliceUponPlayers() {
		object : BukkitRunnable() {
			override fun run() {
				location.world.players
					.filter{ it.location.safeDistance(location) < Config.AURA_RADIUS }
					.forEach{
						it.addPotionEffect(PotionEffect(PotionEffectType.WITHER, 40, 0))
					}
			}
		}.runTask(Coast.instance)
	}
}
