package plus.civ.topshelf.commands

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.inventory.ItemStack
import plus.civ.kotlinplugin.CommandInfo
import plus.civ.kotlinplugin.PermissionDefault
import plus.civ.kotlinplugin.PluginPermission
import plus.civ.topshelf.config.TopShelfConfig

@CommandInfo(
	name = "listbrews",
	description = "Lists all the custom brews added by TopShelf",
	pluginPermission = PluginPermission(
		"topshelf.listbrews",
		default = PermissionDefault.OP,
	),
	usage = "listbrews",
)
class ListBrewsCommand: CommandExecutor {
	override fun onCommand(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): Boolean {
		sender.sendMessage("${ChatColor.UNDERLINE}TopShelf Brews List:")
		for (brew in TopShelfConfig.BEVERAGE_RECIPES) {
			val item = ItemStack(Material.POTION)
			brew.potionMutator.invoke(item)

			val name = item.itemMeta.displayName ?: continue
			val formattingName = name.replace(ChatColor.COLOR_CHAR, '&')
			sender.sendMessage(formattingName)
		}

		sender.sendMessage("")
		sender.sendMessage("Sent list of brews! To view the complete list with copy paste support, look at your minecraft log file.")
		sender.sendMessage("Number of brews: ${TopShelfConfig.BEVERAGE_RECIPES.size}")
		return true
	}
}
