package plus.civ.topshelf

import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PluginInfo
import plus.civ.topshelf.brewing.BrewingStandTicker
import plus.civ.topshelf.config.Config
import plus.civ.topshelf.config.TopShelfConfig
import plus.civ.topshelf.config.generateAllInvalidRecipes
import java.util.logging.Level

@PluginInfo(
	name = "TopShelf",
	author = "sepia",
	description = "Custom brewing recipes and better brewing control",
	depends = ["CivModCore", "Remnant"],
)
class TopShelf: KotlinPlugin() {
	companion object {
		private var _instance: TopShelf? = null
		val instance: TopShelf
			get() = _instance!!
	}

	override fun onEnable() {
		_instance = this
		super.onEnable()
		BrewingStandTicker.runTaskTimer(this, 1, 1)

		val topShelf = server.pluginManager.getPlugin("TopShelf")
		if (topShelf is TopShelf) {
			TopShelfConfig.QUARK_RECIPES.forEach { topShelf.registerPotionRecipe(it) }
			TopShelfConfig.BEVERAGE_RECIPES.forEach { topShelf.registerPotionRecipe(it) }
			TopShelfConfig.VANILLA_RECIPES.forEach { topShelf.registerPotionRecipe(it) }

			generateAllInvalidRecipes().forEach { topShelf.registerFailureRecipe(it) }
		} else {
			logger.log(Level.WARNING, "Could not find TopShelf. No brewing recipes have been added.")
		}

		PlayerDrunkennessDecayTask.runTaskTimer(this, PlayerDrunkennessDecayTask.RUN_FREQUENCY_TICKS, PlayerDrunkennessDecayTask.RUN_FREQUENCY_TICKS)
		PukingTask.runTaskTimer(this, PukingTask.RUN_FREQUENCY_TICKS, PukingTask.RUN_FREQUENCY_TICKS)
	}

	fun registerPotionRecipe(recipe: PotionRecipe) {
		Config.POTION_RECIPES.add(recipe)
	}

	fun registerFailureRecipe(recipe: PotionRecipe) {
		Config.FAILURE_RECIPES.add(recipe)
	}
}
