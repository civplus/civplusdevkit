package plus.civ.topshelf.commands

import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabExecutor
import org.bukkit.entity.Player
import plus.civ.kotlinplugin.CommandInfo
import plus.civ.kotlinplugin.PermissionDefault
import plus.civ.kotlinplugin.PluginPermission
import plus.civ.topshelf.config.Quark
import vg.civcraft.mc.civmodcore.expression.solveToItemStack
import vg.civcraft.mc.civmodcore.extensions.giveItemOrDropOnGround

@CommandInfo(
	name = "givequark",
	description = "Gives you a quark item or potion",
	pluginPermission = PluginPermission(
		"topshelf.givequark",
		default = PermissionDefault.OP,
	),
	usage = "/givequark [quark] (ingredient/potion)",
)
class GiveQuarkCommand: TabExecutor {
	override fun onTabComplete(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): MutableList<String> {
		if (args.isEmpty() || args[0].uppercase() !in Quark.values().map { it.toString() }) {
			val starting = (args.getOrElse(0) {""}).uppercase()
			return Quark.values()
				.map { it.toString() }
				.filter{ it.startsWith(starting) }
				.toMutableList()
		} else {
			val starting = (args.getOrElse(1) {""}).lowercase()
			return listOf("ingredient", "potion")
				.filter{ it.startsWith(starting) }
				.toMutableList()
		}
	}

	override fun onCommand(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): Boolean {
		if (sender !is Player) {
			return false
		}

		if (args.size != 2) {
			return false
		}

		val quark = Quark.values().firstOrNull { it.toString() == args[0].uppercase() } ?: return false
		val ingredient = if (args[1].lowercase() == "ingredient") {
			true
		} else if (args[1].lowercase() == "potion") {
			false
		} else { // it is neither potion nor ingredient
			return false
		}

		val item = if (ingredient) quark.ingredient.example else quark.basePotion.solveToItemStack()

		sender.giveItemOrDropOnGround(item)
		sender.sendMessage("${ChatColor.GREEN}Gave a $quark ${if (ingredient) "ingredient" else "base potion"}!")
		return true
	}
}
