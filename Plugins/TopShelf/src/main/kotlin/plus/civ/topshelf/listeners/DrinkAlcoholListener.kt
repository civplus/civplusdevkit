package plus.civ.topshelf.listeners

import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerItemConsumeEvent
import org.bukkit.inventory.meta.PotionMeta
import org.bukkit.util.noise.PerlinNoiseGenerator
import plus.civ.topshelf.TopShelf
import plus.civ.topshelf.config.alcoholContent
import plus.civ.topshelf.drunkenness

class DrinkAlcoholListener: Listener {
	val immediateDrunkennessPercentage = 0.2

	@EventHandler
	fun playerDrink(event: PlayerItemConsumeEvent) {
		if (event.item.type != Material.POTION) return

		// figure out what the per player natural alcohol tolerance is
		val playerSeed = event.player.uniqueId.hashCode()
		val playerNoise = PerlinNoiseGenerator.getNoise(playerSeed.toDouble()) + 1  // from 0 to 2

		val potion = event.item
		val potionMeta = potion.itemMeta as PotionMeta

		val totalGivenDrunkenness = potion.alcoholContent * playerNoise

		// apply 20% of the drunkenness now
		event.player.drunkenness += totalGivenDrunkenness * immediateDrunkennessPercentage

		val totalSplitDrunkenness = totalGivenDrunkenness * (1 - immediateDrunkennessPercentage)
		if (potionMeta.customEffects.isEmpty()) {
			event.player.drunkenness += totalSplitDrunkenness
			return
		}

		val drunkennessPerEffect = totalSplitDrunkenness / potionMeta.customEffects.size
		for (effect in potionMeta.customEffects) {
			Bukkit.getScheduler().runTaskLater(TopShelf.instance, {
				event.player.drunkenness += drunkennessPerEffect
			}, effect.duration.toLong())
		}
	}
}
