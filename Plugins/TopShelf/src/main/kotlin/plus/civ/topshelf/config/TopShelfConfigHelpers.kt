package plus.civ.topshelf.config

import net.minecraft.server.v1_8_R3.NBTBase
import net.minecraft.server.v1_8_R3.NBTTagList
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.inventory.ItemFlag
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.PotionMeta
import org.bukkit.potion.PotionEffect
import org.bukkit.potion.PotionEffectType
import org.bukkit.potion.PotionEffectType.*
import plus.civ.remnant.items.*
import plus.civ.topshelf.PotionRecipe
import plus.civ.topshelf.config.TopShelfConfig.MAX_AMPLIFIERS
import vg.civcraft.mc.civmodcore.expression.*
import vg.civcraft.mc.civmodcore.extensions.nbt
import vg.civcraft.mc.civmodcore.extensions.potionEffectTypeToPotionColorShort
import vg.civcraft.mc.civmodcore.extensions.prettyName
import kotlin.math.pow

internal fun basePotion(prefix: String, damage: Short = 7, color: PotionEffectType? = null): NBTExpression {
	val expression = NBTExpression()
		.id(Material.POTION)
		.potionHasNoEffects()
		.itemName("${ChatColor.RESET}$prefix Potion")

	if (color != null) {
		return expression.damage(potionEffectTypeToPotionColorShort[color]!!)
		// todo: use bitwise operations to mix the low 4 bits of color with the top 12 of damage
	} else {
		return expression.damage(damage)
	}
}
internal val waterBottle = NBTExpression().id(Material.POTION).damage(0).potionHasNoEffects()

// Important potion bases (should be simple to make)
internal val clearPotion = basePotion("Clear") // positive custom effects
internal val thickPotion = basePotion("Thick") // negative custom effects
internal val awkwardPotion = basePotion("Awkward", 16) // Vanilla potions
internal val ruinedPotion = basePotion("Preposterous", color = POISON).loreLine("Ruined Potion") // you messed up somewhere

// Quark potions (made from a single ingredient)
internal val loudPotion = basePotion("Loud", color = INCREASE_DAMAGE)
internal val nuancedPotion = basePotion("Nuanced", color = INVISIBILITY)
internal val smoothPotion = basePotion("Smooth", color = JUMP)
internal val sparklingPotion = basePotion("Sparkling", color = NIGHT_VISION)
internal val lovelyPotion = basePotion("Lovely", color = REGENERATION)
internal val grossPotion = basePotion("Gross", color = POISON)

// Complex potions (made from a ratio of multiple ingredients)
internal val rotoundPotion = basePotion("Rotound", color = SLOW)
internal val annoyingPotion = basePotion("Annoying", color = JUMP)
internal val eagerPotion = basePotion("Eager", color = SPEED)
internal val acridPotion = basePotion("Acrid", color = INCREASE_DAMAGE)
internal val refinedPotion = basePotion("Refined", color = NIGHT_VISION)
internal val tinyPotion = basePotion("Tiny", color = INVISIBILITY)
internal val flirtatiousPotion = basePotion("Flirtatious", color = REGENERATION)
internal val meekPotion = basePotion("Meek", color = INVISIBILITY)
internal val charmingPotion = basePotion("Charming", color = HEAL)
internal val foulPotion = basePotion("Foul", color = WEAKNESS)
internal val stylishPotion = basePotion("Stylish", color = NIGHT_VISION)
internal val jaggedPotion = basePotion("Jagged", color = WEAKNESS)

internal enum class Quark(val basePotion: NBTExpression, val ingredient: CustomItem) {
	LOUD(loudPotion, QuarkIngredientLoud),
	NUANCED(nuancedPotion, QuarkIngredientNuanced),
	SMOOTH(smoothPotion, QuarkIngredientSmooth),
	SPARKLING(sparklingPotion, QuarkIngredientSparkling),
	LOVELY(lovelyPotion, QuarkIngredientLovely),
	GROSS(grossPotion, QuarkIngredientGross);
}

/**
 * creates a set of potion recipes for the given potion effect, from the ingredient, both to create the potion
 * from an awkward potion, and to increase the duration of the potion with more of the same ingredient
 */
internal fun basicPotion(
	type: PotionEffectType,
	ingredient: NBTExpression,
	startDuration: Int = 30,
	highDuration: Int = 60 * 20,
): Array<PotionRecipe> {
	return arrayOf(
		PotionRecipe(
			ingredient = ingredient,
			potionMutator = ItemMutator()
				.addMainPotionEffect(PotionEffect(type, startDuration * 20, 0))
				.setPotionColor(type)
				.setDisplayName("${ChatColor.RESET}Potion of ${type.prettyName}"),
			allowedStartingPotions = awkwardPotion
		),
		PotionRecipe(
			ingredient = ingredient,
			potionMutator = { input ->
				val meta = input.itemMeta as PotionMeta
				val effect = meta.customEffects.firstOrNull { it.type == type } ?: return@PotionRecipe

				// (a, b) is the starting point
				val a = 0.0
				val b = startDuration * 20.0
				// (c, d) is the point where the potion can no longer have its duration increased by more than a second at a time
				val c = (highDuration * 20.0) / (2.0.pow(effect.amplifier))
				val d = 20.0
				// h and v are used in the duration change function,
				// such that the function passes (a, b) and (c, d)
				val h = (b * a - d * c) / (b - d)
				val v = d * c - d * h

				val oldDuration = effect.duration * 1.0
				val newDuration = (oldDuration + v / (oldDuration - h)).toInt()
				val newEffect = PotionEffect(effect.type, newDuration, effect.amplifier, effect.isAmbient, effect.hasParticles())
				meta.addCustomEffect(newEffect, true)
				input.itemMeta = meta
			},
			allowedStartingPotions = NBTExpression().id(Material.POTION).potionEffect(type)
		),
	)
}

// matches a potion where at least one effect's amplifier is not at the max
internal val potionWithUnmaxxedAmplifiers = NBTExpression()
	.id(Material.POTION)
	.with("tag.CustomPotionEffects", object : TagMatcher {
		override fun matches(tag: NBTBase?): Boolean {
			if (tag !is NBTTagList) return false
			val effects = (0..tag.size()).map { tag.get(it) }
			return effects.any {
				it.getByte("Amplifier") < MAX_AMPLIFIERS.getValue(getById(it.getByte("Id").toInt()))
			}
		}

		override fun solve(): NBTBase {
			val item = ItemStack(Material.POTION)
			val meta = item.itemMeta as PotionMeta
			meta.addCustomEffect(PotionEffect(SPEED, 100, 0), false)
			item.itemMeta = meta
			return item.nbt!!["CustomPotionEffects"]
		}
	})

internal fun basicQuarkIngredient(quark: Quark): PotionRecipe = PotionRecipe(
		ingredient = quark.ingredient.expression,
		potionMutator = ItemReplacer(quark.basePotion),
		allowedStartingPotions = waterBottle,
)

internal fun complexQuarkIngredient(quarkA: Quark, quarkB: Quark, result: NBTExpression): Array<PotionRecipe> {
	return arrayOf(
			PotionRecipe(
					ingredient = quarkA.ingredient.expression,
					potionMutator = ItemReplacer(result),
					allowedStartingPotions = quarkB.basePotion,
			),
			PotionRecipe(
					ingredient = quarkB.ingredient.expression,
					potionMutator = ItemReplacer(result),
					allowedStartingPotions = quarkA.basePotion,
			)
	)
}

internal fun fancyDrink(name: String, color: PotionEffectType, alcoholContent: Double? = null): ItemMutator {
	val mutator = ItemMutator()
		.setDisplayName(name)
		.setPotionColor(color)
		.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS)

	if (alcoholContent != null) {
		return mutator.setAlcoholContent(alcoholContent)
	} else {
		return mutator
	}
}

internal val allPotionMaterialIngredients: Array<Material> = arrayOf(
	Material.NETHER_STALK,
	Material.GLOWSTONE,
	Material.GLOWSTONE_DUST,
	Material.SULPHUR,
	Material.SPECKLED_MELON,
	Material.SUGAR,
	Material.BLAZE_POWDER,
	Material.BLAZE_ROD,
	Material.MAGMA_CREAM,
	Material.RABBIT_FOOT,
	Material.SPIDER_EYE,
	Material.RAW_FISH,
	Material.GOLDEN_CARROT,
	Material.GHAST_TEAR,
	Material.IRON_INGOT,
	Material.GOLDEN_APPLE,
	Material.WHEAT,
	Material.LEATHER,
	Material.APPLE,
	Material.RABBIT,
	Material.POISONOUS_POTATO,
	Material.WOOL,
	Material.DIAMOND,
	Material.BROWN_MUSHROOM,
	Material.INK_SACK,
	Material.OBSIDIAN,
	Material.BOOKSHELF,
	Material.MILK_BUCKET,
	Material.FERMENTED_SPIDER_EYE,
	Material.ENDER_STONE,
	Material.MONSTER_EGGS, // i forget which is which, so both it is
	Material.MONSTER_EGG,
	Material.PUMPKIN,
	Material.PUMPKIN_SEEDS,
	Material.YELLOW_FLOWER,
	Material.RED_ROSE,
	Material.LOG,
	Material.LOG_2,
	Material.STICK,
	Material.POTATO_ITEM,
	Material.STONE,
	Material.MELON,
	Material.MELON_BLOCK,
	Material.MELON_SEEDS,
	Material.PORK,
	Material.GRILLED_PORK,
	Material.COOKED_BEEF,
	Material.RAW_BEEF,
	Material.COOKED_CHICKEN,
	Material.RAW_CHICKEN,
	Material.COOKED_FISH,
	Material.GOLD_INGOT,
	Material.FIREWORK,
)

internal val allPotionLikeIngredients: Array<NBTExpression> = arrayOf(
	*allPotionMaterialIngredients.map { NBTExpression().id(it) }.toTypedArray(),

	waterBottle, clearPotion, thickPotion, awkwardPotion, ruinedPotion,

	loudPotion, smoothPotion, sparklingPotion, lovelyPotion, grossPotion,

	QuarkIngredientLoud.expression, QuarkIngredientSmooth.expression,
	QuarkIngredientSparkling.expression, QuarkIngredientLovely.expression, QuarkIngredientGross.expression,

	rotoundPotion, annoyingPotion, eagerPotion, acridPotion,
	refinedPotion, tinyPotion, flirtatiousPotion, meekPotion,
	charmingPotion, foulPotion, stylishPotion, jaggedPotion,
)

internal fun generateAllInvalidRecipes(): Array<PotionRecipe> {
	return allPotionLikeIngredients.map {
		PotionRecipe(
			ingredient = it,
			potionMutator = ItemReplacer(ruinedPotion),
			allowedStartingPotions = NBTExpression().id(Material.POTION),
		)
	}.toTypedArray()
}
