package plus.civ.topshelf.listeners

import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerMoveEvent
import org.bukkit.potion.PotionEffectType
import plus.civ.topshelf.drunkenness
import vg.civcraft.mc.civmodcore.extensions.safeDistance

class NauseaWalkingListener: Listener {
	val nauseaChancePerBlockAt100Percent = (1.0 / 15.0) // walking 15 blocks should give you nausea
	val nauseaChancePerBlockPerDrunkPoint = nauseaChancePerBlockAt100Percent / 100
	val nauseaStartingDrunkenness = 30.0
	val nauseaTimeTicks = 30 * 20 // Not actually 30 seconds. I'm not sure what this is.

	@EventHandler(ignoreCancelled = true)
	fun move(event: PlayerMoveEvent) {
		if (event.player.drunkenness < nauseaStartingDrunkenness) return

		val distanceMoved = event.to.safeDistance(event.from)
		if (distanceMoved == Double.POSITIVE_INFINITY) return

		val chance = nauseaChancePerBlockPerDrunkPoint * event.player.drunkenness * distanceMoved
		if (Math.random() < chance) {
			event.player.addPotionEffect(PotionEffectType.CONFUSION.createEffect(nauseaTimeTicks, 0))
		}
	}
}
