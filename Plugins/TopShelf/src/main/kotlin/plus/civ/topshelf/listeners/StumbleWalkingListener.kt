package plus.civ.topshelf.listeners

import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerMoveEvent
import org.bukkit.util.Vector
import plus.civ.topshelf.drunkenness
import java.lang.Integer.max
import java.util.*

class StumbleWalkingListener: Listener {
	data class PlayerPush(
		/**
		 * The time in an indefinite unit of how long it has been since the player has been pushed.
		 * Time only progresses while the player is moving and drunk
		 */
		var time: Int = 20,

		/**
		 * The last vector the player was pushed with
		 */
		var push: Vector = Vector(),

		/**
		 * How much the player will be pushed when they are pushed, in an indefinite unit.
		 */
		var stumbleModifier: Int = 1)
	val players = mutableMapOf<UUID, PlayerPush>()

	@EventHandler(ignoreCancelled = true)
	fun move(event: PlayerMoveEvent) {
		val playerData = players.getOrPut(event.player.uniqueId) { PlayerPush() }

		if (event.player.drunkenness >= 10) {
			if (playerData.time > 1) {
				playerData.time -= 1
			} else {
				if (event.from.x != event.to.x || event.from.z != event.to.z) {
					@Suppress("DEPRECATION")
					if (event.player.isOnGround) {
						playerData.time -= 1
						if (playerData.time == 0) {
							playerData.push.x = Math.random() - 0.5
							playerData.push.z = Math.random() - 0.5
							playerData.push.multiply(playerData.stumbleModifier)
							if (playerData.push.lengthSquared() <= 0) {
								playerData.time = -10
								return
							}
							event.player.velocity = playerData.push
						} else if (playerData.time < 0 && playerData.time > -10) {
							// push him some more in the same direction
							event.player.velocity = playerData.push
						} else {
							// when more alc, push him more often
							playerData.time = max((Math.random() * (201.0 - event.player.drunkenness * 2)).toInt(), 1)
						}
					}
				}
			}
		}
	}

}
