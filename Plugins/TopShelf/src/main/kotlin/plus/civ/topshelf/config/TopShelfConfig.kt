package plus.civ.topshelf.config

import org.bukkit.ChatColor
import org.bukkit.DyeColor
import org.bukkit.Material
import org.bukkit.entity.EntityType
import org.bukkit.inventory.meta.PotionMeta
import org.bukkit.potion.PotionEffect
import org.bukkit.potion.PotionEffectType
import plus.civ.remnant.items.Relic
import plus.civ.topshelf.PotionRecipe
import vg.civcraft.mc.civmodcore.expression.*
import kotlin.experimental.or

object TopShelfConfig {
	// note that the number is actually one lower. for example, if 2 is given, then "Effect III" is the max.
	val MAX_AMPLIFIERS = mapOf<PotionEffectType, Byte>(
		Pair(PotionEffectType.SPEED, 1),
		Pair(PotionEffectType.HEAL, 1),
		Pair(PotionEffectType.POISON, 1),
		Pair(PotionEffectType.INCREASE_DAMAGE, 1),
		Pair(PotionEffectType.REGENERATION, 1),
		Pair(PotionEffectType.WATER_BREATHING, 0),
		Pair(PotionEffectType.JUMP, 2),
	).withDefault { 0 }

	val VANILLA_RECIPES = listOf(
		// awkward potion
		PotionRecipe(
			ingredient = NBTExpression().id(Material.NETHER_STALK),
			potionMutator = ItemReplacer(awkwardPotion),
			allowedStartingPotions = waterBottle,
		),
		// amplifier upgrade
		PotionRecipe(
			ingredient = NBTExpression().id(Material.GLOWSTONE_DUST),
			potionMutator = { input ->
				val meta = input.itemMeta as PotionMeta
				meta.customEffects.forEach { effect ->
					if (effect.amplifier < MAX_AMPLIFIERS.getValue(effect.type)) {
						val newEffect = PotionEffect(
							effect.type,
							effect.duration / 2,
							effect.amplifier + 1,
							effect.isAmbient,
							effect.hasParticles()
						)
						meta.addCustomEffect(newEffect, true)
					}
				}
				input.itemMeta = meta
			},
			allowedStartingPotions = potionWithUnmaxxedAmplifiers,
		),
		// splash potions
		PotionRecipe(
			ingredient = NBTExpression().id(Material.SULPHUR),
			potionMutator = { input ->
				input.durability = input.durability.or(0b0100000000000000)
			},
			allowedStartingPotions = NBTExpression().notSplashPotion().without("tag.alcoholContent")
		),
		// health potion
		PotionRecipe(
			ingredient = NBTExpression().id(Material.SPECKLED_MELON),
			potionMutator = ItemMutator()
				.addMainPotionEffect(PotionEffect(PotionEffectType.HEAL, 0, 0))
				.setPotionColor(PotionEffectType.HEAL)
				.setDisplayName("${ChatColor.RESET}Heal Potion"),
			allowedStartingPotions = awkwardPotion
		),
		*basicPotion(PotionEffectType.SPEED, ingredient = NBTExpression().id(Material.SUGAR)),
		*basicPotion(PotionEffectType.INCREASE_DAMAGE, ingredient = NBTExpression().id(Material.BLAZE_POWDER)),
		*basicPotion(PotionEffectType.FIRE_RESISTANCE, ingredient = NBTExpression().id(Material.MAGMA_CREAM)),
		*basicPotion(PotionEffectType.JUMP, ingredient = NBTExpression().id(Material.RABBIT_FOOT)),
		*basicPotion(PotionEffectType.POISON, ingredient = NBTExpression().id(Material.SPIDER_EYE)),
		*basicPotion(PotionEffectType.WATER_BREATHING, ingredient = NBTExpression().id(Material.RAW_FISH).damage(3)),
		*basicPotion(PotionEffectType.NIGHT_VISION, ingredient = NBTExpression().id(Material.GOLDEN_CARROT)),
		*basicPotion(PotionEffectType.REGENERATION, ingredient = NBTExpression().id(Material.GHAST_TEAR)),
		*basicPotion(PotionEffectType.DAMAGE_RESISTANCE, ingredient = NBTExpression().id(Material.IRON_INGOT), startDuration = 30, highDuration = 4 * 60),
	)

	val BEVERAGE_RECIPES = listOf(
		PotionRecipe(
			ingredient = NBTExpression().id(Material.WHEAT),
			potionMutator = fancyDrink("${ChatColor.YELLOW}Beer", PotionEffectType.INVISIBILITY, 5.0)
				.addPotionEffect(PotionEffect(PotionEffectType.INCREASE_DAMAGE, 5 * 20, 0))
				.addPotionEffect(PotionEffect(PotionEffectType.BLINDNESS, 5 * 20, 0)),
			allowedStartingPotions = refinedPotion,
		),
		// Devkit Snip: Recipes on the live server go here. Message Amelorate for more examples if you want to add more recipes.
	)

	val QUARK_RECIPES = listOf(
		basicQuarkIngredient(Quark.LOUD),
		basicQuarkIngredient(Quark.NUANCED),
		basicQuarkIngredient(Quark.SMOOTH),
		basicQuarkIngredient(Quark.SPARKLING),
		basicQuarkIngredient(Quark.LOVELY),
		basicQuarkIngredient(Quark.GROSS),
		// Devkit Snip: complex combinations removed purely to make you sad.
	)
}
