package plus.civ.topshelf

import plus.civ.topshelf.config.ItemMutator
import vg.civcraft.mc.civmodcore.expression.NBTExpression

data class PotionRecipe(
    val ingredient: NBTExpression,
    val potionMutator: ItemMutator,
    val allowedStartingPotions: NBTExpression,
)
