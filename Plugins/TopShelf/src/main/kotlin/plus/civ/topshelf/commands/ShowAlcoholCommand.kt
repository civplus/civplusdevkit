package plus.civ.topshelf.commands

import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import plus.civ.kotlinplugin.CommandInfo
import plus.civ.kotlinplugin.PermissionDefault
import plus.civ.kotlinplugin.PluginPermission
import plus.civ.topshelf.drunkenness

@CommandInfo(
	name = "showalcohol",
	aliases = ["showdrunk", "showdrunkenness"],
	description = "Shows what alcohol value you have",
	pluginPermission = PluginPermission(
		"topshelf.showalcohol",
		default = PermissionDefault.OP,
	),
	usage = "/showalcohol",
)
class ShowAlcoholCommand: CommandExecutor {
	override fun onCommand(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): Boolean {
		if (sender !is Player) {
			return false
		}

		sender.sendMessage("${ChatColor.GREEN}Your drunkness value is ${sender.drunkenness}")
		return true
	}
}
