package plus.civ.topshelf.listeners

import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.block.BrewingStand
import org.bukkit.craftbukkit.v1_8_R3.block.CraftBrewingStand
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.inventory.*
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.event.world.ChunkLoadEvent
import org.bukkit.inventory.BrewerInventory
import org.bukkit.scheduler.BukkitRunnable
import plus.civ.topshelf.TopShelf
import plus.civ.topshelf.brewing.*

class BrewListener: Listener {
	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
	fun onPlayerInteractBrewingStandInventory(event: InventoryClickEvent) {
		if (event.view.topInventory.type != InventoryType.BREWING) return
		val brewInv = event.view.topInventory as BrewerInventory

		if (event.clickedInventory is BrewerInventory) { // if they clicked the brewingstand half of the inventory screen
			// Allow the player to put strange items into the brewing stand ingredient slot
			if (event.slot == 3 && event.cursor != null && event.cursor.type != Material.AIR) {
				when (event.click) {
					ClickType.LEFT -> {
						val oldItem = brewInv.contents[3]
						brewInv.setItem(3, event.cursor)
						event.cursor = oldItem
					}
					ClickType.RIGHT -> {
						val currentIngredient = brewInv.contents[3]
						if (event.cursor.isSimilar(currentIngredient)) {
							if (currentIngredient.amount < currentIngredient.maxStackSize) {
								val cursor = event.cursor
								cursor.amount -= 1
								if (cursor.amount == 0) {
									event.cursor = null
								} else {
									event.cursor = cursor
								}
								currentIngredient.amount += 1
							}
						} else if (currentIngredient == null || currentIngredient.type == Material.AIR) {
							val oldCursor = event.cursor
							val newCursor = event.cursor
							newCursor.amount -= 1
							if (newCursor.amount == 0) {
								event.cursor = null
							} else {
								event.cursor =  newCursor
							}
							brewInv.setItem(3, oldCursor.clone().also{ it.amount = 1 })
						}
					}
					else -> {}
				}
				// VERY IMPORTANT to cancel the event, so that it doesn't double'switch the items, which might cause dupes
				event.isCancelled = true
			}
		}
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = false)
	fun onPlayerTryStartBrew(event: InventoryClickEvent) {
		if (event.view.topInventory.type != InventoryType.BREWING) return
		val brewingStand: BrewingStand = event.view.topInventory.holder as CraftBrewingStand

		object : BukkitRunnable() { // runs in a moment because otherwise we have a before'snapshot of the inventory
			override fun run() {
				brewingStand.evaluate()
			}
		}.runTaskLater(TopShelf.instance, 1)
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onPlayerInteractBrewingStand(event: PlayerInteractEvent) {
		val stand = event.clickedBlock.state
		if (stand is BrewingStand) {
			stand.manage()
			stand.evaluate()
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onHopperBrewingStand(event: InventoryMoveItemEvent) {
		// Based on what the javadocs say, I think it's possible to make this work for custom ingredients
		// I'm not going to bother though.

		val brewingStand = event.destination.holder
		if (brewingStand !is BrewingStand) return

		brewingStand.manage()
		Bukkit.getScheduler().runTaskLater(TopShelf.instance, {
			brewingStand.evaluate()
		}, 1)
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onChunkLoadWithStand(event: ChunkLoadEvent) {
		for (tile in event.chunk.tileEntities) {
			if (tile is BrewingStand) {
				tile.manage()
				tile.evaluate()
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun onBlockBreak(event: BlockBreakEvent) {
		if (event.block.type == Material.BREWING_STAND) {
			(event.block.state as BrewingStand).unmanage()
		}
	}

	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
	fun onVanillaBrewFinish(event: BrewEvent) {
		val stand = event.block.state
		if (stand is BrewingStand) {
			stand.manage()
			stand.evaluate()
		}
		event.isCancelled = true
	}
}
