package plus.civ.topshelf.config

import net.minecraft.server.v1_8_R3.NBTTagCompound
import org.bukkit.inventory.ItemFlag
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.PotionMeta
import org.bukkit.potion.PotionEffect
import org.bukkit.potion.PotionEffectType
import plus.civ.topshelf.PotionRecipe
import vg.civcraft.mc.civmodcore.expression.NBTExpression
import vg.civcraft.mc.civmodcore.expression.matches
import vg.civcraft.mc.civmodcore.expression.solveToItemStack
import vg.civcraft.mc.civmodcore.extensions.nbt
import vg.civcraft.mc.civmodcore.extensions.setPotionColor
import vg.civcraft.mc.civmodcore.extensions.writeNBTTag

internal object Config {
	internal val POTION_RECIPES: MutableList<PotionRecipe> = mutableListOf()
	internal val FAILURE_RECIPES: MutableList<PotionRecipe> = mutableListOf()

	internal fun getRecipesMatchingIngredient(ingredient: ItemStack): List<PotionRecipe> {
		return POTION_RECIPES.filter { it.ingredient.matches(ingredient) }
			.plus(FAILURE_RECIPES.filter { it.ingredient.matches(ingredient) }) // always make sure failure recipes come after good recipes, so we don't accidentally fail
	}
}

typealias ItemMutator = (ItemStack) -> Unit
fun ItemMutator(): (ItemStack) -> Unit {
	return {}
}

fun ItemReplacer(replacement: NBTExpression): ItemMutator {
	val result = replacement.solveToItemStack()
	return {
		it.nbt = result.nbt?.clone() as NBTTagCompound
		it.type = result.type
		it.durability = result.durability
	}
}

fun ItemReplacer(replacement: ItemStack): ItemMutator {
	val type = replacement.type
	val durability = replacement.durability
	val amount = replacement.amount
	val meta = replacement.itemMeta
	return {
		it.type = type
		it.durability = durability
		it.amount = amount
		it.itemMeta = meta
	}
}

fun ItemMutator.addPotionEffect(effect: PotionEffect): ItemMutator {
	return { input ->
		this(input)
		val meta = input.itemMeta as PotionMeta
		meta.addCustomEffect(effect, true)
		input.itemMeta = meta
	}
}

fun ItemMutator.addMainPotionEffect(effect: PotionEffect): ItemMutator {
	return { input ->
		this(input)
		val meta = input.itemMeta as PotionMeta
		meta.addCustomEffect(effect, true)
		meta.setMainEffect(effect.type)
		input.itemMeta = meta
	}
}

fun ItemMutator.setDisplayName(name: String): ItemMutator {
	return { input ->
		this(input)
		val meta = input.itemMeta as PotionMeta
		meta.displayName = name
		input.itemMeta = meta
	}
}

fun ItemMutator.setPotionColor(color: PotionEffectType): ItemMutator {
	return { input ->
		this(input)
		input.setPotionColor(color)
	}
}

fun ItemMutator.addItemFlags(vararg flags: ItemFlag): ItemMutator {
	return { input ->
		this(input)
		val meta = input.itemMeta
		meta.addItemFlags(*flags)
		input.itemMeta = meta
	}
}

fun ItemMutator.removeItemFlags(vararg flags: ItemFlag): ItemMutator {
	return { input ->
		this(input)
		val meta = input.itemMeta
		meta.removeItemFlags(*flags)
		input.itemMeta = meta
	}
}

fun ItemMutator.setLore(lore: MutableList<String>): ItemMutator {
	return { input ->
		this(input)
		val meta = input.itemMeta
		meta.lore = lore
		input.itemMeta = meta
	}
}

/**
 * Adds an alcohol value to an alcoholic drink, where 100.0 is an instant forced log out.
 */
fun ItemMutator.setAlcoholContent(alcohol: Double): ItemMutator {
	return { input ->
		this(input)
		input.alcoholContent = alcohol
	}
}

var ItemStack.alcoholContent: Double
	get() = nbt?.getDouble("alcoholContent") ?: 0.0
	set(value) {
		writeNBTTag("alcoholContent", value)
	}

