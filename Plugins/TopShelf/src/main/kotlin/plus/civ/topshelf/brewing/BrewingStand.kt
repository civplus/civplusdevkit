package plus.civ.topshelf.brewing

import org.bukkit.Material
import org.bukkit.block.BrewingStand
import org.bukkit.scheduler.BukkitRunnable
import plus.civ.topshelf.config.Config
import vg.civcraft.mc.civmodcore.expression.matches
import vg.civcraft.mc.civmodcore.extensions.blockLocation

private data class ManagedBrewingStand(val brewingStand: BrewingStand, var timer: Int = 0, var isActive: Boolean = false) {
	fun brew() {
		val ingredient = brewingStand.inventory.ingredient
		if (ingredient == null || ingredient.type == Material.AIR) {
			stopBrew()
			return
		}
		val bottles = brewingStand.inventory.contents.slice(0..2)
			.filter {
				it != null && it.type != Material.AIR
			}
		if (bottles.isEmpty()) {
			stopBrew()
			return
		}

		val matchingRecipes = Config.getRecipesMatchingIngredient(ingredient)
			.filter { recipe ->
				bottles.all { bottle ->
					recipe.allowedStartingPotions.matches(bottle)
				}
			}

		if (matchingRecipes.isEmpty()) {
			brewingStand.stopBrew()
		} else {
			val recipe = matchingRecipes.first()
			bottles.forEach { recipe.potionMutator(it) }
			ingredient.amount -= 1
			if (ingredient.amount == 0) {
				brewingStand.inventory.ingredient = null
			}
		}
	}

	fun stopBrew() {
		isActive = false
		timer = 0
	}

	fun startBrew() {
		if (!isActive) {
			isActive = true
		}
	}
}
private val managedBrewingStands = HashMap<Triple<Int, Int, Int>, ManagedBrewingStand>()

internal object BrewingStandTicker: BukkitRunnable() {
	override fun run() {
		val (activeStands, inactiveStands) = managedBrewingStands.values.partition { it.isActive }
		activeStands.forEach { managedStand ->
			managedStand.timer += 1
			if (managedStand.timer >= 400) {
				managedStand.brew()
				managedStand.brewingStand.stopBrew()
				managedStand.brewingStand.evaluate()
			} else {
				managedStand.brewingStand.brewingTime = 400 - managedStand.timer
			}
		}
		inactiveStands.forEach { managedStand ->
			managedStand.brewingStand.brewingTime = 0
		}
	}
}

internal fun BrewingStand.startBrew() {
	managedBrewingStands
		.getOrPut(this.location.blockLocation) { ManagedBrewingStand(this) }
		.startBrew()
}

internal fun BrewingStand.stopBrew() {
	(managedBrewingStands[this.location.blockLocation] ?: return)
		.stopBrew()
}

internal fun BrewingStand.manage() {
	managedBrewingStands.getOrPut(this.location.blockLocation) { ManagedBrewingStand(this) }
}

internal fun BrewingStand.unmanage() {
	managedBrewingStands.remove(this.location.blockLocation)
}

internal fun BrewingStand.evaluate() {
	val ingredient = inventory.ingredient
	if (ingredient == null) {
		stopBrew()
		return
	}

	val bottles = inventory.contents.slice(0..2)
		.filter {
			it != null && it.type != Material.AIR
		}
	if (bottles.isEmpty()) {
		stopBrew()
		return
	}
	val matchingRecipes = Config.getRecipesMatchingIngredient(ingredient)
		.filter { recipe ->
			bottles.all { bottle ->
				recipe.allowedStartingPotions.matches(bottle)
			}
		}

	if (matchingRecipes.isEmpty()) {
		stopBrew()
	} else {
		startBrew()
	}
}

val BrewingStand.isBrewing: Boolean
	get() = managedBrewingStands.getOrElse(this.location.blockLocation, {return false}).isActive
