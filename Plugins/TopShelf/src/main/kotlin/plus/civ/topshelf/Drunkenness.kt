package plus.civ.topshelf

import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.OfflinePlayer
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftItem
import org.bukkit.entity.Item
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import org.bukkit.potion.PotionEffectType
import org.bukkit.scheduler.BukkitRunnable
import vg.civcraft.mc.civmodcore.extensions.writeNBTTag
import java.util.*
import kotlin.random.Random


private val playerDrunkenness = mutableMapOf<UUID, Double>()

object PlayerDrunkennessDecayTask: BukkitRunnable() {
	var decayRate = 100.0 / (10.0 * 60.0) // you decay 100% of your alcohol in 10 minutes
	const val RUN_FREQUENCY_TICKS: Long = 20
	// The task gets ran every RUN_FREQUENCY_TICKS and decreases player drunkenness by decayRate every time it runs.
	// By adjusting these two values you can change how long you stay drunk for.

	override fun run() {
		for ((uuid, drunkenness) in playerDrunkenness) {
			playerDrunkenness[uuid] = drunkenness - decayRate
			if ((playerDrunkenness[uuid] ?: 0.0) <= 0.0) {
				playerDrunkenness.remove(uuid)
			}
		}
	}
}

var OfflinePlayer.drunkenness: Double
	get() = playerDrunkenness[uniqueId] ?: 0.0
	set(value) {
		if (value <= 0.0) {
			playerDrunkenness.remove(uniqueId)
		} else {
			playerDrunkenness[uniqueId] = value
		}
	}

object PukingTask: BukkitRunnable() {
	const val RUN_FREQUENCY_TICKS: Long = 1 // run every tick
	val pukingPlayers = mutableSetOf<UUID>()

	const val PUKE_CHANCE = 1.0 / (1.5 * 60.0 * 20.0) // once every 1.5 minutes
	const val PUKE_CHANCE_BOOST_PER_POINT = PUKE_CHANCE / (1.0 * 20.0) // two seconds per point (i think)
	const val MINIMUM_POINTS_TO_PUKE = 90.0
	const val DRUNK_POINTS_REMOVED_PER_ITEM = 1.0

	override fun run() {
		for ((uuid, drunkenness) in playerDrunkenness) {
			if (drunkenness > MINIMUM_POINTS_TO_PUKE) {
				val boost = (drunkenness - MINIMUM_POINTS_TO_PUKE) * PUKE_CHANCE_BOOST_PER_POINT
				if (Math.random() < PUKE_CHANCE + boost) {
					pukingPlayers.add(uuid)
				}
			}
		}

		val finishedPuking = mutableSetOf<UUID>()

		for (uuid in pukingPlayers) {
			val player = Bukkit.getPlayer(uuid) ?: continue
			player.drunkenness -= DRUNK_POINTS_REMOVED_PER_ITEM
			if (player.drunkenness < MINIMUM_POINTS_TO_PUKE) {
				finishedPuking.add(uuid)
			}
			puke(player)
		}

		pukingPlayers.removeAll(finishedPuking)
	}

	val pukeMaterial = Material.SOUL_SAND
	const val pukeDespawntime = 10 * 20 // despawn after 10 seconds
	var pukeNoStackId = 0L

	fun puke(player: Player) {
		player.addPotionEffect(PotionEffectType.HUNGER.createEffect(3, 1))

		val loc = player.location
		loc.y = loc.y + 1.1
		loc.pitch = loc.pitch - 10 + Random.nextInt(20)
		loc.yaw = loc.yaw - 10 + Random.nextInt(20)

		val direction = loc.direction
		direction.multiply(0.5)
		loc.add(direction)

		val pukeItemStack = ItemStack(pukeMaterial)
		pukeItemStack.writeNBTTag("pukeNoStackId", pukeNoStackId++) // prevent puke items from auto-stacking

		val item: Item = player.world.dropItem(loc, pukeItemStack)
		item.velocity = direction
		item.pickupDelay = 32767 // Item can never be picked up when pickup delay is 32767

		if (item is CraftItem) {
			val handle = item.handle
			val age = handle.javaClass.getDeclaredField("age")
			age.isAccessible = true
			age.setInt(handle, handle.world.spigotConfig.itemDespawnRate - pukeDespawntime)
		}
	}
}
