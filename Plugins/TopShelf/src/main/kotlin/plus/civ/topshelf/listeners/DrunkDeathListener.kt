package plus.civ.topshelf.listeners

import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.PlayerDeathEvent
import plus.civ.topshelf.drunkenness

class DrunkDeathListener: Listener {
	@EventHandler
	fun playerDeath(event: PlayerDeathEvent) {
		// make the player not drunk
		// brewery reduces their drunkenness by 20
		event.entity.drunkenness = 0.0
	}
}
