package plus.civ.soymc

import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PluginInfo

@PluginInfo(
		name = "SoyMC",
		description = "Censors certain words from global chat and PMs for new players",
		author = "Amelorate",
		depends = ["CivModCore", "CivChat2"],
)
class SoyMC: KotlinPlugin() {
	companion object {
		private var instanceStorage: SoyMC? = null
		val instance: SoyMC
			get() = instanceStorage!!
	}

	override fun onEnable() {
		super.onEnable()
		instanceStorage = this
	}
}
