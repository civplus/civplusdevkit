package plus.civ.soymc

object CensorshipConfig {
	val censoredGroups = mutableSetOf("!")
	var censorLocalChat = true
	var censorPMs = true

	val censoredStrings = mutableListOf(
		// CivMC
		Pair("civmc", "soymc"),
		Pair("civ mc", "soy mc"),
		Pair("civmc.net", "civ.plus"),

		// CivUniverse
		Pair("civu", "soyu"), // also covers civuniverse
		Pair("civ u", "soy u"),
		Pair("civuniverse.com", "civ.plus"),

		//CivVie
		Pair("civvie", "soyvie"),
		Pair("civvie.com", "civ.plus"),
	)

	val playtimeToNotBeCensored = 15 * 24 * 60 * 20 // 15 days in ticks
}
