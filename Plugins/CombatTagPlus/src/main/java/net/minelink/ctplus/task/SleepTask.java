package net.minelink.ctplus.task;

import net.minelink.ctplus.CombatTagPlus;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class SleepTask extends BukkitRunnable {
	public static Map<UUID, Location> sleepers = new HashMap<>();

	@Override
	public void run() {
		sleepers.clear();
		Bukkit.getOnlinePlayers().stream().filter(Player::isSleeping).forEach((sleeper) -> sleepers.put(sleeper.getUniqueId(), sleeper.getLocation()));
	}

	public static void run(CombatTagPlus plugin) {
		new SleepTask().runTaskTimer(plugin, 1, 1);
	}
}
