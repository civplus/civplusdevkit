package plus.civ.vorpal

import plus.civ.vorpal.tasks.EnchantFuelDecrement

object Config {
	var startingFuelHours: Int = 2 * 7 * 24 // two weeks
	var fuelConsumeTimeTicks: Long = 60L * 60L * 20L
		set(value) {
			EnchantFuelDecrement.task?.cancel()
			EnchantFuelDecrement.runTaskTimer(Vorpal.instance, fuelConsumeTimeTicks, fuelConsumeTimeTicks)
			field = value
		}

	var maxFuelHours: Int = (6.5 * 7 * 24).toInt()
	// 6.5 weeks, per ~~https://gitlab.com/civplus/CivPlus/-/issues/214~~ https://gitlab.com/civplus/CivPlus/-/issues/286

	var fuelDrainEnabled = false // vorpals needing fuel is disabled until civ+ leaves maintenance mode

	var excursionLengthTicks = 5L * 60L * 20L // five minutes in ticks

	var prisonWorld = "world_the_end"

	/**
	 * When you summon a player, they will be spawned in a random spot inside a circle of this radius around the summoner
	 */
	var summonSpawnRadius = 50
}
