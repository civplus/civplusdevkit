package plus.civ.vorpal.listeners

import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.inventory.ItemStack
import plus.civ.vorpal.database.VorpalEnchant.Companion.vorpal

object DropEnchantOnLogout: Listener {
	@EventHandler
	fun onPlayerLogout(event: PlayerQuitEvent) {
		val swords = mutableListOf<ItemStack>()

		for (item in event.player.inventory) {
			if (item == null) {
				continue
			}

			val sword = item.vorpal ?: continue
			if (sword.playersInside.isNotEmpty()) {
				swords.add(item)
			}
		}

		if (swords.isEmpty()) {
			return
		}

		for (sword in swords) {
			event.player.inventory.remove(sword)
			event.player.world.dropItemNaturally(event.player.location, sword)
		}
	}
}
