package plus.civ.vorpal.recipes

import net.minecraft.server.v1_8_R3.NBTBase
import net.minecraft.server.v1_8_R3.NBTTagCompound
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.inventory.ItemStack
import plus.civ.meansofproduction.CraftingGrid
import plus.civ.meansofproduction.recipe.Recipe
import plus.civ.meansofproduction.recipe.RecipeCraftInfo
import plus.civ.meansofproduction.recipe.RecipeProvider
import plus.civ.meansofproduction.recipe.ShapelessCraftingRecipe
import plus.civ.vorpal.database.VorpalEnchant
import vg.civcraft.mc.civmodcore.expression.*

object VorpalRecipe: RecipeProvider, Recipe {
	val relic = NBTExpression.empty()
		.loreLine("${ChatColor.RESET}${ChatColor.GRAY}Vorpal")
		.loreLine("Relic+")

	val catalyst = NBTExpression.empty()
		.id(Material.EMERALD)
		.amountGTE(5)

	val itemToEnchant = NBTExpression.empty()
		.with("", object : TagMatcher {
			override fun matches(tag: NBTBase?): Boolean {
				if (tag !is NBTTagCompound) {
					return false
				}

				if (tag.isEmpty) {
					return false
				}

				return !(relic.matches(tag) || catalyst.matches(tag))
			}

			override fun solve(): NBTBase {
				val tag = NBTTagCompound()
				tag.setString("id", "minecraft:stone")
				tag.setByte("Count", 1)
				return tag
			}
		})

	val baseRecipe = ShapelessCraftingRecipe(
			mutableListOf(relic, catalyst, itemToEnchant),
			"vorpalEnchant",
			ItemStack(Material.STONE)
		)

	override fun register() {
		addUnlistedRecipe(this)
	}

	override fun preview(): Pair<CraftingGrid, ItemStack> {
		val grid = CraftingGrid.empty()

		val relic = this.relic.solveToItemStack()
		val catalyst = this.catalyst.solveToItemStack()
		val itemToEnchant = this.itemToEnchant.solveToItemStack()

		grid[1, 1] = relic
		grid[1, 2] = catalyst
		grid[1, 3] = itemToEnchant

		val result = ItemStack(Material.STICK)
		val resultMeta = result.itemMeta
		resultMeta.lore = listOf("${ChatColor.RESET}${ChatColor.GRAY}Vorpal")
		result.itemMeta = resultMeta

		return Pair(grid, result)
	}

	override fun execute(grid: CraftingGrid): ItemStack? {
		val itemsToEnchant = grid.grid.filter { this.itemToEnchant.matches(it) }
		if (itemsToEnchant.size != 1) {
			return null
		}
		val itemToEnchant = itemsToEnchant.first().clone()

		if (baseRecipe.execute(grid) == null) {
			return null
		}

		val meta = itemToEnchant.itemMeta
		meta.lore = listOf("${ChatColor.RESET}${ChatColor.GRAY}Vorpal")
		itemToEnchant.itemMeta = meta

		return itemToEnchant
	}

	override fun sideEffect(info: RecipeCraftInfo): ItemStack? {
		super.sideEffect(info)

		val itemsToEnchant = info.gridBefore.grid.filter { this.itemToEnchant.matches(it) }
		if (itemsToEnchant.size != 1) {
			return null
		}
		val itemToEnchant = itemsToEnchant.first().clone()

		val enchant = VorpalEnchant()
		enchant.blessItem(itemToEnchant)
		return itemToEnchant
	}

	override val key = baseRecipe.key
}
