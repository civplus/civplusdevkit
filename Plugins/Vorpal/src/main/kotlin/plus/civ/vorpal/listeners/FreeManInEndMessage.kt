package plus.civ.vorpal.listeners

import org.bukkit.ChatColor
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent
import plus.civ.vorpal.Config
import plus.civ.vorpal.database.VorpalEnchant.Companion.isImprisoned

object FreeManInEndMessage: Listener {
	@EventHandler
	fun onLoginInEnd(event: PlayerJoinEvent) {
		if (event.player.location.world.name != Config.prisonWorld)
			return
		if (event.player.isImprisoned)
			return
		event.player.sendMessage("${ChatColor.GREEN}You are a free man. Jump into the void to go back to your own world.")
	}
}
