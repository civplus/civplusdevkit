package plus.civ.vorpal.listeners

import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageByEntityEvent
import plus.civ.vorpal.database.VorpalEnchant.Companion.isImprisoned

/**
 * Make sure imprisoned players can't attack non-imprisoned players
 */
class NoPrisonRevolts: Listener {
	@EventHandler
	fun playerPvp(event: EntityDamageByEntityEvent) {
		val damager = event.damager
		val victim = event.entity

		if (damager !is Player) return
		if (victim !is Player) return

		if (damager.isImprisoned && !victim.isImprisoned) {
			event.isCancelled = true
		}
	}
}
