package plus.civ.vorpal

import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PermissionDefault
import plus.civ.kotlinplugin.PluginInfo
import plus.civ.kotlinplugin.PluginPermission
import plus.civ.vorpal.tasks.EnchantFuelDecrement

@PluginInfo(
	name = "Vorpal",
	description = "Allows imprisoning players within any cursed item by killing them while holding the item.",
	author = "Amelorate",
	depends = ["CivModCore", "RandomSpawn", "CombatTagPlus"],
	softDepends = ["MeansOfProduction"],
	permissions = [
		PluginPermission(
			name = "vorpal.locateany",
			description = "Locate someone else's vorpal",
			default = PermissionDefault.OP,
		),
		PluginPermission(
			name = "vorpal.freeany",
			description = "Free anybody from prison",
			default = PermissionDefault.OP
		),
		PluginPermission(
			name = "vorpal.bless",
			description = "Make any item vorpal",
			default = PermissionDefault.OP
		),
	],
)
class Vorpal: KotlinPlugin() {
	companion object {
		private var instanceStorage: Vorpal? = null
		val instance: Vorpal
			get() = instanceStorage!!
	}

	override fun onEnable() {
		super.onEnable()
		instanceStorage = this

		EnchantFuelDecrement.runTaskTimer(this, Config.fuelConsumeTimeTicks, Config.fuelConsumeTimeTicks)
	}
}
