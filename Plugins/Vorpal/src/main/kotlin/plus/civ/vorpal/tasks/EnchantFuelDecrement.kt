package plus.civ.vorpal.tasks

import org.bukkit.scheduler.BukkitRunnable
import org.bukkit.scheduler.BukkitTask
import plus.civ.vorpal.Config
import plus.civ.vorpal.database.VorpalEnchant

object EnchantFuelDecrement: BukkitRunnable() {
	var task: BukkitTask? = null

	override fun run() {
		if (!Config.fuelDrainEnabled) return

		for (enchant in VorpalEnchant.vorpals!!) {
			if (enchant.playersInside.isEmpty()) {
				continue
			}

			if (enchant.fuelHours - 1 < 0) {
				enchant.freeAllPlayers("ran out of fuel")
			}

			enchant.fuelHours -= 1

			enchant.reevaluateItem()
			enchant.saveLater()
		}
	}
}
