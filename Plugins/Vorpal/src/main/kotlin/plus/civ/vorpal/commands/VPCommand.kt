package plus.civ.vorpal.commands

import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.Location
import org.bukkit.OfflinePlayer
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabExecutor
import org.bukkit.entity.Player
import plus.civ.kotlinplugin.CommandInfo
import plus.civ.vorpal.database.VorpalEnchant
import plus.civ.vorpal.database.VorpalEnchant.Companion.isImprisoned
import plus.civ.vorpal.database.VorpalEnchant.Companion.vorpal
import plus.civ.kotlinplugin.SpaceCommand
import plus.civ.vorpal.Config
import plus.civ.vorpal.database.VorpalEnchant.Companion.isSummoned
import java.util.*

@CommandInfo(
	name = "vp",
	description = "Vorpal's Base Command",
	usage = "/vp help"
)
class VPCommand: SpaceCommand(
	CommandInfo(
		name = "locate",
		description = "Find where your vorpal is located",
		usage = "/vp locate",
		executor = VPLocate,
	),
	CommandInfo(
		name = "locateany",
		description = "Find where someone else's vorpal is located",
		usage = "/vp locateany [player]",
		permission = "vorpal.locateany",
		executor = VPLocateAny,
	),
	CommandInfo(
		name = "free",
		description = "Free someone inside of a vorpal. You can only free people inside of the vorpal you are holding",
		usage = "/vp free [player]",
		executor = VPFree,
	),
	CommandInfo(
		name = "bless",
		description = "Make any item Vorpal",
		usage = "/vp bless",
		permission = "vorpal.bless",
		executor = VPBless,
	),
	CommandInfo(
		name = "info",
		description = "Get info about your Vorpal item.",
		usage = "/vp info",
		executor = VPInfo,
	),
	CommandInfo(
		name = "summon",
		description = "Summon a imprisoned player to your location.",
		usage = "/vp summon [player]",
		executor = VPSummon,
	),
	CommandInfo(
		name = "unsummon",
		description = "Returns a summoned player to the end.",
		usage = "/vp unsummon [player]",
		executor = VPUnsummon,
		aliases = mutableListOf("return")
	),
) {
	object VPLocate: TabExecutor {
		override fun onCommand(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): Boolean {
			if (sender !is Player) {
				return false
			}

			if (sender.isImprisoned) {
				sender.vorpal!!.reevaluateItem()
			}

			vpLocate(sender, sender)
			return true
		}

		override fun onTabComplete(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): MutableList<String> {
			return mutableListOf()
		}

		fun vpLocate(sender: CommandSender, prisoned: OfflinePlayer) {
			if (!prisoned.isImprisoned) {
				sender.sendMessage("${ChatColor.RED}You are not imprisoned.")
				return
			}

			val vorpal = prisoned.vorpal!!

			val holder = vorpal.holdingPlayer
			if (holder != null) {
				sender.sendMessage("${ChatColor.GREEN} You are imprisoned in a Vorpal held by " +
						"${ChatColor.AQUA}${holder.displayName}${ChatColor.GREEN} at ${ChatColor.AQUA}${stringLocation(holder.location)}")
				return
			}

			val location = vorpal.location?.asLocation()
			if (location == null) {
				sender.sendMessage("${ChatColor.GRAY}Your Vorpal can't be found at this time.")
				return
			}

			var message = "${ChatColor.GREEN} You are imprisoned in a Vorpal "
			message += "in a ${location.block.type} "

			message += "at ${ChatColor.AQUA}${stringLocation(location)}"

			sender.sendMessage(message)
			return
		}

		internal fun stringLocation(location: Location): String = "[${location.world!!.name} ${location.blockX} ${location.blockY} ${location.blockZ}]"
	}

	object VPLocateAny: TabExecutor {
		override fun onCommand(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): Boolean {
			if (args.size != 1) {
				return false
			}

			val playerNameOrUUID = args[0]
			val uuid = try {
				UUID.fromString(playerNameOrUUID)
			} catch (e: java.lang.IllegalArgumentException) {
				null
			}

			val player = if (uuid != null) {
				Bukkit.getOfflinePlayer(uuid)
			} else {
				Bukkit.getOfflinePlayer(playerNameOrUUID)
			}

			VPLocate.vpLocate(sender, player)
			return true
		}

		override fun onTabComplete(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): MutableList<String> {
			if (args.size > 1) {
				return mutableListOf()
			}

			val playerName = args.getOrNull(0) ?: return mutableListOf()
			return VorpalEnchant.prisonedPlayers.keys.map { Bukkit.getOfflinePlayer(it).name }.filter { it.lowercase().startsWith(playerName.lowercase()) }.toMutableList()
		}
	}

	object VPFree: TabExecutor {
		override fun onCommand(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): Boolean {
			if (sender !is Player) {
				return false
			}

			if (args.size != 1) {
				return false
			}

			val playerNameOrUUID = args[0]
			val uuid = try {
				UUID.fromString(playerNameOrUUID)
			} catch (e: java.lang.IllegalArgumentException) {
				null
			}

			val player = if (uuid != null) {
				Bukkit.getOfflinePlayer(uuid)
			} else {
				Bukkit.getOfflinePlayer(playerNameOrUUID)
			}

			val noVorpalMessage: String

			val vorpal = if (sender.hasPermission("vorpal.freeany")) {
				noVorpalMessage = "${ChatColor.RED}That player is already free."
				player.vorpal
			} else {
				noVorpalMessage = "${ChatColor.RED}You must be holding a vorpal with $playerNameOrUUID inside in order to free them."
				sender.itemInHand.vorpal
			}

			if (vorpal == null) {
				sender.sendMessage(noVorpalMessage)
				return true
			}

			val hasRemoved = vorpal.freePlayer(player)

			if (!hasRemoved) {
				sender.sendMessage("${ChatColor.RED}That player isn't inside that vorpal, or is already free.")
			} else {
				sender.sendMessage("${ChatColor.AQUA}Freed $playerNameOrUUID")

				if (player.isOnline) {
				}
			}

			return true
		}

		override fun onTabComplete(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): MutableList<String> {
			val prisonList = if (sender.hasPermission("vorpal.freeany")) {
				VorpalEnchant.prisonedPlayers.keys.map { Bukkit.getOfflinePlayer(it).name }
			} else {
				if (sender !is Player) {
					return mutableListOf()
				}

				sender.itemInHand.vorpal?.playersInside?.map { Bukkit.getOfflinePlayer(it).name } ?: return mutableListOf()
			}

			return prisonList.filter { it.lowercase().startsWith(args.getOrElse(0) { "" }.lowercase()) }.toMutableList()
		}
	}

	object VPBless: TabExecutor {
		override fun onCommand(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): Boolean {
			if (sender !is Player) {
				return false
			}

			val item = sender.itemInHand
			val enchant = VorpalEnchant()
			enchant.blessItem(item)
			sender.itemInHand = item
			sender.sendMessage("${ChatColor.AQUA}That item is now Vorpal.")
			return true
		}

		override fun onTabComplete(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): MutableList<String> {
			return mutableListOf()
		}
	}

	object VPInfo: TabExecutor {
		override fun onCommand(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): Boolean {
			if (sender !is Player) {
				return false
			}

			val vorpal = sender.itemInHand.vorpal
			if (vorpal == null) {
				sender.sendMessage("${ChatColor.RED}That isn't a Vorpal item!")
				return true
			}

			vorpal.reevaluateItem()
			val lore = vorpal.generateLore()
			for (line in lore) {
				sender.sendMessage(line)
			}

			sender.sendMessage("")
			sender.sendMessage("${ChatColor.GRAY}ID: ${vorpal._id}")
			if (vorpal.legacyVorpalId != null) {
				sender.sendMessage("${ChatColor.GRAY}Legacy ID: ${vorpal.legacyVorpalId}")
			}
			sender.sendMessage("${ChatColor.GRAY}Fuel Hours: ${vorpal.fuelHours}")

			return true
		}

		override fun onTabComplete(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): MutableList<String> {
			return mutableListOf()
		}
	}

	object VPSummon: TabExecutor {
		override fun onCommand(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): Boolean {
			if (sender !is Player) {
				return false
			}

			if (args.size != 1) {
				return false
			}

			val playerNameOrUUID = args[0]
			val uuid = try {
				UUID.fromString(playerNameOrUUID)
			} catch (e: java.lang.IllegalArgumentException) {
				null
			}

			val player = if (uuid != null) {
				Bukkit.getOfflinePlayer(uuid)
			} else {
				Bukkit.getOfflinePlayer(playerNameOrUUID)
			}

			val vorpal = sender.itemInHand.vorpal
			if (vorpal == null) {
				sender.sendMessage("${ChatColor.RED}That isn't a Vorpal item!")
				return true
			}

			if (!vorpal.playersInside.contains(player.uniqueId)) {
				sender.sendMessage("${ChatColor.RED}That player isn't imprisoned within that Vorpal item!")
				return true
			}

			if (!player.isOnline || player !is Player) {
				sender.sendMessage("${ChatColor.RED}That player needs to be online in order to summon them!")
				return true
			}

			if (player.isSummoned) {
				sender.sendMessage("${ChatColor.RED}That player is already summoned!")
				return true
			}

			vorpal.summonPlayer(player, sender)
			sender.sendMessage("${ChatColor.GREEN}Summoned ${ChatColor.AQUA}${player.name}${ChatColor.GREEN}! " +
				"They are somewhere within ${Config.summonSpawnRadius} blocks.")
			return true
		}

		override fun onTabComplete(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): MutableList<String> {
			if (sender !is Player) {
				return mutableListOf()
			}

			val prisonList = sender.itemInHand.vorpal?.playersInside?.map { Bukkit.getOfflinePlayer(it).name }
				?: return mutableListOf()

			return prisonList.filter { it.lowercase().startsWith(args.getOrElse(0) { "" }.lowercase()) }.toMutableList()
		}
	}

	object VPUnsummon: TabExecutor {
		override fun onCommand(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): Boolean {
			if (sender !is Player) {
				return false
			}

			if (args.size != 1) {
				return false
			}

			val playerNameOrUUID = args[0]
			val uuid = try {
				UUID.fromString(playerNameOrUUID)
			} catch (e: java.lang.IllegalArgumentException) {
				null
			}

			val player = if (uuid != null) {
				Bukkit.getOfflinePlayer(uuid)
			} else {
				Bukkit.getOfflinePlayer(playerNameOrUUID)
			}

			val vorpal = sender.itemInHand.vorpal
			if (vorpal == null) {
				sender.sendMessage("${ChatColor.RED}That isn't a Vorpal item!")
				return true
			}

			if (!vorpal.playersInside.contains(player.uniqueId)) {
				sender.sendMessage("${ChatColor.RED}That player isn't imprisoned within that Vorpal item!")
				return true
			}

			if (!player.isSummoned) {
				sender.sendMessage("${ChatColor.RED}That player isn't summoned!")
				return true
			}

			vorpal.unsummonPlayer(player)
			sender.sendMessage("${ChatColor.GREEN}Sent ${ChatColor.AQUA}${player.name}${ChatColor.GREEN} back to the end.")
			return true
		}

		override fun onTabComplete(sender: CommandSender, command: Command, commandName: String, args: Array<out String>): MutableList<String> {
			if (sender !is Player) {
				return mutableListOf()
			}

			val prisonList = sender.itemInHand.vorpal?.playersInside?.map { Bukkit.getOfflinePlayer(it).name }
				?: return mutableListOf()

			return prisonList.filter { it.lowercase().startsWith(args.getOrElse(0) { "" }.lowercase()) }.toMutableList()
		}
	}
}
