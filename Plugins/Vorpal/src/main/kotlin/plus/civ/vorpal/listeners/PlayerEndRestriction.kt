package plus.civ.vorpal.listeners

import me.josvth.randomspawn.RandomSpawn
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.*
import plus.civ.vorpal.Config
import plus.civ.vorpal.Vorpal
import plus.civ.vorpal.database.VorpalEnchant.Companion.isImprisoned
import plus.civ.vorpal.database.VorpalEnchant.Companion.isNotImpisonedOrIsSummoned
import plus.civ.vorpal.database.VorpalEnchant.Companion.isSummoned
import plus.civ.vorpal.database.VorpalEnchant.Companion.vorpal

/**
 * Teleports the player into the end and keeps them there if they are imprisoned.
 */
object PlayerEndRestriction: Listener {
	val randomSpawn by lazy { Vorpal.instance.server.pluginManager.getPlugin("RandomSpawn") as RandomSpawn }
	val prisonWorld by lazy { Bukkit.getWorld(Config.prisonWorld) }

	fun sendToEnd(player: Player) {
		if (player.location.world == prisonWorld) {
			return
		}

		val spawnPoint = randomSpawn.chooseSpawn(prisonWorld)
		player.teleport(spawnPoint)
	}

	@EventHandler
	fun onPlayerSpawn(event: PlayerRespawnEvent) {
		if (!event.player.isImprisoned) {
			return
		}

		if (event.player.isSummoned) {
			event.player.vorpal?.unsummonPlayer(event.player)
		}

		event.respawnLocation = randomSpawn.chooseSpawn(prisonWorld)
	}

	@EventHandler
	fun onPlayerJoin(event: PlayerJoinEvent) {
		if (event.player.isNotImpisonedOrIsSummoned) {
			return
		}

		sendToEnd(event.player)
	}

	// Some random stuff that makes it hard to exploit anything if you end up in the overworld:

	@EventHandler
	fun onPlayerMove(event: PlayerMoveEvent) {
		if (event.to!!.block.location == event.from.block.location) {
			return
		}

		if (event.player.location.world == prisonWorld) {
			return
		}

		if (event.player.isNotImpisonedOrIsSummoned) {
			return
		}

		sendToEnd(event.player)
	}

	@EventHandler
	fun playerInteract(event: PlayerInteractEvent) {
		if (event.player.location.world == prisonWorld) {
			return
		}

		if (event.player.isNotImpisonedOrIsSummoned) {
			return
		}


		event.isCancelled = true
		sendToEnd(event.player)
	}

	@EventHandler
	fun playerPortal(event: PlayerPortalEvent) {
		if (event.cause != PlayerTeleportEvent.TeleportCause.END_PORTAL) {
			return
		}

		if (event.player.isNotImpisonedOrIsSummoned) {
			return
		}

		event.isCancelled = true
		val spawnPoint = randomSpawn.chooseSpawn(prisonWorld)
		event.player.teleport(spawnPoint)
	}
}
