package plus.civ.vorpal.listeners

import net.minelink.ctplus.CombatTagPlus
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.PlayerDeathEvent
import plus.civ.vorpal.Vorpal
import plus.civ.vorpal.database.VorpalEnchant.Companion.isImprisoned
import plus.civ.vorpal.database.VorpalEnchant.Companion.isSummoned
import plus.civ.vorpal.database.VorpalEnchant.Companion.vorpal

/**
 * Imprisones players if they are killed with a PrisonSword.
 */
object PrisonOnKill: Listener {
    @EventHandler
    fun onPlayerDeath(event: PlayerDeathEvent) {
        val victim = event.entity
        if (victim.isImprisoned && !victim.isSummoned) {
            return // player was killed in the end
        }

        val combatTag = Vorpal.instance.server.pluginManager.getPlugin("CombatTagPlus") as CombatTagPlus

		val victimUUID = if (combatTag.npcPlayerHelper.isNpc(victim)) combatTag.npcPlayerHelper.getIdentity(victim).id else victim.uniqueId

        if (!combatTag.tagManager.isTagged(victimUUID)) {
            return
        }

		val tag = combatTag.tagManager.getTag(victimUUID)

        val attacker = Vorpal.instance.server.getPlayer(tag.attackerId) ?: return

		val item = attacker.itemInHand ?: return

		val vorpal = item.vorpal ?: return
		vorpal.imprison(victimUUID)
		vorpal.reevaluateItem()
    }
}
