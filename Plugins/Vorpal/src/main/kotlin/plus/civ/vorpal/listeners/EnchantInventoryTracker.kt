package plus.civ.vorpal.listeners

import org.bukkit.Bukkit
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.inventory.InventoryDragEvent
import org.bukkit.event.inventory.InventoryMoveItemEvent
import org.bukkit.event.player.PlayerPickupItemEvent
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.PlayerInventory
import plus.civ.vorpal.commands.VPCommand
import plus.civ.vorpal.Vorpal
import plus.civ.vorpal.database.VorpalEnchant.Companion.vorpal
import plus.civ.vorpal.database.VorpalEnchant.Companion.vorpalsInside
import vg.civcraft.mc.civmodcore.database.serializable
import vg.civcraft.mc.civmodcore.extensions.isBlockInventory
import vg.civcraft.mc.civmodcore.extensions.location

object EnchantInventoryTracker: Listener {
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    fun inventoryClick(event: InventoryClickEvent) {
        evalInventory(event.view.bottomInventory)
        evalInventory(event.view.topInventory)
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    fun inventoryDrag(event: InventoryDragEvent) {
        evalInventory(event.view.bottomInventory)
        evalInventory(event.view.topInventory)
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    fun inventoryMoveItem(event: InventoryMoveItemEvent) {
		val vorpal = event.item.vorpal ?: return
        val newLocation = event.destination.location ?: return

        vorpal.location = newLocation.serializable
		vorpal.saveLater()
    }

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	fun playerPickUp(event: PlayerPickupItemEvent) {
		val sword = event.item.itemStack.vorpal ?: return

		Vorpal.instance.server.scheduler.runTask(Vorpal.instance, sword::reevaluateItem)
	}

	fun evalInventory(inventory: Inventory) {
		if (!inventory.isBlockInventory) {
			return
		}

		Vorpal.instance.server.scheduler.runTaskLater(Vorpal.instance, {
			for (vorpal in inventory.vorpalsInside) {
				Vorpal.instance.server.scheduler.runTask(Vorpal.instance, vorpal::reevaluateItem)

				if (inventory !is PlayerInventory && inventory.location != null) {
					vorpal.location = inventory.location!!.serializable
					vorpal.saveLater()
				}

				for (player in vorpal.playersInside.map { Bukkit.getOfflinePlayer(it) }) {
					val onlinePlayer = player.player ?: continue
					VPCommand.VPLocate.vpLocate(onlinePlayer, onlinePlayer)
				}
			}
		}, 1)
	}
}
