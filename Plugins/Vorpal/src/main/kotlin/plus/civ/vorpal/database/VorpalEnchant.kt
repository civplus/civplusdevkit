package plus.civ.vorpal.database

import com.fasterxml.jackson.annotation.JsonIgnore
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.OfflinePlayer
import org.bukkit.entity.Player
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.InventoryHolder
import org.bukkit.inventory.ItemStack
import org.bukkit.scheduler.BukkitRunnable
import org.bukkit.scheduler.BukkitTask
import org.litote.kmongo.*
import plus.civ.vorpal.Config
import plus.civ.vorpal.Vorpal
import plus.civ.vorpal.listeners.PlayerEndRestriction
import vg.civcraft.mc.civmodcore.database.SerializableLocation
import vg.civcraft.mc.civmodcore.extensions.nbt
import vg.civcraft.mc.civmodcore.extensions.toRomanString
import vg.civcraft.mc.civmodcore.extensions.tryToTeleportVertically
import vg.civcraft.mc.civmodcore.extensions.writeNBTTag
import java.time.Instant
import java.util.*
import kotlin.math.roundToInt
import kotlin.random.Random

class VorpalEnchant(
		val _id: Id<VorpalEnchant> = newId(),
		var location: SerializableLocation? = null,
		fuelHours: Int = Config.startingFuelHours,
		val legacyVorpalId: Int? = null,
		val playersInside: MutableSet<UUID> = mutableSetOf(),
		val summonedPlayers: MutableSet<UUID> = mutableSetOf(),
		var excursionBegin: Instant? = null,
) {
	companion object {
		val collection = Database.database.getCollection<VorpalEnchant>("VorpalEnchant")
		var vorpals: MutableSet<VorpalEnchant>? = collection.find().toMutableSet()

		val ItemStack.vorpalId: Id<VorpalEnchant>?
				get() = nbt?.getString("vorpal")?.toId()

		val ItemStack.vorpal: VorpalEnchant?
			get() {
				val id = vorpalId?.toString() ?: return null
				return vorpals!!.firstOrNull { it._id.toString() == id }
			}

		var prisonedPlayers = prisonedPlayers()
		private fun prisonedPlayers(): Map<UUID, VorpalEnchant> = mapOf(*vorpals!!.map { vorpal -> vorpal.playersInside.map { playerInside -> Pair(playerInside, vorpal) } }.flatten().toTypedArray())

		fun reindexPrisonedPlayers() {
			prisonedPlayers = prisonedPlayers()
		}

		var summonedPlayers: Map<UUID, VorpalEnchant> = summonedPlayers()
		private fun summonedPlayers(): Map<UUID, VorpalEnchant> {
			for (vorpal in vorpals!!) {
				val notPrisonedSummonedPlayers = mutableSetOf<UUID>()
				for (summonedPlayer in vorpal.summonedPlayers) {
					if (summonedPlayer !in vorpal.playersInside) {
						notPrisonedSummonedPlayers.add(summonedPlayer)
					}
				}

				val changed = vorpal.summonedPlayers.removeAll(notPrisonedSummonedPlayers)
				if (changed) {
					vorpal.saveLater()
				}
			}

			return mapOf(*vorpals!!.map { vorpal -> vorpal.summonedPlayers.map { playerInside -> Pair(playerInside, vorpal) } }.flatten().toTypedArray())
		}

		fun reindexSummonedPlayers() {
			summonedPlayers = summonedPlayers()
		}

		val OfflinePlayer.isImprisoned: Boolean
			get() = prisonedPlayers.contains(uniqueId)

		val OfflinePlayer.isSummoned: Boolean
			get() = summonedPlayers.containsKey(uniqueId)

		/**
		 * If the player should be allowed in the overworld
		 */
		val OfflinePlayer.isNotImpisonedOrIsSummoned: Boolean
			get() = !isImprisoned || isSummoned

		val OfflinePlayer.vorpal: VorpalEnchant?
			get() = prisonedPlayers[uniqueId]

		val Inventory.vorpalsInside: List<VorpalEnchant>
			get() = iterator().asSequence().filterNotNull().mapNotNull { it.vorpal }.toList()
	}

	init {
		if (vorpals != null) {
			vorpals!!.add(this)
			saveLater()
		}
	}

	var fuelHours = fuelHours
	set(value) {
		field = if (value > Config.maxFuelHours) {
			Config.maxFuelHours
		} else {
			value
		}
	}

	private var saveTask: BukkitTask? = null

	fun saveLater() {
		saveTask?.cancel()
		saveTask = Bukkit.getScheduler().runTaskAsynchronously(Vorpal.instance) {
			saveBlocking()
		}
	}

	fun saveBlocking() {
		collection.updateOneById(_id, this, upsert())
	}

	fun generateLore(): List<String> {
		val result = ArrayList<String>()

		for (player in playersInside.map { Bukkit.getOfflinePlayer(it) }) {
			val summonedTag = if (player.isSummoned) " (s)" else ""
			player.name?.let { result.add("${ChatColor.AQUA}$it$summonedTag") } // add name if not null
			// no idea why name might be null
		}

		if (playersInside.isNotEmpty())
			result.add("")

		result.add("${ChatColor.GRAY}Vorpal " + playersInside.size.toRomanString())
		result.add("${ChatColor.GRAY}Fuel ${(fuelPercentage * 100).roundToInt()}%")

		return result
	}

	@get:JsonIgnore
	val fuelPercentage: Float
		get() = fuelHours.toFloat() / Config.maxFuelHours.toFloat()

	fun freePlayer(player: OfflinePlayer, silent: Boolean = false): Boolean {
		val result = playersInside.remove(player.uniqueId)
		if (player.isSummoned) {
			summonedPlayers.remove(player.uniqueId)
		}

		saveLater()
		reindexPrisonedPlayers()
		reindexSummonedPlayers()
		reevaluateItem()

		if (player.isOnline && !silent) {
			player.player.sendMessage("${ChatColor.AQUA}You've been freed.")
		}

		return result
	}

	fun freeAllPlayers(why: String) {
		playersInside.clear()
		saveLater()
		reindexPrisonedPlayers()
		reindexSummonedPlayers()
	}

	fun summonPlayer(who: Player, to: Player) {
		if (who.uniqueId !in playersInside) {
			return
		}

		summonedPlayers.add(who.uniqueId)
		reindexSummonedPlayers()
		reevaluateItem()
		who.sendMessage("${ChatColor.GREEN}You've been summoned by ${ChatColor.AQUA}${to.name}${ChatColor.GREEN}!")

		val offsetX = Random.nextDouble(-Config.summonSpawnRadius.toDouble(), Config.summonSpawnRadius.toDouble())
		val offsetZ = Random.nextDouble(-Config.summonSpawnRadius.toDouble(), Config.summonSpawnRadius.toDouble())
		val spawnLocation = to.location.add(offsetX, 0.0, offsetZ)
		spawnLocation.y = 0.0
		who.tryToTeleportVertically(spawnLocation)
	}

	fun unsummonPlayer(who: OfflinePlayer) {
		summonedPlayers.remove(who.uniqueId)
		reindexSummonedPlayers()
		reevaluateItem()
		saveLater()

		if (who.isOnline) {
			who.player.teleport(PlayerEndRestriction.randomSpawn.chooseSpawn(PlayerEndRestriction.prisonWorld))
			who.player.sendMessage("${ChatColor.GREEN}You have been sent back to the end.")
		}
	}

	fun beginExcursion(why: String) {
		Vorpal.instance.logger.info("Beginning an excursion on sword $_id because: $why")
		Vorpal.instance.logger.info("playersInside: ${playersInside.map { Bukkit.getOfflinePlayer(it)?.name }}")

		for (uuid in playersInside) {
			val player = Bukkit.getPlayer(uuid)
			player?.sendMessage("${ChatColor.GRAY}You will be freed in ${Config.excursionLengthTicks / 20 / 60} minutes if your vorpal item is not found.")
		}

		excursionBegin = Instant.now()
		object : BukkitRunnable() {
			override fun run() {
				if (getItemStack(false) == null) {
					freeAllPlayers("excursion ran out of time ($why)")
				}
			}
		}.runTaskLater(Vorpal.instance, Config.excursionLengthTicks)
	}

	fun getItemStack(freeIfNotFound: Boolean = true): ItemStack? {
		// check all online players
		for (player in Vorpal.instance.server.onlinePlayers) {
			for (item in player.inventory.iterator()) {
				if (item == null)
					continue
				if (item.vorpalId == _id) {
					return item
				}
			}

			// try the cursor if they have their inventory open
			val cursorItem = player.itemOnCursor
			if (cursorItem?.vorpalId == _id) {
				return cursorItem
			}
		}

		// check last known container
		val location = location?.asLocation()

		if (location?.block?.state is InventoryHolder) {
			val container = location.block.state as InventoryHolder
			for (item in container.inventory) {
				if (item == null)
					continue
				if (item.vorpalId == _id) {
					return item
				}
			}
		}

		// we've failed, free all the players
		if (freeIfNotFound) {
			beginExcursion("Can't find itemstack in players or container at $location")
		}
		return null
	}

	fun swapItem(item: ItemStack, freeIfNotFound: Boolean = true): ItemStack? {
		// check last known container
		val location = location?.asLocation()

		if (location?.block?.state is InventoryHolder) {
			val container = location.block.state as InventoryHolder
			for ((index, containerItem) in container.inventory.iterator().withIndex()) {
				if (containerItem == null)
					continue
				if (containerItem.vorpalId == _id) {
					container.inventory.setItem(index, item)
					return containerItem
				}
			}
		}

		// try all online players
		for (player in Vorpal.instance.server.onlinePlayers) {
			for ((index, containerItem) in player.inventory.iterator().withIndex()) {
				if (containerItem == null)
					continue
				if (containerItem.vorpalId == _id) {
					player.inventory.setItem(index, item)
					return containerItem
				}
			}

			// try the cursor if the player has their inventory open
			val cursorItem = player.itemOnCursor
			if (cursorItem?.vorpalId == _id) {
				player.itemOnCursor = item
				return cursorItem
			}
		}

		// failed to find the item
		if (freeIfNotFound) {
			beginExcursion("Can't find item for swapping in players or in container at $location")
		}
		return null
	}

	fun reevaluateItem(): ItemStack? {
		val lore = generateLore()
		val item = getItemStack() ?: return null

		val meta = item.itemMeta!!
		meta.lore = lore
		item.itemMeta = meta

		return swapItem(item)
	}

	/**
	 * Mutates the item to be enchanted with vorpal.
	 *
	 * Any lore is deleted.
	 */
	fun blessItem(item: ItemStack) {
		item.writeNBTTag("vorpal", _id.toString())

		val lore = generateLore()

		val meta = item.itemMeta!!
		meta.lore = lore
		item.itemMeta = meta
	}

	@get:JsonIgnore
	val holdingPlayer: Player?
		get() {
			for (player in Bukkit.getOnlinePlayers()) {
				for (containerItem in player.inventory) {
					if (containerItem == null) {
						continue
					}

					if (containerItem.vorpalId == _id) {
						return player
					}
				}
			}

			return null
		}

	fun imprison(uuid: UUID) {
		val player = Bukkit.getOfflinePlayer(uuid)
		player.vorpal?.freePlayer(player, silent = true) // this handles if the player is pearled and has been summoned, then killed
		playersInside.add(uuid)
		reindexPrisonedPlayers()
		Bukkit.getScheduler().runTaskLater(Vorpal.instance, this::reevaluateItem, 1L)
		saveLater()
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as VorpalEnchant

		if (_id != other._id) return false
		if (location != other.location) return false
		if (fuelHours != other.fuelHours) return false
		if (legacyVorpalId != other.legacyVorpalId) return false
		if (playersInside != other.playersInside) return false
		if (summonedPlayers != other.summonedPlayers) return false
		if (excursionBegin != other.excursionBegin) return false

		return true
	}

	override fun hashCode(): Int {
		var result = _id.hashCode()
		result = 31 * result + (location?.hashCode() ?: 0)
		result = 31 * result + (legacyVorpalId?.hashCode() ?: 0)
		result = 31 * result + playersInside.hashCode()
		result = 31 * result + summonedPlayers.hashCode()
		result = 31 * result + (excursionBegin?.hashCode() ?: 0)
		result = 31 * result + fuelHours.hashCode()
		return result
	}

	override fun toString(): String {
		return "VorpalEnchant(_id=$_id, location=$location, legacyVorpalId=$legacyVorpalId, playersInside=$playersInside, summonedPlayers=$summonedPlayers, excursionBegin=$excursionBegin, fuelHours=$fuelHours, saveTask=$saveTask)"
	}
}
