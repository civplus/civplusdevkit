package plus.civ.vorpal.database

import com.mongodb.MongoClientSettings
import org.bson.UuidRepresentation
import org.litote.kmongo.KMongo
import org.litote.kmongo.util.KMongoJacksonFeature
import plus.civ.justatest.JustATest

object Database {
	init {
		KMongoJacksonFeature.setUUIDRepresentation(UuidRepresentation.STANDARD)
	}
	val settings = MongoClientSettings.builder()
			.uuidRepresentation(UuidRepresentation.STANDARD)
			.build()
	val client = KMongo.createClient(settings)

	private val databaseName = "Vorpal-${JustATest.shardName}"

	val database = client.getDatabase(databaseName)
}
