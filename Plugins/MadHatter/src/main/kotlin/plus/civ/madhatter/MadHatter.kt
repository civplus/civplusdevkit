package plus.civ.madhatter

import plus.civ.kotlinplugin.KotlinPlugin
import plus.civ.kotlinplugin.PluginInfo

@PluginInfo(
		name = "MadHatter",
		description = "Allows you to put items and blocks on your head",
		author = "Amelorate",
		depends = ["CivModCore"],
)
class MadHatter: KotlinPlugin()
