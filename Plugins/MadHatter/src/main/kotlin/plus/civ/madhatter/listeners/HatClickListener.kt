package plus.civ.madhatter.listeners

import org.bukkit.Bukkit
import org.bukkit.Material.*
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.inventory.ClickType
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.PlayerInventory

class HatClickListener: Listener {
	val helmetItems = setOf(LEATHER_HELMET, CHAINMAIL_HELMET, IRON_HELMET, DIAMOND_HELMET, GOLD_HELMET, PUMPKIN, SKULL, SKULL_ITEM)
	val blacklist = setOf(
			LEATHER_CHESTPLATE, LEATHER_LEGGINGS, LEATHER_BOOTS,
			CHAINMAIL_CHESTPLATE, CHAINMAIL_LEGGINGS, CHAINMAIL_BOOTS,
			GOLD_CHESTPLATE, GOLD_LEGGINGS, GOLD_BOOTS,
			IRON_CHESTPLATE, IRON_LEGGINGS, IRON_BOOTS,
			DIAMOND_CHESTPLATE, DIAMOND_LEGGINGS, DIAMOND_BOOTS
	)
	val helmetSlot = 5

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	fun onHatClick(event: InventoryClickEvent) {
		if (event.clickedInventory !is PlayerInventory) {
			return
		}

		if (event.rawSlot != helmetSlot) {
			return
		}

		if (event.click != ClickType.LEFT && event.click != ClickType.RIGHT) {
			return
		}

		// this check is important enough to repeat twice
		if (event.cursor == null || event.cursor!!.type == AIR) {
			return
		}

		Bukkit.getScheduler().runTask(Bukkit.getPluginManager().getPlugin("MadHatter"), fun() {
			if (event.currentItem?.type != AIR) {
				return
			}

			if (event.cursor.type in helmetItems) {
				return
			}

			if (event.cursor.type in blacklist) {
				return
			}

			if (event.cursor.amount == 0) {
				return
			}

			if (event.cursor == null || event.cursor!!.type == AIR) {
				return
			}

			if (event.click == ClickType.RIGHT && event.cursor.amount > 1) {
				val cursorClone = event.cursor.clone()
				cursorClone.amount = cursorClone.amount - 1
				val slotClone = event.cursor.clone()
				slotClone.amount = 1

				event.whoClicked.itemOnCursor = cursorClone
				event.whoClicked.inventory.helmet = slotClone
			} else {
				event.whoClicked.inventory.helmet = event.cursor
				event.whoClicked.itemOnCursor = ItemStack(AIR)
			}
		})
	}
}
