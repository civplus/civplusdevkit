package vg.civcraft.mc.civmodcore.random

import java.security.SecureRandom
import kotlin.math.roundToInt

val SECURE_RANDOM = SecureRandom()

fun SecureRandom.nextIntBellcurve(center: Int, std: Float) = (center + nextGaussian() * std).roundToInt()
fun SecureRandom.nextIntUniform(start: Int, end: Int) = nextInt(end - start) + start
fun SecureRandom.nextByteBellcurve(center: Byte, std: Float): Byte = (center + nextGaussian() * std).roundToInt().toByte()
fun SecureRandom.nextByteUniform(start: Byte, end: Byte): Byte = (nextInt(end - start) + start).toByte()
fun <T> SecureRandom.selectOneOf(array: Array<T>): T = array[this.nextInt(array.size)]
