package vg.civcraft.mc.civmodcore.extensions

import org.bukkit.DyeColor
import org.bukkit.Material
import org.bukkit.block.Biome
import org.bukkit.entity.EntityType

private val MATERIAL_NAME_OVERRIDES = mapOf(
	Pair(Material.SNOW_BLOCK, "Snow"),
	Pair(Material.HAY_BLOCK, "Haybale"),
	Pair(Material.MELON_BLOCK, "Watermelon"),
	Pair(Material.BED_BLOCK, "Bed"),
	Pair(Material.CAKE_BLOCK, "Cake"),
	Pair(Material.CARROT_ITEM, "Carrot"),
	Pair(Material.SUGAR_CANE_BLOCK, "Sugarcane"),
	Pair(Material.CROPS, "Wheat"),
)

private val ENTITY_NAME_OVERRIDES: Map<EntityType, String> = mapOf(

)

private val BIOME_NAME_OVERRIDES: Map<Biome, String> = mapOf(

)

private val DYE_NAME_OVERRIDES: Map<DyeColor, String> = mapOf(
	Pair(DyeColor.SILVER, "Light Grey")
)

internal fun prettyNameFromEnum(str: String): String {
	return str.toLowerCase().split("_").joinToString(separator = " ") { it.capitalize() }
}

val Biome.prettyName: String
	get() = BIOME_NAME_OVERRIDES.getOrDefault(this, prettyNameFromEnum(this.name))

val EntityType.prettyName: String
	get() = ENTITY_NAME_OVERRIDES.getOrDefault(this, prettyNameFromEnum(this.name))

val Material.prettyName: String
	get() = MATERIAL_NAME_OVERRIDES.getOrDefault(this, prettyNameFromEnum(this.name))

val DyeColor.prettyName: String
	get() = DYE_NAME_OVERRIDES.getOrDefault(this, prettyNameFromEnum(this.name))
