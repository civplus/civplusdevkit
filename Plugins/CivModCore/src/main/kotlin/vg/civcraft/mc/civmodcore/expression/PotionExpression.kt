package vg.civcraft.mc.civmodcore.expression

import net.minecraft.server.v1_8_R3.NBTBase
import net.minecraft.server.v1_8_R3.NBTTagList
import net.minecraft.server.v1_8_R3.NBTTagShort
import org.bukkit.potion.PotionEffectType
import vg.civcraft.mc.civmodcore.deobfuscation.asShort
import vg.civcraft.mc.civmodcore.extensions.potionEffectTypeToPotionColorShort
import kotlin.experimental.and

fun NBTExpression.potionHasNoEffects() = with("tag.CustomPotionEffects", object : TagMatcher {
	override fun matches(tag: NBTBase?): Boolean {
		if (tag == null) return true
		if (tag !is NBTTagList) return true
		return tag.size() == 0
	}

	override fun solve(): NBTBase {
		return NBTTagList()
	}
})

fun NBTExpression.notSplashPotion() = with("Damage", object : TagMatcher {
	override fun matches(tag: NBTBase?): Boolean {
		if (tag !is NBTTagShort) return true
		val damage = tag.asShort()
		// the second bit from the left is the splash bit
		return damage.and(0b0100000000000000) == 0.toShort()
	}

	override fun solve(): NBTBase {
		return NBTTagShort(0)
	}
})

fun NBTExpression.splashPotion() = with("Damage", object : TagMatcher {
	override fun matches(tag: NBTBase?): Boolean {
		if (tag !is NBTTagShort) return false
		val damage = tag.asShort()
		// the second bit from the left is the splash bit
		return damage.and(0b0100000000000000) != 0.toShort()
	}

	override fun solve(): NBTBase {
		return NBTTagShort(0b0100000000000000)
	}
})

fun NBTExpression.potionColor(effect: PotionEffectType) = with("Damage", object : TagMatcher {
	override fun matches(tag: NBTBase?): Boolean {
		if (tag !is NBTTagShort) return false

		val damage = tag.asShort()
		val color = damage.and(0b000000001111)
		return color == potionEffectTypeToPotionColorShort[effect]
	}

	override fun solve(): NBTBase {
		return NBTTagShort(potionEffectTypeToPotionColorShort[effect] ?: 7)
	}
})
