package vg.civcraft.mc.civmodcore.loottable

import org.bukkit.inventory.ItemStack

/**
 * @param generator an inexhaustible sequence which yields ItemStacks to drop
 * @param chance the chance of dropping this entry (in some cases, this is used as a chance out of 1.0f, and in others, out of the total chance in the table)
 */
class LootTableEntry(val generator: () -> ItemStack, val chance: Float)
