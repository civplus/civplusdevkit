package vg.civcraft.mc.civmodcore.loottable

import org.bukkit.inventory.ItemStack
import vg.civcraft.mc.civmodcore.random.SECURE_RANDOM

class TieredLootTable(val badDrops: LootTable, val goodDrops: LootTable, val defaultRareChance: Float = 0.3f, val chancePerLootingLevel: Float = 0.1f): LootTable() {
	fun getRandomDrops(lootingLevel: Int): List<ItemStack> {
		val roll = SECURE_RANDOM.nextFloat()
		if (roll < defaultRareChance + lootingLevel * chancePerLootingLevel) {
			return goodDrops.getRandomDrops()
		} else {
			return badDrops.getRandomDrops()
		}
	}

	fun getRandomDrop(lootingLevel: Int): ItemStack {
		val roll = SECURE_RANDOM.nextFloat()
		if (roll < defaultRareChance + lootingLevel * chancePerLootingLevel) {
			return goodDrops.getRandomDrop()
		} else {
			return badDrops.getRandomDrop()
		}
	}

	override fun getRandomDrops(): List<ItemStack> = getRandomDrops(0)
	override fun getRandomDrop(): ItemStack = getRandomDrop(0)
}
