package vg.civcraft.mc.civmodcore.extensions

import net.minecraft.server.v1_8_R3.EnumColor
import org.bukkit.ChatColor
import org.bukkit.DyeColor
import org.bukkit.DyeColor.*

/**
 * An inexact mapping of dye colors to chat colors
 *
 * Some colors are completely off, as they aren't even close to 1:1
 */
val DyeColor.chatColor: ChatColor
	get() {
		return when (this) {
			WHITE -> ChatColor.WHITE
			ORANGE -> ChatColor.GOLD
			MAGENTA -> ChatColor.DARK_AQUA
			LIGHT_BLUE -> ChatColor.AQUA
			YELLOW -> ChatColor.YELLOW
			LIME -> ChatColor.GREEN
			PINK -> ChatColor.LIGHT_PURPLE
			GRAY -> ChatColor.DARK_GRAY
			SILVER -> ChatColor.GRAY
			CYAN -> ChatColor.BLUE
			PURPLE -> ChatColor.DARK_PURPLE
			BLUE -> ChatColor.DARK_BLUE
			BROWN -> ChatColor.RED
			GREEN -> ChatColor.DARK_GREEN
			RED -> ChatColor.DARK_RED
			BLACK -> ChatColor.BLACK
		}
	}

fun DyeColor.mixWith(other: DyeColor): DyeColor? {
	data class Combination(val a: DyeColor, val b: DyeColor, val equals: DyeColor) {
		val triplet: Pair<Set<DyeColor>, DyeColor>
			get() = Pair(setOf(a, b), equals)
	}

	if (this == other) return this

	val mixtures = mapOf(
		Combination(BLUE, WHITE, equals = LIGHT_BLUE).triplet,
		Combination(WHITE, GRAY, equals = SILVER).triplet,
		Combination(GREEN, WHITE, equals = LIME).triplet,
		Combination(PURPLE, PINK, equals = MAGENTA).triplet,
		Combination(RED, YELLOW, equals = ORANGE).triplet,
		Combination(RED, WHITE, equals = PINK).triplet,
		Combination(GREEN, BLUE, equals = CYAN).triplet,
		Combination(BLACK, WHITE, equals = GRAY).triplet,
		Combination(RED, BLUE, equals = PURPLE).triplet,
	)

	return mixtures[setOf(this, other)]
}

val EnumColor.bukkitColor: DyeColor
	get() = DyeColor.values()[this.ordinal]

val DyeColor.nmsColor: EnumColor
	get() = EnumColor.values()[this.ordinal]
