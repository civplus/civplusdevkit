package vg.civcraft.mc.civmodcore.extensions

import net.minecraft.server.v1_8_R3.*
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.block.BlockFace
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer
import org.bukkit.entity.Player
import org.bukkit.event.player.PlayerBedEnterEvent
import org.bukkit.util.Vector
import vg.civcraft.mc.civmodcore.deobfuscation.isOverworld
import java.util.*
import kotlin.math.floor

/**
 * Searches for an online player by a string, usually the argument to a command.
 *
 * @param str The player's name/uuid/whatever may identify the player in the future
 * @return The player, or null if the player wasn't found
 */
fun getPlayerByString(str: String): Player? {
	return try {
		Bukkit.getPlayer(UUID.fromString(str))
	} catch (e: IllegalArgumentException) {
		Bukkit.getPlayer(str)
	}
}

fun Player.tryToTeleportVertically(location: Location): Boolean {
	var loc = location.clone()
	loc.x = floor(loc.x) + 0.500000
	loc.y = floor(loc.y) + 0.02
	loc.z = floor(loc.z) + 0.500000
	val baseLoc = loc.clone()
	// Check if teleportation here is viable
	var spaceToTeleport: Boolean = !loc.block.type.isSolid && !loc.block.getRelative(BlockFace.UP).type.isSolid
	if (!spaceToTeleport) {
		loc.y = loc.y + 1.000000
		spaceToTeleport = !loc.block.type.isSolid && !loc.block.getRelative(BlockFace.UP).type.isSolid
	}
	if (spaceToTeleport) {
		velocity = Vector()
		teleport(loc)
		return true
	}
	loc = baseLoc.clone()
	// Create a sliding window of block types and track how many of those
	//  are solid. Keep fetching the block below the current block to move down.
	var airCount = 0
	val airWindow = LinkedList<Material>()
	loc.y = (baseLoc.world.maxHeight.toFloat() - 2).toDouble()
	var block = baseLoc.world.getBlockAt(loc)
	for (i in 0..3) {
		val blockMat = block.type
		if (!blockMat.isSolid) {
			++airCount
		}
		airWindow.addLast(blockMat)
		block = block.getRelative(BlockFace.DOWN)
	}
	// Now that the window is prepared, scan down the Y-axis.
	while (block.y >= 1) {
		val blockMat = block.type
		if (blockMat.isSolid) {
			if (airCount == 4) {
				velocity = Vector()
				loc = block.location
				loc.x = floor(loc.x) + 0.500000
				loc.y = loc.y + 1.02
				loc.z = floor(loc.z) + 0.500000
				teleport(loc)
				return true
			}
		} else {
			++airCount
		}
		airWindow.addLast(blockMat)
		if (!airWindow.removeFirst().isSolid) {
			--airCount
		}
		block = block.getRelative(BlockFace.DOWN)
	}
	return false
}

fun Player.forceIntoBed(bed: Block): EntityHuman.EnumBedResult {
	if (this !is CraftPlayer) {
		return EntityHuman.EnumBedResult.OTHER_PROBLEM
	}

	val bedPosition = BlockPosition(bed.x, bed.y, bed.z)
	return handle.forceIntoBed(bedPosition)
}

private fun EntityHuman.forceIntoBed(bedPosition: BlockPosition): EntityHuman.EnumBedResult {
	if (!world.isClientSide) {
		if (this.isSleeping || !this.isAlive) {
			return EntityHuman.EnumBedResult.OTHER_PROBLEM
		}
		if (world.w()) {
			return EntityHuman.EnumBedResult.NOT_POSSIBLE_NOW
		}
		if (!world.worldProvider.isOverworld) {
			return EntityHuman.EnumBedResult.NOT_POSSIBLE_HERE
		}
	}

	if (au()) { // the player is riding something
		mount(null as Entity?) // remove the mount
	}

	if (bukkitEntity is Player) {
		val player = bukkitEntity as Player
		val bed = world.world.getBlockAt(bedPosition.x, bedPosition.y, bedPosition.z)
		val event = PlayerBedEnterEvent(player, bed)
		world.server.pluginManager.callEvent(event)
		if (event.isCancelled) {
			return EntityHuman.EnumBedResult.OTHER_PROBLEM
		}
	}

	setSize(0.2f, 0.2f)
	if (world.isLoaded(bedPosition)) {
		val enumdirection = world.getType(bedPosition).get(BlockDirectional.FACING) as EnumDirection
		var f = 0.5f
		var f1 = 0.5f
		when (enumdirection) {
			EnumDirection.SOUTH -> f1 = 0.9f
			EnumDirection.NORTH -> f1 = 0.1f
			EnumDirection.WEST -> f = 0.1f
			EnumDirection.EAST -> f = 0.9f
			else -> error("impossible direction")
		}
		EntityHuman::class.java.getDeclaredMethod("a", EnumDirection::class.java).also { it.isAccessible = true }.invoke(this, enumdirection)
		setPosition((bedPosition.x.toFloat() + f).toDouble(), (bedPosition.y.toFloat() + 0.6875f).toDouble(), (bedPosition.z.toFloat() + f1).toDouble())
	} else {
		setPosition((bedPosition.x.toFloat() + 0.5f).toDouble(), (bedPosition.y.toFloat() + 0.6875f).toDouble(), (bedPosition.z.toFloat() + 0.5f).toDouble())
	}

	sleeping = true
	sleepTicks = 0
	bx = bedPosition
	motX = 0.0.also { motY = it }.also { motZ = it }
	if (!world.isClientSide) {
		world.everyoneSleeping()
	}

	if (this is EntityPlayer) {
		val bedPacket = PacketPlayOutBed(this, bedPosition)
		u().getTracker().a(this, bedPacket)
		this.playerConnection.sendPacket(bedPacket)
	}

	return EntityHuman.EnumBedResult.OK
}

fun Player.fakeDimensionChange(dimension: Int) {
	if (this !is CraftPlayer) {
		return
	}

	val entity = handle

	if (health == 0.0 || entity.dead ||
			entity.playerConnection == null || entity.playerConnection.isDisconnected || entity.passenger != null) {
		return
	}

	eject()

	if (this.handle.activeContainer !== this.handle.defaultContainer) {
		this.handle.closeInventory()
	}

	if (handle.bukkitEntity.world.environment.id == dimension) {
		handle.playerConnection.sendPacket(PacketPlayOutRespawn(if (dimension >= 0) -1 else 0, handle.world.difficulty, handle.world.getWorldData().type, handle.playerInteractManager.gameMode))
	}
	handle.playerConnection.sendPacket(PacketPlayOutRespawn(dimension, handle.world.difficulty, handle.world.getWorldData().type, handle.playerInteractManager.gameMode))
	handle.u().playerChunkMap.removePlayer(handle)
	handle.u().playerChunkMap.flush()
	handle.u().playerChunkMap.addPlayer(handle)
	handle.u().playerChunkMap.flush()
	handle.playerConnection.teleport(Location(handle.world.world, handle.locX, handle.locY, handle.locZ, handle.yaw, handle.pitch))
	handle.playerConnection.sendPacket(PacketPlayOutSpawnPosition(handle.world.spawn))
	handle.playerConnection.sendPacket(PacketPlayOutExperience(handle.exp, handle.expTotal, handle.expLevel))
	handle.updateInventory(handle.defaultContainer)
	handle.bukkitEntity.updateScaledHealth()
	handle.playerConnection.sendPacket(PacketPlayOutHeldItemSlot(handle.inventory.itemInHandIndex))
	handle.updateAbilities()
	for (effect in handle.getEffects()) {
		handle.playerConnection.sendPacket(PacketPlayOutEntityEffect(handle.id, effect))
	}
	handle.u().chunkProviderServer.getChunkAt(handle.locX.toInt() shr 4, handle.locZ.toInt() shr 4)
}

/**
 * The player's ping, in milliseconds
 */
val Player.ping: Int
	get() {
		if (this !is CraftPlayer) {
			return 0
		}

		return this.handle.ping
	}

/**
 * Various packets that can be sent to players to change the color of the sky.
 *
 * The names assume the player does not have night vision.
 */
enum class SpecialSky(val rain: Float, val thunder: Float) {
	NORMAL(0f, 0f),
	DARK_NORAIN(0.00001f, 100000f),
	VISIBLE_STARS(-1f, 0f),
	YELLOW_SKY(2f, 0f),
	RED_SKY(3f, 0f),
	RED_SKY_NO_SUNLIGHT(4f, 0f),
	RED_SKY_NO_SUNLIGHT_DARK_SHADOWS_YELLOW_LIGHTS(5f, 0f),
	GRAY_SKY_NORAIN_YELLOW_FULLBRIGHT(0.00001f, 10000000f),
	BLACK_SKY_NORAIN_YELLOW_LIGHTS(0.00001f, 1000000f),
	BLACK_SKY_NO_SUNLIGHT_DARK_SHADOWS_YELLOW_LIGHTS(6f, 0f),
	BLACK_SKY_NO_SUNLIGHT_YELLOW_LIGHTS(7f, 0f), // this one with night vision is GRAY_SKY_BRIGHT_HORIZON_DARKBLUE_FULLBRIGHT
	BLACK_SKY_YELLOW_LIGHTS(1f, 12f),
	BLACK_SKY_YELLOW_FULLBRIGHT(1f, 15f),
	NORMAL_SKY_BLACK_TERRAIN(Float.NEGATIVE_INFINITY, 0f),
	BLUE_SKY_WHITE_HORIZON_CLOUDS(3f, 10f),
	CYAN_SKY_INVERTED_CLOUDS(4f, 10f),
	BLACK_EVERYTHING_NORAIN(Float.POSITIVE_INFINITY, 0f),
	NO_SUNLIGHT_NORAIN(0.00001f, 200000f),
	SUPER_NO_SUNLIGHT_NORAIN(0.00001f, 500000f),
}

/**
 * Sets the player's sky color using rain packets.
 *
 * This isn't perfect, because if vanilla rain starts or ends incorrect packets will get sent, and the sky will be set
 * to unexpected colors until it is set back to a known value.
 * As such, vanilla rain should be disabled if this is used, or you should use ProtocolLib to cancel any
 * vanilla rain packets as they arrive.
 */
fun Player.setSky(sky: SpecialSky) = setSky(sky.rain, sky.thunder)

private const val RAIN_LEVEL_CHANGE = 7
private const val THUNDER_LEVEL_CHANGE = 8

/**
 * Sets the player's sky color using rain packets.
 *
 * @see Player.setSky
 */
fun Player.setSky(rain: Float?, thunder: Float?) {
	if (rain != null && this is CraftPlayer) {
		handle.playerConnection.sendPacket(PacketPlayOutGameStateChange(RAIN_LEVEL_CHANGE, rain))
	}
	if (thunder != null && this is CraftPlayer) {
		handle.playerConnection.sendPacket(PacketPlayOutGameStateChange(THUNDER_LEVEL_CHANGE, thunder))
	}
}
