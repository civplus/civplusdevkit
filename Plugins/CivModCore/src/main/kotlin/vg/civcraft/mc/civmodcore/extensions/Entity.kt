package vg.civcraft.mc.civmodcore.extensions

import net.minecraft.server.v1_8_R3.NBTTagCompound
import org.bukkit.Material
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity
import org.bukkit.entity.Entity
import org.bukkit.entity.HumanEntity
import org.bukkit.inventory.ItemStack

val Entity.displayableName: String
	get() = this.customName ?: this.type.prettyName

fun HumanEntity.giveItemOrDropOnGround(vararg items: ItemStack) {
	val result = inventory.addItem(*items)

	for (item in result.values) {
		if (item.type == Material.AIR) {
			continue
		}

		location.world!!.dropItemNaturally(location, item)
	}
}

fun <T> Entity.writeNBTTag(key: String, value: T) {
	val nmsEntity = (this as CraftEntity).handle
	val nbt = nmsEntity.nbtTag ?: NBTTagCompound()
	nmsEntity.c(nbt)
	nbt.writeTag(key, value)
	nmsEntity.f(nbt)
}

val Entity.nbt: NBTTagCompound?
	get() {
		val nmsEntity = (this as CraftEntity).handle
		return nmsEntity.nbtTag
	}
