package vg.civcraft.mc.civmodcore.extensions

import net.minecraft.server.v1_8_R3.Block
import net.minecraft.server.v1_8_R3.Item
import org.bukkit.Material

private val MATERIAL_TO_ID: Map<Material, String> = run {
	val result = mutableMapOf<Material, String>()

	for (key in Item.REGISTRY.keySet()) {
		@Suppress("DEPRECATION")
		val material = Material.getMaterial(Item.getId(Item.REGISTRY.get(key)))

		result[material] = key.toString()
	}

	for (key in Block.REGISTRY.keySet()) {
		@Suppress("DEPRECATION")
		val material = Material.getMaterial(Block.getId(Block.REGISTRY.get(key)))

		result[material] = key.toString()
	}

	result
}

private val ID_TO_MATERIAL: Map<String, Material> = run {
	val result = mutableMapOf<String, Material>()

	for (key in Item.REGISTRY.keySet()) {
		@Suppress("DEPRECATION")
		val material = Material.getMaterial(Item.getId(Item.REGISTRY.get(key)))

		result[key.toString()] = material
	}

	for (key in Block.REGISTRY.keySet()) {
		@Suppress("DEPRECATION")
		val material = Material.getMaterial(Block.getId(Block.REGISTRY.get(key)))

		result[key.toString()] = material
	}

	result
}

val Material.vanillaId: String
 get() = MATERIAL_TO_ID[this]!!

fun materialFromId(id: String): Material? = ID_TO_MATERIAL[id]
