package vg.civcraft.mc.civmodcore.expression

import com.google.common.collect.HashMultimap
import com.google.common.collect.Multimap
import net.minecraft.server.v1_8_R3.*
import vg.civcraft.mc.civmodcore.deobfuscation.getBase
import vg.civcraft.mc.civmodcore.deobfuscation.remove

open class NBTExpression(val paths: Multimap<String, TagMatcher>) {
	constructor() : this(HashMultimap.create())

	companion object {
		fun empty(): NBTExpression {
			return NBTExpression(HashMultimap.create())
		}

		fun exactly(tag: NBTTagCompound): NBTExpression {
			return empty().with("", ExactlyNBTTagMatcher(tag))
		}
	}

	fun with(path: String, matcher: TagMatcher): NBTExpression {
		if (paths.containsKey("")) {
			throw NBTExpressionException("can not add anything to a NBTExpression that's already matching its entire input")
		}

		if (paths[path].contains(matcher)) {
			throw NBTExpressionException("can not add the same matcher to the same tag path")
		}

		paths.put(path, matcher)

		val solution = solve()
		if (!matches(solution)) {
			throw NBTExpressionException("added path $path and now NBTExpression does not match its own solution")
		}

		return this
	}

	fun with(path: String, string: String): NBTExpression = with(path, RegexMatcher.literally(string))
	fun with(path: String, number: Byte): NBTExpression = with(path, NumberMatcher.equal(NBTTagByte(number)))
	fun with(path: String, number: Short): NBTExpression = with(path, NumberMatcher.equal(NBTTagShort(number)))
	fun with(path: String, number: Int): NBTExpression = with(path, NumberMatcher.equal(NBTTagInt(number)))
	fun with(path: String, number: Long): NBTExpression = with(path, NumberMatcher.equal(NBTTagLong(number)))
	fun with(path: String, number: Float): NBTExpression = with(path, NumberMatcher.equal(NBTTagFloat(number)))
	fun with(path: String, number: Double): NBTExpression = with(path, NumberMatcher.equal(NBTTagDouble(number)))
	fun without(path: String): NBTExpression = with(path, TagNotExistsMatcher)

	open fun matches(nbt: NBTTagCompound): Boolean {
		val myNBT = nbt.clone()
		for ((key, matcher) in paths.entries()) {
			val parsedKey = parsePath(key)
			if (!matchesPath(myNBT as NBTTagCompound, parsedKey, matcher)) {
				return false
			}
		}

		return true
	}

	private fun matchesPath(nbt: NBTTagCompound, path: List<ParseElement>, matcher: TagMatcher): Boolean {
		var lastNBT: NBTBase? = null // the NBT tag holding browsingNBT
		var browsingNBTKey = "" // the key of lastNBT (as a object) that holds browsingNBT
		var browsingNBTIndex = -1 // the index of lastNBT (as an array) that holds browsingNBT
		var browsingNBT: NBTBase = nbt // the key we're currently looking at and browsing through using the path

		for ((i, part) in path.iterator().withIndex()) { // i is used for recursion in case of ArrayGrouping
			if (browsingNBT !is NBTTagCompound || !browsingNBT.hasKey(part.name)) {
				// If the type is wrong, or if the key doesn't exist, tell the matcher
				return matcher.matches(null)
			}

			browsingNBTIndex = -1 // reset the array index if it has been set
			lastNBT = browsingNBT
			browsingNBTKey = part.name
			browsingNBT = browsingNBT[part.name]

			when (part) {
				is ParseElement.Basic -> {
					// do nothing, everything that we needed was done up there
				}
				is ParseElement.ArrayGrouping -> {
					if (browsingNBT !is NBTTagList) {
						return matcher.matches(null)
					}

					val iterNBT = browsingNBT.clone() as NBTTagList // we make a copy because we mutate browsingNBT here
					var atLeastOneMatch = false
					for (browsingNBTI in 0 until browsingNBT.size()) { // NBTTagList doesn't implement an iterator()
						val tag = iterNBT.getBase(browsingNBTI)

						val matcherResult = if (tag is NBTTagCompound) {
							matchesPath(tag, path.subList(i + 1, path.size), matcher)
							// ^^ Recursion is fine here, because it's bounded by path.
						} else {
							matcher.matches(tag)
						}

						if (matcherResult) {
							atLeastOneMatch = true
						}

						if (part.matchingMode == ListMatchingMode.ANY && matcherResult) {
							// if any one of the things match, we are done and can return
							browsingNBT.remove(browsingNBTI) // make sure that only this matcher can match over the element
							return true
						} else if (part.matchingMode == ListMatchingMode.ALL) {
							if (matcherResult) {
								browsingNBT.remove(browsingNBTI) // same as above, make sure only this matcher can match over the elements it does
							} else {
								return false // if it doesn't match something, we're in ALL mode, so return that it did not match
							}
						}
					}

					if (part.matchingMode == ListMatchingMode.ANY && !atLeastOneMatch && !browsingNBT.isEmpty) {
						return false
					}

					// if we're in all mode and everything matched, or there are no elements, return true
					return true
				}
			}
		}

		// We've reached our element, so we check if it matches

		if (!matcher.matches(browsingNBT)) {
			return false
		}

		// if it does, then remove the tag so no other matchers can touch it
		if (browsingNBTKey != "") {
			if (lastNBT !is NBTTagCompound) {
				error("expected lastNBT to be a NBTTagCompount, is instead $lastNBT")
				// we use error because it's not expected to be possible
			}

			lastNBT.remove(browsingNBTKey)
		} else if (browsingNBTIndex != -1) {
			if (lastNBT !is NBTTagList) {
				error("expected lastNBT to be a NBTTagList, is instead $lastNBT")
			}

			lastNBT.remove(browsingNBTIndex)
		}

		// We've done everything, now just return the result (true)
		return true
	}

	open fun solve(): NBTTagCompound {
		if (paths.size() == 1 && paths.keys().first() == "") {
			val matcher = paths.values().first()
			val solution = matcher.solve()
			return if (solution is NBTTagCompound) { solution } else { NBTTagCompound() }
		}

		val result = NBTTagCompound()
		for ((path, matcher) in paths.entries()) {
			val parsedPath = parsePath(path)
			val solution = matcher.solve()

			var browsingNBT: NBTTagCompound = result

			for ((i, pathPart) in parsedPath.iterator().withIndex()) {
				when(pathPart) {
					is ParseElement.Basic -> {
						if (i == parsedPath.lastIndex) {
							if (browsingNBT.hasKey(pathPart.name)) {
								throw NBTExpressionSolvingException("While solving, NBT has a value in $path, $result")
							}

							if (solution != null) {
								browsingNBT[pathPart.name] = solution.clone()
							}
							break
						}

						if (!browsingNBT.hasKey(pathPart.name)) {
							browsingNBT[pathPart.name] = NBTTagCompound()
						} else if (browsingNBT[pathPart.name] !is NBTTagCompound) {
							throw NBTExpressionSolvingException("While solving, NBT has a non-compound value in $path: $result")
						}

						browsingNBT = browsingNBT[pathPart.name] as NBTTagCompound
					}
					is ParseElement.ArrayGrouping -> {
						if (browsingNBT.hasKey(pathPart.name)) {
							if (pathPart.matchingMode == ListMatchingMode.ALL) {
								throw NBTExpressionSolvingException("Can't merge lists with ALL mode (*) while solving, path: $path, nbt: $result")
							}
						} else {
							browsingNBT[pathPart.name] = NBTTagList()
						}

						if (browsingNBT[pathPart.name] !is NBTTagList) {
							throw NBTExpressionSolvingException("While solving, NBT has a non-list value in $path: $result")
						}

						if (i == parsedPath.lastIndex) {
							if (solution != null) {
								(browsingNBT[pathPart.name] as NBTTagList).add(solution.clone())
							}
							break
						}

						val list = browsingNBT[pathPart.name] as NBTTagList
						if (list.size() == 0)  {
							list.add(NBTTagCompound())
						}

						browsingNBT = list.getBase(0) as NBTTagCompound
					}
				}
			}
		}

		return result
	}

	open fun clone(): NBTExpression {
		val newPaths = HashMultimap.create<String, TagMatcher>()
		newPaths.putAll(paths)

		return NBTExpression(newPaths)
	}

	/**
	 * Returns a NBTExpression that will match anything this expression will, and also anything that the passed expression will.
	 *
	 * When the resulting expression is solved, the solution of the this expression will be returned
	 */
	fun or(expression: NBTExpression): NBTExpression {
		return OrNBTExpression(this, expression)
	}

	private class OrNBTExpression(base: NBTExpression, val other: NBTExpression): NBTExpression(base.paths) {
		override fun matches(nbt: NBTTagCompound): Boolean {
			return super.matches(nbt) || other.matches(nbt)
		}

		override fun clone(): NBTExpression {
			return OrNBTExpression(super.clone(), other.clone())
		}

		override fun equals(other: Any?): Boolean {
			if (this === other) return true
			if (javaClass != other?.javaClass) return false
			if (!super.equals(other)) return false

			other as OrNBTExpression

			if (other != other.other) return false

			return true
		}

		override fun hashCode(): Int {
			var result = super.hashCode()
			result = 31 * result + other.hashCode()
			return result
		}

		override fun toString(): String {
			return "OrNBTExpression(this=${super.toString()}, other=$other)"
		}
	}


	private sealed class ParseElement(val name: String) {
		class Basic(name: String): ParseElement(name)
		class ArrayGrouping(name: String, val matchingMode: ListMatchingMode): ParseElement(name)
	}
	private enum class ListMatchingMode { ANY, ALL }

	private fun parsePath(path: String): List<ParseElement> {
		open class Mode {
			inner class Base: Mode()
			inner class String: Mode()
			inner class ArrayOpen: Mode()
			inner class ArrayClose: Mode()
		}

		var mode: Mode = Mode().Base()
		val result = mutableListOf<ParseElement>()

		var characterIndex: Long = 0
		char@for (character in path) {
			characterIndex++
			when (mode) {
				is Mode.Base -> {
					when (character) {
						'[', ']' -> {
							throw NBTExpressionParsingException("while parsing nbt path at column $characterIndex, found '$character' while looking for the start of a key")
						}
						'.' -> {
						}
						else -> {
							result.add(ParseElement.Basic(character.toString()))
							mode = Mode().String()
							continue@char
						}
					}
				}
				is Mode.String -> {
					when (character) {
						'.' -> {
							mode = Mode().Base()
							continue@char
						}
						'[' -> {
							mode = Mode().ArrayOpen()
							continue@char
						}
						']' -> {
							throw NBTExpressionParsingException("error while parsing nbt path at column $characterIndex, found '$character' while looking for more of a key")
						}
						else -> {
							val basic = result[result.lastIndex]
							assert(basic is ParseElement.Basic)
							result[result.lastIndex] = ParseElement.Basic(basic.name + character)
						}
					}
				}
				is Mode.ArrayOpen -> {
					when (character) {
						'*', '_' -> {
							val listMatchingMode = if (character == '*') ListMatchingMode.ALL else ListMatchingMode.ANY

							val basic = result[result.lastIndex]
							assert(basic is ParseElement.Basic)
							result[result.lastIndex] = ParseElement.ArrayGrouping(basic.name, listMatchingMode)
							mode = Mode().ArrayClose()
							continue@char
						}
						']' -> {
							throw NBTExpressionParsingException("error while parsing nbt path at column $characterIndex, found ']' to close array, was expecting * or _")
						}
						else -> {
							throw NBTExpressionParsingException("error while parsing nbt path at column $characterIndex, unknown character '$character' in array literal, was looking for * or _")
						}
					}
				}
				is Mode.ArrayClose -> {
					if (character != ']') {
						throw NBTExpressionParsingException("error while parsing nbt path at column $characterIndex, was expecting ']' to close an array, found '$character' (was expecting * or _)")
					}
					mode = Mode().Base()
					continue@char
				}
			}
		}

		return result
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as NBTExpression

		if (paths != other.paths) return false

		return true
	}

	override fun hashCode(): Int {
		return paths.hashCode()
	}

	override fun toString(): String {
		return "NBTExpression$paths"
	}
}

open class NBTExpressionException(cause: String): Exception(cause)
class NBTExpressionSolvingException(cause: String): NBTExpressionException(cause)
class NBTExpressionParsingException(cause: String): NBTExpressionException(cause)
