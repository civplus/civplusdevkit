package vg.civcraft.mc.civmodcore.expression

import net.minecraft.server.v1_8_R3.NBTBase

class ExactlyNBTTagMatcher(val tag: NBTBase?): TagMatcher {
	override fun matches(tag: NBTBase?): Boolean {
		return this.tag == tag
	}

	override fun solve(): NBTBase? {
		return tag?.clone()
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as ExactlyNBTTagMatcher

		if (tag != other.tag) return false

		return true
	}

	override fun hashCode(): Int {
		return tag.hashCode()
	}

	override fun toString(): String {
		return "ExactlyNBTTagMatcher$tag"
	}
}
