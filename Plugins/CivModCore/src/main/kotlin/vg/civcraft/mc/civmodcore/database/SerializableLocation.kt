package vg.civcraft.mc.civmodcore.database

import org.bukkit.Bukkit
import org.bukkit.Location

data class SerializableLocation(val x: Double, val y: Double, val z: Double, val pitch: Float, val yaw: Float, val world: String) {
	constructor(location: Location) : this(location.x, location.y, location.z, location.pitch, location.yaw, location.world.name)
	fun asLocation(): Location = Location(Bukkit.getWorld(world), x, y, z, pitch, yaw)
}

val Location.serializable: SerializableLocation
	get() = SerializableLocation(this)
