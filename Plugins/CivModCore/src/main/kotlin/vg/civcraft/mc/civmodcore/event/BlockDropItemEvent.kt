package vg.civcraft.mc.civmodcore.event

import org.bukkit.block.Block
import org.bukkit.entity.Player
import org.bukkit.event.Event
import org.bukkit.event.HandlerList
import org.bukkit.inventory.ItemStack

class BlockDropItemEvent(val block: Block, var droppedItems: MutableList<ItemStack>? = null, val maybePlayer: Player? = null, originEvent: Event): Event() {
	companion object {
		@JvmStatic
		private val handlers = HandlerList()

		@JvmStatic
		fun getHandlerList(): HandlerList {
			return handlers
		}
	}

	override fun getHandlers(): HandlerList {
		return Companion.handlers
	}

	fun setToDefaultDrops() {
		droppedItems = if (maybePlayer != null) {
			block.getDrops(maybePlayer.itemInHand).toMutableList()
		} else {
			block.drops.toMutableList()
		}
	}
}
