package vg.civcraft.mc.civmodcore.deobfuscation

import net.minecraft.server.v1_8_R3.WorldProvider

val WorldProvider.isOverworld: Boolean
	get() = e()
