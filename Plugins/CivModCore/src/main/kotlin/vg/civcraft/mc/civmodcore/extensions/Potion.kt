package vg.civcraft.mc.civmodcore.extensions

import org.bukkit.potion.PotionEffectType
import org.bukkit.potion.PotionEffectType.*

val PotionEffectType.prettyName: String
	get() = when (this) {
		SLOW -> "Slowness"
		FAST_DIGGING -> "Haste"
		SLOW_DIGGING -> "Mining Fatigue"
		INCREASE_DAMAGE -> "Strength"
		HEAL -> "Health"
		HARM -> "Harming"
		JUMP -> "Jumping"
		FIRE_RESISTANCE -> "Fire Immunity"
		else -> prettyNameFromEnum(name)
	}
