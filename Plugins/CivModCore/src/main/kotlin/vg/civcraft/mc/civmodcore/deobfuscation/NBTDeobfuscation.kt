package vg.civcraft.mc.civmodcore.deobfuscation

import net.minecraft.server.v1_8_R3.*

fun NBTTagList.remove(index: Int): NBTBase = a(index)

fun NBTBase.NBTNumber.asByte(): Byte = f()
fun NBTBase.NBTNumber.asShort(): Short = e()
fun NBTBase.NBTNumber.asInt(): Int = d()
fun NBTBase.NBTNumber.asLong(): Long = c()
fun NBTBase.NBTNumber.asFloat(): Float = h()
fun NBTBase.NBTNumber.asDouble(): Double = g()

fun NBTTagString.getString(): String = a_()

fun NBTTagList.getBase(index: Int): NBTBase = g(index)
