package vg.civcraft.mc.civmodcore.expression

import dk.brics.automaton.Automaton
import dk.brics.automaton.RegExp
import net.minecraft.server.v1_8_R3.NBTBase
import net.minecraft.server.v1_8_R3.NBTTagString
import vg.civcraft.mc.civmodcore.deobfuscation.getString

class RegexMatcher(val regex: Automaton): TagMatcher {
	val firstMatch: String = regex.getShortestExample(true)

	companion object {
		fun new(regex: RegExp): RegexMatcher = RegexMatcher(regex.toAutomaton())
		fun new(regex: String): RegexMatcher = RegexMatcher(RegExp(regex).toAutomaton())
		fun literally(string: String): RegexMatcher = RegexMatcher(Automaton.makeString(string))
	}

	override fun matches(tag: NBTBase?): Boolean {
		if (tag !is NBTTagString) {
			return false
		}

		return regex.run(tag.getString())
	}

	override fun solve(): NBTBase {
		return NBTTagString(firstMatch)
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as RegexMatcher

		if (regex != other.regex) return false

		return true
	}

	override fun hashCode(): Int {
		return regex.hashCode()
	}

	override fun toString(): String {
		return "RegexMatcher(regex=$regex, firstMatch='$firstMatch')"
	}
}
