package vg.civcraft.mc.civmodcore.extensions

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import de.undercouch.bson4jackson.BsonFactory
import net.minecraft.server.v1_8_R3.NBTBase
import net.minecraft.server.v1_8_R3.NBTTagCompound

private val validator = BasicPolymorphicTypeValidator.builder().allowIfBaseType(Any::class.java).build() // allow anything to be instantiated, since we're only using internal data
private val bsonMapper = ObjectMapper(BsonFactory()).setPolymorphicTypeValidator(validator).registerKotlinModule()

fun <T> NBTTagCompound.writeTag(key: String, value: T) {
	if (value == null) {
		this.remove(key)
		return
	}
	when (value) {
		is Boolean -> this.setBoolean(key, value)
		is String -> this.setString(key, value)
		is Byte -> this.setByte(key, value)
		is ByteArray -> this.setByteArray(key, value)
		is Short -> this.setShort(key, value)
		is Int -> this.setInt(key, value)
		is IntArray -> this.setIntArray(key, value)
		is Long -> this.setLong(key, value)
		is Double -> this.setDouble(key, value)
		is Float -> this.setFloat(key, value)
		is NBTBase -> this.set(key, value)
		else -> {
			val bytes = bsonMapper.writeValueAsBytes(value)
			this.setByteArray(key, bytes)
		}
	}
}

// this can't use the fancy kotlin <reified T> thing because it crashes the kotlin compiler
fun <T> NBTTagCompound.readObject(key: String, clazz: Class<T>): T? {
	val bytes: ByteArray = this.getByteArray(key)
	if (bytes.isEmpty()) return null
	return bsonMapper.readValue(bytes, clazz)
}
