package vg.civcraft.mc.civmodcore.event

import org.bukkit.Bukkit
import org.bukkit.GameMode
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.event.Event
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.block.BlockExplodeEvent
import org.bukkit.event.entity.EntityExplodeEvent
import vg.civcraft.mc.civmodcore.extensions.dropItemAsIfBroken

object BlockDropItemListener: Listener {
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	fun playerBreakBlock(event: BlockBreakEvent) {
		val itemEvent = BlockDropItemEvent(event.block, null, event.player, event)
		Bukkit.getPluginManager().callEvent(itemEvent)
		if (itemEvent.droppedItems == null) return

		if (event.player.gameMode == GameMode.CREATIVE) return

		event.isCancelled = true
		event.block.type = Material.AIR
		for (item in itemEvent.droppedItems!!) {
			event.block.world.dropItemAsIfBroken(event.block.location, item)
		}
	}

	fun explosionBreakBlock(event: Event, yieldChance: Float, blocks: MutableList<Block>) {
		val removedBlocks = mutableMapOf<Block, BlockDropItemEvent>()

		for (block in blocks) {
			val itemEvent = BlockDropItemEvent(block, null, null, event)
			Bukkit.getPluginManager().callEvent(itemEvent)
			if (itemEvent.droppedItems == null) continue

			removedBlocks[block] = itemEvent

		}

		for ((block, itemEvent) in removedBlocks) {
			blocks.remove(block)

			block.type = Material.AIR

			if (Math.random() > yieldChance) {
				Bukkit.getScheduler().runTaskLater(Bukkit.getPluginManager().getPlugin("CivModCore"), {
					for (item in itemEvent.droppedItems!!) {
						block.world.dropItemAsIfBroken(block.location, item)
					}
				}, 1L)
			}
		}
	}

	@EventHandler(ignoreCancelled = true)
	fun explosionBreakBlock(event: EntityExplodeEvent) {
		explosionBreakBlock(event, event.yield, event.blockList())
	}

	@EventHandler(ignoreCancelled = true)
	fun explosionBreakBlock(event: BlockExplodeEvent) {
		explosionBreakBlock(event, event.yield, event.blockList())
	}
}
