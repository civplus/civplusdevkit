package vg.civcraft.mc.civmodcore.loottable

import org.bukkit.inventory.ItemStack
import vg.civcraft.mc.civmodcore.random.SECURE_RANDOM

open class LootTable(vararg val entries: LootTableEntry = arrayOf()) {
	private val totalChance = entries.map{ it.chance }.sum()

	/**
	 * @return 0 or more random drops, interpreting drop chance as being out of 1.0f for each potential drop
	 */
	open fun getRandomDrops(): List<ItemStack> {
		val ret = ArrayList<ItemStack>()
		val roll = SECURE_RANDOM.nextFloat()
		for (entry in entries) {
			if (roll < entry.chance) {
				ret.add(entry.generator())
			}
		}
		return ret
	}

	/**
	 * @return a random drop, interpreting drop chances as being out of the total chance in the table
	 */
	open fun getRandomDrop(): ItemStack {
		var roll = SECURE_RANDOM.nextFloat() * totalChance
		var ind = 0
		while (roll >= 0) {
			roll -= entries[ind].chance
			ind += 1
		}
		val chosenItem = entries[ind - 1].generator()
		return chosenItem
	}

	fun toSingledropSubtable(chance: Float): LootTableEntry {
		return LootTableEntry(
			generator = { this@LootTable.getRandomDrop() },
			chance = chance,
		)
	}
}
