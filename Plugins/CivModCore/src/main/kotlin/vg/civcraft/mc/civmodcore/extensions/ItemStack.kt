package vg.civcraft.mc.civmodcore.extensions

import net.minecraft.server.v1_8_R3.NBTTagCompound
import org.bukkit.Bukkit
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack
import org.bukkit.inventory.ItemStack
import org.bukkit.potion.PotionEffectType
import kotlin.experimental.and
import kotlin.experimental.or

fun <T> ItemStack.writeNBTTag(key: String, value: T) {
	val nmsItem = CraftItemStack.asNMSCopy(this)
	val tag = if (nmsItem.hasTag()) nmsItem.tag else NBTTagCompound()
	tag.writeTag(key, value)
	nmsItem.tag = tag
	this.itemMeta = CraftItemStack.asBukkitCopy(nmsItem).itemMeta
}

/**
 * @return A copy of the item's NBT tag.
 */
var ItemStack.nbt: NBTTagCompound?
	get() {
		val nmsItem = CraftItemStack.asNMSCopy(this)
		return nmsItem?.tag
	}
	set(value) {
		if (this is CraftItemStack) {
			val handleField = CraftItemStack::class.java.getDeclaredField("handle")
			handleField.isAccessible = true
			val handle = handleField.get(this)
			if (handle is net.minecraft.server.v1_8_R3.ItemStack) {
				handle.tag = value
			} else {
				error("Handle is a ${handleField.type}, not nms ItemStack!")
			}
		} else {
			error("Expected a CraftItemStack, got $this")
		}
	}

/***
 * Places the ItemStack into an inventory, and returns the result
 *
 * Spigot will do some normalization on itemstacks when you put it into an inventory, which can break .equals() and even .isSimilar()
 * This stimulates the normalization so you can account for it.
 *
 * This clones the ItemStack
 */
val ItemStack.normalized: ItemStack
	get() {
		val inventory = Bukkit.getServer().createInventory(null, 9)
		inventory.setItem(0, this)
		return inventory.getItem(0)
	}

val potionEffectTypeToPotionColorShort = mapOf<PotionEffectType, Short>(
	Pair(PotionEffectType.REGENERATION, 1),
	Pair(PotionEffectType.SPEED, 2),
	Pair(PotionEffectType.FIRE_RESISTANCE, 3),
	Pair(PotionEffectType.POISON, 4),
	Pair(PotionEffectType.HEAL, 5),
	Pair(PotionEffectType.NIGHT_VISION, 6),
	Pair(PotionEffectType.WEAKNESS, 8),
	Pair(PotionEffectType.INCREASE_DAMAGE, 9),
	Pair(PotionEffectType.SLOW, 10),
	Pair(PotionEffectType.JUMP, 11),
	Pair(PotionEffectType.HARM, 12),
	Pair(PotionEffectType.WATER_BREATHING, 13),
	Pair(PotionEffectType.INVISIBILITY, 14)
).withDefault { 7 }
fun ItemStack.setPotionColor(color: PotionEffectType) {
	durability = durability
		// vanilla uses the last 4 bits to determine color
		.and(0b111111110000)
		.or(potionEffectTypeToPotionColorShort.getValue(color))
}
