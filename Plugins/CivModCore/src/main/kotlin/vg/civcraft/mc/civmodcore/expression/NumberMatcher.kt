package vg.civcraft.mc.civmodcore.expression

import net.minecraft.server.v1_8_R3.*
import vg.civcraft.mc.civmodcore.deobfuscation.*
import vg.civcraft.mc.civmodcore.expression.NumberMatcher.NumberOperation.*

data class NumberMatcher(val number: NBTBase.NBTNumber, val operation: NumberOperation): TagMatcher {
	init {
		fun bad(): Nothing = error("can not do $operation on tag that is $number, no possible solution")

		when (number) {
			is NBTTagByte -> {
				if (number.asByte() == Byte.MAX_VALUE && operation == GREATER_THAN) {
					bad()
				}
				if (number.asByte() == Byte.MIN_VALUE && operation == LESS_THAN) {
					bad()
				}
			}
			is NBTTagShort -> {
				if (number.asShort() == Short.MAX_VALUE && operation == GREATER_THAN) {
					bad()
				}
				if (number.asShort() == Short.MIN_VALUE && operation == LESS_THAN) {
					bad()
				}
			}
			is NBTTagInt -> {
				if (number.asInt() == Int.MAX_VALUE && operation == GREATER_THAN) {
					bad()
				}
				if (number.asInt() == Int.MIN_VALUE && operation == LESS_THAN) {
					bad()
				}
			}
			is NBTTagLong -> {
				if (number.asLong() == Long.MAX_VALUE && operation == GREATER_THAN) {
					bad()
				}
				if (number.asLong() == Long.MIN_VALUE && operation == LESS_THAN) {
					bad()
				}
			}
			is NBTTagFloat -> {
				if (number.asFloat() == Float.MAX_VALUE && operation == GREATER_THAN) {
					bad()
				}
				if (number.asFloat() == Float.MIN_VALUE && operation == LESS_THAN) {
					bad()
				}
			}
			is NBTTagDouble -> {
				if (number.asDouble() == Double.MAX_VALUE && operation == GREATER_THAN) {
					bad()
				}
				if (number.asDouble() == Double.MIN_VALUE && operation == LESS_THAN) {
					bad()
				}
			}
			else -> {
				error("Expected a built-in minecraft server type, instead got $number")
			}
		}
	}

	enum class NumberOperation {
		GREATER_THAN,
		LESS_THAN,
		GREATER_THAN_OR_EQUAL,
		LESS_THAN_OR_EQUAL,
		EQUAL,
		NOT_EQUAL,
	}

	companion object {
		fun greaterThan(number: NBTBase.NBTNumber): NumberMatcher = NumberMatcher(number, GREATER_THAN)
		fun lessThan(number: NBTBase.NBTNumber): NumberMatcher = NumberMatcher(number, LESS_THAN)
		fun greaterThanOrEqual(number: NBTBase.NBTNumber): NumberMatcher = NumberMatcher(number, GREATER_THAN_OR_EQUAL)
		fun lessThanOrEqual(number: NBTBase.NBTNumber): NumberMatcher = NumberMatcher(number, LESS_THAN_OR_EQUAL)
		fun equal(number: NBTBase.NBTNumber): NumberMatcher = NumberMatcher(number, EQUAL)
		fun notEqual(number: NBTBase.NBTNumber): NumberMatcher = NumberMatcher(number, NOT_EQUAL)

		fun isTrue() = NumberMatcher(NBTTagByte(1), EQUAL)
		fun isFalse() = NumberMatcher(NBTTagByte(0), EQUAL)
	}

	override fun matches(tag: NBTBase?): Boolean {
		if (tag !is NBTBase.NBTNumber) {
			return false
		}

		return when (tag) {
			is NBTTagByte, is NBTTagShort, is NBTTagInt, is NBTTagLong -> {
				val ourNumber = number.asLong()
				val number = tag.asLong()

				when (operation) {
					GREATER_THAN -> ourNumber < number
					LESS_THAN -> ourNumber > number
					GREATER_THAN_OR_EQUAL -> ourNumber <= number
					LESS_THAN_OR_EQUAL -> ourNumber >= number
					EQUAL -> ourNumber == number
					NOT_EQUAL -> ourNumber != number
				}
			}
			is NBTTagFloat, is NBTTagDouble -> {
				val ourNumber = tag.asDouble()
				val number = tag.asDouble()

				when (operation) {
					GREATER_THAN -> ourNumber < number
					LESS_THAN -> ourNumber > number
					GREATER_THAN_OR_EQUAL -> ourNumber <= number
					LESS_THAN_OR_EQUAL -> ourNumber >= number
					EQUAL -> ourNumber == number
					NOT_EQUAL -> ourNumber != number
				}
			}
			else -> {
				error("unknown number type $tag")
			}
		}
	}

	override fun solve(): NBTBase {
		// this is the objectively best way to do it
		// sorry
		return when (number) {
			is NBTTagByte -> {
				when (operation) {
					GREATER_THAN -> NBTTagByte((number.asByte() + 1).toByte())
					LESS_THAN -> NBTTagByte((number.asByte() - 1).toByte())
					EQUAL, GREATER_THAN_OR_EQUAL, LESS_THAN_OR_EQUAL -> number.clone()
					NOT_EQUAL -> if (number.asByte() == 0.toByte()) NBTTagByte(1) else NBTTagByte(0)
				}
			}
			is NBTTagShort -> {
				when (operation) {
					GREATER_THAN -> NBTTagShort((number.asShort() + 1).toShort())
					LESS_THAN -> NBTTagShort((number.asShort() - 1).toShort())
					EQUAL, GREATER_THAN_OR_EQUAL, LESS_THAN_OR_EQUAL -> number.clone()
					NOT_EQUAL -> if (number.asShort() == 0.toShort()) NBTTagShort(1) else NBTTagShort(0)
				}
			}
			is NBTTagInt -> {
				when (operation) {
					GREATER_THAN -> NBTTagInt((number.asInt() + 1))
					LESS_THAN -> NBTTagInt((number.asInt() - 1))
					EQUAL, GREATER_THAN_OR_EQUAL, LESS_THAN_OR_EQUAL -> number.clone()
					NOT_EQUAL -> if (number.asInt() == 0) NBTTagInt(1) else NBTTagInt(0)
				}
			}
			is NBTTagLong -> {
				when (operation) {
					GREATER_THAN -> NBTTagLong((number.asLong() + 1))
					LESS_THAN -> NBTTagLong((number.asLong() - 1))
					EQUAL, GREATER_THAN_OR_EQUAL, LESS_THAN_OR_EQUAL -> number.clone()
					NOT_EQUAL -> if (number.asLong() == 0.toLong()) NBTTagLong(1) else NBTTagLong(0)
				}
			}
			is NBTTagFloat -> {
				when (operation) {
					GREATER_THAN -> NBTTagFloat(((number.asFloat() + 1.0F)))
					LESS_THAN -> NBTTagFloat(((number.asFloat() - 1.0F)))
					EQUAL, GREATER_THAN_OR_EQUAL, LESS_THAN_OR_EQUAL -> number.clone()
					NOT_EQUAL -> if (number.asFloat() == 0.0F) NBTTagFloat(1.0F) else NBTTagFloat(0.0F)
				}
			}
			is NBTTagDouble -> {
				when (operation) {
					GREATER_THAN -> NBTTagDouble(((number.asDouble() + 1.0)))
					LESS_THAN -> NBTTagDouble(((number.asDouble() - 1.0)))
					EQUAL, GREATER_THAN_OR_EQUAL, LESS_THAN_OR_EQUAL -> number.clone()
					NOT_EQUAL -> if (number.asDouble() == 0.0) NBTTagDouble(1.0) else NBTTagDouble(0.0)
				}
			}
			else -> {
				error("Expected another type, is $number instead")
			}
		}
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as NumberMatcher

		if (number != other.number) return false
		if (operation != other.operation) return false

		return true
	}

	override fun hashCode(): Int {
		var result = number.hashCode()
		result = 31 * result + operation.hashCode()
		return result
	}

	override fun toString(): String {
		return "NumberMatcher(number=$number, operation=$operation)"
	}
}
