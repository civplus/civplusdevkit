package vg.civcraft.mc.civmodcore.expression

import net.minecraft.server.v1_8_R3.NBTBase
import net.minecraft.server.v1_8_R3.NBTTagCompound
import net.minecraft.server.v1_8_R3.NBTTagShort
import org.bukkit.enchantments.Enchantment
import vg.civcraft.mc.civmodcore.deobfuscation.asInt

class EnchantmentMatcher(val enchants: Set<Enchantment>, val mode: EnchantMatchMode, val level: NumberMatcher): TagMatcher {
	companion object {
		fun enchant(enchant: Enchantment, level: NumberMatcher) = EnchantmentMatcher(setOf(enchant), EnchantMatchMode.IN_SET, level)
		fun enchant(enchant: Enchantment, level: Short) = EnchantmentMatcher(setOf(enchant), EnchantMatchMode.IN_SET, NumberMatcher.equal(NBTTagShort(level)))
	}

	enum class EnchantMatchMode {
		IN_SET,
		NOT_IN_SET,
	}

	override fun matches(tag: NBTBase?): Boolean {
		if (tag !is NBTTagCompound) {
			return false
		}

		if (!level.matches(tag["lvl"] ?: NBTTagShort(0))) {
			return false
		}

		val enchantID = tag["id"]
		if (enchantID !is NBTBase.NBTNumber) {
			return false
		}

		val enchant = Enchantment.getById(enchantID.asInt())

		return when (mode) {
			EnchantMatchMode.IN_SET -> enchant in enchants
			EnchantMatchMode.NOT_IN_SET -> enchant !in enchants
		}
	}

	override fun solve(): NBTBase {
		val enchant = when (mode) {
			EnchantMatchMode.IN_SET -> enchants.iterator().next().id
			EnchantMatchMode.NOT_IN_SET -> {
				var maybeEnchant: Enchantment? = null
				for (enchant in Enchantment.values()) {
					if (enchant !in enchants) {
						maybeEnchant = enchant
					} else {
						continue
					}
				}

				if (maybeEnchant == null) {
					throw NBTExpressionSolvingException("a NOT_IN_SET EnchantmentMatcher with every Enchantment can't be solved")
				}

				maybeEnchant.id
			}
		}

		val level = level.solve()

		val compound = NBTTagCompound()

		compound["id"] = NBTTagShort(enchant.toShort())
		compound["lvl"] = level

		return compound
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as EnchantmentMatcher

		if (enchants != other.enchants) return false
		if (mode != other.mode) return false
		if (level != other.level) return false

		return true
	}

	override fun hashCode(): Int {
		var result = enchants.hashCode()
		result = 31 * result + mode.hashCode()
		result = 31 * result + level.hashCode()
		return result
	}

	override fun toString(): String {
		return "EnchantmentMatcher(enchants=$enchants, mode=$mode, level=$level)"
	}
}
