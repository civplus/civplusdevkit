package vg.civcraft.mc.civmodcore.extensions

import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.World
import org.bukkit.inventory.ItemStack
import java.util.logging.Level

fun World.getHighestNonTransparentBlockYAt(x: Int, z: Int): Int {
	for (ry in 0..255) {
		val y = 255 - ry
		val type = this.getBlockAt(x, y, z).type
		if (!type.isTransparent) return y
	}
	return 256
}

val World.moonPhase: MoonPhase
	get() {
		val days = fullTime.toInt() / 24000
		val phase = days % 8

		return MoonPhase.values()[phase]
	}

enum class MoonPhase {
	FULL,
	WANING_GIBBOUS,
	THIRD_QUARTER,
	WANING_CRESCENT,
	NEW,
	WAXING_CRESCENT,
	FIRST_QUARTER,
	WAXING_GIBBOUS,
}

/**
 * Drops the item at the location with the same physics as if a player broke a block at the location, and the block dropped that item
 */
fun World.dropItemAsIfBroken(location: Location, item: ItemStack) {
	val x = (Math.random() * 0.5f) + (1.0f - 0.5f).toDouble() * 0.5
	val y = (Math.random() * 0.5f) + (1.0f - 0.5f).toDouble() * 0.5
	val z = (Math.random() * 0.5f) + (1.0f - 0.5f).toDouble() * 0.5
	val dropLocation = location.clone().add(x, y, z)
	this.dropItem(dropLocation, item)
}
