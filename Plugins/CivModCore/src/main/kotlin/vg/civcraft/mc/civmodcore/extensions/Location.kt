package vg.civcraft.mc.civmodcore.extensions

import org.bukkit.Location
import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.sqrt

val Location.blockCoordsString: String
	get() = "(${this.blockX}, ${this.blockY}, ${this.blockZ})"

fun Location.safeDistance(other: Location): Double {
	if (this.world != other.world) {
		return Double.POSITIVE_INFINITY
	} else {
		return this.distance(other)
	}
}

/**
 * Returns the distance between points according to taxicab geometry.
 *
 * This is useful for creating diamond-shaped fields, like torches in vanilla minecraft
 */
fun Location.taxicabDistance(other: Location): Double {
	return abs(x - other.x) + abs(y - other.y) + abs(z - other.z)
}

/**
 * Returns the distance between points according to chebyshev geometry.
 *
 * This is useful for creating cubic fields, like snitches or square bastions.
 */
fun Location.chebyshevDistance(other: Location): Double {
	return maxOf(abs(x - other.x), abs(y - other.y), abs(z - other.z))
}

/**
 * Returns the distance between points, ignoring the y dimension
 */
fun Location.distanceIgnoringHeight(other: Location): Double {
	return sqrt((x - other.x).pow(2) + (z - other.z).pow(2))
}

val Location.blockLocation: Triple<Int, Int, Int>
	get() = Triple(this.blockX, this.blockY,  this.blockZ)
