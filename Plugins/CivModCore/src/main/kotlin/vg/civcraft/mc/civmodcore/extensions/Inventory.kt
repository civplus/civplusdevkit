package vg.civcraft.mc.civmodcore.extensions

import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.BlockState
import org.bukkit.block.DoubleChest
import org.bukkit.entity.Entity
import org.bukkit.inventory.AnvilInventory
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.ItemStack

fun Inventory.removeOne(mat: Material): Boolean {
	if (!this.contains(mat)) return false
	val i = this.contents[this.first(mat)]
	if (i.amount == 1) {
		this.remove(i)
	} else {
		i.amount -= 1
	}
	return true
}

val Inventory.location: Location?
get() {
	val holder = holder ?: return null

	when (holder) {
		is DoubleChest -> {
			return Location(holder.world, holder.x, holder.y, holder.z)
		}
		is BlockState -> { // Beacon, BrewingStand, Chest, Dispensor, Dropper, Furnace, Hopper
			return holder.location
		}
		is Entity -> { // HopperMinecart, Horse, HumanEntity (Player), StorageMinecart
			return holder.location
		}
		else -> {
			return null
		}
	}
}
val Inventory.isBlockInventory: Boolean
get() {
	when (holder) {
		is DoubleChest -> {
			return true
		}
		is BlockState -> { // Beacon, BrewingStand, Chest, Dispensor, Dropper, Furnace, Hopper
			return true
		}
		is Entity -> { // HopperMinecart, Horse, HumanEntity (Player), StorageMinecart
			return false
		}
		else -> {
			return false
		}
	}
}
val Inventory.isEmpty: Boolean
get() {
	for (i in this.contents) {
		if (i == null) return false
		if (i.type != Material.AIR) return false
	}
	return true
}
val AnvilInventory.resultItem: ItemStack?
	get() = this.getItem(2)
val AnvilInventory.mainInputItem: ItemStack?
	get() = this.getItem(0)
val AnvilInventory.secondaryInputItem: ItemStack?
	get() = this.getItem(1)
