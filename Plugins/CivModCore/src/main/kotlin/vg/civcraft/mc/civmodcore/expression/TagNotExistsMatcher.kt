package vg.civcraft.mc.civmodcore.expression

import net.minecraft.server.v1_8_R3.NBTBase

object TagNotExistsMatcher: TagMatcher {
	override fun matches(tag: NBTBase?): Boolean {
		return tag == null
	}

	override fun solve(): NBTBase? {
		return null
	}
}
