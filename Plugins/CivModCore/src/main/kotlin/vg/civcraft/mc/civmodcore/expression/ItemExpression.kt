package vg.civcraft.mc.civmodcore.expression

import net.minecraft.server.v1_8_R3.*
import org.bukkit.DyeColor
import org.bukkit.Material
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack
import org.bukkit.inventory.ItemStack
import org.bukkit.potion.PotionEffectType
import vg.civcraft.mc.civmodcore.deobfuscation.*
import vg.civcraft.mc.civmodcore.extensions.vanillaId

fun NBTExpression.matches(item: ItemStack): Boolean {
	val craftItem = CraftItemStack.asNMSCopy(item)
	val serialized = if (craftItem != null) craftItem.save(NBTTagCompound()) else NBTTagCompound()
	return matches(serialized)
}

fun NBTExpression.solveToItemStack(): ItemStack {
	val nbt = solve()
	if (nbt == NBTTagCompound()) {
		return ItemStack(Material.AIR)
	}

	if (!nbt.hasKey("id")) {
		nbt["id"] = NBTTagString("minecraft:stone")
	}
	if (!nbt.hasKey("Count")) {
		nbt["Count"] = NBTTagByte(1)
	}

	return CraftItemStack.asBukkitCopy(net.minecraft.server.v1_8_R3.ItemStack.createStack(nbt))
}

fun NBTExpression.Companion.exactly(item: ItemStack): NBTExpression {
	if (item.type == Material.AIR) {
		return airItem()
	}

	return exactly(CraftItemStack.asNMSCopy(item).save(NBTTagCompound()))
}

fun NBTExpression.Companion.airItem(): NBTExpression = exactly(NBTTagCompound())

fun NBTExpression.amount(numberMatcher: NumberMatcher): NBTExpression = with("Count", numberMatcher)
fun NBTExpression.amountGT(amount: Byte): NBTExpression = amount(NumberMatcher.greaterThan(NBTTagByte(amount)))
fun NBTExpression.amountGTE(amount: Byte): NBTExpression = amount(NumberMatcher.greaterThanOrEqual(NBTTagByte(amount)))
fun NBTExpression.amountLT(amount: Byte): NBTExpression = amount(NumberMatcher.lessThan(NBTTagByte(amount)))
fun NBTExpression.amountLTE(amount: Byte): NBTExpression = amount(NumberMatcher.lessThanOrEqual(NBTTagByte(amount)))
fun NBTExpression.amountEqual(amount: Byte): NBTExpression = amount(NumberMatcher.equal(NBTTagByte(amount)))
fun NBTExpression.id(regexMatcher: RegexMatcher): NBTExpression = with("id", regexMatcher)
fun NBTExpression.id(id: String): NBTExpression = with("id", RegexMatcher.literally(id))
fun NBTExpression.id(material: Material): NBTExpression = id(material.vanillaId)
fun NBTExpression.damage(numberMatcher: NumberMatcher): NBTExpression = with("Damage", numberMatcher)
fun NBTExpression.damage(damage: Short): NBTExpression = damage(NumberMatcher.equal(NBTTagShort(damage)))
fun NBTExpression.dye(color: DyeColor): NBTExpression = id(Material.INK_SACK).damage(color.dyeData.toShort())
fun NBTExpression.itemName(name: String): NBTExpression = with("tag.display.Name", name)
fun NBTExpression.loreLine(line: String): NBTExpression = with("tag.display.Lore[_]", line)
fun NBTExpression.potionEffect(effectType: PotionEffectType): NBTExpression = with("tag.CustomPotionEffects[_].Id", NumberMatcher.equal(NBTTagInt(effectType.id)))

fun NBTExpression.getAmount(): Byte {
	var counts = paths["Count"]
	if (counts == null || counts.isEmpty()) {
		counts = null
	}
	val amountNBT = counts?.first()?.solve()
	return if (amountNBT is NBTTagByte) amountNBT.asByte() else 1
}

fun NBTExpression.toItemGenerator(amountGenerator: () -> Byte): () -> ItemStack {
	val item = solveToItemStack()
	return {
		item.clone().also { it.amount = amountGenerator().toInt() }
	}
}

fun NBTExpression.toItemGenerator(): () -> ItemStack {
	val item = solveToItemStack()
	return {
		item.clone()
	}
}
