package vg.civcraft.mc.civmodcore.extensions

// I liked writing this
val Boolean.onOrOff: String
	get() {
		return if (this) "on" else "off"
	}

val Long.minutesToSeconds: Long
	get() = this * 60
val Long.hoursToMinutes: Long
	get() = this * 60
val Long.hoursToSeconds: Long
	get() = this.hoursToMinutes.minutesToSeconds
val Long.secondsToMinutes: Long
	get() = this / 60
val Long.minutesToHours: Long
	get() = this / 60
val Long.secondsToHours: Long
	get() = this.secondsToMinutes.minutesToHours
val Long.millisToSeconds: Long
	get() = this / 1000
val Long.millisToHours: Long
	get() = this.millisToSeconds.secondsToHours
val Long.millisToMinutes: Long
	get() = this.millisToSeconds.secondsToMinutes

val Int.minutesToSeconds: Int
	get() = this * 60
val Int.hoursToMinutes: Int
	get() = this * 60
val Int.hoursToSeconds: Int
	get() = this.hoursToMinutes.minutesToSeconds
val Int.secondsToMinutes: Int
	get() = this / 60
val Int.minutesToHours: Int
	get() = this / 60
val Int.secondsToHours: Int
	get() = this.secondsToMinutes.minutesToHours
val Int.millisToSeconds: Int
	get() = this / 1000
val Int.millisToHours: Int
	get() = this.millisToSeconds.secondsToHours
val Int.millisToMinutes: Int
	get() = this.millisToSeconds.secondsToMinutes

fun Number.toRomanString(): String {
	val romanNumerals = mapOf(
			1000 to "M",
			900 to "CM",
			500 to "D",
			400 to "CD",
			100 to "C",
			90 to "XC",
			50 to "L",
			40 to "XL",
			10 to "X",
			9 to "IX",
			5 to "V",
			4 to "IV",
			1 to "I"
	)

	if (this == 0)
		return ""

	var num = this.toLong()
	val result = StringBuffer()
	for ((multiple, numeral) in romanNumerals.entries) {
		while (num >= multiple) {
			num -= multiple
			result.append(numeral)
		}
	}
	return result.toString()
}
