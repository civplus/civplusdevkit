package vg.civcraft.mc.civmodcore.random

fun UniformByte(min: Byte, max: Byte): () -> Byte {
	return {
		SECURE_RANDOM.nextByteUniform(min, max)
	}
}

fun NormalByte(center: Byte, std: Float): () -> Byte {
	return {
		SECURE_RANDOM.nextByteBellcurve(center, std)
	}
}

fun NormalItemAmount(center: Byte, std: Float): () -> Byte = NormalByte(center, std)
	.let{ {
		it().coerceAtLeast(1).coerceAtMost(64)
	} }
