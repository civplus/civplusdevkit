package vg.civcraft.mc.civmodcore.expression

import net.minecraft.server.v1_8_R3.NBTBase

interface TagMatcher {
	fun matches(tag: NBTBase?): Boolean
	fun solve(): NBTBase?
}
