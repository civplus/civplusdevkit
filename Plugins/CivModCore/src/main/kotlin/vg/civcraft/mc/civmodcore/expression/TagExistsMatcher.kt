package vg.civcraft.mc.civmodcore.expression

import net.minecraft.server.v1_8_R3.NBTBase

class TagExistsMatcher(val defaultValue: NBTBase): TagMatcher {
	override fun matches(tag: NBTBase?): Boolean {
		return tag != null
	}

	override fun solve(): NBTBase {
		return defaultValue.clone()
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as TagExistsMatcher

		if (defaultValue != other.defaultValue) return false

		return true
	}

	override fun hashCode(): Int {
		return defaultValue.hashCode()
	}

	override fun toString(): String {
		return "TagExistsMatcher(defaultValue=$defaultValue)"
	}
}
