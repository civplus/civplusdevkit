package vg.civcraft.mc.civmodcore.database

import net.minecraft.server.v1_8_R3.MojangsonParser
import net.minecraft.server.v1_8_R3.NBTTagCompound
import org.bukkit.Material
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack
import org.bukkit.inventory.ItemStack

data class SerializableItemStack(val item: String?) {
	private fun getNBT(): NBTTagCompound? {
		return MojangsonParser.parse(item ?: return null)!!
	}

	constructor(item: ItemStack?):
			this(
					if (item == null)
						null
					else if (item.type != Material.AIR)
						CraftItemStack.asNMSCopy(item).save(NBTTagCompound()).toString()
					else
						null)
	fun asItemStack(): ItemStack = if (item != null) CraftItemStack.asBukkitCopy(net.minecraft.server.v1_8_R3.ItemStack.createStack(getNBT())) else ItemStack(Material.AIR)
}

val ItemStack?.serializable: SerializableItemStack
	get() = SerializableItemStack(this)
