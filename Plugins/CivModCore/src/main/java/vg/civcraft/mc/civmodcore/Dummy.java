package vg.civcraft.mc.civmodcore;

import vg.civcraft.mc.civmodcore.event.BlockDropItemListener;
import vg.civcraft.mc.civmodcore.inventorygui.ClickableInventory;

public class Dummy extends ACivMod{
	protected String getPluginName(){
		return "CivModCore";
	}
	@Override
	public void onLoad()
	{
		//Don't want it to load config
	}
	@Override
	public void onEnable()
	{
		ClickableInventory.setPlugin(this);
		getServer().getPluginManager().registerEvents(BlockDropItemListener.INSTANCE, this);
	}
}
