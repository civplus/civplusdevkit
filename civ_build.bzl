load("@rules_kotlin//kotlin:jvm.bzl", "kt_jvm_import", "kt_jvm_library")

# legacy plugins that use the oldschool plugin.yml file are expected not to provide a qualified_main_class
# the qualified_main_class is used to find the plugin.yml information specified in code annotations
def civ_plugin_jar(name, deps = [], runtime_deps = [], qualified_main_class = None):
    # Collect Kotlin and Java sources
    kt_srcs = native.glob(["src/main/kotlin/**/*.kt"])
    java_srcs = native.glob(["src/main/java/**/*.java"])

    kt_jvm_library(
        name = name,
        srcs = kt_srcs + java_srcs,
        deps = deps,
        runtime_deps = runtime_deps,
        visibility = ["//visibility:public"],
    )

    non_included_deps = ("//Spigot", "//KotlinPlugin")
    if (qualified_main_class != None):
        collected_deps_list = []
        for dep in deps:
            if (dep.startswith(non_included_deps)):
                continue
            if (dep.startswith("//")):
                collected_deps_list.append("$(locations " + dep + ")")
                pass
            if (dep.startswith("@")):
                collected_deps_list.append("$(locations " + dep + ")")
        collected_deps = " ".join(collected_deps_list)

        native.genrule(
            name = "plugin_yml",
            tools = ["//KotlinPlugin:PluginYMLGenerator_deploy.jar"] + deps,
            srcs = [":" + name + ".jar"],
            cmd = "java -jar $(location //KotlinPlugin:PluginYMLGenerator_deploy.jar) $(location :" + name + ".jar) " + qualified_main_class + " $@ " + collected_deps,
            outs = ["src/main/resources/plugin.yml"],
        )

    civ_deps = []
    for dep in deps:
        if (dep.startswith(non_included_deps)):
            pass
        elif (dep.startswith("//")):
            if (dep.find(":") != -1): # the dep looks like //Plugins/CivModCore:CivModCore
                civ_deps.append(dep + "_plugin_binary")
            else: # the dep looks like //Plugins/CivModCore
                dep_core_name = dep.split("/").pop()
                civ_deps.append(dep + ":" + dep_core_name + "_plugin_binary")
        else:
            pass

    native.java_binary(
        name = name + "_plugin_binary",
        main_class = "none",
        deploy_env = civ_deps,
        runtime_deps = [":" + name] + runtime_deps,
        resources = native.glob(
            ["src/main/resources/**"],
        ) + ([":plugin_yml"] if qualified_main_class != None else []),
        visibility = ["//visibility:public"],
    )

    native.genrule(
       name = name + "-release",
       srcs = [":" + name + "_plugin_binary_deploy.jar"],
       outs = [name + "-release.jar"],
       cmd = "cp $< $@",
       visibility = ["//visibility:public"],
    )
