workspace(name = "CivPlusPlugins")

### Java

load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

RULES_JVM_EXTERNAL_TAG = "4.2"

RULES_JVM_EXTERNAL_SHA = "cd1a77b7b02e8e008439ca76fd34f5b07aecb8c752961f9640dea15e9e5ba1ca"

http_archive(
    name = "rules_jvm_external",
    sha256 = RULES_JVM_EXTERNAL_SHA,
    strip_prefix = "rules_jvm_external-%s" % RULES_JVM_EXTERNAL_TAG,
    url = "https://github.com/bazelbuild/rules_jvm_external/archive/%s.zip" % RULES_JVM_EXTERNAL_TAG,
)

### Maven Dependencies

load("@rules_jvm_external//:defs.bzl", "maven_install")

maven_install(
    artifacts = [
        "com.zaxxer:HikariCP:3.3.1",
        "org.slf4j:slf4j-api:1.7.32",
        "io.papermc:paperlib:1.0.2",
        "org.mockito:mockito-core:2.21.0",
        "org.apache.logging.log4j:log4j-core:2.17.1",
        "co.aikar:acf-bukkit:0.5.0-SNAPSHOT",
        "dk.brics:automaton:1.12-1",
        "xyz.xenondevs:particle:1.7",
        "io.github.waterfallmc:waterfall-api:1.18-R0.1-SNAPSHOT",
        "io.github.waterfallmc:waterfall-event:1.18-R0.1-SNAPSHOT",
        "io.github.waterfallmc:waterfall-chat:1.18-R0.1-SNAPSHOT",
        "io.github.waterfallmc:waterfall-config:1.18-R0.1-SNAPSHOT",
        "io.github.waterfallmc:waterfall-parent:1.18-R0.1-SNAPSHOT",
        "io.github.waterfallmc:waterfall-protocol:1.18-R0.1-SNAPSHOT",
        "com.moandjiezana.toml:toml4j:0.7.2",
        "org.litote.kmongo:kmongo:4.8.0",
        "org.apache.commons:commons-imaging:1.0",
        "org.jetbrains.kotlin:kotlin-reflect:1.7.10",
    ],
    fail_on_missing_checksum = False,
    repositories = [
        "https://jcenter.bintray.com/",
        "https://repo1.maven.org/maven2",
        # Devoted Build Server
        "https://build.devotedmc.com/plugin/repository/everything/",
        # DynMap (used by WorldBorder)
        "https://repo.mikeprimm.com/",
        # PaperLib (used by WorldBorder)
        "https://papermc.io/repo/repository/maven-public/",
        # CivModCore dependency acf-bukkit
        "https://repo.aikar.co/content/groups/aikar/",
        # paper (used for waterfall)
        "https://papermc.io/repo/repository/maven-public/",
        # ImageJ (used for apache commons imaging)
        "http://maven.imagej.net/content/repositories/public/",
    ],
)

load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_jar")

### Spigot jar
http_jar(
    name = "Spigot",
    url = "https://amel.pw/spigot/spigot-1.8.8.jar",
)

###  Third-Party Plugin Jars
http_jar(
    name = "ProtocolLib",
    url = "https://github.com/dmulloy2/ProtocolLib/releases/download/4.7.0/ProtocolLib.jar",
)

http_jar(
    name = "HolographicDisplays",
    url = "https://amel.pw/spigot/HolographicDisplays-3.0.5-SNAPSHOT.jar",
)

# Dependency for NameLayer. To be removed soon I hope
http_jar(
    name = "Mercury",
    url = "https://amel.pw/spigot/Mercury-1.2.19.jar",
)

http_jar(
    name = "WorldBorder",
    url = "https://media.forgecdn.net/files/883/629/WorldBorder.jar",
)

http_jar(
    name = "SuperVanish",
    url = "https://github.com/LeonMangler/SuperVanish/releases/download/6.2.6/SuperVanish-6.2.6.jar",
)

http_jar(
    name = "NoCheatPlus",
    url = "https://amel.pw/spigot/NoCheatPlus-3.16.0-RC-sMD5NET-b1134.jar",
)

http_jar(
    name = "BarAPI",
    url = "https://amel.pw/spigot/BarAPI-3.5.jar",
)

http_jar(
    name = "BossBarAPI",
    url = "https://github.com/InventivetalentDev/BossBarAPI/releases/download/2.4.3-SNAPSHOT/BossBarAPI_v2.4.3-SNAPSHOT.jar",
)

http_jar(
    name = "ActionBarAPI",
    url = "https://amel.pw/spigot/ActionBarAPI-1.5.4.jar",
)

http_jar(
    name = "FastPot",
    url = "https://amel.pw/spigot/FastPot-5.0.jar",
)

http_jar(
    name = "ViaVersion",
    url = "https://amel.pw/spigot/ViaVersion-4.9.2.jar",
)

http_jar(
    name = "WorldEdit",
    url = "https://amel.pw/spigot/worldedit-bukkit-6.1.9.jar",
)

### Kotlin

load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

rules_kotlin_version = "1.9.0"
rules_kotlin_sha = "5766f1e599acf551aa56f49dab9ab9108269b03c557496c54acaf41f98e2b8d6"
http_archive(
    name = "rules_kotlin",
    urls = ["https://github.com/bazelbuild/rules_kotlin/releases/download/v%s/rules_kotlin-v%s.tar.gz" % (rules_kotlin_version, rules_kotlin_version)],
    sha256 = rules_kotlin_sha,
)

load("@rules_kotlin//kotlin:repositories.bzl", "kotlin_repositories")
kotlin_repositories()

register_toolchains("//:kotlin_toolchain")
